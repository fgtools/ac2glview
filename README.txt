file: README.txt - for ac2glview project - 20130609

Main application: ac3d_browser2.

This started out as a project called ac3d2gl, where two methods 
were used to read an AC3D *.ac file, and 'generate' OpenGL code 
to render the model.

BUt I wanted to do MORE than just 'render' the model. After the reading 
of an AC3D *.ac file, I wanted to be able to 'edit', or at least 'view' 
the sub-model parts.

So this project combines with FLTK (and FLU) for the windows handling, 
to present a FLU tree view of the AC3D file, where the tree could be 
expanded, and individual compontents selected, and even deleted from 
the AC3D file.

Since I now needed to be able to delete some 'objects' from the 
tree, combine and write the remainder to a temporary AC3D *.ac file, 
then parse and show this changed file, I first had to convert the 
original ac2gl.lib, to a more C++ oriented ac2gl2.lib.

This has not been fully completed in that it still uses some 
'global' functions. And to try to remain compatible with the older 
ac2gl library there is a CMake option, USE_AC2GL_ONE, which is OFF 
be default, and will be removed when development reaches a final 
stage.

And the result is called ac3d_browser2. 

This is the main app. It is a FLTK duel window. The left pane contains an 
Fl_GL_Window for viewing the models, and the right pane is a FLU Tree view.

License:

Certain components used to build this project were in fact 'Public 
Domain' software. That is no license.

BUt it is my intention to release this source under the GNU GPL 
Version 2 (or later at your discretion) License.


Version:

As with some other cmake build projects, the version resides in a 
single version.txt file in this root. At this stage it is 1.x.x, 
denoting this is the first development release.


Dependencies:

OpenGL, GLUT, FLTK, FLU are the main dependencies to be found 
via CMake find-package files, so of which do NOT exist in the 
standard cmake distribution, so the prject has its own CMakeModules 
folder.

Building:

Being a native cmake project, the build should be a simple -

$ cd build
$ cmake ..
$ make
$ [sudo] make install (if desired)

And just slightly modified in Windows, using the MSVC IDE -

> cd build
> cmake ..
> cmake --build . --config Release
> cmake --build . --config Debug (if needed)
> cmake --build . --config Release --target INSTALL (if desired)

Running:

$ ./ac3d_browser2 name-of-ac-file [-s script_file]

will load the *.ac file, and present the tree of objects in the right 
pane, and a prview of the model in the Fl_GL_Window in the left pane.

An ESC will exit the application any time. It uses Fl_Preferences to 
save the window size and position. A '?' key will show the keyboard 
interface, most to do with sizing, positioning and rotating the preview 
model.

The FLU Tree view can be expanded, and 'objects' selected. If the 
selection is a 'viewable' object, then it will be rendered in the 
left pane Fl_GL_Window. After an object is selected, it can be 
delete through the menu.

Obviously certain 'object' can NOT be deleted, like the root of 
the tree MATERIALS and OBJECTS, nor the main 'world' object, nor 
the individual materials.

And naturally only 'whole' objects can be deleted, thus selecting 
children of such objects will only allow the 'whole' parent object 
to be deleted.

It supports simple scripting, to do the deletions, write a new ac file,
export the object to three.js json, and exit. See delete.txt for a 
sample of the 'verbs' actioned.

See the TODO.txt file for other details.

In every way this is most certainly a WORK-IN-PROGRESS

Enjoy...

Geoff.

# eof
