/* file : ac3d2.hxx */
#ifndef _AC3D2_HXX_
#define _AC3D2_HXX_
#include <stdio.h>  // FILE *
#include <vector>
#include <sstream> // ostringstream msg;

#define NEW_DELETE_BEHAVIOUR

#define MX_TOKEN_PTRS 30
#define ADD_SURFS_GT4

typedef struct ACPoint_t {
    float x, y, z;
} ACPoint, *PACPoint;


typedef struct ACNormal_t {
    float x, y, z;
} ACNormal, *PACNormal;


typedef struct ACVertex_t {
    float x, y, z;
    ACNormal normal;
} ACVertex, *PACVertex;

typedef struct ACUV_t {
    float u, v;
} ACUV, *PACUV;


typedef struct ACSurface_t {
    int *vertref;
    ACUV *uvs;
    int num_vertref;
    ACNormal normal;
    int flags;
    int mat;
    int mindx;
    int tmpindx;    // just a tempory index
    int file_line;  // file line number of this SURF
} ACSurface, *PACSurface;

/* bounding x,y,z box */
typedef struct BBOX_t {
    int num_verts;
    float minx, maxx;
    float miny, maxy;
    float minz, maxz;
    ACPoint center; /* model center */
    float radius;
}BBOX, *PBBOX;

class ac3d2gl;

typedef struct ACObject_t {
    int     order;  // just the order of creation - also unique id
    ACPoint loc;
    int      got_loc;
    char    *name;
    char    *data;
    char    *url;
    ACVertex *vertices;
    int     num_vert;

    ACSurface *surfaces;
    int     num_surf;
    float   texture_repeat_x, texture_repeat_y;
    float   texture_offset_x, texture_offset_y;
    float   crease;   /* 2013/05/06 added */
    int     had_crease; /* 2013/07/31 added */
    int     subdiv;   /* 2013/05/06 added */
    int     num_kids;
    struct ACObject_t **kids;
    float   matrix[9];
    int     type;
    int     texture;        // with GL texture value
    char   *texture_name;   // name of texture filename
    ac3d2gl *me;
    void    *parent;    // top most will be ZERO
    PBBOX   pbbox;      // will be ZERO if NO vertices
    int     ucount;     // value added during fill_Tree to separate same named objects
    bool    deleted;    // an idea to NOT actually DELETE an OBJECT, merely MARK it as DELETED
} ACObject, *PACObject;

typedef struct ACCol_t {
    float r, g, b, a;
} ACCol, *PACCol;

typedef struct Material_t {
    ACCol rgb; /* diffuse **/
    ACCol ambient;
    ACCol specular;
    ACCol emissive;
    float shininess;
    float transparency;
    char *name;
} ACMaterial, *PACMaterial;

typedef struct ACImage_t {
    unsigned short width, height, depth;    
    void *data; 
    int index;
    char *name;
    int amask;
    char *origname; /** do not set - set automatically in texture_read function **/
} ACImage, *PACImage;

typedef struct tagGL_PARMS {
    float angle_rate; // change at rate at ?? degrees per second
    int frozen;
    float x_axis, y_axis, z_axis;
    float rot;
    // --- TODO - ADD TO CONFIG ---
    float x_trans, y_trans, z_trans;
    float eyex, eyey, eyez;
    float centrex, centrey, centrez;
    float upx, upy, upz;
    // ====================
    double left;    // The coordinate for the left-vertical clipping plane.
    double right;   // The coordinate for the right-vertical clipping plane.
    double bottom;  // The coordinate for the bottom-horizontal clipping plane.
    double top;     //    The coordinate for the bottom-horizontal clipping plane. 
    double zNear;   //  The distances to the near-depth clipping plane. Must be positive.
    double zFar;    //  The distances to the far-depth clipping planes. Must be positive.
} GL_PARMS, *PGL_PARMS;

#define OBJECT_WORLD 999
#define OBJECT_NORMAL 0
#define OBJECT_GROUP 1
#define OBJECT_LIGHT 2

#define SURFACE_SHADED (1<<4)
#define SURFACE_TWOSIDED (1<<5)

#define SURFACE_TYPE_POLYGON (0)
#define SURFACE_TYPE_CLOSEDLINE (1)
#define SURFACE_TYPE_LINE (2)

#ifdef WIN32
#if  (defined(AC2GL_STATIC) || !defined(AC2GL_DLL))
#define Prototype
#else
#define Prototype  __declspec( dllexport )
#endif
#else
#define Prototype
#endif

#define Private static
#define Boolean int

#ifndef TRUE
#define FALSE (0)
#define TRUE (!FALSE)
#endif

#define STRING(s)  (char *)(strcpy((char *)myalloc(strlen(s)+1), s))
#define streq(a,b)  (!strcmp(a,b))

/* MACRO for memory allocation, re-allocation and feeing */
#define myalloc malloc
#define myfree free
#define myrealloc realloc

#define MX_LINE_BUFF    255
#define MX_MATERIALS    255

Prototype int ac_set_verbosity( int v );
Prototype int ac_get_verbosity( void );
Prototype int ac_verb1( void );
Prototype int ac_verb2( void );
Prototype int ac_verb5( void );
Prototype int ac_verb9( void );
Prototype void ac_prepare_render();
Prototype int ac_display_list_render_object(ACObject *ob);
Prototype int ac_load_texture(char *name);
Prototype int ac_load_rgb_image(char *fileName);
Prototype void ac_set_file_name( char *name );
Prototype void ac_add_texture_paths( char *path );
Prototype char *ac_get_texture_paths(void);
Prototype ACImage *ac_get_texture(int ind);
Prototype int ac_load_and_render_ac3d(char *fname, PGL_PARMS pgl = 0, PBBOX pbb = 0);


#if 0
Prototype ACObject *ac_load_ac3d(char *filename);
Prototype ACMaterial *ac_palette_get_material(int index);
Prototype void ac_object_free(ACObject *ob);
Prototype void ac_dump_simple(ACObject *ob, FILE *fp); /* 2013/05/06 - exposed, and added output file */

/* 2013/05/06 - additional functions */
Prototype void ac_export_threejs(ACObject *ob, FILE *fp); 
/* 2013/05/12 - got a core dump when loading textures, so added -n to inhibit loads */
Prototype int ac_load_textures();   /*  { return do_texture_load; } */
Prototype void ac_set_load_textures(int v); /* { do_texture_load = v; } */
Prototype void ac_set_add_normals(int v); /* { add_export_norms = v; } */
Prototype float ac_get_scale(); /* { return def_scale; } */
Prototype void ac_set_scale(float f); /* { def_scale = f; } */
Prototype void ac_free_objects();   /* free ALL memory */
Prototype int ac_palette_get_mat_size();
Prototype const char *ac_objecttype_to_string(int t);
#endif // 0

typedef std::vector<ACVertex> vACV;

typedef struct tagWORKSTR {
    ACObject *ob;
    std::ostringstream verts;
    std::ostringstream faces;
    vACV    vACVectors; // cummulative vertice count
    int     ncount; // cummulative normals count
    int     facecnt; // added a face - need comma before next
    int     mcount; // cummulative materials count
    float   scale; // TODO: How to set this SCALE??? Is it important???
    char    *info;
    char    *tmpbuf;
}WORKSTR, *PWORKSTR;

typedef struct tagACFNDOBJ {
    ACObject *ob;
    int vcount;
}ACFNDOBJ, *PACFNDOBJ;

// commence a class
class ac3d2gl
{
public:
    ac3d2gl();
    ~ac3d2gl();
    ACObject *ac_load_ac3d2(char *filename);
    ACObject *ac_load_object2(FILE *f, ACObject *parent);
    int get_tokens2(char *s, int *argc, char *argv[]);
    ACObject *new_object2(ACObject *parent);
    const char *ac_objecttype_to_string2(int t);
    int string_to_objecttype2(char *s);
    //int ac_load_texture2(char *name);
    ACSurface *read_surface2(FILE *f, ACSurface *s, ACObject *ob);
    void init_surface2(ACSurface *s);
    void tri_calc_normal2(ACPoint *v1, ACPoint *v2, ACPoint *v3, ACPoint *n);
    void ac_free_objects2();   /* free ALL memory */
    void ac_object_free2(ACObject *ob);
    ACMaterial *ac_palette_get_material2(int id);
    int ac_palette_get_mat_size2() { return num_palette2; }
    void ac_calc_vertex_normals2(ACObject *ob);
    int ac_trim_in_buff(char *buf);
    void ac_object_calc_vertex_normals2(ACObject *ob);
    int ac_set_bounding_box2(ACObject *ob, PBBOX pbb);
    int ac_set_model_size_pos2(ACObject *ob, PGL_PARMS pgl, PBBOX pbb = 0);
    int ac_get_opt_load_textures2()  { return option_texture_load; }
    void ac_set_opt_load_textures2(int v)  { option_texture_load = v; }
    int ac_get_opt_drop_groups() { return option_drop_groups; }
    void ac_set_opt_drop_groups(int v) { option_drop_groups = v; }
    PBBOX ac_get_obj_bbox( ACObject *ob );
    int ac_vertex_in_bbox( ACObject *ob, ACVertex *p );
    int ac_xy_in_bbox( ACObject *ob, ACVertex *p, float fudge = 0.0 );
    int ac_export_threejs(ACObject *ob, FILE *fp); 
    void get_verts_faces_norms_uvs( WORKSTR &ws );
    ACObject *find_Object(const char *name, ACObject *ob, int type);
    ACObject *find_Largest( PACFNDOBJ pfo, ACObject *ob );
    char *get_Object_Name( ACObject *ob );

// these could be private I suppose
    int option_drop_groups;
    int option_texture_load;
    ACMaterial *palette2; // [MX_MATERIALS+1];
    int tokc2;
    char *tokv2[MX_TOKEN_PTRS+1];
    int next_id2;
    ACObject *first_obj2;
    Boolean read_line2(FILE *f);
    int line2;
    char *buff1;
    char *buff2;
    int startmatindex2;
    int num_palette2;
    int fix_warnings;
    char *file_name;
    int vert_warnings1;
    int vert_warnings2;
};

enum Face_Bits {
    No_Bits  = 0x00,
    QuadBit  = 0x01,
    MatBit   = 0x02,
    FaceUv   = 0x04,
	VertUv   = 0x08,
    FaceNorm = 0x10,
	VertNorm = 0x20,
	FaceColr = 0x40,
    VertColr = 0x80
};

#endif /* #ifndef _AC3D2_HXX_ */
/* eof - ac3d2.hxx */
