/* ======================================================================
    modules : ac3d2.cxx
    from : README.txt
    AC2GL2 version 2.0.0 

    All the files here are free of copyright so you can use them as you 
    please.  If you do use them them please give credit to AC3D. www.ac3d.org

    2.0.0 20130621 - geoff
        Massage into a 'crude' C++ implementation. Not all function done yet.

    1.14 2013/05/06 - geoff
        Add keyboard interface to control position, orientation and rotation of
        view, and added a WritePPM dump capability
        Added 'crease %f' and 'subdiv %d' to ac3d.c parsing
    1.13 reduced the default rotation speed; fixed fatal bug with argc when file 
         name was passed to program on commandline
    1.11 added cdecl Prototype for WIN32 
    1.12 added ac_object_free

   ====================================================================== */
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <vector>
#include <sstream> // ostringstream msg;

#include "misc_lib.hxx" // just some misc functions
#include "ac3d2.hxx"

#ifndef SPRTF
#define SPRTF sprtf
#endif

#ifndef EndBuf
#define EndBuf(a) ( a + strlen(a))
#endif

static const char *mod_name = "ac3d2.cxx";

static int verbosity = 1;
static int show_sscan_warn = 0;
static char *texture_paths = 0; // any added texture paths
static char *file_name = 0; // input file name
static int paths_len = 0;
static int max_index_warnings = 5;
static int index_warnings = 0;

Prototype int ac_set_verbosity( int v )
{ 
    int i = verbosity;
    verbosity = v;
    return i;
}
Prototype int ac_get_verbosity( void ) { return verbosity; }
Prototype int ac_verb1( void ) { return (verbosity >= 1 ? 1 : 0); }
Prototype int ac_verb2( void ) { return (verbosity >= 2 ? 1 : 0); }
Prototype int ac_verb5( void ) { return (verbosity >= 5 ? 1 : 0); }
Prototype int ac_verb9( void ) { return (verbosity >= 9 ? 1 : 0); }

/* ------------------------------------------------------------
 * add a user given texture path
 * also include the path to the .ac file
 * ------------------------------------------------------------ */
Prototype void ac_add_texture_paths( char *path )
{
    int len = (int)strlen(path);
    if (len) {
        if (texture_paths) {
            texture_paths = (char *)myrealloc(texture_paths, paths_len + len + 1);
            if (!texture_paths) {
                SPRTF("%s: ERROR: memory re-allocation FAILED %d bytes! aborting...\n",
                    mod_name, paths_len + len + 1);
                exit(1);
            }
            strcpy(&texture_paths[paths_len-1],path);
            paths_len += len + 1;
            texture_paths[paths_len] = 0;
        } else {
            paths_len = len + 2;
            texture_paths = (char *)myalloc( len + 2 );
            if (!texture_paths) {
                SPRTF("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                    mod_name, len + 2);
                exit(1);
            }
            strcpy(texture_paths,path);
            texture_paths[len] = 0;     /* double ZERO termination */
            texture_paths[len+1] = 0;
        }
    }
}


Prototype void ac_set_file_name( char *name )
{ 
    int i, c, len;
    file_name = STRING(name);
    len = (int)strlen(file_name);
    for (i = len - 1; i >= 0; i--) {
        c = file_name[i];
        if ((c == '/')||(c == '\\')) {
            file_name[i] = 0; /* zero it */
            ac_add_texture_paths(file_name);
            file_name[i] = (char)c; /* put it back */
            break;
        }
    }
}

/* note: this can be a double zero terminated list of paths */
Prototype char *ac_get_texture_paths(void) { return texture_paths; }

// =======================================================================
// rough initial C++ implementation
// ================================

ac3d2gl::ac3d2gl()
{ 
    line2 = 0;
    buff1 = new char[MX_LINE_BUFF+1];
    buff2 = new char[MX_LINE_BUFF+1];
    startmatindex2 = 0;
    num_palette2 = 0;
    palette2 = new ACMaterial[MX_MATERIALS+1];
    next_id2 = 0;
    first_obj2 = 0;
    option_texture_load = 0;    // default to OFF - has to be specifically enabled
    option_drop_groups = 0;     // TODO: default to OFF - has to be specifically enabled - to be coded
    index_warnings = 0;
    fix_warnings = 0;
    file_name = (char *)"no_name";
    vert_warnings1 = 0;
    vert_warnings2 = 0;

}

ac3d2gl::~ac3d2gl()
{ 
    delete buff1;
    delete buff2;
    delete palette2;
}


ACObject *ac3d2gl::ac_load_ac3d2(char *fname)
{
    file_name = strdup(fname);  // keep copy of file name
    FILE *f = fopen(fname, "r");
    ACObject *obj = 0;

    index_warnings = 0; // restart index out-of-range warnings
    fix_warnings = 0;
    vert_warnings1 = 0;
    vert_warnings2 = 0;

    if (!f) {
	    SPRTF("can't open %s\n", fname);
	    return obj;
    }

    read_line2(f); /* read first line of file */

    if (strncmp(buff2, "AC3D", 4)) {
        /* NO AC3D 'signature', so */
    	SPRTF("%s: ac_load_ac3d2('%s') is NOT a valid AC3D file.", mod_name, fname);
	    fclose(f);
	    return obj;
    }

    if (ac_verb9()) SPRTF("Loading file [%s]\n", fname);

    startmatindex2 = num_palette2;


    obj = ac_load_object2(f, NULL);

	
    fclose(f);

    if(obj)
        ac_calc_vertex_normals2(obj);

    if (fix_warnings) {
    	SPRTF("%s: WARNING: ac_load_ac3d2('%s') has %d 'fix' warnings.", mod_name, fname, fix_warnings);
    }
    if (vert_warnings1 || vert_warnings2) {
    	SPRTF("%s: WARNING: ac_load_ac3d2('%s') has %d & %d 'vert' warnings.", mod_name, fname, vert_warnings1, vert_warnings2);
    }

    return obj;
}

Boolean ac3d2gl::read_line2(FILE *f)
{
    fgets(buff2, MX_LINE_BUFF, f);
    strcpy(buff1,buff2);    // make a COPY
    line2++;
    return TRUE;
}

#define STRING2(a,b) { \
    a = (char *)new char[strlen(b)+2]; \
    strcpy(a,b); }


ACObject *ac3d2gl::ac_load_object2(FILE *f, ACObject *parent)
{
    ACObject *ob = 0;
    int     tc, res;
    char    type[32];
    char    strg[32];
    char    *pstr;
	int     len, n, num;
    char    t[32];
    float   rot[9];
    int     verb9 = ac_verb9();

    index_warnings = 0; // restart index out-of-range warnings

    while (!feof(f)) 
    {
        read_line2(f);

	    res = sscanf(buff2, "%s", t);
        if ((res != 1) && show_sscan_warn) {
            SPRTF("%s: WARNING: sscanf of '%s' not 1! got %d\n",
                mod_name, buff2, res);
        }

        if (streq(t, "MATERIAL")) {
            ACMaterial m;
            memset(&m,0,sizeof(ACMaterial));
	        tc = get_tokens2(buff2, &tokc2, tokv2); 
            if ( tc != 22) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: %d: WARNING: expected 21 params after \"MATERIAL\", got %d!\n[%s]\n",
                    mod_name, line2, tc, buff1 );
	        } else {
                if (num_palette2 >= MX_MATERIALS) {
                    SPRTF("%s: ERROR: Reached maximum materials %d. Increase MX_MATERIALS and recompile!\n", 
                        mod_name, num_palette2);
                    exit(1);
                }
                // build the material
                //if (line2 == 11)
                //    m.name = 0;
                m.name = new char[ strlen(tokv2[1]) + 2 ];  // allocate NAME
                strcpy(m.name,tokv2[1]);    // copy in the NAME

		        m.rgb.r = (float)atof(tokv2[3]);
		        m.rgb.g = (float)atof(tokv2[4]);
		        m.rgb.b = (float)atof(tokv2[5]);

		        m.ambient.r = (float)atof(tokv2[7]);
		        m.ambient.g = (float)atof(tokv2[8]);
		        m.ambient.b = (float)atof(tokv2[9]);

		        m.emissive.r = (float)atof(tokv2[11]);
		        m.emissive.g = (float)atof(tokv2[12]);
		        m.emissive.b = (float)atof(tokv2[13]);

		        m.specular.r = (float)atof(tokv2[15]);
		        m.specular.g = (float)atof(tokv2[16]);
		        m.specular.b = (float)atof(tokv2[17]);

		        m.shininess = (float)atof(tokv2[19]);
		        m.transparency = (float)atof(tokv2[21]);

		        // shi = (float)atof(tokv2[6]);
		        // tran = (float)atof(tokv2[7]);

		        palette2[num_palette2++] = m;
                if (verb9) { 
                    ac_trim_in_buff(buff1);
                    SPRTF("%d: %s\n", line2, buff1);
                    SPRTF("%d: MATERIAL \"%s\" rgb %s %s %s  amb %s %s %s  emis %s %s %s  spec %s %s %s  shi %s  trans %s\n", line2,
                    m.name, gtf(m.rgb.r), gtf(m.rgb.g), gtf(m.rgb.b),
                    gtf(m.ambient.r), gtf(m.ambient.g), gtf(m.ambient.b),
                    gtf(m.emissive.r), gtf(m.emissive.g), gtf(m.emissive.b), 
                    gtf(m.specular.r), gtf(m.specular.b), gtf(m.specular.b),  
                    gtf(m.shininess), gtf(m.transparency) );

                }

	        }
	    } else if (streq(t, "OBJECT")) {
		    ob = new_object2(parent);
		    res = sscanf(buff2, "%s %s", strg, type);
            if ((res != 2) && show_sscan_warn) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: WARNING: sscanf of '%s' not 2! got %d\n[%s]\n",
                    mod_name, buff2, res, buff1);
            }
		    ob->type = string_to_objecttype2(type);
            if (verb9) SPRTF("%d: OBJECT %s\n",line2, type);
        } else if (streq(t, "data")) {
            tc = get_tokens2(buff2, &tokc2, tokv2); 
		    if ( tc != 2) {
                ac_trim_in_buff(buff1);
    			SPRTF("%s: %d: WARNING: expected 'data <number>'! got %d tokens\n[%s]\n", 
                    mod_name, line2, tc, buff1);
            } else {
			    len = atoi(tokv2[1]);
			    if (len > 0) {
			        pstr = new char[len+2];
                    if (!pstr) {
                        SPRTF("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                            mod_name, len + 1);
                        exit(1);
                    }
			        res = fread(pstr, len, 1, f);  // read the data length (from next line)
			        pstr[len] = 0;           // zero terminate the string
			        fscanf(f, "\n");        // get to next line
                    line2++;
			        ob->data = pstr;
			    }
		    }
		} else if (streq(t, "name")) {
			tc = get_tokens2(buff2, &tokc2, tokv2);
			if (tc != 2) {
                ac_trim_in_buff(buff1);
			    SPRTF("%s: %d: WARNING: expected quoted name (got %d tokens)\n[%s]\n",
                    mod_name, line2, tc, buff1);
			} else {
			    ob->name = new char[strlen(tokv2[1])+1];
                strcpy(ob->name,tokv2[1]);
                if (verb9) SPRTF("%d: name %s\n", line2, ob->name);
            }
	    } else if (streq(t, "texture")) {
            // NOTE: get_tokens2() already strips any double quotes from the name
            tc = get_tokens2(buff2, &tokc2, tokv2);
            if (tc != 2) {
                ac_trim_in_buff(buff1);
				SPRTF("%s: %d: WARNING: expected quoted texture name (got %d tokens)\n[%s]\n", 
                    mod_name, line2, tc, buff1);
            } else {
                ob->texture_name = strdup(tokv2[1]);    // save texture name for ac output
                if (option_texture_load) {
				    ob->texture = ac_load_texture(tokv2[1]); // TODO - redo texture loading
                }
                if (verb9) SPRTF("%d: texture %s\n", line2, tokv2[1]);
			}
		} else if (streq(t, "texrep")) {
            tc = get_tokens2(buff2, &tokc2, tokv2); 
		    if ( tc != 3) {
                ac_trim_in_buff(buff1);
			    SPRTF("%s: %d: WARNING: expected 'texrep <float> <float>' (got %d tokens)\n[%s]\n", 
                    mod_name, line2, tc, buff1);
            } else {
                ob->texture_repeat_x = (float)atof(tokv2[1]);
				ob->texture_repeat_y = (float)atof(tokv2[2]);
			}
		} else if (streq(t, "texoff")) {
            tc = get_tokens2(buff2, &tokc2, tokv2); 
            if ( tc != 3) {
                ac_trim_in_buff(buff1);
				SPRTF("%s: %d: WARNING: expected 'texoff <float> <float>' got %d tokens\n[%s]\n",
                    mod_name, line2, tc, buff1);
            } else {
                ob->texture_offset_x = (float)atof(tokv2[1]);
				ob->texture_offset_y = (float)atof(tokv2[2]);
            }
		} else if (streq(t, "rot")) {
			int n;
            /* why no check of count? */
			res = sscanf(buff2, "%s %f %f %f %f %f %f %f %f %f", strg, 
                &rot[0], &rot[1], &rot[2], &rot[3], &rot[4], &rot[5], &rot[6], &rot[7], &rot[8] );
            if ((res != 10) && show_sscan_warn) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: WARNING: sscanf of '%s' not 10! got %d\n",
                    mod_name, buff1, res);
            } else {
                for (n = 0; n < 9; n++) {
                    ob->matrix[n] = rot[n];
                }
                if (verb9) {
                    SPRTF("%d: rot %s %s %s %s %s %s %s %s %s\n", line2,
                        gtf(rot[0]), gtf(rot[1]), gtf(rot[2]), gtf(rot[3]),
                        gtf(rot[4]), gtf(rot[5]), gtf(rot[6]), gtf(rot[7]), gtf(rot[8]) );
                }
            }
        } else if (streq(t, "loc")) {
            /* why no check of count? */
            res = sscanf(buff2, "%s %f %f %f", strg,
                &ob->loc.x, &ob->loc.y, &ob->loc.z);
            if ((res != 4) && show_sscan_warn) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: WARNING: sscanf of '%s' not 4! got %d\n",
                    mod_name, buff1, res);
            } else {
                ob->got_loc = 1;
                if (verb9) {
                    SPRTF("%d: loc %s %s %s\n", line2,
                        gtf(ob->loc.x), gtf(ob->loc.y), gtf(ob->loc.z));
                }
            }
        } else if (streq(t, "url")) {
            tc = get_tokens2(buff2, &tokc2, tokv2); 
            if ( tc != 2) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: %d: WARNING: expected one arg to url (got %d tokens)\n[%s]\n", 
                    mod_name, line2, tc, buff1);
            } else {
                STRING2(ob->url,tokv2[1]);
                if (verb9) SPRTF("%d: url %s\n", line2, ob->url);
            }
        } else if (streq(t, "numvert")) {
            num = 0;
            res = sscanf(buff2, "%s %d", strg, &num);
            if ((res != 2) && show_sscan_warn) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff1, res);
            }
            if (num > 0) {
                ob->num_vert = num;
                ob->vertices = (ACVertex *)new ACVertex[num];
                if (!ob->vertices) {
                    SPRTF("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                        mod_name, (int)(sizeof(ACVertex)*num));
                    exit(1);
                } 
                if (verb9) SPRTF("%d: Collecting %d vertices...\n", line2, num);
                for (n = 0; ((n < num) && !feof(f)); n++) {
                    ACVertex p;
                    p.x = 0.0f;
                    p.y = 0.0f;
                    p.z = 0.0f;
                    p.normal.x = 0.0f;
                    p.normal.y = 0.0f;
                    p.normal.z = 0.0f;
                    read_line2(f);
                    //res = sscanf(buff2, "%f %f %f", &p.x, &p.y, &p.z);
                    res = get_tokens2(buff2, &tokc2, tokv2); 
                    p.x = (float)atof(tokv2[0]);
				    p.y = (float)atof(tokv2[1]);
				    p.z = (float)atof(tokv2[2]);

                    //res = fscanf(f, "%f %f %f\n", &p.x, &p.y, &p.z);
                    //line2++;
                    if ((res != 3) && show_sscan_warn) {
                        ac_trim_in_buff(buff1);
                        SPRTF("%s: WARNING: sscanf of '%s' not 3! got %d\n",
                            mod_name, buff1, res);
                    }
                    ob->vertices[n] = p;
                    if (verb9) {
                        ac_trim_in_buff(buff1);
                        SPRTF("%d: %s\n", line2, buff1);
                        SPRTF("%d: %s %s %s\n", line2, gtf(p.x), gtf(p.y), gtf(p.z));  // USE_GTF
                        //SPRTF("%d: %f %f %f\n", line2, p.x, p.y, p.z);
                    }
                }
                if (n < num) {
                    SPRTF("%s: WARNING: FIle ended while reading %d of %d vertices!\n", mod_name, n, num);
                }
            }
        } else if (streq(t, "numsurf")) {
            num = 0;
            /* why no check of count? */
			res = sscanf(buff2, "%s %d", strg, &num);
			if (num > 0) {
                ob->num_surf = num;
				ob->surfaces = (ACSurface *)new ACSurface[num];
                if (!ob->surfaces) {
                    SPRTF("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                        mod_name, (int)(sizeof(ACSurface) * num));
                    exit(1);
                }
                if (verb9) SPRTF("%d: Reading %d surfaces...\n", line2, num);
                for (n = 0; n < num; n++) {
                    ACSurface *news = read_surface2(f, &ob->surfaces[n], ob);
                    if (news == NULL) {
                        SPRTF("%s: %d: WARNING: error whilst reading surface\n",
                            mod_name, line2);
                        return 0;
                    }
                }
            }
        } else if (streq(t, "crease")) { /** 2013/05/06 - added 'crease %f' **/
            tc = get_tokens2(buff2, &tokc2, tokv2); 
            if ( tc != 2 ) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: %d: WARNING: expected one arg to crease (got %d tokens)\n[%s]\n", 
                    mod_name, line2, tc, buff1);
            } else {
                ob->crease = (float)atof(tokv2[1]);
                ob->had_crease = 1;
            }

        } else if (streq(t, "subdiv")) { /** 2013/05/06 - added 'subdive %d' **/
            tc = get_tokens2(buff2, &tokc2, tokv2); 
            if ( tc != 2 ) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: %d: WARNING: expected one arg to subdiv (got %d tokens)\n[%s]\n", 
                    mod_name, line2, tc, buff1);
            } else {
                ob->subdiv = atoi(tokv2[1]);
            }

        } else if (streq(t, "kids")) { /** 'kids' is the last token in an object **/
            num = 0;
            /* why no check of count? */
			res = sscanf(buff2, "%s %d", t, &num);
			if (num > 0) {
                ob->kids = (ACObject **)new ACObject *[num];
                if (!ob->kids) {
                    SPRTF("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                        mod_name, (int)(num * sizeof(ACObject *)));
                    exit(1);
                }
                ob->num_kids = num;
                if (verb9) SPRTF("%d: kids %d\n", num);
                for (n = 0; n < num; n++) {
                    ACObject *k = ac_load_object2(f, ob);
                    ob->kids[n] = k;    /* NULL or not, set the value */
                    if (k == 0) {
                        SPRTF("%s: %d: WARNING: error reading expected child object %d of %d\n",
                            mod_name, line2, n+1, num);
                        ob->num_kids = n;   /* 2013/05/06 REDUCE expected 'kids' count to this */
                        return ob;
                    }
                }
            }
            return ob;
        } else {
            ac_trim_in_buff(buff1);
            SPRTF("%s: %d: WARNING: unparsed token '%s'\n[%s]\n",
                mod_name, line2, t, buff1 );
        }
    }   /*  while (!feof(f)) */

    return ob;
}

/** bung '\0' chars at the end of tokens and set up the array (tokv) and count (tokc)
	like argv argc - this fragments the buff2
    A COPY of the original read for diagnostic is in buff1
    **/
int ac3d2gl::get_tokens2(char *s, int *argc, char *argv[])
{
    char *p = s;
    char *st;
    char c;
    int tc;

    tc = 0;
    while ((c=*p) != 0) {
        if ((c != ' ') && (c != '\t') && (c != '\n') && ( c != 13)) {
            if (c == '"') {
		        c = *p++;
		        st = p;
                while ((c = *p) && ((c != '"')&&(c != '\n')&& ( c != 13)) ) {
                    if (c == '\\')
                        strcpy(p, p+1);
		            p++;
		        }

		        *p=0;

		        argv[tc++] = st;

                if (tc >= MX_TOKEN_PTRS) {
                    SPRTF("%s: ERROR: Maximum tokens reached %d. Change MX_TOKEN_PTRS and recompile! aborting...\n",
                        mod_name, tc);
                    exit(1);
                }
            } else {
		        st = p;
		        while ((c = *p) && ((c != ' ') && (c != '\t') && (c != '\n') && ( c != 13)) )
		            p++;

		        *p=0;

		        argv[tc++] = st;

                if (tc >= MX_TOKEN_PTRS) {
                    SPRTF("%s: ERROR: Maximum tokens reached %d. Change MX_TOKEN_PTRS and recompile! aborting...\n",
                        mod_name, tc);
                    exit(1);
                }
	        }			
	    }
	    p++;
    }

    *argc = tc;
    return tc;
}

/* ------------------------------------------------------------------------
    Allocate a new object, and set the defaults - init object init_object
   ------------------------------------------------------------------------ */
ACObject *ac3d2gl::new_object2(ACObject *parent)
{
    size_t size = sizeof(ACObject);
    ACObject *ob = new ACObject;    /* init new_object2 */
    if (!ob) {
        SPRTF("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                    mod_name, (int)size);
        exit(1);
    }
    memset(ob,0,size);  /* init new_object2 - zero it all */
    next_id2++;
    ob->pbbox = 0;          // will be zero if no vertices or the bounding calc not done
    ob->parent = parent;    // will be ZERO for top most object(s)
    ob->order = next_id2;    // set a unique id/order of this object
    ob->loc.x = ob->loc.y = ob->loc.z = 0.0; /* init new_object2 */
    ob->got_loc = 0;        /* init new_object2 */
    ob->name = ob->url = NULL;  /* init new_object2 */
    ob->data = NULL;
    ob->vertices = NULL;    /* init new_object2 */
    ob->num_vert = 0;       /* init new_object2 */
    ob->surfaces = NULL;    /* init new_object2 */
    ob->num_surf = 0;       /* init new_object2 */
    ob->texture = -1;       /* init new_object2 NO texture */
    ob->texture_name = 0;   /* init new_object2 NO texture name */
    ob->texture_repeat_x = ob->texture_repeat_y = 1.0; /* init new_object2 */
    ob->texture_offset_x = ob->texture_offset_y = 0.0; /* init new_object2 */
    ob->kids = NULL;        /* init new_object2 */
    ob->num_kids = 0;       /* init new_object2 */
    ob->matrix[0] = 1;      /* init new_object2 */
    ob->matrix[1] = 0;
    ob->matrix[2] = 0;
    ob->matrix[3] = 0;
    ob->matrix[4] = 1;
    ob->matrix[5] = 0;
    ob->matrix[6] = 0;
    ob->matrix[7] = 0;
    ob->matrix[8] = 1;
    ob->crease = 61.0f;  /* 2013/05/06 added - def from OSG ac3d.cpp */ 
    ob->had_crease = 0;  /* 2013/07/31 added - only output if set  */
    ob->subdiv = -1;     /* 2013/05/06 added - -1 means not existing */
    ob->me = this;      /* init new_object2 */
    ob->ucount = 0;     /* init new_object2 2013/07/28 - first try to handle oject names */
    ob->deleted = false; /* init new_object2 2013/07/28 - idea to only MARK an object as deleted */
    if (!first_obj2) {
        first_obj2 = ob;  /* store the FIRST object - THE PARENT */
    }
    return(ob);
}

const char *ac3d2gl::ac_objecttype_to_string2(int t)
{
    if (t == OBJECT_WORLD) return "world";
    if (t == OBJECT_NORMAL) return "poly";
    if (t == OBJECT_GROUP) return "group";
    if (t == OBJECT_LIGHT) return "light";
    return "poly";  // assume normal???
}


int ac3d2gl::string_to_objecttype2(char *s)
{
    if (streq("world", s))
        return(OBJECT_WORLD);
    if (streq("poly", s))
        return(OBJECT_NORMAL);
    if (streq("group", s))
        return(OBJECT_GROUP);
    if (streq("light", s))
        return(OBJECT_LIGHT);
    return(OBJECT_NORMAL);
}

/* ------------------------------------------------------------
   Read an AC3D surface entry.
   "SURF", "mat", "refs" - exit as end of refs count
   Technically these should be in strict order... but presently 
   not checked, except 'refs' assumed to be the last, 
   and is forgiving if some other token found
   ----------------------------------------------------------- */
ACSurface *ac3d2gl::read_surface2(FILE *f, ACSurface *s, ACObject *ob)
{
    char t[64]; /* was 20! */
    int tc;
    int verb9 = ac_verb9();
    int vertcnt = ob->num_vert; // extract number of vertices for this OBJECT

    init_surface2(s);

    while (!feof(f)) {
        read_line2(f);
        sscanf(buff2, "%s", t);
        if (streq(t, "SURF")) {
            s->file_line = line2;   // set line number of this SURF entry
            int flgs;
            tc = get_tokens2(buff2, &tokc2, tokv2);
            if (tc != 2) {
		        SPRTF("%s: %d: WARNING: SURF should be followed by one flags argument\n",
                    mod_name, line2);
            } else {
                flgs = strtol(tokv2[1], NULL, 0);
		        s->flags = flgs;
                if (verb9) SPRTF("%d: SURF %02X\n", line2, s->flags);
            }
        } else if (streq(t, "mat")) {
		    int mindx, res;
            mindx = 0;
		    res = sscanf(buff2, "%s %d", t, &mindx);
            if ((res != 2) && show_sscan_warn) {
                SPRTF("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff2, res);
            }
		    s->mat = mindx + startmatindex2;
            s->mindx = mindx;
            s->tmpindx = mindx;
	    } else if (streq(t, "refs")) {
		    int num, n, len;
		    int ind, res;
		    float tx, ty;
  
            /* check of count? */
            num = 0;
		    res = sscanf(buff2, "%s %d", t, &num);        
            if ((res != 2) && show_sscan_warn) {
                ac_trim_in_buff(buff1);
                SPRTF("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff1, res);
            }
		    s->num_vertref = num;
            if (num) {
    		    s->vertref = (int *)new int[num];
	    	    s->uvs = (ACUV *)new ACUV[num];
            }
            if (verb9) SPRTF("%d: Collection of refs %d...\n", line2, num);
            // 20130828 - Collecting VERTICES - in reading ppe (prettypolyeditor) found the following information
            // ==================================================================================================
            //   int type = ( current_flags & 0x0F ) ;
            int type = s->flags & 0xF;
            // Handle line type objects by creating a single leaf for each line segment.
            //  if ( type == 1 || type == 2 ) {
            //    GLenum gltype = ( type == 1 ) ? GL_LINE_LOOP : GL_LINE_STRIP ;
            //  } else if (type == 0) {
            //     // Handle surface datatypes.
            //     // Each surface is triangulated and each triangle is put
            //     // into the index array current_triindexarray.
            //     int first_vertind = -1 ;
            //     int prev_vertind  = 0 ;
            //     sgVec2 first_texcoord ;
            //     sgVec2 prev_texcoord ;
            //  }
            int first_vertind = -1;
            int prev_vertind  = 0; 
		    for (n = 0; ((n < num) && !feof(f)); n++) {
                /* no check of count? */
                ind = 0;
                tx = 0.0f;
                ty = 0.0f;
                read_line2(f);
			    res = sscanf(buff2, "%d %f %f\n", &ind, &tx, &ty);
			    //res = fscanf(f, "%d %f %f\n", &ind, &tx, &ty);
                //line2++;
                if ((res != 3) && show_sscan_warn) {
                    ac_trim_in_buff(buff1);
                    SPRTF("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                        mod_name, buff1, res);
                }
			    s->vertref[n] = ind;
			    s->uvs[n].u = tx;
			    s->uvs[n].v = ty;
                if (verb9) {
                    ac_trim_in_buff(buff1);
                    SPRTF("%d: %s\n", line2, buff1);
                    //SPRTF("%d: %d %f %f\n", line2, ind, tx, ty);
                    SPRTF("%d: %d %s %s\n", line2, ind, gtf(tx), gtf(ty)); // USE_GTF
                }
                if (ind >= vertcnt) {
                    if (index_warnings < max_index_warnings) {
    		            SPRTF("%s: %d: WARNING: SURF refs %d of %d has index %d GTT|EQU vertices count %d!\n",
                        mod_name, line2, (n+1), num, ind, vertcnt );
                    }
                    index_warnings++;
                    continue;   // do NOT put a BAD index into previous
                }
                if (first_vertind < 0) {
                    // Store the first index texcoord pair.
                    // This one is referenced for every triangle.
                    first_vertind = ind;
                }
                //if (2 <= n) {
                    // we have THREE, so can set the TRIANGLE
                    // Store the edges of the triangle
                    // add_textured_vertex_edge ( first_vertind, first_texcoord ) ;
                    // add_textured_vertex_edge ( prev_vertind, prev_texcoord ) ;
                    // add_textured_vertex_edge ( vertind, texcoord ) ;
                //}
                prev_vertind = ind;
		    }
            if (n < num) {
                SPRTF("%s: Ran out of file while collecting %d of %d refs!\n", mod_name, (n+1), num);
    		    s->num_vertref = num = n;
            }
		    /** calc surface normal - 
                why if only >= 3??? and 
                why only the first 3 vertices used to get the normal for the whole surface?
                20130516: Now check the 'index' is in range of the vertices array **/
		    if (s->num_vertref >= 3) 
            {
                int ind1 = s->vertref[0];
                int ind2 = s->vertref[1];
                int ind3 = s->vertref[2];
                len = ob->num_vert;
                //if (len) {
                if ((ind1 < len) && (ind2 < len) && (ind3 < len)) {
    			    tri_calc_normal2((ACPoint *)&ob->vertices[ind1], 
					    (ACPoint *)&ob->vertices[ind2], 
					    (ACPoint *)&ob->vertices[ind3],
                        (ACPoint *)&s->normal);
                } else {
                    if (len) {
            	        SPRTF("%s: %d: WARNING: 'refs %d' but index to 'numvert %d' out of range! Got %d %d %d - No normal gen'd.\n",
                            mod_name, line2, num, len, ind1, ind2, ind3 );
                    } else {
            	        SPRTF("%s: %d: WARNING: got 'refs %d' but no earlier 'numvert ?'! So no normal generated.\n",
                            mod_name, line2, num);
                    }
                }
            }
		    return s;
		} else {
            ac_trim_in_buff(buff1);
	        SPRTF("%s: %d: WARNING: ignoring '%s'\n",
                mod_name, line2, buff1);
        }
    }   /* while (!feof(f)) */
    return 0;
}

void ac3d2gl::init_surface2(ACSurface *s)
{
    s->vertref = NULL;
    s->uvs = NULL;
    s->num_vertref = 0;
    s->flags = 0;
    s->mat = 0;
    s->mindx = 0;
    s->tmpindx = 0;
    s->normal.x = 0.0;
    s->normal.y = 0.0;
    s->normal.z = 0.0; 
    s->file_line = line2;   // file line fo this SURF
}

/* ---------------------------------------------------------------------
    Given 3 vector points (x1,y1,z1), (x2,y2,z2), (x3,y3,z3), then
    normal_x = (y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1);
    normal_y = (z2 - z1) * (x3 - x1) - (y2 - y1) * (z3 - z1);
    normal_z = (x2 - x1) * (y3 - y1) - (x2 - x1) * (x3 - x1);
    length   = sqrt( normal_x * normal_x + normal_y * normal_y + normal_z * normal_z )
    if (length > 0.0) // normalise - reduce to unit 1 = x + y + z
        normal_x /= lenght; normal_y /= length; normal_y /= length;
   --------------------------------------------------------------------- */
void ac3d2gl::tri_calc_normal2(ACPoint *v1, ACPoint *v2, ACPoint *v3, ACPoint *n)
{
    double len;

    n->x = (v2->y - v1->y) * (v3->z - v1->z) - (v3->y - v1->y) * (v2->z - v1->z);
    n->y = (v2->z - v1->z) * (v3->x - v1->x) - (v3->z - v1->z) * (v2->x - v1->x);
    n->z = (v2->x - v1->x) * (v3->y - v1->y) - (v3->x - v1->x) * (v2->y - v1->y);
    len = sqrt(n->x * n->x + n->y * n->y + n->z * n->z);

    if (len > 0)
    {
    	n->x /= (float)len;
	    n->y /= (float)len;
	    n->z /= (float)len;  
    }

}

void ac3d2gl::ac_free_objects2()   /* free ALL memory */
{
    int n;
    if (first_obj2) {
        ac_object_free2(first_obj2); // this will also free all 'kids'
        first_obj2 = 0;
        for (n = 0; n < num_palette2; n++) {
            ACMaterial m = palette2[n];
            delete m.name;
            m.name = 0;
        }
    }
    next_id2 = 0;
    num_palette2 = 0;
    startmatindex2 = 0;
    index_warnings = 0;
}

/* ---------------------------------------------------------------
   Free an object
   --------------------------------------------------------------- */
void ac3d2gl::ac_object_free2(ACObject *ob)
{
    int n, s;

	for (n = 0; n < ob->num_kids; n++)
		ac_object_free2(ob->kids[n]);

	if (ob->vertices)
		delete ob->vertices;
    ob->vertices = 0;

	for (s = 0; s < ob->num_surf; s ++) {
		delete ob->surfaces[s].vertref;
		delete ob->surfaces[s].uvs;
		ob->surfaces[s].vertref = 0;
		ob->surfaces[s].uvs = 0;
	}

	if (ob->surfaces)
		delete ob->surfaces;
    ob->surfaces = 0;

	if (ob->data)
		delete ob->data;
    ob->data = 0;

	if (ob->url)
		delete ob->url;
    ob->url = 0;

	if (ob->name)
		delete ob->name;
    ob->name = 0;

    // bounding box if there is one...
    if (ob->pbbox)
        delete ob->pbbox;
    ob->pbbox = 0;

    index_warnings = 0; // restart index out-of-range warnings

    delete ob;
}

ACMaterial *ac3d2gl::ac_palette_get_material2(int id)
{
    if (id < num_palette2) // ensure value in range
        return(&palette2[id]);
    return 0;
}

void ac3d2gl::ac_calc_vertex_normals2(ACObject *ob)
{
    int n;
    
    ac_object_calc_vertex_normals2(ob);
    if (ob->num_kids) {
	    for (n = 0; n < ob->num_kids; n++) {
	        ac_calc_vertex_normals2(ob->kids[n]);
        }
    }
}

void ac3d2gl::ac_object_calc_vertex_normals2(ACObject *ob)
{
    int s, v, vr;

    /** for each vertex in this object **/
    for (v = 0; v < ob->num_vert; v++) {
	    ACNormal n = {0, 0, 0};
	    int found = 0;

	    /** go through each surface **/
	    for (s = 0; s < ob->num_surf; s++) {
	        ACSurface *surf = &ob->surfaces[s];

	        /** check if this vertex is used in this surface **/
	        /** if it is, use it to create an average normal **/
	        for (vr = 0; vr < surf->num_vertref; vr++) {
		        if (surf->vertref[vr] == v) {
		            n.x+=surf->normal.x;
		            n.y+=surf->normal.y;
		            n.z+=surf->normal.z;
		            found++;
		        }
            }
	    }
	    if (found > 0) {
	        n.x /= found;
	        n.y /= found;
	        n.z /= found;
	    }
	    ob->vertices[v].normal = n;
    }

}

int ac3d2gl::ac_trim_in_buff(char *buf)
{
    size_t len = strlen(buf);
    while (len) {
        len--;
        if (buf[len] > ' ')
            break;
        buf[len] = 0;
    }
    return len;
}

static void ac_set_vertices(ACObject *ob, PBBOX pbb)
{
    int n;
    for (n = 0; n < ob->num_vert; n++) {
        ACVertex p = ob->vertices[n];
        if (pbb->num_verts == 0) {
            pbb->maxx = pbb->minx = p.x;
            pbb->maxy = pbb->miny = p.y;
            pbb->maxz = pbb->minz = p.z;
        } else {
            if (p.x > pbb->maxx) pbb->maxx = p.x;
            if (p.x < pbb->minx) pbb->minx = p.x;
            if (p.y > pbb->maxy) pbb->maxy = p.y;
            if (p.y < pbb->miny) pbb->miny = p.y;
            if (p.z > pbb->maxz) pbb->maxz = p.z;
            if (p.z < pbb->minz) pbb->minz = p.z;
        }
        pbb->num_verts++;
    }
}

PBBOX ac3d2gl::ac_get_obj_bbox( ACObject *ob )
{
    if (ob->num_vert) {
        if (ob->pbbox) return ob->pbbox;
        // does not yet exist, create it
        ob->pbbox = new BBOX;
        if (!ob->pbbox) return 0;   // MEMORY FAILED!!!

        ob->pbbox->num_verts = 0;   // zero vertice count
        ac_set_vertices( ob, ob->pbbox );   // fill in min/max x,y,z
        return ob->pbbox;   // return pointer
    }
    return 0;
}

int ac3d2gl::ac_vertex_in_bbox( ACObject *ob, ACVertex *p )
{
    PBBOX pbb = ob->pbbox;
    if (!pbb) return 0;
    if ( ( p->x <= pbb->maxx ) &&
         ( p->x >= pbb->minx ) &&
         ( p->y <= pbb->maxy ) &&
         ( p->y >= pbb->miny ) &&
         ( p->z <= pbb->maxz ) &&
         ( p->z >= pbb->minz ) ) {
        return 1;
    }
    return 0;
}

int ac3d2gl::ac_xy_in_bbox( ACObject *ob, ACVertex *p, float fudge )
{
    PBBOX pbb = ob->pbbox;
    if (!pbb) return 0;
    if ( ( p->x <= pbb->maxx ) &&
         ( p->x >= pbb->minx ) &&
         ( p->y <= pbb->maxy ) &&
         ( p->y >= pbb->miny ) ) {
        return 1;
    }
    if (fudge > 0.0) {
        if ( ( (p->x - fudge) <= pbb->maxx ) &&
             ( (p->x + fudge) >= pbb->minx ) &&
             ( (p->y - fudge) <= pbb->maxy ) &&
             ( (p->y + fudge) >= pbb->miny ) ) {
            return 1;
        }
    }
    return 0;
}


static void ac_set_kids(ACObject *ob, PBBOX pbb)
{
    int n;
    ac_set_vertices(ob,pbb);
    for (n = 0; n < ob->num_kids; n++)
        ac_set_kids( ob->kids[n], pbb );

}
/* =================================================================

from: http://stackoverflow.com/questions/4180289/fit-3d-model-inside-a-window

The following is from a posting by Dave Shreiner on setting up a basic viewing system:

First, compute a bounding sphere for all objects in your scene. This should provide you 
with two bits of information: the center of the sphere (let ( c.x, c.y, c.z ) be that point) 
and its diameter (call it "diam").

Next, choose a value for the zNear clipping plane. General guidelines are to choose something 
larger than, but close to 1.0. So, let's say you set

zNear = 1.0; zFar = zNear + diam; 
Structure your matrix calls in this order (for an Orthographic projection):

GLdouble left = c.x - diam; 
GLdouble right = c.x + diam;
GLdouble bottom c.y - diam; 
GLdouble top = c.y + diam; 
glMatrixMode(GL_PROJECTION); 
glLoadIdentity(); 
glOrtho(left, right, bottom, top, zNear, zFar); 
glMatrixMode(GL_MODELVIEW); 
glLoadIdentity(); 
This approach should center your objects in the middle of the window and stretch them to fit 
(i.e., its assuming that you're using a window with aspect ratio = 1.0). If your window isn't 
square, compute left, right, bottom, and top, as above, and put in the following logic before 
the call to glOrtho():

GLdouble aspect = (GLdouble) windowWidth / windowHeight; 
if ( aspect < 1.0 ) { 
    // window taller than wide 
    bottom /= aspect; 
    top /= aspect; 
} else { 
    left *= aspect; 
    right *= aspect;
} 
The above code should position the objects in your scene appropriately. If you intend to 
manipulate (i.e. rotate, etc.), you need to add a viewing transform to it.

A typical viewing transform will go on the ModelView matrix and might look like this:

GluLookAt (0., 0., 2.*diam, c.x, c.y, c.z, 0.0, 1.0, 0.0);
   ================================================================= */
int ac3d2gl::ac_set_bounding_box2(ACObject *ob, PBBOX pbb)
{
    int n;
    float maxx, maxy, maxz, r;
    pbb->num_verts = 0;
    ac_set_vertices(ob,pbb);
    for (n = 0; n < ob->num_kids; n++)
        ac_set_kids( ob->kids[n], pbb );
    if (pbb->num_verts) { /* set the CENTER */
        // glTranslatef(-(max.x + min.x)/2.0f,-(max.y + min.y)/2.0f,-(max.z + min.z)/2.0f);
        ACPoint p;
        maxx = pbb->maxx;
        maxy = pbb->maxy;
        maxz = pbb->maxz;
        p.x = ((pbb->minx + maxx) / 2.0f);
        p.y = ((pbb->miny + maxy) / 2.0f);
        p.z = ((pbb->minz + maxz) / 2.0f);
        pbb->center = p;
        r = sqrt((maxx - p.x)*(maxx - p.x) + 
                 (maxy - p.y)*(maxy - p.y) +
                 (maxz - p.z)*(maxz - p.z));
        pbb->radius = r;
        // from : http://www.opengl.org/discussion_boards/showthread.php/130876-automatically-center-3d-object
        // float fDistance = r / 0.57735f; // where 0.57735f is tan(30 degrees)
        // double dNear = fDistance - r;
        // double dFar = fDistance + r;
        // glFrustum(-r, +r, +r, -r, dNear, dFar); // ignoring aspect ratio of the window for now!
        // All we need to do is to position the camera back along the z axis to where we want it -
        // glMatrixMode(GL_MODELVIEW);
        // glLoadIdentity();
        // gluLookAt(0.0f, 0.0f, fDistance, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        // and then move the model to align its centre with the origin -
        // glTranslatef(-p.x, -p.y, -p.z);
    }
    return pbb->num_verts;
}

int ac3d2gl::ac_set_model_size_pos2(ACObject *ob, PGL_PARMS pgl, PBBOX pbb)
{
static BBOX _s_bb;
    if (!pbb)
        pbb = &_s_bb;
    if (!ob->me->ac_set_bounding_box2(ob, pbb))
        return 1;
    float r = pbb->radius;
    ACPoint p = pbb->center;
    float fDistance = r / 0.57735f; // where 0.57735f is tan(30 degrees)
    double dNear = fDistance - r;
    double dFar = fDistance + r;
    // The glFrustum function multiplies the current matrix by a perspective matrix.
    // void glFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar);
    pgl->left = -r;  //  The coordinate for the left-vertical clipping plane.
    pgl->right = +r; //  The coordinate for the right-vertical clipping plane.
    pgl->bottom = +r;   //The coordinate for the bottom-horizontal clipping plane.
    pgl->top = -r;  //   The coordinate for the bottom-horizontal clipping plane. 
    pgl->zNear = dNear; // The distances to the near-depth clipping plane. Must be positive.
    pgl->zFar = dFar; //   The distances to the far-depth clipping planes. Must be positive.
    //glFrustum(-r, +r, +r, -r, dNear, dFar); // ignoring aspect ratio of the window for now!
    // All we need to do is to position the camera back along the z axis to where we want it -
    // glMatrixMode(GL_MODELVIEW);
    // glLoadIdentity();
    // gluLookAt(0.0f, 0.0f, fDistance, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    pgl->eyex = 0.0f;
    pgl->eyey = 0.0f;
    pgl->eyez = fDistance;
    pgl->centrex = 0.0f;
    pgl->centrey = 0.0f;
    pgl->centrez = 0.0f;
    pgl->upx = 0.0f;
    pgl->upy = 1.0f;
    pgl->upz = 0.0f;
    // and then move the model to align its centre with the origin -
    // glTranslatef(-p.x, -p.y, -p.z);
    // glTranslatef ( x_trans, y_trans, z_trans ) ;
    pgl->x_trans = -p.x;
    pgl->y_trans = -p.y;
    pgl->z_trans = -p.z;
    return 0;
}

int add_mat_one = 0;

int need_fix(const char *msg) 
{
    int i;
    i = (int)strlen(msg);
    return i;
}

int check_me(const char *msg)
{
    int i;
    i = (int)strlen(msg);
    return i;
}

// TODO: Get normals, uvs AND MATERIALS if desired, required
// =========================================================
void ac3d2gl::get_verts_faces_norms_uvs( WORKSTR &ws )
{
    int cnt = 0;
    int i, s, vr, vnum, snum, rnum, ok, vind;
    int flag;
    int vvcnt = 0;
    int dnvw = 0;   // done vector warning

    // init items
    ACObject *ob = ws.ob;   // get object
    char *info   = ws.info;   // get information buffer
    //int vcount   = ws.vcount;   // get counts of vectors added so far
    char *tb     = ws.tmpbuf;
    int vcount   = (int)ws.vACVectors.size();

    vnum = ob->num_vert;
    snum = ob->num_surf;
    if (vnum && snum) {
        // only IFF there are vertices AND faces to add
        if (vcount)
            ws.verts << ",";
        sprintf(info,"vectors prev %d (%d), new %d (%d), %d surfaces ", vcount, (vcount * 3), vnum, (vnum * 3), snum );
        for (cnt = 0; cnt < vnum; cnt++) {
            ACVertex p = ob->vertices[cnt];
            sprintf(tb,"%s,%s,%s", gtf(p.x), gtf(p.y), gtf(p.z));  // USE_GTF
            //sprintf(tb,"%f,%f,%f", p.x, p.y, p.z);
            ws.verts << tb;
            if ((cnt + 1) < vnum)
                ws.verts << ",";
            ws.vACVectors.push_back(p); // store the vector
        }

        vvcnt = (int)ws.vACVectors.size();   // linear count of vector list
        sprintf(EndBuf(info),"%d (%d) tot.vecs %d (%d)", (vcount + vnum), ((vcount + vnum) * 3), vvcnt, (vvcnt * 3));
        vvcnt *= 3; // x,y,z for each = * 3
        for (s = 0; s < snum; s++) {
	        ACSurface *surf = &ob->surfaces[s];
            rnum = surf->num_vertref;
            ok = 1;
            flag = 0;
            if (rnum == 3) {
                //flag = 0;
            } else if (rnum == 4) {
                flag |= QuadBit;
            } else {
                ok = 0;
                if (fix_warnings == 0) {
                    SPRTF("WARNING: NEED FIX: surf->num_vertref not 3 or 4! Got %d\nLine %d in file %s\n", rnum, surf->file_line, file_name);
                }
                need_fix("NEED FIX: surf->num_vertref not 3 or 4!");
                fix_warnings++;
            }
            if (add_mat_one)
                flag |= MatBit;

            if (ok && rnum) {
                if (ws.facecnt)
                    ws.faces << ",";    // start with the comma
                sprintf(tb,"%d", flag);    // add flag
	            for (vr = 0; vr < rnum; vr++) {
                    strcat(tb,","); // add comma
                    vind = surf->vertref[vr];
                    if (vind >= vnum) {
                        if (vert_warnings1 == 0) {
                            SPRTF("WARNING: raw vertices index %d GTT|EQU vector count %d! VERY BAD!!\nFile Line %d in %s\n", vind, vnum, surf->file_line, file_name);
                        }
                        if (!dnvw) need_fix("WARNING: raw vertices index GTT|EQU vector count");
                        dnvw = 1;
                        vert_warnings1++;
                    }
                    vind += vcount;
                    if (vind == 999999)
                        check_me("vind = 999999");

                    sprintf(EndBuf(tb),"%d", vind );    // add index
                    vind *= 3;
                    if (!dnvw && ((vind + 2) >= vvcnt)) {
                        if (vert_warnings2 == 0) {
                            SPRTF("WARNING: vertices index %d+2 GTT|EQU vector count %d! VERY BAD!! ((%d + %d) * 3)\nFile line %d in %s\n", vind, (int)vvcnt,
                            surf->vertref[vr], vcount, surf->file_line, file_name);
                        }
                        if (!dnvw) need_fix("WARNING: vertices index GTT|EQU vector count");
                        dnvw = 1;
                        vert_warnings2++;
                    }
                }
                if (add_mat_one) {
                    strcat(tb,",0");
                }
                ws.faces << tb;    // add flag and offsets
                ws.facecnt++;
            }
        }
        if (ac_verb9())
            SPRTF("%s\n",info);
    } else if (snum) {
        SPRTF("WARNING: How can there be %d SURFaces with 0 VERTICES!!!\n", snum);
        vnum = 0;
        need_fix("WARNING: How can there be SURFaces with 0 VERTICES!!!");
    }

    for (i = 0; i < ob->num_kids; i++) {
        ws.ob = ob->kids[i];    // set OBJECT
        get_verts_faces_norms_uvs( ws );    // process it
    }
}

const char *hdr3j = "{\n"
"\n"
"\t\"metadata\" :\n"
"\t{\n"
"\t\t\"formatVersion\" : 3.1,\n"
"\t\t\"generatedBy\"   : \"%s\",\n"
"\t\t\"vertices\"      : %d,\n"
"\t\t\"faces\"         : %d,\n"
"\t\t\"normals\"       : %d,\n"
"\t\t\"colors\"        : 0,\n"
"\t\t\"uvs\"           : [],\n"
"\t\t\"materials\"     : %d,\n"
"\t\t\"morphTargets\"  : 0,\n"
"\t\t\"bones\"         : 0\n"
"\t},\n"
"\n"
"\t\"scale\"         : %s,\n"
"\n";

const char *morph  = "\t\"morphTargets\" : [],\n\n";
const char *norms  = "\t\"normals\"      : [],\n\n";
const char *colors = "\t\"colors\"       : [],\n\n";
const char *uvs    = "\t\"uvs\"          : [],\n\n";

const char *tail3j = "\t\"bones\"        : [],\n"
"\n"
"\t\"skinIndices\"   : [],\n"
"\n"
"\t\"skinWeights\"   : [],\n"
"\n"
"\t\"animation\"     : {}\n"
"\n"
"}\n\n";

const char *gen = "ac3d_browser2 exporter";

// ==================
const char *mat0 =
    "\t\"materials\" : [],\n\n";

const char *mat1 =
"\t\"materials\" : [{\n"
"\t\t\"DbgColor\"      : 1,\n"
"\t\t\"DbgIndex\"      : 0,\n"
"\t\t\"DbgName\"       : \"Material.001\",\n"
"\t\t\"blending\"      : \"NormalBlending\",\n"
"\t\t\"colorAmbient\"  : [0.800000011920929, 0.800000011920929, 0.800000011920929],\n"
"\t\t\"colorDiffuse\"  : [0.800000011920929, 0.800000011920929, 0.800000011920929],\n"
"\t\t\"colorSpecular\" : [0.0, 0.0, 0.0],\n"
"\t\t\"depthTest\"     : true,\n"
"\t\t\"depthWrite\"    : true,\n"
"\t\t\"shading\"       : \"Lambert\",\n"
"\t\t\"specularCoef\"  : 50,\n"
"\t\t\"transparency\"  : 1.0,\n"
"\t\t\"transparent\"   : false,\n"
"\t\t\"vertexColors\"  : false\n"
"\t}],\n\n";


int ac3d2gl::ac_export_threejs(ACObject *ob, FILE *fp)
{
    static char _s_info_buf[2048];
    WORKSTR ws;
    size_t len, res, tmp;
    int verb = ac_get_verbosity();

    //ac_set_verbosity(9); // JUST FOR DEBUG

    // init WORKSTR
    ws.ob     = ob;
    ws.facecnt = 0;
    ws.ncount = 0;
    ws.mcount = 1;
    ws.scale  = 1.0f;   // TODO: HOW TO SET THIS?
    ws.info = _s_info_buf;  // general info buffer
    ws.tmpbuf = GetNxtBuf();
    ws.faces.clear();
    ws.verts.clear();

    // gather the information
    get_verts_faces_norms_uvs( ws );

    int fcnt = ws.facecnt;  // count of FACES added
    int ncnt = ws.ncount;
    int mcnt = ws.mcount;
    int vcnt = (int)ws.vACVectors.size();
    float scale = ws.scale; // TODO: what to do about this SCALE???

    ac_set_verbosity(verb);

    char *tb = ws.tmpbuf;
    // setup "generatedBy" string
    char *gb = GetNxtBuf();
    sprintf(gb,"%s %s", gen, Get_Current_UTC_Time_Stg());

    // HEADER - metadata
    // ==============================================================
    len = sprintf(tb,hdr3j, gb, vcnt, fcnt, ncnt, mcnt, gtf(scale));
    res = fwrite(tb, 1, len, fp );
    if (res != len) return 1;
    // ==============================================================

    // MATERIALS
    // ==============================================================
    if (add_mat_one) {
        len = strlen(mat1);
        res = fwrite( mat1, 1, len, fp );
        if (res != len) return 1;
    } else {
        len = strlen(mat0);
        res = fwrite( mat0, 1, len, fp );
        if (res != len) return 1;
    }
    // ==============================================================

    // VERTICES
    // ==============================================================
    //        "\t\"vertices\"      : []\n"
    strcpy(tb,"\t\"vertices\"      : [");
    tmp = strlen(tb);
    res = fwrite( tb, 1, tmp, fp );
    if (res != tmp) return 1;
    len = ws.verts.str().size();
    if (len) {
        res = fwrite( ws.verts.str().c_str(), 1, len, fp );
        if (res != len) return 1;
    }
    strcpy(tb,"],\n\n");
    tmp = strlen(tb);
    res = fwrite( tb, 1, tmp, fp );
    if (res != tmp) return 1;
    // ==============================================================

    len = strlen(morph);
    res = fwrite( morph, 1, len, fp );
    if (res != len) return 1;

    // TODO: Add normals, maybe if really needed
    len = strlen(norms);
    res = fwrite( norms, 1, len, fp );
    if (res != len) return 1;

    len = strlen(colors);
    res = fwrite( colors, 1, len, fp );
    if (res != len) return 1;

    len = strlen(uvs);
    res = fwrite( uvs, 1, len, fp );
    if (res != len) return 1;

    // FACES
    // ==============================================================
    //        "\t\"faces\"         : []\n"
    strcpy(tb,"\t\"faces\"         : [");
    tmp = strlen(tb);
    res = fwrite( tb, 1, tmp, fp );
    if (res != tmp) return 1;
    len = ws.faces.str().size();
    if (len) {
        res = fwrite( ws.faces.str().c_str(), 1, len, fp );
        if (res != len) return 1;
    }
    strcpy(tb,"],\n\n");
    tmp = strlen(tb);
    res = fwrite( tb, 1, tmp, fp );
    if (res != tmp) return 1;
    // ==============================================================

    len = strlen(tail3j);
    res = fwrite( tail3j, 1, len, fp );
    if (res != len) return 1;

    return 0;
}

ACObject *ac3d2gl::find_Object(const char *name, ACObject *ob, int type)
{
    ACObject *fob = 0;
    if (ob->name) {
        if ((strcmp(name,ob->name)==0) &&
            (ob->type == type) )
        {
            return ob;
        }
    }
    int i;
    for (i = 0; i < ob->num_kids; i++) {
        fob = find_Object(name, ob->kids[i], type);
        if (fob)
            return fob;
    }
    return fob;
}

ACObject *ac3d2gl::find_Largest( PACFNDOBJ pfo, ACObject *ob )
{
    if (ob->num_vert > pfo->vcount) {
        pfo->ob = ob;
        pfo->vcount = ob->num_vert;
    }
    int i;
    for (i = 0; i < ob->num_kids; i++) {
        find_Largest( pfo, ob->kids[i] );
    }
    return pfo->ob;
}

char *ac3d2gl::get_Object_Name( ACObject *ob )
{
    char *cp = GetNxtBuf();
    strcpy( cp, ac_objecttype_to_string2(ob->type) );
    strcat(cp," ");
    if (ob->name && *ob->name) {
        strcat(cp,ob->name);
    } else {
        strcat(cp,"noName");
    }
    return cp;
}

/* eof - ac3d2.cxx */
