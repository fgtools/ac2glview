#
# Try to find FLU library and include path.
# Once done this will define
#
# FLU_FOUND
# FLU_INCLUDE_PATH
# FLU_LIBRARY
#
if (FLU_ROOT_DIR)
    message(STATUS "*** Given a FLU_ROOT_DIR=${FLU_ROOT_DIR}")
endif ()

IF (WIN32)
 FIND_PATH( FLU_INCLUDE_PATH FLU/Flu_Version.h
       ${FLU_ROOT_DIR}/include
     DOC "The directory where FLU headers resides")
 IF (NV_SYSTEM_PROCESSOR STREQUAL "AMD64")
   FIND_LIBRARY( FLU_LIBRARY
     NAMES flu64
     PATHS ${FLU_ROOT_DIR}/lib64
     DOC "The FLU library (64-bit)")
 ELSE(NV_SYSTEM_PROCESSOR STREQUAL "AMD64")
    FIND_LIBRARY( FLU_LIB_REL
        NAMES flu
        PATHS ${FLU_ROOT_DIR}/lib
        DOC "The FLU Release library")
    FIND_LIBRARY( FLU_LIB_DBG
        NAMES flud
        PATHS ${FLU_ROOT_DIR}/lib
        DOC "The FLU Debug library")
    if (FLU_LIB_REL)
        set(FLU_LIBRARY "optimized;${FLU_LIB_REL}")
        if (FLU_LIB_DBG)
            list(APPEND FLU_LIBRARY "debug;${FLU_LIB_DBG}")
        else ()
            list(APPEND FLU_LIBRARY "debug;${FLU_LIB_REL}")
        endif ()
    endif ()
 ENDIF(NV_SYSTEM_PROCESSOR STREQUAL "AMD64")
ELSE (WIN32)
 FIND_PATH( FLU_INCLUDE_PATH FLU/Flu_Version.h
   /usr/include
   /usr/local/include
   /sw/include
   /opt/local/include
   ${FLU_ROOT_DIR}/include
   DOC "The directory where FLU/Flu_Version.h resides")
 # find libflu.a static library
 FIND_LIBRARY( FLU_LIBRARY
   NAMES flu
   PATHS
    /usr/lib
    /usr/lib64
    /usr/local/lib64
    /usr/local/lib
    /sw/lib
    /opt/local/lib
    ${FLU_ROOT_DIR}/lib
   DOC "The FLU library")
ENDIF (WIN32)

SET(FLU_FOUND "NO")
IF (FLU_INCLUDE_PATH AND FLU_LIBRARY)
 SET(FLU_LIBRARIES ${FLU_LIBRARY})
 SET(FLU_FOUND "YES")
ELSE ()
    message(STATUS "*** FLU NOT FOUND")
    if (FLU_ROOT_DIR)
        message(STATUS "*** Was given a FLU_ROOT_DIR=${FLU_ROOT_DIR}")
    else ()
        message(STATUS "*** If in a specific path define FLU_ROOT_DIR")
    endif ()
ENDIF ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FLU DEFAULT_MSG FLU_LIBRARY FLU_INCLUDE_PATH)

# eof

