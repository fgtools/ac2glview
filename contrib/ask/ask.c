/*  ********************************************************************************
    ask.c

    BSD 3 Clause License

    Copyright (c) 2013, Geoff R. McLane

    All rights reserved.

    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright 
    notice, this list of conditions and the following disclaimer in 
    the documentation and/or other materials provided with the distribution.

    Neither the name of the Geoff R. McLane nor the names of other contributors 
    may be used to endorse or promote products derived from this software 
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    ******************************************************************************** */

//
// July 2013 - Compile in Unbuntu linux
// minor update - 1 Sept, 2008 - extend HELP inputs to '/?' or '/h'
// does nothing but output a prompt, if one is given on the command line,
// waits for a character input
// then exits with ERRORLEVEL 1 if 'Y' or 'y' entered,
// else exits with ERRORLEVEL 0 for all other characters
// note: will accept redirected, like c:\>ask < yes.txt
// geoff r. mclane - July 13, 2006
// This some 40KB executable replaces my 16 bit 233 bytes COM file
// ===============================================================

// options - not all tested
#undef  _USE_WINDOWS
#define  _USE_KBHIT

#ifdef   _USE_KBHIT
#undef   _USE_WINDOWS
#endif // #ifdef _USE_KBHIT

#ifdef _MSC_VER
#pragma warning( disable:4996 )
#endif

#ifdef   _USE_WINDOWS
#include <windows.h>
#include <io.h>
#endif // _USE_WINDOWS

#include <stdlib.h>  // toupper
#include <stdio.h>   // fflush
#ifdef _MSC_VER
#include <conio.h>   // _getch
#else
#include <unistd.h>  // for STDIN_FILENO
#include <termio.h> 
#include <fcntl.h>  // for F_GETFL
#include <time.h>   // for nanosleep
#endif
#include <ctype.h>   // toupper
#include <string.h>  // strcmpi

#ifdef _MSC_VER
#define SLEEP _sleep
#define GETCHAR _getch
#else
#define SLEEP nsleep
#define GETCHAR getchar
#endif

#ifndef _MSC_VER
// a unix port of int _kbhit()
int _kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}

void nsleep( int ms )
{
    //  max 999999999 ns
    struct timespec req;
    struct timespec rem;
    req.tv_sec = ms / 1000;
    ms -= req.tv_sec * 1000;
    req.tv_nsec = ms * 1000000;
    nanosleep( &req, &rem );
}

#endif
// try _kbhit
// Return Value
// _kbhit returns a nonzero value if a key has been pressed. Otherwise, it returns 0.
int wait_kbd( void )
{
    int ch;
    while( _kbhit() == 0 ) {
      SLEEP(55);
    }
    ch = GETCHAR();
    return ch;
}

static int verbal = 0;

int main( int argc, char * * argv )
{
   int   chr;
   int iret = 0;
   char * arg;
   int   argcnt = 0;
#ifdef   _USE_WINDOWS
   char buf[264];
   HANDLE h; // = GetStdHandle(STD_INPUT_HANDLE);
   int res;
#endif // _USE_WINDOWS
   // fflush(stdin); CAN NOT DO THIS if < yes.txt to be used
   if( argc > 1 ) {
      int i;
      /* preprcess arguments looking for HELP indication */
      for( i = 1; i < argc; i++ ) {
         arg = argv[i];
         if(( strcmp(arg, "-?") == 0 )||
            ( strcmp(arg, "-h") == 0 )||
            ( strcmp(arg, "--help") == 0 )||
            ( strcmp(arg, "/?") == 0) ||
            ( strcmp(arg, "/h") == 0) )
         {
            printf( "Brief Help on Usage - " __DATE__ " " __TIME__ "\n" );
            printf( "%s [prompt]\n", argv[0] );
            printf( " If 'Y' input, errorexit set to 1\n" );
            printf( " Else all other input, errorexit 0\n" );
            exit(0);
         } else if( strcmp(arg, "-v") == 0 ) {
            verbal = 1;
         }
      }
      for( i = 1; i < argc; i++ ) {
         arg = argv[i];
         /* skip any -v command */
         if (strcmp(arg, "-v")) {
            printf( "%s ",arg );
            argcnt++;
         }
      }
   }
#ifdef   _USE_WINDOWS
   // this needs windows.h
   res = feof(stdin);
   h = GetStdHandle(STD_INPUT_HANDLE); 
   //fread( buf, 1, 1, h );
   res = _read((int)h, buf, 1);
   if( res == -1 ) {
      //chr = toupper(_getch());
      chr = toupper(fgetc( stdin ));
   } else {
      chr = toupper(buf[0]);
   }
#else // !_USE_WINDOWS
#ifdef   _USE_KBHIT
   chr = toupper(wait_kbd());
   if( argc > 1 ) {
      if( chr == 'Y' ) {
         printf( "Yes" );
      } else {
         printf( "No" );
      }
   }
#else // !_USE_KBHIT
   if( feof(stdin) ) {
      chr = toupper(_getch());
   } else {
      chr = toupper(fgetc( stdin ));
   }
   //chr = toupper(getchar());
   //chr = toupper(fgetc( stdin ));
   //chr = toupper(getc( stdin ));
#endif   // _USE_KBHIT function
#endif // _USE_WINDOWS y/n
   fflush(stdin);
   if(chr == 'Y') {
      iret = 1;
   }
   if( argcnt ) {
      printf( "\n" );
   }
   if(verbal)
      printf( "ERRORLEVEL = %d\n", iret );

   return iret;
}

// eof - ask.c

