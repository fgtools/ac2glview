#!/bin/sh
#< test1.sh - 20130730
BN=`basename $0`

TMPRUN="./ask"
if [ ! -f ask ]; then
    ask -h >/dev/null
    if [ ! "$?" = "0" ]; then
        echo "$BN: Can NOT find either local program ./ask"
        echo "     nor one in the PATH"
        echo "Build ask first..."
        exit 1
    fi
    TMPRUN="ask"
fi

echo "Testing ASK ..."
echo ""
$TMPRUN "Test 1: Enter a Y ..."
if [ "$?" = "1" ]; then
    echo "$BN: Got a Yes"
else
    echo "$BN: DID NOT GET A YES ..."
fi
echo ""

$TMPRUN Test 2: With a long prompt ... Do you want to CONTINUE ... y or otherwise
if [ "$?" = "1" ]; then
    echo "$BN: Got a Yes"
else
    echo "$BN: DID NOT GET A YES ..."
fi

echo ""
echo "Testing automation... that is redirected input from a file..."
if [ -f "y.txt" ]; then
    rm -f y.txt
fi
echo "y" >y.txt

$TMPRUN < y.txt
if [ "$?" = "1" ]; then
    echo "$BN: Got a Yes"
else
    echo "$BN: DID NOT GET A YES ..."
fi
if [ -f "y.txt" ]; then
    rm -f y.txt
fi
echo ""
echo "$BN: Done testing on $TMPRUN binary"
echo ""

# eof


