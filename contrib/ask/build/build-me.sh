#!/bin/sh
#< build-me.sh - ask project - 20130730
BN=`basename $0`

echo ""
echo "$BN: Some suggested cmake options to use for debug..."
echo "  -DCMAKE_VERBOSE_MAKEFILE=TRUE - use a verbose Makefile good to see flags. switches, libraries, etc..."
echo "  -DCMAKE_BUILD_TYPE=DEBUG - to add symbols for gdb use (add -g compiler switch)"
echo "  Then run gdb with '\$ gdb --args ask prompt'"
echo ""

TMPOPTS=""
for arg in $@; do
    TMPOPTS="$TMPOPTS $arg"
done

echo "$BN: Doing 'cmake .. $TMPOPTS'..."
cmake .. $TMPOPTS
if [ ! "$?" = "0" ]; then
    echo "$BN: Have configuration, generation error"
    exit 1
fi

echo ""
echo "$BN: Doing 'make'"
make
if [ ! "$?" = "0" ]; then
    echo "$BN: Have compile, link error"
    exit 1
fi

echo ""
echo "$BN: appears successful... maybe '[sudo] make install' next?"
echo ""

# eof

