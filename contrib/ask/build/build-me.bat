@setlocal

@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=C:\MDOS

:RPT
@if "%~1x" == "x" goto GOTOPTS
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT

:GOTOPTS

cmake .. %TMPOPTS%
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Release
@if ERRORLEVEL 1 goto ERR2

@echo Appears a successfile build...
@echo Maybe run 'cmake --build . --config Release --target INSTALL

@goto END

:Err1
@echo Error: cmake config or gen FAILED!
@goto END

:Err2
@echo Error: cmake compile or link FAILED!
@goto END

:END

@REM eof



