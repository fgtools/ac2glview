#!/bin/sh
#< zip-me.sh - askk - 20130730
BN=`basename $0`

TMPVER="03"
TMPZIP="ask-$TMPVER.zip"
TMPOPT="-o -r"

echo ""
if [ ! -f "../../ask/build/zip-me.sh" ]; then
    echo "$BN: Error: can NOT find myself... zipping failed..."
    echo ""
    exit 1
fi

if [ -f "CMakeCache.txt" ]; then
    echo "$BN: Error: found file CMakeCache.txt!"
    echo "$BN: Need to clean build components before zipping..."
    echo ""
    exit 1
fi

if [ -d "CMakeFiles" ]; then
    echo "$BN: Error: found direcotry CMakeFiles!"
    echo "$BN: Need to clean build components before zipping..."
    echo ""
    exit 1
fi

cd ../..
if [ -f "$TMPZIP" ]; then
    TMPOPT="-u $TMPOPT"
fi

zip $TMPOPT $TMPZIP ask/*

# eof

