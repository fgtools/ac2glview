file : README.OFF.txt - 20130630

Description of OFF file format -

/* --------------------------------------------------------------------------
    Object File Format (.off)
    Object File Format (.off) files are used to represent the geometry of a model by specifying the 
    polygons of the model's surface. The polygons can have any number of vertices.
    The .off files in the Princeton Shape Benchmark conform to the following standard. OFF files 
    are all ASCII files beginning with the keyword OFF. The next line states the number of vertices, 
    the number of faces, and the number of edges. The number of edges can be safely ignored.
    The vertices are listed with x, y, z coordinates, written one per line. After the list of 
    vertices, the faces are listed, with one face per line. For each face, the number of 
    vertices is specified, followed by indices into the list of vertices. See the examples below.
    Note that earlier versions of the model files had faces with -1 indices into the vertex list. 
    That was due to an error in the conversion program and should be corrected now.
    OFF numVertices numFaces numEdges
    x y z
    x y z
    ... numVertices like above
    NVertices v1 v2 v3 ... vN
    MVertices v1 v2 v3 ... vM
    ... numFaces like above
    Note that vertices are numbered starting at 0 (not starting at 1), and that numEdges will always be zero.
    Comment lines may be inserted anywhere, and are denoted by the character # appearing in column 1. Blank lines are also acceptable.
    ---------------------------------------------------------------------------- */

Search: 'OFF file viewers'

1: http://mview.sourceforge.net/

Downloaded 0.3.3, add CMakeLists.txt, and compiled.
- Made the OFF file reading more robust (I hope!)

Led to : http://www.geomview.org/
And to : https://github.com/geomview/geomview
https://github.com/geomview/geomview.git
Last commit : Jorge Barros de Abreu authored 4 months ago




2: http://www.holmes3d.net/graphics/roffview/

Led to : http://www.holmes3d.net/graphics/meshman/
And to : http://www.holmes3d.net/graphics/roffview/tools/

But the ONLY source found was for roffview (I think!)


