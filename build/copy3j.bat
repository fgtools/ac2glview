@setlocal
@if "%~1x" == "x" goto HELP
@set TMPFIL1=%1.js
@set TMPSRC=..\data\%TMPFIL1%
@if NOT EXIST %TMPSRC% goto NOSRC
@set TMPDST=C:\OSGeo4W\apache\htdocs\fgx-globe\cookbook\load-plane-3d\data
@if NOT EXIST %TMPDST%\nul  goto NODST
@set TMPFIL2=%TMPDST%\%TMPFIL1%
@if NOT EXIST %TMPFIL2% goto DOCOPY
@call dirmin %TMPFIL2%
@call dirmin %TMPSRC%
@echo Overwrite 1st with 2nd?
@echo *** CONTINUE? ***
@pause

:DOCOPY

copy %TMPSRC% %TMPFIL2%

@goto END

:NOSRC
@echo Can NOT locate %TMPSRC%
@goto END

:NODST
@echo ERROR: Destination %TMPDST% does NOT exist!
@goto END

:HELP
@echo Give base file name to copy from ..\data...
@goto END

:END
