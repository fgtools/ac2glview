build/README.txt - 20130729

In Windows should be able to use build-me.bat to build this 
ac2glview project, however you may want to change the install 
location if you intend to install it.

However, in essence the cmake build process is -

> cmake ..	# do the configuration and generation step
> cmake --build . --config Release	# to build the project

Most of the other batch files here ae highly specific to my personal 
tool box, and may not work for you. However they may provide some indication 
of what can be done. ac2gl_browser2 program supports a scripting interface.


Enjoy.

Geoff.
20130729

# eof

