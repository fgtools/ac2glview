@setlocal
@set TMPSRC=Release
@set TMPFIL1=ac2glview.exe
@set TMPFIL2=ac3d_browser2.exe
@set TMPDST=C:\MDOS

@set TMPEXE1=%TMPSRC%\%TMPFIL1%
@set TMPEXE2=%TMPSRC%\%TMPFIL2%
@set TMPDST1=%TMPDST%\%TMPFIL1%
@set TMPDST2=%TMPDST%\%TMPFIL2%


@if NOT EXIST %TMPEXE1% goto DNONE
@if NOT EXIST %TMPDST1% goto DOCOPY1
@echo WARNING: Replace
@call dirmin %TMPDST1%
@echo *** WITH ***
@call dirmin %TMPEXE1%
@fc4 %TMPEXE1% %TMPDST1% -q -v0 -b >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are EXACTLY the SAME
)
@ask *** CONTINUE? *** Only y continues. All others abort...
@if ERRORLEVEL 1 goto DOCOPY1
@echo Skipping copy...
@goto DNCOPY1

:DOCOPY1
copy %TMPEXE1% %TMPDST1%
@goto DNCOPY1
:DNONE
@echo.
@echo WARNING: Can NOT find the SOURCE %TMPEXE1%
@echo.
@REM pause
:DNCOPY1

@if NOT EXIST %TMPEXE2% goto DNTWO

@if NOT EXIST %TMPDST2% goto DOCOPY2
@echo WARNING: Replace
@call dirmin %TMPDST2%
@echo *** WITH ***
@call dirmin %TMPEXE2%
@fc4 %TMPEXE2% %TMPDST2% -q -v0 -b >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are EXACTLY the SAME
)
@ask *** CONTINUE? *** Only y continues. All others abort...
@if ERRORLEVEL 1 goto DOCOPY2
@echo Skipping copy...
@goto DNCOPY2

:DOCOPY2
copy %TMPEXE2% %TMPDST2%
@goto DNCOPY2

:DNTWO
@echo WARNING: Can NOT find the SOURCE %TMPEXE2%
@pause
:DNCOPY2
