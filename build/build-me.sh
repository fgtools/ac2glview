#!/bin/sh
#< build-me.sh - ac2glview project - 20130729
BN=`basename $0`

TMPLOG="bldlog-1.txt"

echo "$BN: Building the ac2glview project..."
echo "$BN: A good option to add is -DCMAKE_VERBOSE_MAKEFILE=TRUE"
echo "$BN: To add debug symbols for gdb use -DCMAKE_BUILD_TYPE=Debug and -g is added."
echo ""

# define this if FLU is installed in a non-standard path, like
TMPOPTS="-DFLU_ROOT_DIR:PATH=$HOME/projects/fltk/install/fltk"
for arg in $@; do
   TMPOPTS="$TMPOPTS $arg"
done

if [ -f "$TMPLOG" ]; then
    rm -f -v $TMPLOG
fi
echo "$BN: Doing 'cmake .. $TMPOPTS'... all output to $TMPLOG..."
echo "$BN: Doing 'cmake .. $TMPOPTS'... " > $TMPLOG

cmake .. $TMPOPTS >> $TMPLOG 2>&1
if [ ! "$?" = "0" ]; then
    echo "$BN: Error during cmake configuration and generation.. see $TMPLOG"
    exit 1
fi

echo "$BN: Doing 'make'"
make >> $TMPLOG 2>&1
if [ ! "$?" = "0" ]; then
    grep error $TMPLOG
    echo "$BN: Error during make... see $TMPLOG"
    exit 1
fi

grep warning $TMPLOG
echo "$BN: Appears a successful build... maybe install next? if desired."

# EOF

