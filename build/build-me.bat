@setlocal
@set TMPBGN=%TIME%
@set TMPRT=C:\FG\18
@set TMPVER=1
@set TMPPRJ=ac2glview
@set TMPSRC=%TMPRT%\%TMPPRJ%
@set TMPBGN=%TIME%
@set TMPINS=%TMPRT%\3rdParty
@set TMPLOG=bldlog-1.txt

@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=C:\FG\18\3rdParty
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo Appears OK... see %TMPLOG%... maybe time to run instexe.bat to copy to C:\MDOS?

@goto END

@echo Building installation zips... moment...
@call build-zips Debug
@call build-zips Release
@echo Done installation zips...

@call elapsed %TMPBGN%

@echo Continue with install? Only Ctrl+c aborts...

@pause

cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_dbg.txt >nul
)

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_rel.txt >nul
)

@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%


@goto END

:ERR1
@echo cmake configuration or generations ERROR
@goto END1

:ERR2
@echo cmake build Debug ERROR
@goto END1

:ERR3
@echo cmake build Release ERROR
@goto END1


:END1
@echo See %TMPLOG% for details...
:END

@REM eof
