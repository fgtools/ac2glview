README.tris.txt - 20130828

After a LONG search seem to have found how to 'triangluate' 
a set of vectors when the count is greater than 3.

In PLIB ssgLoadAC.cxx there are some comments that indicate 
the the FIRST index (into the vector array) is used as the 
base for EVERY triange. I had seen other code that seemed to 
do this, but this time found the telling comment -

double texrep[2];
double texoff[2];
int current_flags;
First some setup -
void init_SURF() {
  sgSetVec2 ( texrep, 1.0f, 1.0f ) ;
  sgSetVec2 ( texoff, 0.0f, 0.0f ) ;
}
In ac2glview this is -
  ob->texture_repeat_x = ob->texture_repeat_y = 1.0; /* init new_object2 */
  ob->texture_offset_x = ob->texture_offset_y = 0.0; /* init new_object2 */

static int do_texrep ( char *s ) {  // "texrep"
  if ( sscanf ( s, "%f %f", &texrep[0], &texrep[1] ) != 2 )
    ulSetError ( UL_WARNING, "ac_to_gl: Illegal texrep record." ) ;
  return PARSE_CONT ;
}
In ac2glview this is -
	ob->texture_repeat_x = (float)atof(tokv2[1]);
	ob->texture_repeat_y = (float)atof(tokv2[2]);

static int do_texoff ( char *s ) { // "texoff"
  if ( sscanf ( s, "%f %f", &texoff[0], &texoff[1] ) != 2 )
    ulSetError ( UL_WARNING, "ac_to_gl: Illegal texoff record." ) ;
  return PARSE_CONT ;
}
In ac2glview this is
        ob->texture_offset_x = (float)atof(tokv2[1]);
	ob->texture_offset_y = (float)atof(tokv2[2]);

static int do_surf     ( char *s ) { // "SURF"
  current_flags = strtol ( s, NULL, 0 ) ;
  ...
}
In ac2glview this is -
	flgs = strtol(tokv2[1], NULL, 0);
	s->flags = flgs;

Then getting the indexes...

int type = ( current_flags & 0x0F ) ;
if (type == 0) {
  int first_vertind = -1;
  int prev_vertind = 0;
  for (i = 0; i < num_verts; i++) {
    // fetch the line into buffer
    if ( sscanf ( buffer, "%d %f %f", &vertind,&texcoord[0],&texcoord[1] ) != 3 )
            ulSetError ( UL_FATAL, "ac_to_gl: Illegal ref record." ) ; // DIE NOW
        
    texcoord[0] *= texrep[0] ;
    texcoord[1] *= texrep[1] ;
    texcoord[0] += texoff[0] ;
    texcoord[1] += texoff[1] ;

    // Store the first index texcoord pair.
    // This one is referenced for every triangle.
    if ( first_vertind < 0 ) {
        first_vertind = vertind ;
        sgCopyVec2( first_texcoord, texcoord );
    }

    if ( 2 <= i) {
        // Store the edges of the triangle
        add_textured_vertex_edge ( first_vertind, first_texcoord ) ;
        add_textured_vertex_edge ( prev_vertind, prev_texcoord ) ;
        add_textured_vertex_edge ( vertind, texcoord ) ;
          
        // Store the material and flags for this surface. 
        current_matindexarray -> add ( current_materialind );
        current_flagsarray -> add ( current_flags );
    }

    // Copy current -> previous
    prev_vertind = vertind ;
  }
}

That's it folks ;=)) A way, a method to convert every set of 
indexes to a set of TRIANGLES.

The first index is referenced in EVERY triangle, made up of
triangle(  first_vertind, prev_vertind, vertind );

# eof

