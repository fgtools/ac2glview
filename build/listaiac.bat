@setlocal
@set TMPLIST=tempailist.txt
@set TMPDIR=F:\fgdata\AI\Aircraft
@if NOT EXIST %TMPDIR%\nul goto NODIR
@Echo List all *.ac files to %TMPLIST%
@set TMPCNT=0
@echo On %DATE% %TIME% >%TMPLIST%

@echo Processing %TMPDIR%
@for /D %%i in (%TMPDIR%\*.*) do @(call :CHKIT %%i)

@echo Processed folder %TMPDIR%...
@echo Listed %TMPCNT% aircraft .ac files to %TMPLIST%...

@goto END

:CHKIT
@if "%~1x" == "x" goto :EOF
@set TMPDIR2=%1\Models
@echo %1
@for %%i in (%TMPDIR2%\*.ac) do @(call :CHKAC %%i)
@goto :EOF

:CHKAC
@if "%~1x" == "x" goto :EOF
@echo %1
@set TMPFIL=%~n1
@echo %1 >> %TMPLIST%
@set /A TMPCNT+=1
@goto :EOF

:NOEXE
@echo ERROR: Can NOT find %TMPEXE%
@goto END

:NODIR
@echo ERROR: Can NOT find %TMPDIR%
@goto END

:END
