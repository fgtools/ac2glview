@setlocal
@set TMPEXE=Debug\ac3d_browser2d.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@set TMPDIR=C:\FG\fgdata\Aircraft
@if NOT EXIST %TMPDIR%\nul goto NODIR

@echo Processing %TMPDIR%
@for /D %%i in (%TMPDIR%\*.*) do @(call :CHKIT %%i)

@goto END

:CHKIT
@if "%~1x" == "x" goto :EOF
@set TMPDIR2=%1\Models
@echo %1
@for %%i in (%TMPDIR2%\*.ac) do @(call :CHKAC %%i)
@goto :EOF

:CHKAC
@if "%~1x" == "x" goto :EOF
@echo %1
@set TMPFIL=%~n1
@set TMPFILJS=..\data\%TMPFIL%.js
@if EXIST %TMPFILJS% (
@call dirmin %TMPFILJS%
)
@ask Process this file [%TMPFIL%]?
@if ERRORLEVEL 1 goto DOFILE
@goto :EOF

:DOFILE
@echo LOAD %1 > temps.txt
@echo EXPORT %TMPFILJS% >>temps.txt
%TMPEXE% -s temps.txt
@set TMPFIL2=..\data\%TMPFIL%.txt
@if NOT EXIST temp%TMPFIL%.txt goto DNFIL
copy temp%TMPFIL%.txt %TMPFIL2%
:DNFIL
@pause
@goto :EOF

:NOEXE
@echo ERROR: Can NOT find %TMPEXE%
@goto END

:NODIR
@echo ERROR: Can NOT find %TMPDIR%
@goto END

:END
