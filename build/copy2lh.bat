@setlocal
@set TMPDST1=C:\OSGeo4W\apache\htdocs\Three.js\ac
@set TMPDST2=C:\OSGeo4W\apache\htdocs\fgx-globe\cookbook\load-plane-3d\data2
@set TMPFIL=%1
@set TMPEXT=%~x1
@set TMPNAM=%~n1

@if "%TMPFIL%x" == "x" goto HELP
@if NOT EXIST %TMPFIL% goto NOFIL
@if NOT EXIST %TMPDST1%\nul goto NODST1
@if NOT EXIST %TMPDST2%\nul goto NODST2
@set TMPFIL1=%TMPDST1%\%TMPFIL%
@set TMPFIL2=%TMPDST2%\%TMPFIL%

@echo Working with file %TMPNAM%, with EXTENT %TMPEXT%

@REM Copy the JS file
@REM ================

@if NOT EXIST %TMPFIL1% goto DOCOPY1
@echo WARNING: Replace
@call dirmin %TMPFIL1%
@echo with
@call dirmin %TMPFIL%
@fc4 %TMPFIL1% %TMPFIL% -q -v0 >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are EXACTLY the SAME
)
@ask *** CONTINUE? ***
@if ERRORLEVEL 1 goto DOCOPY1
@echo NO copy being done...
@goto DNCOPY1
:DOCOPY1
copy %TMPFIL% %TMPDST1%
:DNCOPY1

@if NOT EXIST %TMPFIL2% goto DOCOPY2
@echo WARNING: Replace
@call dirmin %TMPFIL2%
@echo with
@call dirmin %TMPFIL%
@fc4 %TMPFIL2% %TMPFIL% -q -v0 >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are EXACTLY the SAME
)
@ask *** CONTINUE? ***
@if ERRORLEVEL 1 goto DOCOPY2
@echo NO copy being done...
@goto DNCOPY2
:DOCOPY2
copy %TMPFIL% %TMPDST2%
:DNCOPY2

@if NOT "%TMPEXT%x" == ".jsx" goto END

@REM Copy the AC file
@REM ================

@echo Checking if an ac file of the same name %TMPNAM% exists...
@set TMPFIL=%TMPNAM%.ac
@set TMPFIL1=%TMPDST1%\%TMPFIL%
@set TMPFIL2=%TMPDST2%\%TMPFIL%

@if EXIST %TMPFIL% goto CHKCOPY
@call dirmin %TMPNAM%*
@echo Unable to locate file %TMPFIL% so not ac copy done...
@goto END

:CHKCOPY
@if NOT EXIST %TMPFIL1% goto DOCOPY3
@echo WARNING: Replace
@call dirmin %TMPFIL1%
@echo with
@call dirmin %TMPFIL%
@fc4 %TMPFIL1% %TMPFIL% -q -v0 -b >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are EXACTLY the SAME
)
@ask *** CONTINUE? ***
@if ERRORLEVEL 1 goto DOCOPY3
@echo NO copy being done...
@goto DNCOPY3
:DOCOPY3
copy %TMPFIL% %TMPDST1%
:DNCOPY3


@if NOT EXIST %TMPFIL2% goto DOCOPY4
@echo WARNING: Replace
@call dirmin %TMPFIL2%
@echo with
@call dirmin %TMPFIL%
@fc4 %TMPFIL2% %TMPFIL% -q -v0 -b >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are EXACTLY the SAME
)
@ask *** CONTINUE? ***
@if ERRORLEVEL 1 goto DOCOPY4
@echo NO copy being done...
@goto DNCOPY4
:DOCOPY4
copy %TMPFIL% %TMPDST2%
:DNCOPY4

@REM Copy the SCRIPT file
@REM ====================

@set TMPFIL=%TMPNAM%.script
@set TMPFIL1=%TMPDST1%\%TMPFIL%
@set TMPFIL2=%TMPDST2%\%TMPFIL%

@if EXIST %TMPFIL% goto CHKCOPY2
@call dirmin %TMPNAM%*
@echo Unable to locate file %TMPFIL% so not ac copy done...
@goto END

:CHKCOPY2
@if NOT EXIST %TMPFIL1% goto DOCOPY5
@echo WARNING: Replace
@call dirmin %TMPFIL1%
@echo with
@call dirmin %TMPFIL%
@fc4 %TMPFIL1% %TMPFIL% -q -v0 >nul
@if ERRORLEVEL 1 (
@echo *** Files are DIFFERENT! ***
) else (
@echo *** Files are EXACTLY THE SAME ***
)
@ask *** CONTINUE? ***
@if ERRORLEVEL 1 goto DOCOPY5
@echo NO copy being done...
@goto DNCOPY5
:DOCOPY5
copy %TMPFIL% %TMPDST1%
:DNCOPY5


@if NOT EXIST %TMPFIL2% goto DOCOPY6
@echo WARNING: Replace
@call dirmin %TMPFIL2%
@echo with
@call dirmin %TMPFIL%
@fc4 %TMPFIL2% %TMPFIL% -q -v0 >nul
@if ERRORLEVEL 1 (
@echo *** Files are DIFFERENT! ***
) else (
@echo *** Files are EXACTLY THE SAME ***
)
@ask *** CONTINUE? ***
@if ERRORLEVEL 1 goto DOCOPY6
@echo NO copy being done...
@goto DNCOPY6
:DOCOPY6
copy %TMPFIL% %TMPDST2%
:DNCOPY6
@REM ================================================================================


@goto END

:NOFIL
@echo Error: Caan NOT locate %TMPFIL%! Check name, location...
:HELP
@echo Give valid file name to copy to -
@echo 1: %TMPDST1%
@echo 2: %TMPDST2%
@goto END

:NODST1
@echo Error: Destination 1 %TMPDST1% does NOT exist!
@goto END

:NODST2
@echo Error: Destination 2 %TMPDST2% does NOT exist!
@goto END

:END

