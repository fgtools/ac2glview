@setlocal
@set TMPEXE=Debug\ac3d_browser2d.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@set TMPIN=usage2.txt
@set TMPODIR=..\data
@set TMPIDIR=data
@set TMPSCRIP=temps2.txt
@if NOT EXIST %TMPIN% goto NOFIL
@if NOT EXIST %TMPODIR%\nul got o NODIR

@for /F "tokens=1,*" %%i in (%TMPIN%) do @(call :DOIT %%i %%j)

@goto END

:DOIT
@echo %1 %2
@set TMPFIL=%1
@set TMPOUT=..\data\%1.js
@set TMPFIL2=..\data\%1.txt
@set TMPFIL3=..\data\%1.ac
@set TMPPREV=%TMPIDIR%\%1.txt
@set TMPSRC=%2
@if NOT EXIST %TMPSRC% goto NOSRC
@if NOT EXIST %TMPOUT% goto DOIT2
@call dirmin %TMPOUT%
@echo File EXISTS! *** OVERWRITE? ***
@ask Need 'y' to continue :
@if ERRORLEVEL 1 goto DOIT2
@goto :EOF

:DOIT2
@echo # action %TMPSRC% > %TMPSCRIP%
@if EXIST %TMPPREV% (
@echo Adding previous script %TMPPREV%
@type %TMPPREV% >> %TMPSCRIP%
)
@echo LOAD %TMPSRC% >> %TMPSCRIP%
@echo WRITE %TMPFIL3% >> %TMPSCRIP%
@echo EXPORT %TMPOUT% >> %TMPSCRIP%
@echo DELAY 0 >> %TMPSCRIP%

%TMPEXE% -s %TMPSCRIP%

@if NOT EXIST temp%TMPFIL%.txt goto DNFIL
copy temp%TMPFIL%.txt %TMPFIL2%
:DNFIL

@pause
@goto :EOF

:NOSRC
@echo Can NOT locate source %TMPSRC%
@pause
@goto NOSRC

:NOFIL
@echo Can NOT find %TMPIN% file!
@goto END

:NOEXE
@echo Can NOT find %TMPEXE% file!
@goto END

:NODIR
@echo Can NOT find %TMPDIR% folder!
@goto END

:END
