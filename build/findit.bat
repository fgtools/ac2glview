@setlocal
@fa4 -? >nul 2>&1
@if ERRORLEVEL 1 goto NOFA4
@if "%~1x" == "x" goto HELP
@set TMPCMD="%1" -R -X:build -X:::
@shift
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

cd ..
fa4 %TMPCMD% *

@goto END

:NOFA4
@echo Can NOT find 'FA4.exe" in PATH!
@goto END

:HELP
@echo Enter the word or phrase to find, plus options for FA4
@fa4 -?
@echo Options here already include -X:build and -X::: to exclude these from the search...
@got END

:END
