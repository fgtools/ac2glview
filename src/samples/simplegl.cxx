// simple OpenGL / FlTk program
// written 10/16/99, Michael L Gleicher
   
#include <FL/Fl_Gl_Window.H> // 20130731 - correct case for unix compile
#include <Fl/Fl.H>
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
   
// make a Gl window that draws something:
class MyGlWindow : public Fl_Gl_Window {
public:
	MyGlWindow(int x, int y, int w, int h) :
	  Fl_Gl_Window(x,y,w,h,"My GL Window")
	  {
	  }
private:
	void draw() {	// the draw method must be private
		glClearColor(0,0,0,0);		// clear the window to black
		glClear(GL_COLOR_BUFFER_BIT);	// clear the window
   
		glColor3f(1,1,0);			// draw in yellow
		glRectf(-.5,-.5,.5,.5);		// draw a filled rectangle
	};
};
   
// the main routine makes the window, and then runs an even loop
// until the window is closed
int main(int argc, char **argv)
{
	MyGlWindow* gl = new MyGlWindow(100,100,500,500);

	gl->show();	// this actually opens the window
   
	Fl::run();

	delete gl;
   
	return 0;
}

