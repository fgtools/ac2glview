// simple OpenGL / FlTk program
// program simple2.cpp
// 20130731 - sub-class the main window in order to get mouse events in order to create a context menu
// written 10/16/99, Michael L Gleicher
   
#include <Fl/Fl_Gl_Window.H> // 20130731 - correct case
#include <Fl/Fl.H>
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <Fl/Fl_Double_Window.H>
#include <Fl/Fl_Button.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Input_Choice.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <Fl/Fl_Preferences.H>

#include <stdio.h> // for printf() in unix
#include <stdlib.h> // for exit() in unix
#include <time.h>


// main window preferences, and last file
static const char *vendor = "geoffair";
static const char *mod_name = "simple2";

static const char *szwin_height = "win_height";
static const char *szwin_width = "win_width";
static const char *szwin_posx = "win_posx";
static const char *szwin_posy = "win_posy";
static Fl_Preferences *prefs = 0; 

// make a Gl window that draws something:
class MyGlWindow : public Fl_Gl_Window {
public:
	float r,g,b;
	MyGlWindow(int x, int y, int w, int h) :
	  Fl_Gl_Window(x,y,w,h,"My GL Window")
	  {
		  r = g = 1;
		  b = 0;
	  }
private:
	void draw() {	// the draw method must be private
		glClearColor(0,0,0,0);		// clear the window to black
		glClear(GL_COLOR_BUFFER_BIT);	// clear the window
   
		glColor3f(r,g,b);			// draw in yellow
        // void glRectf( GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2 );
		glRectf(-0.5,-0.5,0.5,0.5);		// draw a filled rectangle
	};
};

// 20130731 - sub-class the main window to have a 'handle' to receive mouse events
// establish a re-size callback to save sizes to prefs
typedef void (*RESIZE) (int x, int y, int w, int h);

// sub class
class main_Win : public Fl_Double_Window {
public:
    main_Win( int x, int y, int w, int h, const char *title );
    int handle(int e);
    void set_resizecb( RESIZE rscb )  { resizecb  = rscb; }

//protected:
        RESIZE resizecb;
        virtual void resize(int x, int y, int w, int h) {
                Fl_Double_Window::resize(x, y, w, h);
                if (resizecb) resizecb(x,y,w,h);
        }

};

main_Win::main_Win( int x, int y, int w, int h, const char *title ) :
Fl_Double_Window( x, y, w, h, title ) 
{
    // any intialisation
    resizecb = 0;   // clear this...
}

static void CreatePopup( int x, int y );

// keyboard (and mouse) callbacks
// ******************************
int main_Win::handle(int e)
{
    int res = 0;
    int x, y;
    switch(e)
    {
    case FL_PUSH:
        x = Fl::event_x();
        y = Fl::event_y();
        if ( Fl::event_button() == FL_RIGHT_MOUSE ) {
            // time for a context menu
            CreatePopup( x, y );
            res = 1;
        } else {
            res = Fl_Window::handle(e);
        }
        break;
    default:
        res = Fl_Window::handle(e);
        break;
    }
    return res;
}

//static Fl_Double_Window* wind;
static main_Win *wind;
static MyGlWindow *gl;
static Fl_Button *bt;
static Fl_Button *bt2;
static Fl_Input_Choice *pin;

static void quitCB(Fl_Widget*, void*) 
{
    printf("quitCB called\n");
    exit(0);
}
static void testCB(Fl_Widget*, void*) 
{
    printf("testCB called\n");
}
static void subCB(Fl_Widget*, void *vp) 
{
    printf("subCB called %p\n", vp);
}

static void CreatePopup( int x, int y )
{
    // Dynamically create menu, pop it up
    Fl_Menu_Button menu(x, y, 80, 1);
    menu.add("Test",       0, testCB, (void*)&menu);
    // now how to create a sub-menu
    menu.add("Sub-menu", 0, 0, 0, FL_SUBMENU);
    // and how to add items to this sub-menu?
    menu.add("Sub-menu/one", 0, subCB, (void *)1);
    menu.add("Sub-menu/two", 0, subCB, (void *)2);
    menu.add("Sub-menu/three", 0, subCB, (void *)3);

    menu.add("Quit",       0, quitCB, (void*)&menu);
    menu.popup();

}
   
#define USE_RAND_COLOR
#define drand48() (((float) rand())/((float) RAND_MAX))
#define srand48(x) (srand((x)))

// a simple callback to do something
void changeColor(Fl_Widget* /*button*/, MyGlWindow* myWind)
{
#ifdef USE_RAND_COLOR
    static bool dn_rand_init = false;
    if (!dn_rand_init) {
        srand48(time(0));
        dn_rand_init = true;
    }
    myWind->r = drand48();
    myWind->g = drand48();
    myWind->b = drand48();
    printf("Set color r=%f g=%f b=%f\n", myWind->r, myWind->g, myWind->b);
#else
	myWind->r = 1 - myWind->r;
	myWind->g = 1 - myWind->g;
	myWind->b = 1 - myWind->b;
#endif
	myWind->damage(1);
}

void testbuttonCB(Fl_Widget* /*button*/, void * /*data*/)
{
    printf("testbuttonCB called\n");
    // screen size
    int scw = Fl::w();
    int sch = Fl::h();
    // get position and size of the main window
    int x = wind->x();
    int y = wind->y();
    int w = wind->w();
    int h = wind->h();

    printf("Main x,y,w,h %d,%d,%d,%d on %dx%d\n", x, y, w, h, scw, sch );

    // position sub-window centrally over the main window
    int m = 20;
    int xp = 20;
    int yp = 40;
    int wx = 300;
    int wh = 180;
    int bw = wx - (2 * m);
    int bh = wh - (2 * m);

    //xp = x + m;
    //yp = y + m;
    xp = x + ((w - wx) / 2);
    if (xp < 0)
        xp = 0;
    else if ((xp + wx) > scw)
        xp = scw - wx;
    yp = y + ((h - wh) / 2);
    if (yp < 0)
        yp = 0;
    else if ((yp + wh) > sch)
        yp = sch - wh;

    printf("Sub  x,y,w,h %d,%d,%d,%d on %dx%d\n", xp, yp, wx, wh, scw, sch );
    //Fl_Window *window = new Fl_Window(300, 180, "window");
    Fl_Window *window = new Fl_Window(xp, yp, wx, wh, "window");

    window->begin();
        // add a box with a label
        //Fl_Box *box = new Fl_Box(FL_UP_BOX,20,40,260,100,"Hello World");
        Fl_Box *box = new Fl_Box(FL_UP_BOX,m,m,bw,bh,"Hello World");
        box->labelfont(FL_BOLD+FL_ITALIC);
        box->labelsize(36);
        box->labeltype(FL_SHADOW_LABEL);
    window->end();

    window->show();

    while (window->shown())
        Fl::wait();

}

void input_choice_cb(Fl_Widget* /*button*/, void *data)
{
    Fl_Input_Choice *pin = (Fl_Input_Choice *)data;
    printf("Input Value='%s'\n", pin->value());
}

static int wind_x = 100;
static int wind_y = 100;
static int wind_w = 400;
static int wind_h = 300;

void resizeCB( int x, int y, int w, int h )
{
    printf("main resizes %d %d %d %d\n", x, y, w, h );
    if (prefs) {
        int res, cnt;
        cnt = 0;
        res = 0;
        if ((x != wind_x) || (y != wind_y) || (w != wind_w) || (h != wind_h)) 
        {
            if (wind_x != x) {
                wind_x = x;
                res += prefs->set(szwin_posx, wind_x);
                cnt++;
            }
            if (wind_y != y) {
                wind_y = y;
                res += prefs->set(szwin_posy, wind_y);
                cnt++;
            }
            if (wind_w != w) {
                wind_w = w;
                res += prefs->set(szwin_width, wind_w);
                cnt++;
            }
            if (wind_h != h) {
                wind_h = h;
                res += prefs->set(szwin_height, wind_h);
                cnt++;
            }
        }
        if (cnt || res) {
            prefs->flush(); // commit to disk
        }
    }
}

void init_preferences()
{
    prefs = new Fl_Preferences( Fl_Preferences::USER, vendor, mod_name ); 
}

void load_preferences()
{
    prefs->get(szwin_height, wind_h, wind_h);
    prefs->get(szwin_width, wind_w, wind_w);
    prefs->get(szwin_posx, wind_x, wind_x);
    prefs->get(szwin_posy, wind_y, wind_y);
}

bool add_post_resize = true;
// the main routine makes the window, and then runs an even loop
// until the window is closed
int main(int argc, char **argv)
{
    // SOME VERY UGLY EXPERIMENTS IN RE-SIZING AND LOCATING WIDGETS
    // maybe the better idea is to always start window at fixed known size
    // then load preferences and ask fltk to resize everything accordingly
    // OK, doing post loading of preference and post resizing is OK, but FAR from perfect ;=((
    // but for sure it is better than trying to do all the math myself ;=))
    // =======================================================================================
    init_preferences();

    if (!add_post_resize)
        load_preferences();

	//Fl_Double_Window* wind = new Fl_Double_Window(100,100,400,300,"GL Sample2");
	//wind = new main_Win(100,100,400,300,"GL Sample2");
	wind = new main_Win(wind_x,wind_y,wind_w,wind_h,"GL Sample2");

    // gl window - in left 3/4 pane
    int gl_x = 10;
    int gl_y = 10;
    int gl_w = 280;
    int gl_h = 280;
    int bt_x = 300;
    int bt_y = 10;
    int bt_w = 70;
    int bt_h = 25;
    int bt2_x = 300;
    int bt2_y = 45;
    int bt2_w = 70;
    int bt2_h = 25;
    int in_x = 310;
    int in_y = 90;
    int in_w = 80;
    int in_h = 25;
    if (!add_post_resize) {
        // try to do the math myself
        int left_x = (wind_w * 3) / 4;
        int left_y = (wind_h * 3) / 4;
        int right_x = wind_w - left_x;
        //int right_y = wind_h - left_y;

        gl_x = (left_x - gl_w) / 2;
        gl_y = (left_y - gl_h) / 2;
        //bt_x = left_x + 10;
        bt_x = left_x + ((right_x - bt_w) / 2); // center on right pane
        bt2_x = left_x + ((right_x - bt2_w) / 2); // center on right pane
        in_x = left_x + ((right_x - in_w) / 2); // center on right pane
    }

	wind->begin();		// put widgets inside of the window
		//gl = new MyGlWindow(10,10,280,280);
		gl = new MyGlWindow(gl_x,gl_y,gl_w,gl_h);
		//bt = new Fl_Button(300,10,70,25,"Color");
		bt = new Fl_Button(bt_x,bt_y,bt_w,bt_h,"Color");
		bt->callback((Fl_Callback*) changeColor, gl);
		//bt2 = new Fl_Button(300,45,70,25,"Test");
		bt2 = new Fl_Button(bt2_x,bt2_y,bt2_w,bt2_h,"Test");
		bt2->callback(testbuttonCB, 0);
        //pin = new Fl_Input_Choice(310,90,80,25,"In");
        pin = new Fl_Input_Choice(in_x,in_y,in_w,in_h,"In");
        pin->callback(input_choice_cb, (void*)pin);
        pin->add("one");
        pin->add("two");
        pin->add("three");
        pin->value(1);
	wind->end();

    wind->resizable(wind); // allow re-sizing to see what that does
    if (add_post_resize) {
        load_preferences();
        wind->resize(wind_x,wind_h,wind_w,wind_h);
    }
    wind->set_resizecb(resizeCB);

	wind->show();	// this actually opens the window
   
	Fl::run();
	delete wind;
   
	return 0;
}

// eof

