// simple OpenGL / FlTk program
// program simple3.cpp
// this one takes simple2 and does animation
// written 10/16/99, Michael L Gleicher

#include <FL/Fl_Gl_Window.H>
#include <Fl/Fl.H>
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <Fl/Fl_Double_Window.H>
#include <Fl/Fl_Button.H>

#include "RunButton.h"

// make a Gl window that draws something:
class MyGlWindow : public Fl_Gl_Window {
public:
    Fl_Slider* time;			// what time is the animation at?
	MyGlWindow(int x, int y, int w, int h) :
	  Fl_Gl_Window(x,y,w,h,"My GL Window")
	  {
		  time = 0;
	  }
private:
	void draw() {	// the draw method must be private
		glMatrixMode(GL_PROJECTION);	// set up coord system
		glLoadIdentity();			// reset it
		glOrtho(-1,1,-1,1,-1,1);	// set it to be -1 to 1 in each axis
		glMatrixMode(GL_MODELVIEW);	// back to normal mode
		glPushMatrix();
			glLoadIdentity();			// clear what anyone else did
			if (time) {
				double t = time->value()/time->maximum();	// normalize time
				glRotated(t * 360.,0,0,1);
			}
			glClearColor(0,0,0,0);		// clear the window to black
			glClear(GL_COLOR_BUFFER_BIT);	// clear the window
			glColor3d(1,1,0);			// draw in yellow
			glRectd(.7,-.1,.9,.1);		// draw a filled rectangle
		glPopMatrix();
	};
};

static MyGlWindow* gl;
static RunSlider* rt;
static RunButton* rb;


// the main routine makes the window, and then runs an even loop
// until the window is closed
int main(int argc, char **argv)
{
	Fl_Double_Window* wind = new Fl_Double_Window(100,100,300,350,"GL Sample");

	wind->begin();		// put widgets inside of the window
		gl = new MyGlWindow(10,10,280,280);
		rt = new RunSlider(gl,100,100,300,190,40);
		gl->time = rt;
		rb = new RunButton(rt,10,300,80,40);
	wind->end();

	wind->show();	// this actually opens the window

	Fl::run();
	delete gl;

	return 0;
}


// eof


