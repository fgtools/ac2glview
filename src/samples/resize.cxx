// resize test - want to detect when the windows is RESIZED

#ifdef WIN32
#include <windows.h>
#endif
#include <stdio.h> // printf() in unix
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Radio_Button.H>
#include <FL/Fl_Round_Button.H>
#include <GL/gl.h>
#ifdef WIN32
#include <FL/x.H>               // needed for fl_display
#include <windows.h>            // needed for LoadIcon()
#endif /*WIN32*/

typedef void (*RESIZE) (int x, int y, int w, int h);

struct i_callback
{ // main program should be inherited: class main: public i_callback
  // and overwrite Resize():
        virtual void Resize(int x, int y, int w, int h)= 0;
};

class Fl_Resize_Window: public Fl_Double_Window
{ // Window with extended resize() method to replace Fl_Double_Window
public:
        Fl_Resize_Window(int w, int h, const char* l= 0):
                Fl_Double_Window(w, h, l)
        { mp_Callback = 0; resizecb = 0; }

        Fl_Resize_Window(int x, int y, int w, int h, const char* l= 0):
                Fl_Double_Window(x, y, w, h, l)
        { mp_Callback = 0; resizecb = 0; }

        virtual ~Fl_Resize_Window() {}
        
        inline void Callback(i_callback* pC)    { mp_Callback = pC; }
        void set_callback( RESIZE rscb )        { resizecb  = rscb; }
protected:
        i_callback*     mp_Callback;
        RESIZE resizecb;

        virtual void resize(int x, int y, int w, int h)
        {
                Fl_Double_Window::resize(x, y, w, h);
                if (mp_Callback) mp_Callback->Resize(x, y, w, h);
                if (resizecb) resizecb(x,y,w,h);
        }
};

static int pos_x = 0;
static int pos_y = 0;
static int win_w = 0;
static int win_h = 0;
void new_size(int x, int y, int w, int h)
{
    if ((x != pos_x) || (y != pos_y) || (w != win_w) || (h != win_h)) {
        printf("New size: x=%d, y=%d, w=%d, h=%d\n", x, y, w, h );
        pos_x = x;
        pos_y = y;
        win_w = w;
        win_h = h;
    }
}

void checkCB(Fl_Widget* w, void*) 
{
    Fl_Check_Button *cb = (Fl_Check_Button *)w;
    int val = cb->value();
    printf("CheckCB val = %d\n", val);
}

#define MX_RBUTTONS 4
static Fl_Group *group;
static const char *rbuts[MX_RBUTTONS] =
{ "one", "two", "three", "four" };
static Fl_Round_Button *roundButton[MX_RBUTTONS];
static int current_radio = 2;

void radioCB(Fl_Widget* w, void *vp) 
{
    Fl_Round_Button *cb = (Fl_Round_Button *)w;
    int val = cb->value();
    // int b = (int)vp;    // error in gcc - error: cast from ‘void*’ to ‘int’ loses precision [-fpermissive]
    // int b = reinterpret_cast<int>(vp); // likewise FAILED
    //int b = *static_cast<int*>(vp); // hmmm, this worked ;=)) in gcc
    int b = *((int*)(&vp)); // and so did this
    printf("CheckCB %d (v=%d)\n", b, val);
}

static int wind_x = 100;
static int wind_y = 100;
static int wind_w = 300;
static int wind_h = 350;

// the main routine makes the window, and then runs an even loop
// until the window is closed
int main(int argc, char **argv)
{
    int x, i;
	Fl_Resize_Window* wind = new Fl_Resize_Window(wind_x,wind_y,wind_w,wind_h,"Resize Sample");

	wind->begin();		// put widgets inside of the window
    {
        Fl_Check_Button *cb = new Fl_Check_Button(10,10,150,25,"Check Box");
        cb->callback( checkCB );
        // radio buttons example
        group = new Fl_Group(10, 35, 300, 25);
        {
            for ( i = 0; i < MX_RBUTTONS; i++) {
                x = (10 + (i*65));
                //                                       x  y   w   h   text
                Fl_Round_Button *b = new Fl_Round_Button(x, 45, 22, 20, rbuts[i]);
                b->type(FL_RADIO_BUTTON);
                b->down_box(FL_ROUND_DOWN_BOX);
                //b->callback(radioCB, (void *)i); // establish CB with its value - get gcc warning
                b->callback(radioCB, reinterpret_cast<void*>(i));   // hmmm, no warning
                if (i == current_radio)
                    b->set(); // same as b->value(1)
                roundButton[i] = b;
            }
        }
        group->end();
    }
	wind->end();
    // set range to make window resizeables
    //wind->size_range( 200, 250 );
    wind->resizable(wind);

#ifdef WIN32    // ADDING AN ICON
    // need to have an *.cio file, and an *.rc file in the link defining the ICON
    //wind->icon((char*)LoadIcon(fl_display, MAKEINTRESOURCE(101))); // <-- ADD THIS LINE
#else // if UNIX
    // Usually, just a png image 32x32 or 64x64, of the same name as the executable in the same directory is all that's needed...
    // if __APPLE__ On the Mac, application icons are part of the .app bundle for your app.
#endif

    wind->set_callback(new_size);

	wind->show();	// this actually opens the window

	i = Fl::run();  // run it...

	return i;
}


// eof
