/* ======================================================================================
 * misc_lib.cxx - just some stuff
 *
 * LICENSE:
 *
 * Copyright (C) 2013  Geoff R. McLane - http://geoffair.org/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ====================================================================================== */

#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#ifdef WIN32
#include <Windows.h>
#else
#include <sys/time.h> // gettimeofday()
#include <string.h> // strlen()
#include <stdarg.h> // va_start/va_end
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
#endif
#include <stdlib.h> // malloc()
#include <stdio.h>
#include <GL/gl.h>
#include <math.h>
#include <string>
#include "misc_lib.hxx"


const char *get_misc_lib_vers()
{
	return "1.0.0";
}

#ifdef WIN32
void gettimeofday( struct timeval *tv, void *dummy )
{
	LARGE_INTEGER perfCount;
	LARGE_INTEGER perfFreq;

	double freq, count, time;
	tv->tv_sec = 0;
	tv->tv_usec= 0;

	// Get the frequency
	if( !QueryPerformanceFrequency( &perfFreq ) )
		return;

	// Get the current count.
	if( !QueryPerformanceCounter( &perfCount ) )
		return;

	freq  = (double) perfFreq.LowPart;
	count = (double) perfCount.LowPart;
	freq += (double) perfFreq.HighPart  *  4294967296.0;
	count+= (double) perfCount.HighPart *  4294967296.0;

	time = count / freq;

	tv->tv_sec = (int) time;
	tv->tv_usec= (int) ((time - (double)tv->tv_sec) * 1000000.0);

}
#endif // WIN32

static struct timeval tv;
static int ts, tu;
static int ets, etu;

void timer_start()
{
    gettimeofday(&tv, NULL);
    ts = tv.tv_sec;
    tu = tv.tv_usec;
}

double timer_stop()
{
    double s, u;
    gettimeofday(&tv, NULL);
    ets = tv.tv_sec;
    etu = tv.tv_usec;
    s = (double) (ets - ts);
    u = (double) (etu - tu);
    return (double)(s + (u / 1.0E+6));
}


///////////////////////////////////////////////////////////////////////////////
#define USE_PERF_COUNTER
///////////////////////////////////////////////////////////////////////////////
#if (defined(WIN32) && defined(USE_PERF_COUNTER))
// QueryPerformanceFrequency( &frequency ) ;
// QueryPerformanceCounter(&timer->start) ;
double get_seconds()
{
    static double dfreq;
    static int done_freq = 0;
    static int got_perf_cnt = 0;
    double d;
    if (!done_freq) {
        LARGE_INTEGER frequency;
        if (QueryPerformanceFrequency( &frequency )) {
            got_perf_cnt = 1;
            dfreq = (double)frequency.QuadPart;
        }
        done_freq = 1;
    }
    if (got_perf_cnt) {
        LARGE_INTEGER counter;
        QueryPerformanceCounter (&counter);
        d = (double)counter.QuadPart / dfreq;
    }  else {
        DWORD dwd = GetTickCount(); // milliseconds that have elapsed since the system was started
        d = (double)dwd / 1000.0;
    }
    return d;
}

#else // !WIN32
double get_seconds()
{
    struct timeval tv;
    gettimeofday(&tv,0);
    double t1 = (double)(tv.tv_sec+((double)tv.tv_usec/1000000.0));
    return t1;
}
#endif // WIN32 y/n
///////////////////////////////////////////////////////////////////////////////


#ifdef _MSC_VER
#define M_IS_DIR _S_IFDIR
#else // !_MSC_VER
#define M_IS_DIR S_IFDIR
#endif

static 	struct stat _s_buf = {0};
int is_file_or_directory ( char * path )
{
    if (!path || (*path == 0))
        return MFT_NONE;
	if (stat(path,&_s_buf) == 0) {
		if (_s_buf.st_mode & M_IS_DIR)
			return MFT_DIR;
		else
			return MFT_FILE;
	}
	return MFT_NONE;
}
size_t get_last_stat_file_size() { return _s_buf.st_size; }


/* ****************************************************************************************
   WritePPM
   **************************************************************************************** */
/*
   Write the current view to a file
   The multiple fputc()s can be replaced with
      fwrite(image,width*height*3,1,fptr);
   If the memory pixel order is the same as the destination file format.
   from : http://paulbourke.net/miscellaneous/windowdump/

*/

int WritePPM(int width, int height, const char *name, int stereo)
{
    static int counter = 0; /* This supports animation sequences */
    int i,j;
    FILE *fptr;
    char fname[32];
    char fname2[32];
    unsigned char *image;
    int OpenFile;

    /* Allocate our buffer for the image */
    if ((image = (unsigned char *)malloc(3*width*height*sizeof(char))) == NULL) {
        fprintf(stderr,"Failed to allocate memory for image\n");
        return(FALSE);
    }

    glPixelStorei(GL_PACK_ALIGNMENT,1);

    if (name) {
        stereo = 0;
    } else {
        name = fname;
        /* Open the file */
        OpenFile = 1;
        while (OpenFile) {
           if (stereo)
              sprintf(fname,"L_%04d.ppm",counter);
           else
              sprintf(fname,"C_%04d.ppm",counter);
           if (is_file_or_directory(fname))
               counter++;   /* don't overwrite any existing file */
           else
               OpenFile = 0;
       }
    }

    if ((fptr = fopen(name,"w")) == NULL) {
        fprintf(stderr,"Failed to open file [%s] for window dump\n",name);
        free(image);
        return(FALSE);
    }

    /* Copy the image into our buffer */
    glReadBuffer(GL_BACK_LEFT);
    glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);

    /* Write the PPM file */
    fprintf(fptr,"P6\n%d %d\n255\n",width,height); /* for ppm */
    for ( j = height-1; j >= 0 ;j-- ) {
        for ( i = 0; i < width; i++ ) {
            fputc( image[3*j*width+3*i+0], fptr);
            fputc( image[3*j*width+3*i+1], fptr);
            fputc( image[3*j*width+3*i+2], fptr);
        }
    }
    fclose(fptr);

    if (stereo) {
        /* Open the file */
        sprintf(fname2,"R_%04d.ppm",counter);
        if ((fptr = fopen(fname2,"w")) == NULL) {
            fprintf(stderr,"Failed to open file for window dump\n");
            free(image);
            return(FALSE);
        }

        /* Copy the image into our buffer */
        glReadBuffer(GL_BACK_RIGHT);
        glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);

        /* Write the PPM file */
        fprintf(fptr,"P6\n%d %d\n255\n",width,height); /* for ppm */
        for ( j = height-1; j >= 0; j-- ) {
            for ( i = 0; i < width ; i++ ) {
                fputc( image[3*j*width+3*i+0], fptr);
                fputc( image[3*j*width+3*i+1], fptr);
                fputc( image[3*j*width+3*i+2], fptr);
            }
        }
        fclose(fptr);
    }

    /* Clean up */
    counter++;
    free(image);
    printf("PPM image %dx%d, written to %s\n", width, height, name);
    if (stereo)
       printf("2nd image %dx%d, written to %s\n", width, height, fname2);

    return(TRUE);   // SUCCESS
}

/* ******************************************************************************* */

char *get_base_name( char *name )
{ 
    int i, c, len;
    char *bn = name;
    len = (int)strlen(name);
    for (i = 0; i < len; i++) {
        c = name[i];
        if ((c == '/')||(c == '\\')) {
            bn = &name[i+1];
        }
    }
    return bn;
}

char *get_path_and_name( char *path )
{ 
    int i, c, len;
    char *name = GetNxtBuf();
    strcpy(name,path);
    len = (int)strlen(name);
    for (i = (len - 1); i >= 0; i--) {
        c = name[i];
        if (c == '.') {
            name[i] = 0;
            return name;
        } else if (( c == '/' ) || ( c == '\\' ) || ( c == ':' ))
            break;  // stop this mess
    }
    return name;
}

char *get_file_path_only( char *full_file )
{
    char *cp = GetNxtBuf();
    strcpy(cp,full_file);
    int len = (int)strlen(cp);
    int i, c;
    if (len) {
        for (i = len - 1; i >= 0; i--) {
            c = cp[i];
            if ((c == '/')||(c == '\\')) {
                cp[i + 1] = 0;
                break;
            }
        }
    }
    return cp;
}
void clean_buff_tail(char *rb)
{
    size_t len = strlen(rb);
    while (len) {
        len--;
        if (rb[len] > ' ')
            break;
        rb[len] = 0;
    }
}

/////////////////////////////////////////////////////////////////////
/////// FILE IO
////////////////////////////////////////////////////////////////////
static FILE *log_out_file = stderr;

void set_log_file( FILE *fp ) { log_out_file = fp; }

void log_write_stg( char *cp )
{
    fprintf(log_out_file, "%s", cp);
    if (log_out_file != stderr)
        fprintf(stderr, "%s", cp);
}

int _C_T sprtf( const char * pf, ... )
{
    static char _s_sprtfbuf[MMX_BUFFER+1];
    char * pb = _s_sprtfbuf;
    int   i;
    va_list arglist;
    va_start(arglist, pf);
    //i = vsprintf( pb, pf, arglist );
#ifdef _MSC_VER
    i = vsprintf_s(pb,MMX_BUFFER,pf,arglist);
    if (i < 0) {
        fprintf(stderr, "WARNING: vsprintf_s FAILED\n");
        return i;
    }
#else
    i = vsnprintf(pb,MMX_BUFFER,pf,arglist);
#endif
    va_end(arglist);
    log_write_stg(pb);
    return i;
}

static char _s_nxt_buf[NXT_BUF_SIZE * NXT_BUF_COUNT];
static int _s_nxt_index = 0;
size_t GetBufSiz() { return NXT_BUF_SIZE; }
char *GetNxtBuf()
{
    _s_nxt_index++;
    if (_s_nxt_index >= NXT_BUF_COUNT)
        _s_nxt_index = 0;
    return &_s_nxt_buf[_s_nxt_index * NXT_BUF_SIZE];
}

//#define DBL_BUF_SIZ 32
//#define DBL_BUF_CNT 256
static char _s_dbl_buf[DBL_BUF_SIZ * (DBL_BUF_CNT+1)];
static int _s_dbl_index = 0;
char *getdblbuf(int len)
{
    if (len >= DBL_BUF_SIZ)
        return GetNxtBuf();
    _s_dbl_index++;
    if (_s_dbl_index >= DBL_BUF_CNT)
        _s_dbl_index = 0;
    return &_s_dbl_buf[_s_dbl_index * DBL_BUF_SIZ];
}
static char _s_gtf_buf[1024];
#ifdef USE_PRECISION_8
const char *fform = "%0.8f";
#else
const char *fform = "%f";
#endif

char *gtf( double d )
{
    char *cp = _s_gtf_buf;
    int len = sprintf(cp,fform,d);
    while (len) {
        len--;
        if (cp[len] == '.') {
            cp[len] = 0;
            break;
        } else if (cp[len] > '0') {
            len++;
            break;
        }
        cp[len] = 0;
    }
    char *cp2 = getdblbuf(len);   // get a buffer
    strcpy(cp2,cp); // copy in the content
    return cp2; // and return it...
}

char *gtf_VERY_BAD_IDEA( double d )
{
    static char _s_gtf_buf[1024];
    static double div1 = 100000000.0;
    static double div2 = 10000000.0;
    static double div3 = 1000000.0;
    static double div4 = 100000.0;
    char *cp = _s_gtf_buf;
    int i;
    double d2;
    if (d > 10000.0) {
        i = (int) d;
        d2 = ((double)i);
    } else if (d > 1000.0) {
        i = (int)( d * div4 );
        d2 = ((double)i / div4);
    } else if (d > 100.0) {
        i = (int)( d * div3 );
        d2 = ((double)i / div3);
    } else if (d > 10.0) {
        i = (int)( d * div2 );
        d2 = ((double)i / div2);
    } else {
        i = (int)( d * div1 );
        d2 = ((double)i / div1);
    }
    if ( d2 < 0.0 ) {
        i = (int)ceilf(d2);
    } else {
        i = (int)floorf(d2);
    }

    if ((double)i == d2)
        i = sprintf(cp,"%d",i);
    else {
        i = sprintf(cp,"%f",d2);
        while(i) {
            i--;
            if (cp[i] == '.') {
                cp[i] = 0;
                break;
            } else if (cp[i] > '0')
                break;
            cp[i] = 0;
        }
    }
    char *cp2 = getdblbuf(i);   // get a buffer
    strcpy(cp2,cp); // copy in the content
    return cp2; // and return it...
}

const char *ts_form = "%04d-%02d-%02d %02d:%02d:%02d UTC";
// Creates the UTC time string from timestamp (unix secs since epoch)
char *Get_UTC_Time_Stg(time_t Timestamp)
{
    char *ps = GetNxtBuf();
    tm  *ptm;
    ptm = gmtime (& Timestamp);
    sprintf (
        ps,
        ts_form,
        ptm->tm_year+1900,
        ptm->tm_mon+1,
        ptm->tm_mday,
        ptm->tm_hour,
        ptm->tm_min,
        ptm->tm_sec );
    return ps;
}

char *Get_Current_UTC_Time_Stg()
{
    time_t Timestamp = time(0);
    return Get_UTC_Time_Stg(Timestamp);
}

#ifndef IS_DIGIT
#define IS_DIGIT(a) (( a >= '0' ) && ( a <= '9' ))
#endif

int is_digits(char *buf)
{
    size_t len = strlen(buf);
    if (len == 0) return 0;
    size_t ii;
    int c;
    for (ii = 0; ii < len; ii++) {
        c = buf[ii];
        if (!IS_DIGIT(c))
            return 0;
    }
    return 1; // success
}

int is_double(char *buf)
{
    size_t len = strlen(buf);
    if (len == 0) return 0;
    size_t ii;
    int c;
    for (ii = 0; ii < len; ii++) {
        c = buf[ii];
        if ( ! ( IS_DIGIT(c) || (c == '.') || ((c == '+')&&(ii == 0)) ) )
            return 0;
    }
    return 1; // success
}

int rename_to_old_bak( const char *file )
{
    int iret = 0;
    if (is_file_or_directory((char *)file) != MFT_FILE)
        return iret;   // there is no such file - no renaming
    char *nn;
    std::string f(file);
#if 0 // not a good idea
    std::string::size_type pos = f.rfind('.');  // find the last '.'
    std::string::size_type pos1 = f.rfind('\\');
    std::string::size_type pos2 = f.rfind('/');
    if (pos != std::string::npos) {
        if ((pos1 == std::string::npos)&&(pos2 == std::string::npos))
            f = f.substr(0,pos);    // no path separator so is ok
        else { 
            if (pos1 == std::string::npos)
                pos1 = pos2;
            if (pos > pos1)
                f = f.substr(0,pos); // got path sep but '.' is greater
        }
    }
#endif // 0
    std::string tst(f);
    tst += ".old";
    if (is_file_or_directory((char *)tst.c_str()) != MFT_FILE) {
        // ok, going to ren-name it OLD
#if _MSC_VER
        nn = get_base_name((char *)tst.c_str());
#else
        nn = (char *)tst.c_str();
#endif
        if (rename(file,nn))
            return -1;  // failed rename to OLD
        iret = 1;
    } else {
        // already have an OLD, so will rename to BAK, removing any previous
        tst = f;    // start again
        tst += ".bak";  // append '.bak'
        if (is_file_or_directory((char *)tst.c_str()) == MFT_FILE)
            unlink( tst.c_str() );  // delete any previous .BAK
#if _MSC_VER
        nn = get_base_name((char *)tst.c_str());
#else
        nn = (char *)tst.c_str(); // TODO: check what is needed in unix
#endif
        if (rename(file,nn))
            return -2;
        iret = 2;
    }
    return iret;
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION   : InStr
// Return type: int 
// Arguments  : char *lpb - source
//            : char *lps - needle
// Description: Return the position of the FIRST instance of the string in lps
//              Emulates the Visual Basic function.
///////////////////////////////////////////////////////////////////////////////
int InStr( char *lpb, char *lps )
{
   int   iRet = 0;
   int   i, j, k, l, m;
   char  c;
   i = (int)strlen(lpb);
   j = (int)strlen(lps);
   if( i && j && ( i >= j ) ) {
      c = *lps;   // get the first we are looking for
      l = i - ( j - 1 );   // get the maximum length to search
      for( k = 0; k < l; k++ ) {
         if( lpb[k] == c ) {
            // found the FIRST char so check until end of compare string
            for( m = 1; m < j; m++ ) {
               if( lpb[k+m] != lps[m] )   // on first NOT equal
                  break;   // out of here
            }
            if( m == j ) {  // if we reached the end of the search string
               iRet = k + 1;  // return NUMERIC position (that is LOGICAL + 1)
               break;   // and out of the outer search loop
            }
         }
      }  // for the search length
   }
   return iRet;
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION   : InStri
// Return type: int 
// Arguments  : char *lpb - source
//            : char *lps - needle
// Description: Return the position of the FIRST instance of the string in lps
//              Ignores case.
///////////////////////////////////////////////////////////////////////////////
int InStri( char *lpb, char *lps )
{
   int   iRet = 0;
   int   i, j, k, l, m;
   char  c;
   i = (int)strlen(lpb);
   j = (int)strlen(lps);
   if( i && j && ( i >= j ) ) {
      c = toupper(*lps);   // get the first we are looking for
      l = i - ( j - 1 );   // get the maximum length to search
      for( k = 0; k < l; k++ ) {
         if( toupper(lpb[k]) == c ) {
            // found the FIRST char so check until end of compare string
            for( m = 1; m < j; m++ ) {
               if( toupper(lpb[k+m]) != toupper(lps[m]) )   // on first NOT equal
                  break;   // out of here
            }
            if( m == j ) {  // if we reached the end of the search string
               iRet = k + 1;  // return NUMERIC position (that is LOGICAL + 1)
               break;   // and out of the outer search loop
            }
         }
      }  // for the search length
   }
   return iRet;
}

// eof - misc_lib.cxx

	
