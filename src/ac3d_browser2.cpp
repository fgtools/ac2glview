/* ======================================================================================
 * ac3d_browser2.cpp
 *
 * 20130730 - Dramatically change the DELETE behaviour. Instead of removing, just mark as 'deleted'
 *            That object then does NOT get displayed, or written to an *.ac file output. Specially 
 *            attention to the kids count. It must always remain at its original value to correctly 
 *            iterate an object.
 * 20130621 - Initial release
 * 20130608 - Looking good
 * 20130526 - Initial cut
 *
 * Started as some experiments is parsing an AC3D .ac model file, and reviewing the items it contains.
 * Had the idea to allow modification, by deleting kids, and writing the modified object 
 * back to a new .ac file, now competed.
 *
 * Also can write the 'object' to a Three.js JSON file for web loading of models
 *
 * Dependencies: OpenGL, FLTK, FLU
 *
 * It was also a learning tool for FLTK and FLU libraries... with just a little touch of using 
 * fluid to generate the preferences dialog - see dialog1.fl - but NOT automated. Presently 
 * modify dialog1.fl, and generate dialog1.cxx/h outside source, and cut/paste into the source
 * dialog1.cxx/h. TODO: Possible integrate this fluid step into the CMake... but maybe not...
 *
 * LICENSE:
 *
 * Copyright (C) 2013  Geoff R. McLane - http://geoffair.org/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ====================================================================================== */

#ifndef FREEGLUT_STATIC
#define FREEGLUT_STATIC
#endif
#ifndef FREEGLUT_LIB_PRAGMAS
#define FREEGLUT_LIB_PRAGMAS 0
#endif
#include <stdio.h>
#include <time.h>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Counter.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>
#include <FL/fl_show_colormap.H>
#include <FL/filename.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Preferences.H>
#include <FL/gl.h>
#include <FL/Fl_Gl_Window.H>
#include <FL/names.h>         // defines fl_eventnames[]
#include <FL/Fl_Menu_Bar.H>

#include <FLU/Flu_File_Chooser.h>
#include <FLU/Flu_Tree_Browser.h>
#include <FLU/flu_pixmaps.h>

#include <GL/glu.h>

#include <vector>
#include <string>
#include <algorithm>
#include <map>

#include "misc_lib.hxx" // just some misc functions
#include "ac3d_browser2.h"
#ifdef ADD_PREF_DIALOG
#include "dialog1.h"
#endif
#ifdef _MSC_VER
#include <direct.h> // for _getcwd()
#endif

#include <ac3d2.hxx>

/* ***********************************************************************
   20130728 - major change in handling of duplicate name in the list
   Presently by apppending a .num. change to [num], and always remove this [num]
   if present when searching, and this number gives the offset to find.
 ************************************************************************* */

// forward refs
// ===============================================
static void clean_up();
static void check_script();
void open_cb(Fl_Widget*, void*);
static void writeACCB( Fl_Widget*, void* );
static void removeCB( Fl_Widget*, void* );
static void exportCB( Fl_Widget*, void* );
static void export3J( Fl_Widget*, void* );
static void preferencesCB( Fl_Widget*, void* );
void quit_cb(Fl_Widget*, void*);
static void refreshCB( Fl_Widget*, void* );
char *get_obj_name( ACObject *ob );
void rest_to_full_display(char *cp = 0);
char *get_in_file();
char *get_output_file();
char *get_curr_output_file();
char *get_new_ac_out();
void ensure_os_seps(std::string & s);
bool generate_a_script( char *out_file = 0 );
void loadscriptCB(Fl_Widget *, void *);
static void writescriptCB( Fl_Widget*, void* );
void set_info_display( char *cp );
char *getTempFileName() ;
int writeAC3DSingle( char *cpf, ACObject *bob, ACObject *ob);
void set_last_js_out( char *cmd );
// ===============================================

#define SPRTF sprtf
#ifndef MAX_PATH
#define MAX_PATH 264
#endif

#ifndef _MSC_VER
int DeleteFile( char *name )
{
    // TODO: Add delete file for NOT windows
    return 0;
}
#endif

const char *def_log = "tempacview.txt";
const char *output_path = 0;
FILE *log_fp = 0;

typedef struct tagDELOBJS {
    ACObject *ob;
    int index, count;
    ACObject *parent;
}DELOBJS, *PDELOBJS;

typedef std::vector<PDELOBJS> vOBJS;

#ifdef NEW_DELETE_BEHAVIOUR // no longer release this memory. That is done when the main object is deleted
static vOBJS *pvDeleteList = 0;    // keep deleted list
#else
static vOBJS *pvDeleted = 0;    // keep deleted list
#endif

static PreviewWin *preview;
static mainWin *mwin;
// static Fl_Double_Window *win;
static Flu_Tree_Browser *tree;
#ifdef ADD_NEW_NODE2
static Fl_Input *newNode2;     // ac output file name
#endif // #ifdef ADD_NEW_NODE2

#ifndef ADD_ACTIONS_2_MENU
static Fl_Button *addChild, *removeBtn, *refreshBtn;
#endif
// from : http://seriss.com/people/erco/fltk/#Fl_Multiline_Output
static Fl_Multiline_Output *textOutput2 = 0; 
// static int next = 1;
static Flu_Tree_Browser::Node* mat_Node = 0;
static Flu_Tree_Browser::Node* obj_Node = 0;
static int deletions = 0;
// current AC input
static char *ac_in_file = 0;
static char *full_ac_in_file = 0;
static char *ac_in_name = 0;    // just basename of file

#ifdef ADD_NEW_NODE2
static const char *out_file = "temp.ac";
#else 
static char *output_ac_file = 0;
#endif // #ifdef ADD_NEW_NODE2
static char *script_ac_write = 0;

static const char *out_filejs = "temp.js";
static const char *last_out_path = 0;
static char *last_outjs = 0;
static int multi_write = 0;
static int mat_ind = 0;
static char *script_file = 0;
static char *script_buf = 0;
static double script_delay_secs = 1.0;
static double script_next_secs;
static int script_has_exit = 0;
static int script_delete_cnt = 0;
static int always_write_script = 1; // whether deletions or not
static ACObject *last_ob = 0;   // last found with mouse
#define EndBuf(a) ( a + strlen(a))

static bool abort_on_script_err = true;
static int has_duplicate_names = 0;

static char inp_file_buffer[1024];
static char info_buffer[1024];
static char file_path_buffer[FL_PATH_MAX];
static bool no_sort_list = true;
static bool write_temp_acfile = false;
static bool write_temp_single = false;

char *get_curr_script_file() { return script_file; }

static bool ask_assimp_run = false;

static const char *proot_node = "ac3d browser";
static const char *pmat_node = "MATERIAL";
static const char *pobj_node = "OBJECT";

typedef struct tagMapName {
    std::string s;
    std::string name;
}MapName;
typedef std::vector<MapName> vMAPNM;
typedef struct tagMatOff {
    int i, n;
}MatOff;

typedef std::vector<MatOff> vMOFFS;

typedef std::vector<std::string> vSTG;
typedef std::vector<int> vINT;

static vSTG vObjNames;
static vSTG vMatNames;

static vMAPNM vMapNameObj;
static vMAPNM vMapNameMat;

typedef std::vector<Flu_Tree_Browser::Node *> vTNODE;

static const char  *mod_name = "ac3d_browser2";

// forward refs
void populate_Tree();
void clearNameVector();
static int showSelected(char *cp, Flu_Tree_Browser::Node *in_n, int rend = 1);
static int writeObject(ACObject *ob, FILE *fp, int add_kids = 1, int kid_cnt = 0, int alert = 0 );
static void writeTempAC3DFile(ACObject *ob);
static int keyfn ( unsigned char c, int i1, int i2 );

static ac3d2gl *parser = 0;
static ACObject *current = 0;   // ob = 0;
static int done_gl_init = 0;
static int delete_temp_files = 0;
static int fixed_temp_file = 1;
static int show_wire_frame = 0;

typedef struct tagFNDOBJ {
    ACObject *obj_found;
    int index;
}FNDOBJ, *PFNDOBJ;
//static ACObject *ob_found = 0; // set object found

static FNDOBJ found_obj = {0};

ACObject *get_current_obj() { return current; }
ACObject *get_found_obj()   { return found_obj.obj_found; }

static const char *vendor = "geoffair";
// some initial positions and sizes, but any changes in pos or size are written to preferences
// and will be restored on next restart
// In windows preferences written to C:\Users\user\AppData\Roaming\geoffair\ac3d_browser.prefs
// ===========================================================================================
static int pos_x = 20;
static int pos_y = 20;
static int win_width = WINDOW_WIDTH;
static int win_height = WINDOW_HEIGHT;
static int tree_width = TREE_WIDTH;
static int tree_height = TREE_HEIGHT;
static int group_width = GROUP_WIDTH;
static int group_height = GROUP_HEIGHT;
// static int button_width = BUTTON_WIDTH; // 80 // was 100
// static int button_height = BUTTON_HEIGHT;   // 20
static int initial_y_val = INITIAL_Y_VAL;   // 20    // was 370
static int y_val_steps = Y_VAL_STEPS;   // 30
static int multi_width = MULTI_WIDTH;   // (GROUP_WIDTH - 20)
static int multi_height = MULTI_HEIGHT; // (WINDOW_HEIGHT - (5 * Y_VAL_STEPS))
static int group_x = 0;
static int group_y = 0;
static int tree_x = GROUP_WIDTH;
static int tree_y = 0;
static int rgl_width = GL_WIDTH;   // (GROUP_WIDTH - 20)
static int rgl_height = GL_HEIGHT; // (WINDOW_HEIGHT - (5 * Y_VAL_STEPS))
// ==============================================================================================

// main window preferences, and last file
static const char *szwin_height = "win_height";
static const char *szwin_width = "win_width";
static const char *szwin_posx = "win_posx";
static const char *szwin_posy = "win_posy";
static const char *szlast_outpath = "last_out_path";
static const char *szlast_outfile = "last_out_file";
static const char *szlast_outjs = "last_outjs";
static const char *szlast_outac = "last_outac";
static const char *szlast_infile = "last_in_file";
static Fl_Preferences *prefs = 0; 

void pgm_exit(int v)
{
    clean_up();
    exit(v);
}

/* =============================================================================
   mainWin = GL
  ================================================================================ */

mainWin::mainWin( int x, int y, int w, int h, const char *title ) :
Fl_Double_Window( x, y, w, h, title ) 
{
    // any intialisation
    resizecb = 0;   // clear this...
}

//static void Menu_CB(Fl_Widget*, void *data) 
//{
//    char name[80];
//    ((Fl_Menu_Button*)data)->item_pathname(name, sizeof(name)-1);
//    SPRTF("Menu Callback: %s\n", name);
//}
static void deselectCB(Fl_Widget *w, void *data) 
{
    tree->unselect_all();
    rest_to_full_display();
}

#ifdef NEW_DELETE_BEHAVIOUR // search for object to delete, and mark it - stop when found
int undeleteObject(ACObject *del_ob, ACObject *rob )
{
    if (del_ob == rob) {
        if (rob->deleted) {
            rob->deleted = false;   // put it back into the display list
            return 1;
        }
    }
    int n;
  	for (n = 0; n < rob->num_kids; n++) {
        if (undeleteObject(del_ob,rob->kids[n]))
            return 1; 
    }
    return 0;
}
#endif // #ifdef NEW DELETE BEHAVIOUR

static void undoCB(Fl_Widget *w, void *data) 
{
#ifdef NEW_DELETE_BEHAVIOUR     // undoCB - presently only undelete last pushed - TODO offer choice
    // is now a simple matter of removing the 'delete' flag, so have the possibility
    // of undeleting one or more objects at a time
    char *cp = GetNxtBuf();
    ACObject *rob = get_current_obj();
    ACObject *un_ob = (ACObject *)data;
    *cp = 0;
    if (rob && un_ob && pvDeleteList && pvDeleteList->size()) {
        size_t max = pvDeleteList->size();
        size_t ii;
        PDELOBJS dob = 0;
        ACObject *ob = 0;
        for (ii = 0; ii < max; ii++) {
            dob = pvDeleteList->at(ii);
            ob = dob->ob;
            if (ob == un_ob)
                break;  // found in list of deleted
        }
        if ((ii < max) && dob && ob) {
            if (undeleteObject(un_ob,rob)) {
                pvDeleteList->erase( pvDeleteList->begin() + ii );
                delete dob;
                deletions = (int)pvDeleteList->size();
                int list = ac_display_list_render_object(rob);
                if (list > 0) {
                    glDeleteLists(preview->full_list, 1);    // delete the current list
                    preview->full_list = preview->display_list = list;
                    // success undo and creating a new display list
                    populate_Tree();    // LAZY: Just reset the WHOLE Tree ;=()
                    sprintf(cp,"Object %s UNDELETE", (un_ob->name ? un_ob->name : ""));
                } else {
                    sprintf(cp,"UNDELETE %s failed! No display list", 
                        (un_ob->name ? un_ob->name : ""));
                }
            } else {
                sprintf(cp,"UNDELETE %s failed! Not in objects?", 
                   (un_ob->name ? un_ob->name : ""));
            }
        } else {
            sprintf(cp,"Object %s not found. UNDELETE failed!", 
                (un_ob->name ? un_ob->name : ""));
        }
    } else {
        sprintf(cp,"Failed to UNDELETE object!");
    }
    if (*cp) {
        set_info_display(cp);
        SPRTF("%s\n",cp);
    }

#else // !#ifdef NEW DELETE BEHAVIOUR

    if (pvDeleted && pvDeleted->size()) {
        char *cp = GetNxtBuf();
        PDELOBJS dob = pvDeleted->at(pvDeleted->size() - 1);
        ACObject *ob = dob->ob;
        int n = dob->index;
        ACObject *parent = dob->parent;
        sprintf(cp,"UNDelete %s, par=%s, kids %d", (ob->name ? ob->name : "object"), get_obj_name(parent), parent->num_kids+1 );
        // forget the index - just put it at the end
        n = parent->num_kids;
        parent->kids[n] = ob;   // put back in list
        parent->num_kids++; // bump kid count
        pvDeleted->pop_back();    // deleted last entry
        deletions = (int)pvDeleted->size();
        ACObject *rob = get_current_obj();
        int list = ac_display_list_render_object(rob);
        if (list > 0) {
            glDeleteLists(preview->full_list, 1);    // delete the current list
            preview->full_list = preview->display_list = list;
            // success undo and creating a new display list
            // deselectCB(w,data);
            populate_Tree();    // LAZY: Just reset the WHOLE Tree ;=()
            SPRTF("%s\n",cp);
        }
    }
#endif // #ifdef NEW DELETE BEHAVIOUR y/n
}

// ==================================================
// right click context popup menu
// ==================================================
static void Create_Popup( int x, int y )
{
    char *cp = GetNxtBuf();
    char *inf = get_in_file();
    // Dynamically create menu, pop it up
    Fl_Menu_Button menu(x, y, 80, 1);
    //int sel_count = tree->num_selected();
    Flu_Tree_Browser::Node* n = tree->get_selected( 1 );
    if( n ) {
        // got a SELECTION, so 1 and 2 delete or deselect
        sprintf(cp,"DELETE %s", n->label());
        menu.add(cp, 0, removeCB, (void*)&menu);
        menu.add("Deselect", 0, deselectCB, (void*)&menu);
    }
#ifdef NEW_DELETE_BEHAVIOUR // popup - offer undelete TODO should show list
    // now have a chance of presenting a LIST for choice

    if (deletions && pvDeleteList && pvDeleteList->size()) {
        size_t max = pvDeleteList->size();
        size_t ii, i2 = 0;
        PDELOBJS dob;
        ACObject *ob;
        for (ii = 0; ii < max; ii++) {
            dob = pvDeleteList->at(ii);
            ob = dob->ob;
            if (ob->name) i2++;
        }
        // how to establish a sub-menu list, and then return to main?????
        // EASY sub use same prime/sub
        if (i2 > 1) {
            int i;
            sprintf(cp,"UNDelete %d", (int)i2);
            menu.add(cp, 0, 0, 0, FL_SUBMENU);
            char *cp2 = GetNxtBuf();
            for (i = (int)(max - 1); i >= 0; i--) {
                dob = pvDeleteList->at(i);
                ob = dob->ob;
                if (ob->name) {
                    sprintf(cp2,"%s/%s", cp, ob->name);
                    menu.add(cp2, 0, undoCB, (void*)ob);
                }
            }
        } else {
            dob = pvDeleteList->at(pvDeleteList->size() - 1);
            ob = dob->ob;
            sprintf(cp,"UNDelete %s", ob->name );
            menu.add(cp, 0, undoCB, (void*)ob);
       }
    }
#else
    if (deletions && pvDeleted && pvDeleted->size()) {
        PDELOBJS dob = pvDeleted->at(pvDeleted->size() - 1);
        ACObject *ob = dob->ob;
        sprintf(cp,"UNDelete %s", ob->name );
        menu.add(cp, 0, undoCB, (void*)&menu);
    }
#endif
    strcpy(cp,"Save AC...");
    menu.add(cp, 0, writeACCB, (void*)&menu);

    menu.add("Export 3js..." , 0, export3J, (void*)&menu);
    menu.add("assimp 3js..." , 0, exportCB, (void*)&menu);
    menu.add("Write script...", 0, writescriptCB, (void*)&menu );
    menu.add("Load Script...", 0, loadscriptCB, (void*)&menu );
    if (deletions && inf) {
        sprintf(cp,"Reload %s",inf);
        menu.add(cp , 0, refreshCB, (void*)&menu);
    }

    menu.add("Open AC...",   0, open_cb,  (void*)&menu);

#ifdef ADD_PREF_DIALOG
    menu.add("Preferences..." , 0, preferencesCB, (void*)&menu);
#endif
    menu.add("Quit",       0, quit_cb, (void*)&menu);
    menu.popup();
    //SPRTF("Exit popup...\n");
}

// ================================================================
// keyboard (and mouse) callbacks
// ******************************
int mainWin::handle(int e)
{
    static int last_event = 0;
    int res = 1;
    int key = 0;
    int x,y;
    const char *text;
    switch (e) {
    case FL_KEYDOWN:
        if (Fl::event_state() & FL_CTRL) break;
        key = Fl::event_key();
        text = Fl::event_text();
        if (key == FL_Escape) { // ESC key
           res = Fl_Window::handle(e);
        } else {
            if (text && *text)
                key = *text;
            if (!keyfn( key, 0, 0 )) {
                // if key NOT used, pass to fl handler
               res = Fl_Window::handle(e);
            }
        }
        break;
    case FL_PUSH:
        x = Fl::event_x();
        y = Fl::event_y();
        if ( Fl::event_button() == FL_RIGHT_MOUSE ) {
            // time for a context menu
            Create_Popup( x, y );
            res = 1;
        } else {
            res = Fl_Window::handle(e);
        }
        break;
    case FL_RELEASE:
        if ( Fl::event_button() == FL_RIGHT_MOUSE ) {
            // time for a context menu
            res = 1;
        } else {
            res = Fl_Window::handle(e);
        }
        break;
    default:
       res = Fl_Window::handle(e);
    }
    if ((e != last_event) && ac_verb9()) {
        SPRTF("Event was %s (%d) res %d key %x\n", fl_eventnames[e], e, res, key);
    }
    last_event = e;
    return res;
}


static void Enable_WireFrame() 
{
    // Turn on wireframe mode
    glPolygonMode(GL_FRONT, GL_LINE);
    glPolygonMode(GL_BACK, GL_LINE);
}

static void Disable_WireFrame()
{   
    // Turn off wireframe mode
    glPolygonMode(GL_FRONT, GL_FILL);
    glPolygonMode(GL_BACK, GL_FILL);
}


/*  ================================================================================ */
char *escape_backslash(char *file)
{
    char *cp = GetNxtBuf();
    size_t len = strlen(file);
    size_t ii, out;
    int c;
    out = 0;
    for (ii = 0; ii < len; ii++) {
        c = file[ii];
        if (c == '/')
            c = '\\';
        if (c == '\\')
            cp[out++] = (char)c;
        cp[out++] = (char) c;
    }
    return cp;
}

void set_info_display( char *cp )
{
    if (textOutput2)
        textOutput2->value(cp);
}


void set_output_file( char *file )
{
    if (output_ac_file)
        free(output_ac_file);
    output_ac_file = strdup(file);
    // Save to preferences
    prefs->set(szlast_outac, file);
    prefs->flush();
}

char *get_output_file()
{
    return output_ac_file;
}
char *get_curr_output_file()
{
    return output_ac_file;
}

void set_output_path( char *file )
{
    char *path = get_file_path_only(file);
    if (last_out_path)
        free((void *)last_out_path);
    last_out_path = strdup(path);
    // Save to preferences
    prefs->set(szlast_outpath, path);
    prefs->flush();

}


ACObject *Get_Object2( ACObject *ob, ACVertex *pv )
{
#ifdef NEW_DELETE_BEHAVIOUR // Get_Object2: only if NOT deleted
    if (ob->deleted)
        return 0;
#endif
    PBBOX pbb = parser->ac_get_obj_bbox(ob);
    if (pbb) {
        if (parser->ac_vertex_in_bbox( ob, pv ))
            return ob;
    }
    return 0;
}
ACObject *Get_Object3( ACObject *ob, ACVertex *pv, float fudge = 0.0 );

ACObject *Get_Object3( ACObject *ob, ACVertex *pv, float fudge )
{
#ifdef NEW_DELETE_BEHAVIOUR // Get_Object3: only if NOT deleted
    if (ob->deleted)
        return 0;
#endif
    PBBOX pbb = parser->ac_get_obj_bbox(ob);
    if (pbb) {
        if (parser->ac_xy_in_bbox( ob, pv, fudge ))
            return ob;
    }
    return 0;
}

static float min_fudge = 0.1f;
static float max_fudge = 0.6f;
static float inc_fudge = 0.1f;

ACObject *Get_Object( ACObject *ob, double ox, double oy, double oz )
{
    if (!parser)
        return 0;
#ifdef NEW_DELETE_BEHAVIOUR  // Get_Object: do NOT find objects deleted
    if (ob->deleted)
        return 0;
#endif
    ACVertex p;
    p.x = ox;
    p.y = oy;
    p.z = oz;
    ACObject *pob = Get_Object2( ob, &p );
    if (pob) {
       return pob;
    }
    int i;
    for (i = 0; i < ob->num_kids; i++) {
        pob = Get_Object2( ob->kids[i], &p );
        if (pob) {
            return pob;
        }
    }

    // just try with x,y coordinates
    pob = Get_Object3( ob, &p );
    if (pob) return pob;
    for (i = 0; i < ob->num_kids; i++) {
        pob = Get_Object3( ob->kids[i], &p );
        if (pob) return pob;
    }

    float fudge = min_fudge;
    while (fudge <= max_fudge) {
        pob = Get_Object3( ob, &p, fudge );
        if (pob) return pob;
        for (i = 0; i < ob->num_kids; i++) {
            pob = Get_Object3( ob->kids[i], &p, fudge );
            if (pob) return pob;
        }
        fudge += inc_fudge;
    }
    return 0;
}

/* --------------------------------------------------------------------
   Flu_Tree_Browser::Node *get_Tree_obj_Node_from_name( char * name )
   The tree label may have [num] appended, so must strip this for the compare

   -------------------------------------------------------------------- */

Flu_Tree_Browser::Node *get_Tree_obj_Node_from_name( char * name )
{
    Flu_Tree_Browser::Node *obj = 0;
    if (obj_Node) {
        obj = obj_Node->find(name);
        if (!obj) {
            Flu_Tree_Browser::Node *np = obj_Node->first();
            while (np) {
                std::string label(np->label());
                std::string::size_type pos = label.find('[');
                if (pos != std::string::npos)
                    label = label.substr(0,pos);    // trim back the name
                if (strcmp(name,label.c_str()) == 0) {
                    obj = np;
                    break;
                }
                np = np->next();
            }
        }
    }
    return obj;
}

static GLdouble ox = 0.0, oy = 0.0, oz = 0.0;
static GLfloat wx,wy,wz;

void Mouse_Pos(int x_in,int y_in, int select = 1);
// Find the object that the mouse is ON, if possible
// by default SELECT this item in the Tree
// 20130624 - If nothing found, then try the last mouse move slections
static char _s_last_find[256] = {0};
static int last_x, last_y;
static int try_with_last = 0;   // hmmm, seemed to SLOW things up too much - maybe????
void set_ox_oy_oz_from_mouse( int x_in,int y_in )
{
    int x = x_in;
    int y = y_in;
    GLint viewport[4];
    GLdouble modelview[16],projection[16];
    wx = x;
    glGetIntegerv(GL_VIEWPORT,viewport);
    y = viewport[3]-y;  // hmmm, why this????
    wy = y;
    glGetDoublev(GL_MODELVIEW_MATRIX,modelview);
    glGetDoublev(GL_PROJECTION_MATRIX,projection);
    // Depth values are read from the depth buffer.
    glReadPixels(x,y,1,1,GL_DEPTH_COMPONENT,GL_FLOAT,&wz);
    // get GL ox, oy, oz (somehow?)
    gluUnProject(wx,wy,wz,modelview,projection,viewport,&ox,&oy,&oz);
}

void Mouse_Pos(int x_in,int y_in, int select)
{
    char *lf = _s_last_find;
    set_ox_oy_oz_from_mouse( x_in, y_in );
    ACObject *rob = get_current_obj();
    if (rob) {
        ACObject *ob = Get_Object( rob, ox, oy, oz );
        if (ac_verb9()) {
            SPRTF("CLICK: x=%d y=%d wx=%s wy=%s wz=%s ox=%s oy=%s oz=%s %s\n", x_in, y_in, 
                gtf(wx), gtf(wy), gtf(wz),
                gtf(ox), gtf(oy), gtf(oz),
                (ob ? (ob->name ? ob->name : "object") : "NF") );
        }
        // not so sure about this - using the LAST mouse hover selection, but try...
        if (!ob && try_with_last && *lf && last_ob && select) {
            set_ox_oy_oz_from_mouse( last_x, last_y );
            ob = Get_Object( rob, ox, oy, oz );
        }
        if (ob && ob->name) {
            Flu_Tree_Browser::Node *n = obj_Node;
            if (!n) return;
            char *name = strdup(get_obj_name(ob));
            //int count = n->children();  // get child count
            Flu_Tree_Browser::Node *obj = get_Tree_obj_Node_from_name( name );
            if (!obj) {
                SPRTF("Failed to find [%s] in Tree!\n", name);
                free(name);
                return; // forget it
            }
            free(name);
            const char *path = tree->find_path(obj);
            if (select) {
                tree->unselect_all();
                n->open(true);
                obj->activate();
                obj->select(true);
                obj->open(true);
                // TODO: how to scroll this selection into view?????????
                // *****************************************************
                //Fl_Tree_Item *ti = tree->find_item(path);
                //if (ti) ti->show();
                //const char *label = obj->label();
                //if (label && *label) {
                    //obj->
                    //Fl_Tree_Item *ti = tree->find(path);
                    //if (ti) ti->show();
                    //count = 0;
                //}
                SPRTF("Selected [%s]\n",path);
            } else {
                if (strcmp(lf,path)) {
                    SPRTF("Found [%s]\n",path);
                    strcpy(lf,path);
                    last_x = x_in;
                    last_y = y_in;
                    last_ob = ob;
                }
            }
        }
    }
}

/* =============================================================================
   PreviewWin = GL
  ================================================================================ */

PreviewWin::PreviewWin(int x,int y,int w,int h,const char *l) : Fl_Gl_Window(x,y,w,h,l)
{
    done_init = display_list = select_list = full_list = 0; // any init required
    // default mode is FL_RGB|FL_DOUBLE|FL_DEPTH
}
int show_all_events = 0;
int PreviewWin::handle(int e)
{
    static int last_event2 = 0;
    int x = 0;
    int y = 0;
    if ((e == FL_MOVE)||(e == FL_PUSH)) {
        // mouse move and down event
        x = Fl::event_x();
        y = Fl::event_y();
        if (e == FL_PUSH) {
            if ( Fl::event_button() == FL_RIGHT_MOUSE ) {
                // time for a context menu
            } else {
                Mouse_Pos(x,y); // mouse CLICK
            }
        } else {
            Mouse_Pos(x,y,0); // just a moving mouse - NO SELECT
        }
    }
    int res = Fl_Window::handle(e);
    //if ((e != last_event2) && ac_verb9()) {
    if ((e != last_event2) && (ac_verb9() || show_all_events) ) {
        SPRTF("GL Event was %s (%d) res %d x=%d y=%d\n", fl_eventnames[e], e, res, x, y);
    }
    last_event2 = e;
    return res;
}
/* ================================================================================ */


void set_projection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float)w/(float)h, 0.1, 10000.0);
}

void init_gfx(int w, int h)
{
    ac_prepare_render();
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    set_projection(w,h);
}

// ===========================================
// read and write this set to a config file
// ===========================================
static float x_trans =   0.0f;
static float y_trans =  -4.0f;  // was 0.0;
static float z_trans = -10.0f;  // was -3.0
static float angle_rate = 25.0f; // change at rate at ?? degrees per second
static int frozen = 0;
static float x_axis = 0.0f;
static float y_axis = 1.0f;
static float z_axis = 0.0f;
static float rot = 0.0;
// --- TODO - ADD TO CONFIG ---
static float eyex = 0.0f;
static float eyey = 1.8f;
static float eyez = 4.0f;
static float centrex = 0.0f;
static float centrey = 0.0f;
static float centrez = 0.0f;
static float upx = 0.0f;
static float upy = 1.0f;
static float upz = 0.0f;
// ---------------------------

static GL_PARMS found_parms;    // GL params for selection
static BBOX found_bb;
static GL_PARMS file_parms; // GL params for FULL
static GL_PARMS file_parms2;
static BBOX file_bb2;

static double prev_secs = 0.0;
static time_t last_secs = 0;
static double frametime;
static double tottime = 0.0;
static double total_time = 0.0;
static int framec = 0;
static int frames = 0;
static int frames_per_sec = 0;
static int elap_secs = 0;
static int last_framec = 0;
static double last_tottime = 0.0;

void show_bb(PBBOX pbb)
{
    SPRTF("BBOX:%d: min %s,%s,%s max %s %s %s rad %s\n",
        pbb->num_verts,
        gtf(pbb->minx), gtf(pbb->miny), gtf(pbb->minz),
        gtf(pbb->maxx), gtf(pbb->maxy), gtf(pbb->maxz),
        gtf(pbb->radius) );
}

void show_gl_parms( PGL_PARMS pgl )
{
    SPRTF("GLP: Trans(%s,%s,%s) LookAt(%s,%s,%s,%s,%s,%s,%s,%s,%s)\n",
            gtf(pgl->x_trans),gtf(pgl->y_trans),gtf(pgl->z_trans),
            gtf(pgl->eyex),gtf(pgl->eyey),gtf(pgl->eyez),
            gtf(pgl->centrex),gtf(pgl->centrey),gtf(pgl->centrez),
            gtf(pgl->upx),gtf(pgl->upy),gtf(pgl->upz));
}

void set_current_obj(ACObject *ob) 
{ 
    current = ob;
    if (parser) {
        parser->ac_set_model_size_pos2(ob,&file_parms2,&file_bb2);   // calc the GL stuff
        SPRTF("Specifications of %s OBJECT kids %d vert %d\n",
            (ob->name ? ob->name : "<blank>"), 
            ob->num_kids, ob->num_vert );
        show_bb(&file_bb2);
        show_gl_parms(&file_parms2);
    }
}

static void reset_defaults(PGL_PARMS pgl = 0);

static void reset_defaults(PGL_PARMS pgl)
{
    x_trans =   0.0f;
    y_trans =  -4.0f;  // was 0.0;
    z_trans = -10.0f;  // was -3.0
    angle_rate = 25.0f; // change at rate at ?? degrees per second
    //frozen = 0;
    x_axis = 0.0f;
    y_axis = 1.0f;
    z_axis = 0.0f;
    rot = 0.0;

    eyex = 0.0f;
    eyey = 1.8f;
    eyez = 4.0f;
    centrex = 0.0f;
    centrey = 0.0f;
    centrez = 0.0f;
    upx = 0.0f;
    upy = 1.0f;
    upz = 0.0f;
    if (pgl) {
        x_trans = pgl->x_trans;
        y_trans = pgl->y_trans;
        z_trans = pgl->z_trans;
        eyex = pgl->eyex;
        eyey = pgl->eyey;
        eyez = pgl->eyez;
        centrex = pgl->centrex;
        centrey = pgl->centrey;
        centrez = pgl->centrez;
        upx = pgl->upx;
        upy = pgl->upy;
        upz = pgl->upz;
        //SPRTF("GL_PARMS: glTranslatef(%s,%s,%s); gluLookAt(%s, %s, %s, %s, %s, %s, %s, %s, %s);\n",
        //    gtf(x_trans),gtf(y_trans),gtf(z_trans),
        //    gtf(eyex),gtf(eyey),gtf(eyez),
        //    gtf(centrex),gtf(centrey),gtf(centrez),
        //    gtf(upx),gtf(upy),gtf(upz));
    }
    GL_PARMS p;
    get_gl_params_to_gl_parms(&p);
    show_gl_parms(&p);
}

void get_gl_params_to_gl_parms( PGL_PARMS pgl )
{
    pgl->x_trans = x_trans;
    pgl->y_trans = y_trans;
    pgl->z_trans = z_trans;
    pgl->angle_rate = angle_rate; // change at rate at ?? degrees per second
    pgl->frozen = frozen;
    pgl->x_axis = x_axis;
    pgl->y_axis = y_axis;
    pgl->z_axis = z_axis;
    pgl->rot = rot;
    pgl->eyex = eyex;
    pgl->eyey = eyey;
    pgl->eyez = eyez;
    pgl->centrex = centrex;
    pgl->centrey = centrey;
    pgl->centrez = centrez;
    pgl->upx = upx;
    pgl->upy = upy;
    pgl->upz = upz;
}

void set_gl_paras_to_gl_params( PGL_PARMS pgl )
{
    x_trans = pgl->x_trans;
    y_trans = pgl->y_trans;
    z_trans = pgl->z_trans;
    angle_rate = pgl->angle_rate; // change at rate at ?? degrees per second
    frozen = pgl->frozen;
    x_axis = pgl->x_axis;
    y_axis = pgl->y_axis;
    z_axis = pgl->z_axis;
    rot = pgl->rot;
    eyex = pgl->eyex;
    eyey = pgl->eyey;
    eyez = pgl->eyez;
    centrex = pgl->centrex;
    centrey = pgl->centrey;
    centrez = pgl->centrez;
    upx = pgl->upx;
    upy = pgl->upy;
    upz = pgl->upz;
}


static void key_help()
{
    int tframes = frames_per_sec;
    printf("Keyboard Help - to size, position and rotate the model.\n");
    printf(" ESC or q = Quit\n");
    printf(" f/F      = Toggle rotation freeze. (def=%s)\n", (frozen ? "On" : "Off"));
    printf(" s/S      = Slow/Speed up rotation speed. angle_rate=%s\n", gtf(angle_rate));
    printf(" x/X      = Left/Right - x translation factor (%s)\n", gtf(x_trans));
    printf(" y/Y      = Up/Down    - y translation factor (%s)\n", gtf(y_trans));
    printf(" z/Z      = Near/Far   - z translation factor (%s)\n", gtf(z_trans));
    printf(" ?/h      = This help, frame rate and current angle...\n");
    printf(" 1-7      = Set glRotate(rot,x,y,z) - %s,%s,%s\n", gtf(x_axis), gtf(y_axis), gtf(z_axis));
    printf(" d/D      = Dump scene to numbered PPM file in work directory. (D in stereo)\n");
    printf(" v/V      = Reduce/Increase verbosity. (def=%d)\n", ac_get_verbosity());
    printf(" w/W      = Write/Read config file.\n");
    printf(" r/R      = Reset all defauts\n");
    printf(" i/I      = Toggle wireframe mode. (def=%d)\n", show_wire_frame);
    printf(" fps=%d, rotation angle=%s, secs %d\n", tframes, gtf(rot), elap_secs);
}

// in the range -10 to 10 subtract 0.5
// if lesser or greater   subtract 1/20 of the value
// if zero,               subtract 0.5
float sub_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next -= (next / 20.0f);
        } else {
            if (next < 0.5f)
                next = 0.0f;
            else
                next -= 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next -= (-next / 20.0f);
        else 
            next -= 0.5f;
    } else {
        next -= 0.5f;
    }
    return next;
}
// in the range -10 to 10 add 0.5
// if lesser or greater   add 1/20 of the value
// if zero,               add 0.5
float add_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next += (next / 20.0f);
        } else {
            next += 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next += (-next / 20.0f);
        else {
            if (next < -0.5f)
                next += 0.5f;
            else
                next = 0.0f;
        }
    } else {
        next += 0.5f;
    }
    return next;
}

char *save_file_preferences();
char *get_file_preferences();

bool generate_a_script( char *out_file )
{
    PDELOBJS dob;
    char *cp = GetNxtBuf(); // get buffer NXT_BUF_SIZE(2048) size -see misc_lib
    // ok, generate a script
    int verb5 = ac_verb5();
    std::string base;
    std::string script;
    std::string file;
    FILE *fp = 0;
    size_t max = 0;
    bool ok = false;
    std::string::size_type pos;
#ifdef NEW_DELETE_BEHAVIOUR // generate script
    if (pvDeleteList)
        max = pvDeleteList->size();
#else // !#ifdef NEW DELETE BEHAVIOUR
    if (pvDeleted)
        max = pvDeleted->size();
#endif
    size_t ii, len, res;
    char *inf = get_in_file();  // get the base file name of last in_file
    if (inf && ( max || always_write_script ) ) {
        base = inf;
        pos = base.rfind('.');  // find the last '.'
        if (pos != std::string::npos)
            base = base.substr(0,pos);
        // call output file 'temp<input>.txt'
        script = "temp";
        script += base;
        file = script;
        file += ".script";
        ii = 0;
        // but do NOT overwrite any others - get unique file by adding number
        while (is_file_or_directory((char *)file.c_str()) == MFT_FILE) {
            ii++;
            sprintf(cp,"%s%d.script", script.c_str(), (int)ii);
            file = cp;
        }
        script = file;
        if (out_file) {
            script = out_file;
        }
        fp = fopen(script.c_str(),"w");
        if (fp) {
            // add header to the script file
            len = sprintf(cp,"# script %s %s\n", script.c_str(), Get_Current_UTC_Time_Stg());
            res = fwrite(cp,1,len,fp);
            ok = ((res == len) ? true : false);
            len = sprintf(cp,"# %d deletions\n\n", (int)max);
            if (ok) {
                res = fwrite(cp,1,len,fp);
                ok = ((res == len) ? true : false);
            }
            len = sprintf(cp,"LOAD %s\n\n", ac_in_file);
            if (ok) {
                res = fwrite(cp,1,len,fp);
                ok = ((res == len) ? true : false);
            }
        }
    }

#ifdef NEW_DELETE_BEHAVIOUR // writing deleted to script file - maybe should not use pvDeleteList at all
    for (ii = 0; ii < max; ii++) {
        dob = pvDeleteList->at(ii);
        ACObject *ob = dob->ob;
        len = sprintf(cp,"DELETE %s %d %d\n", ob->name, ob->type, ob->ucount );
        if (verb5) SPRTF("%s",cp);
        if (fp && ok) {
            res = fwrite(cp,1,len,fp);
            ok = ((res == len) ? true : false);
        }
    }
#else
    if (pvDeleted) {
        for (ii = 0; ii < max; ii++) {
            dob = pvDeleted->at(ii);
            ACObject *ob = dob->ob;
            len = sprintf(cp,"DELETE %s %d %d\n", ob->name, ob->type, ob->ucount );
            if (verb5) SPRTF("%s",cp);
            if (fp && ok) {
                res = fwrite(cp,1,len,fp);
                ok = ((res == len) ? true : false);
            }
        }
    }
#endif

    // if we have loaded a file, done some deletions (maybe), 
    // and maybe have written out an ac file
    // then setup a RUN assimp <cmd> to export the JS file.
    // ===================================================
    if (fp && ok) {
        char *of = script_ac_write;
        if (!of)    // no script written file, but maybe
            of = output_ac_file;
        if (of) {
            // TODO: should maybe use the relative name by at least stripping the current ork directory
            // but for now use the full output file name
            len = sprintf(cp,"\nWRITE %s\n", of );
            res = fwrite(cp,1,len,fp);
            ok = ((res == len) ? true : false);

            base = get_base_name(of);   // this will be the base output name
            if (base.size()) {
                pos = base.rfind('.');  // find the last '.'
                if (pos != std::string::npos)
                    base = base.substr(0,pos);
                file = base;
                file += ".js";
                ii = 0;
                // but do NOT overwrite any others - get unique file by adding number
                while (is_file_or_directory((char *)file.c_str()) == MFT_FILE) {
                    ii++;
                    sprintf(cp,"%s%d.js", base.c_str(), (int)ii);
                    file = cp;  // get NEW filename
                }
                // got input = last output (script or otherwise)
                // got js output - unique file in this directory
                len = sprintf(cp,"\nRUN assimp export %s %s -fjs -ptv -gn -l -v -lo -tri\n", of, file.c_str());
                if (ok) {
                    res = fwrite(cp,1,len,fp);
                    ok = ((res == len) ? true : false);
                }
            }
        }
        len = sprintf(cp,"\n# EXIT\n");
        if (ok) {
            res = fwrite(cp,1,len,fp);
            ok = ((res == len) ? true : false);
        }

        len = sprintf(cp,"\n# eof\n");
        if (ok) {
            res = fwrite(cp,1,len,fp);
            ok = ((res == len) ? true : false);
        }
        fclose(fp);
        if (ok)
            SPRTF("Written script to %s\n", script.c_str());
        else
            SPRTF("Failed writing script to %s\n", script.c_str());
    }
    return ok;
}

// /////////////////////////////////////////////////////////////////////////////////
// void ClearDeleteVector()
// and ALSO generate a 'script' file to be able to re-run this session
// /////////////////////////////////////////////////////////////////////////////////
void ClearDeleteVector()
{
#ifdef NEW_DELETE_BEHAVIOUR  // clear delete list, delete list object but not the ac obj
    if (pvDeleteList) {
        size_t max = pvDeleteList->size();
        size_t ii;
        for (ii = 0; ii < max; ii++) {
            PDELOBJS dob = pvDeleteList->at(ii);
            //ACObject *ob = dob->ob;
            delete dob;
        }
        pvDeleteList->clear();
        delete pvDeleteList;
    }
    pvDeleteList = 0;
#else // !#ifdef NEW DELETE BEHAVIOUR
    if (pvDeleted || always_write_script) {
        generate_a_script(); // ok, generate a temp script
    }
    if (pvDeleted) {
        size_t max = pvDeleted->size();
        size_t ii;
        for (ii = 0; ii < max; ii++) {
            PDELOBJS dob = pvDeleted->at(ii);
            ACObject *ob = dob->ob;
            if (parser) {
                parser->ac_object_free2(ob);   // free this and any kids
            } else {
                delete ob; // should NEVER happen this way, but...
            }
            delete dob;
        }
        pvDeleted->clear();
        delete pvDeleted;
    }
    pvDeleted = 0;
#endif // #ifdef NEW DELETE BEHAVIOUR
}

static void clean_up()
{
    ClearDeleteVector();
    clearNameVector();
    if (preview) {
        if (preview->display_list == preview->full_list) {
            get_gl_params_to_gl_parms( &file_parms );   // save user state
        }
        set_gl_paras_to_gl_params( &file_parms );   // restore where it was
        save_file_preferences();
        delete preview;
        preview = 0;
    }
    if (tree) {
        tree->clear();
        delete tree;
        tree = 0;
    }
    if (textOutput2) {
        delete textOutput2;
        textOutput2 = 0;
    }
    if (log_fp) fclose(log_fp);
    log_fp = 0;

    if (ac_in_file) free(ac_in_file);
    ac_in_file = 0;
    if (full_ac_in_file) free(full_ac_in_file);
    full_ac_in_file = 0;

    if (script_file) free(script_file);
    script_file = 0;
    if (script_buf) free(script_buf);
    script_buf = 0;
    if (parser) {
        int fixw = parser->fix_warnings;
        parser->ac_free_objects2();
        if (fixw) 
            SPRTF("NOTE: %d 'fix' me warnings...\n", fixw);
        delete parser;
        parser = 0;
    }
}

static int keyfn ( unsigned char c, int i1, int i2 )
{
    int ret = 0;
    int v, sxyz = 0;
    char *fil  = 0;
    switch (c)
    {
    case 'q':
    case 0x1b:
        SPRTF("Got exit key...\n");
        clean_up();
        pgm_exit ( 0 ) ;
        break;
    case '?':
    case 'h':
        key_help();
        ret = 1;
        break;
    case 'w':
         fil = save_file_preferences();
         if (fil && ac_verb2()) SPRTF("w = Saves prefs for %s\n", fil);
//        v = write_config();
//       if (ac_verb2()) SPRTF("w = Written config %s (%d)\n", get_config_file_name(), v);
         ret = 1;
         break;
    case 'W':
//        v = read_config();
//        if (ac_verb2()) SPRTF("W = Read config %s (%d)\n", get_config_file_name(), v);
        fil = get_file_preferences();
        if (fil && ac_verb2()) SPRTF("W = Read prefs for %s\n", fil);
        ret = 1;
        break;
    case 'f':
    case 'F':
        if (frozen)
            frozen = 0;
        else
            frozen = 1;
        if (ac_verb2()) SPRTF("f = Toggle frozen %d\n", frozen);
        ret = 1;
        break;
    case 's':
        angle_rate -= 1.0;
        if (ac_verb2()) SPRTF("angle_rate = %s\n", gtf(angle_rate));
        ret = 1;
        break;
    case 'S':
        angle_rate += 1.0;
        if (ac_verb2()) SPRTF("angle_rate = %s\n", gtf(angle_rate));
        ret = 1;
        break;
    case 'x':
        //x_trans -= 0.5f;
        x_trans = sub_5_percent(x_trans);
        if (ac_verb2()) SPRTF("x_trans = %s\n", gtf(x_trans));
        ret = 1;
        break;
    case 'X':
        //x_trans += 0.5f;
        x_trans = add_5_percent(x_trans);
        if (ac_verb2()) SPRTF("x_trans = %s\n", gtf(x_trans));
        ret = 1;
        break;
    case 'y':
        //y_trans -= 0.5f;
        y_trans = sub_5_percent(y_trans);
        if (ac_verb2()) SPRTF("y_trans = %s\n", gtf(y_trans));
        ret = 1;
        break;
    case 'Y':
        //y_trans += 0.5f;
        y_trans = add_5_percent(y_trans);
        if (ac_verb2()) SPRTF("y_trans = %s\n", gtf(y_trans));
        ret = 1;
        break;
    case 'z':
        //z_trans -= 0.5f;
        z_trans = sub_5_percent(z_trans);
        if (ac_verb2()) SPRTF("z_trans = %s\n", gtf(z_trans));
        ret = 1;
        break;
    case 'Z':
        //z_trans += 0.5;
        z_trans = add_5_percent(z_trans);
        if (ac_verb2()) SPRTF("z_trans = %s\n", gtf(z_trans));
        ret = 1;
        break;
    case '1':
        x_axis = 1.0;
        y_axis = 0.0;
        z_axis = 0.0;
        sxyz = 1;
        ret = 1;
        break;
    case '2':
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 0.0;
       sxyz = 1;
        ret = 1;
        break;
    case '3':
       x_axis = 0.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
        break;
    case '4':
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 0.0;
        ret = 1;
        break;
    case '5':
       x_axis = 1.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
        break;
    case '6':
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
        break;
    case '7':
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
        break;
    case 'd':
        WritePPM( win_width, win_height, 0 );
        ret = 1;
        break;
    case 'D':
        WritePPM( win_width, win_height, 0, 1 );
        ret = 1;
        break;
    case 'v':
        v = ac_get_verbosity();
        if (v) {
            v--;
            ac_set_verbosity(v);
        }
        ret = 1;
        break;
    case 'V':
        v = ac_get_verbosity();
        v++;
        ac_set_verbosity(v);
        ret = 1;
        break;
    case 'r':
    case 'R':
        reset_defaults();
        break;
    case 'i':
    case 'I':
        if (show_wire_frame)
            show_wire_frame = 0;
        else
            show_wire_frame = 1;
        break;
    }
    if (sxyz && ac_verb2()) SPRTF("%c: set x,y,z axis %s,%s,%s\n", c, gtf(x_axis), gtf(y_axis), gtf(z_axis));
    return ret;
}


void drawscene(int w, int h, int dl, int val)
{
    timer_start();

    double curr = get_seconds();
    double elap = curr - prev_secs;
    time_t currs = time(0);
    GLfloat light_position[] = { -1000.0, 1000.0, 1000.0, 0.0 };


    if (val)
        set_projection(w,h);

    if (show_wire_frame) {
        Enable_WireFrame();
    } else {
        Disable_WireFrame();
    }
    // gluLookAt(0, 1.8, 4, 0, 0, 0, 0, 1, 0);
    gluLookAt(eyex, eyey, eyez, centrex, centrey, centrez, upx, upy, upz);

    glClearColor(0,0,0,0);
    glClearColor(0.5,0.5,0.5,0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glTranslatef ( x_trans, y_trans, z_trans ) ;

    // this can be TOO FAST!
    //rot += 0.01f; // was 1
    if (!frozen) {
        // bump the rotation angle 
        rot += (float) ( elap * angle_rate );
    }

    if (rot >= 360.0) rot -= 360.0f;

    // glRotatef(rot, 0, 1, 0);
    glRotatef(rot, x_axis, y_axis , z_axis );

    glCallList(dl);
    // think these are done by FL????
    // glFlush();
    // glFinish();
    // glutSwapBuffers(); // done automatically, I think

    frametime = timer_stop();   // hmmm, seems this is always 0.0????????
    tottime += frametime;
    framec++;
    if (tottime > 5.0) {
        total_time += tottime;
        if (ac_verb5()) {
    	    SPRTF("%.1f: Approx frames per sec: %.1f (%d)\n", 
                total_time, 1.0/(tottime/framec), frames_per_sec);
        }
        last_tottime = tottime;
        last_framec = framec;
	    tottime = 0;
	    framec = 0;
    }

    frames++;
    if (currs != last_secs) {
        frames_per_sec = frames;
        frames = 0;
        last_secs = currs;
        elap_secs++;
    }
    prev_secs = curr;
}


void PreviewWin::draw()
{
    if (!done_init) {
        SPRTF("Init of draw preview of ac\n");
        init_gfx( w(), h() );
        ACObject *rob = get_current_obj();
        if (rob) {
            full_list = display_list = ac_display_list_render_object(rob);
            if (display_list > 0) {
                SPRTF("Got display_list %d\n", display_list);
                done_gl_init = 1;
            } else {
                SPRTF("ERROR: Failed to get display list!\n");
            }
        }
        done_init = 1;
    }
    if (display_list > 0) {
        int val = 0;
        if (!valid()) {
            val = 1;
        }
        drawscene(w(), h(), display_list, val);
        //draw_overlay();
        //invalidate();
        check_script();
        damage(1); // OK, this causes a re-paint
    }
}


static int is_deletable( Flu_Tree_Browser::Node *n )
{
    Flu_Tree_Browser::Node* pn = n->parent();
    if( !pn ) {
        return 0;
    }
    Flu_Tree_Browser::Node* mn = pn->parent();
    if (!mn) {
        return 0;
    }
    return 1;
}

// sort of get_parent, but really only up one if the parent is
// NOT one of the primary nodes
static Flu_Tree_Browser::Node *get_parent( Flu_Tree_Browser::Node *n )
{
    Flu_Tree_Browser::Node *tn = n->parent();
    if (strcmp(pmat_node,tn->label()) == 0)
        return n;
    else if (strcmp(pobj_node,tn->label()) == 0)
        return n;
    return tn;
}

int not_in_vTNODE(vTNODE &vDel,Flu_Tree_Browser::Node *tn)
{
    size_t max = vDel.size();
    size_t ii;
    for (ii = 0; ii < max; ii++) {
        if (vDel[ii] == tn)
            return 0;   // not not - 0 is it IS in vector
    }
    return 1; // 1 says it is NOT in vector
}

// potential future restore of an object
//char *getTempFileName() 
//    if (fixed_temp_file) { 
char *getTempACFileName()
{
    char *inf = get_in_file();
    if (inf) {
        inf = get_base_name(inf);
    } else {
        inf = GetNxtBuf();
        strcpy(inf,"temp.ac");
    }
    std::string file(inf);
    std::string::size_type pos = file.rfind('.');
    if (pos != std::string::npos)
        file = file.substr(0,pos);
    std::string nfile("temp");
    nfile += file;
    file = nfile;
    file += ".ac";
    int count = 0;
    char *cp = GetNxtBuf();
    while (is_file_or_directory((char *)file.c_str()) == MFT_FILE) {
        count++;
        sprintf(cp,"%s%d.ac", nfile.c_str(), count);
        file = cp;
    }
    strcpy(cp,file.c_str());
    return cp;
}

void writeTempSingle( ACObject *ob_main, ACObject *ob_del )
{
    char *cpf = getTempACFileName();
    bool del = ob_del->deleted;
    ob_del->deleted = false;    // MUST undelete to WRITE object
    writeAC3DSingle( cpf, ob_main, ob_del);
    ob_del->deleted = del;
}


int deleteObject( ACObject *ob, ACObject *ob_del )
{
    int iret = 1;   // set FAILED
    // delete the ob_del from the full objects
#ifdef NEW_DELETE_BEHAVIOUR // mark object as deleted - get new display list
    int index = 0;
    char *cp = GetNxtBuf();
    *cp = 0;
    last_ob = 0;    // clear any last mouse hover object
    if (ob_del->parent) {   // get the PARENT
        ACObject *parent = (ACObject *)ob_del->parent;
        if (parent->num_kids) {
            int n;
            for (n = 0; n < parent->num_kids; n++) {
                if (parent->kids[n] == ob_del) {
                    index = n;
                    break; // found the child to die
                }
            }
            if (n < parent->num_kids) {
                PDELOBJS dob = new DELOBJS;
                if (pvDeleteList == 0)
                    pvDeleteList = new vOBJS;
                ob_del->deleted = true; // mark as DELETED
                dob->ob = ob_del;
                dob->parent = parent;
                dob->index = index;
                dob->count = ob_del->ucount;
                pvDeleteList->push_back(dob);    // keep deleted
                deletions++;    // count a DELETION Node and children
                ACObject *rob = get_current_obj();
                int list = ac_display_list_render_object(rob);
                if (list > 0) {
                    glDeleteLists(preview->full_list, 1);    // delete the current list
                    preview->full_list = preview->display_list = list;
                    if (write_temp_acfile)
                        writeTempAC3DFile(rob);
                    if (write_temp_single)
                        writeTempSingle( rob, ob_del);
                    populate_Tree(); // LAZY way to fox tree
                    iret = 0;   // success in deletion and creating a new display list
                    sprintf(cp,"DELETED %s ok", (ob_del->name ? ob_del->name : "<null>"));
                } else {
                    sprintf(cp,"DELETE %s FAILED! no display list", (ob_del->name ? ob_del->name : "<null>"));
                }
            } else {
                sprintf(cp,"DELETE %s FAILED! not found in %d kids", (ob_del->name ? ob_del->name : "<null>"), n);
            }
        } else {
            sprintf(cp,"DELETE %s FAILED! parent no kids", (ob_del->name ? ob_del->name : "<null>"));
        }
    } else {
        sprintf(cp,"DELETE %s FAILED! no parent", (ob_del->name ? ob_del->name : "<null>"));
    }
    if (*cp) {
        set_info_display(cp);
        SPRTF("%s\n",cp);
    }
#else // !#ifdef NEW DELETE BEHAVIOUR
    int index = 0;
    last_ob = 0;    // clear any last mouse hover object
    if (ob_del->parent) {   // get the PARENT
        ACObject *parent = (ACObject *)ob_del->parent;
        if (parent->num_kids) {
            // going to reduce this parent by this kid
            int n;
            for (n = 0; n < parent->num_kids; n++) {
                if (parent->kids[n] == ob_del) {
                    index = n;
                    break; // found the child to die
                }
            }
            if (n < parent->num_kids) {
                //parser->ac_object_free2(ob_del);   // free this and any kids
                PDELOBJS dob = new DELOBJS;
                if (pvDeleted == 0)
                    pvDeleted = new vOBJS;
                dob->ob = ob_del;
                dob->parent = parent;
                dob->index = index;
                dob->count = ob_del->ucount;
                pvDeleted->push_back(dob);    // keep deleted

                n++;    // move to next
                for (; n < parent->num_kids; n++) {
                    parent->kids[n-1] = parent->kids[n];
                }
                parent->num_kids--; // reduce kid count
                if (parent->num_kids == 0) {
                    // TODO: Should also delete this parent???
                    n = 0;
                }
                deletions++;    // count a DELETION Node and children
                ACObject *rob = get_current_obj();
                int list = ac_display_list_render_object(rob);
                if (list > 0) {
                    glDeleteLists(preview->full_list, 1);    // delete the current list
                    preview->full_list = preview->display_list = list;
                    writeTempAC3DFile(rob);
                    iret = 0;   // success in deletion and creating a new display list
                }
            }
        }
    }
#endif // #ifdef NEW DELETE BEHAVIOUR
    return iret;   // FAILED
}


// examines the selection and if allowed deletes selection from tree
// =================================================================
static void removeCB( Fl_Widget*, void* )
{
    int cnt = 0;
    char *cp = info_buffer;
    vTNODE vNodes, vDel;
    int sel_count = tree->num_selected();
    Flu_Tree_Browser::Node* n = tree->get_selected( 1 );
    if( !n ) {
        fl_alert("Appears no nodes are selected for deletion!");
        return;
    }
    Flu_Tree_Browser::Node* tn = n;
    while (tn) {
        cnt++;
        vNodes.push_back(tn);
        tn = tree->get_selected( cnt+1 ); // get NEXT
    }
    size_t max = vNodes.size();
    size_t ii;
    sprintf(cp,"Got %d selections (%d)\n", (int)max, sel_count);
    for (ii = 0; ii < max; ii++) {
        tn = vNodes[ii];
        if (is_deletable(tn)) {
            tn = get_parent(tn);
            if (tn && not_in_vTNODE(vDel,tn)) {
                vDel.push_back(tn);
                sprintf(EndBuf(cp),"%s (ok)\n", tn->label());
            }
        } else {
            sprintf(EndBuf(cp),"%s NO DELETE!\n", tn->label());
        }
    }
    max = vDel.size();
    sprintf(EndBuf(cp),"DELETIONS: %d for deletion\n", (int)max);
    for (ii = 0; ii < max; ii++) {
        tn = vNodes[ii];
        tn->select(1);
        cnt = showSelected(cp,tn,0);  // find it, but NO rendering - it is going to be deleted
        tree->remove(tn);   // remove from tree
        // now REMOVE it from the object, and fix any kids counts - quite tought
        // =====================================================================
        if (cnt == 0) {
            // object is in ob_found
            ACObject *rob = get_current_obj();
            ACObject *fob = get_found_obj();
            sprintf(EndBuf(cp),"Delete %s\n", get_obj_name(fob));
            deleteObject( rob, fob );

        }
    }
    set_info_display(cp);
    //tree->redraw();
#ifndef ADD_ACTIONS_2_MENU
    //removeBtn->color(0xff000000);
    //removeBtn->redraw_label();
#endif
}

//static int writeObject(ACObject *ob, FILE *fp, int add_kids = 1, int kid_cnt = 0, int alert = 0);
// Writing object, perhaps after some deletions - change 'world' name to 'our' name
static int writeObject(ACObject *ob, FILE *fp, int add_kids, int kid_cnt, int alert)
{
    int iret = 0;
    char *cp = info_buffer;
    char *obj = (char *)ob->me->ac_objecttype_to_string2( ob->type );
    size_t len = sprintf(cp,"OBJECT %s\n", obj);
    size_t res;
    int i;
    int verb9 = ac_verb9();
    int is_world = (strcmp(obj,"world") ? 0 : 1);

#ifdef NEW_DELETE_BEHAVIOUR // write an object ONLY if NOT deleted
    if (ob->deleted) {
        if (ob->type == OBJECT_GROUP) {
            add_kids = 0;   // if a GROUP is DELETED, then all its children are deemed DELETED
        }
        goto Dn_Write;
    }
#endif
    res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"ERROR 1: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
        SPRTF("%s\n",cp);
        if (alert) fl_alert("%s",cp);
        return 1;
    }
    if (verb9)SPRTF("%s",cp);
    if (ob->name || is_world) {
        if (is_world) {
            len = sprintf(cp,"name \"%s\"\n", "ac3d_browser2"); // TODO: could add the input file name like "ac3d_browser2_707_ac"
        } else {
            len = sprintf(cp,"name \"%s\"\n", ob->name);
        }
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"ERROR 2: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
            SPRTF("%s\n",cp);
            if (alert) fl_alert("%s",cp);
            return 2;
        }
        if (verb9)SPRTF("%s",cp);
    }
    if (ob->got_loc) {
        len = sprintf(cp,"loc %s %s %s\n", gtf(ob->loc.x), gtf(ob->loc.y), gtf(ob->loc.z) );
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"ERROR 3: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
            SPRTF("%s\n",cp);
            if (alert) fl_alert("%s",cp);
            return 3;
        }
        if (verb9)SPRTF("%s",cp);
    }

    // 20130731 - add texture name to ac output should be quoted string
    if (ob->texture_name) {
        len = sprintf(cp,"texture \"%s\"\n", ob->texture_name);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"ERROR 4: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
            SPRTF("%s\n",cp);
            if (alert) fl_alert("%s",cp);
            return 4;
        }
    }

    // 20130731 - add crease %f, if it was in input file
    if (ob->had_crease) {
        len = sprintf(cp,"crease %s\n", gtf(ob->crease));
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"ERROR 4: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
            SPRTF("%s\n",cp);
            if (alert) fl_alert("%s",cp);
            return 4;
        }

    }
    if (ob->num_vert) {
        len = sprintf(cp,"numvert %d\n", ob->num_vert);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"ERROR 4: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
            SPRTF("%s\n",cp);
            if (alert) fl_alert("%s",cp);
            return 4;
        }
        if (verb9)SPRTF("%s",cp);
        for (i = 0; i < ob->num_vert; i++) {
            ACVertex p = ob->vertices[i];
            len = sprintf(cp,"%s %s %s\n", gtf(p.x), gtf(p.y), gtf(p.z)); // USE_GTF
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"ERROR 5: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
                SPRTF("%s\n",cp);
                if (alert) fl_alert("%s",cp);
                return 5;
            }
            if (verb9)SPRTF("%s",cp);
        }
    }
    if (ob->num_surf) {
        len = sprintf(cp,"numsurf %d\n", ob->num_surf);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"ERROR 6: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
            SPRTF("%s\n",cp);
            if (alert) fl_alert("%s",cp);
            return 6;
        }
        if (verb9)SPRTF("%s",cp);
        for (i = 0; i < ob->num_surf; i++) {
	        ACSurface *s = &ob->surfaces[i];
            len = sprintf(cp,"SURF 0x%02x\n", s->flags);
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"ERROR 7: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
                SPRTF("%s\n",cp);
                if (alert) fl_alert("%s",cp);
                return 7;
            }
            if (verb9)SPRTF("%s",cp);
            //len = sprintf(cp,"mat %d\n", s->mindx);
            // but if writing just ONE object, then have ONLY written the 
            // MATERIAL it uses, so need to get a new releative index
            // this should be put in a temp index
            len = sprintf(cp,"mat %d\n", 
                (add_kids ? s->mat : s->tmpindx) );   // TODO: CHECK THIS??? rel index in this file = s->mat = mindx + startmatindex;
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"ERROR 8: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
                SPRTF("%s\n",cp);
                if (alert) fl_alert("%s",cp);
                return 8;
            }
            if (verb9)SPRTF("%s",cp);
            len = sprintf(cp,"refs %d\n", s->num_vertref);
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"ERROR 9: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
                SPRTF("%s\n",cp);
                if (alert) fl_alert("%s",cp);
                return 9;
            }
            if (verb9)SPRTF("%s",cp);

            int vr;
	        for (vr = 0; vr < s->num_vertref; vr++) {
                len = sprintf(cp,"%d %s %s\n", s->vertref[vr], gtf(s->uvs[vr].u), gtf(s->uvs[vr].v));
                res = fwrite(cp,1,len,fp);
                if (res != len) {
                    sprintf(cp,"ERROR 10: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
                    SPRTF("%s\n",cp);
                    if (alert) fl_alert("%s",cp);
                    return 10;
                }
                if (verb9)SPRTF("%s",cp);
            }
        }
    }
    if (add_kids) {
#ifdef NEW_DELETE_BEHAVIOUR // write an object ONLY if NOT deleted - can not use ob->num_kids - have to count not deleted only
        kid_cnt = 0;
        for (i = 0; i < ob->num_kids; i++) {
            if (!ob->kids[i]->deleted)
                kid_cnt++;
        }
        len = sprintf(cp,"kids %d\n", kid_cnt);
#else
        len = sprintf(cp,"kids %d\n", ob->num_kids);
#endif
    } else {
        len = sprintf(cp,"kids %d\n", kid_cnt);
    }
    res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"ERROR 11: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
        SPRTF("%s\n",cp);
        if (alert) fl_alert("%s",cp);
        return 11;
    }
    if (verb9)SPRTF("%s",cp);

#ifdef NEW_DELETE_BEHAVIOUR // write an object ONLY if NOT deleted - label to avoid write
Dn_Write:
#endif
    if (add_kids) {
        for (i = 0; i < ob->num_kids; i++) {
            iret = writeObject( ob->kids[i], fp, add_kids, kid_cnt, alert ); // writeObject: resursively write
            if (iret)
                break;
        }
    }
    return iret;
}

int write_a_Material(ACMaterial *m, FILE *fp, int alert = 0);
int write_a_Material(ACMaterial *m, FILE *fp, int alert)
{
    char *cp = info_buffer;
    size_t len = sprintf(cp,"MATERIAL \"%s\" rgb %s %s %s ", (m->name ? m->name : "NoName"),
                gtf(m->rgb.r), gtf(m->rgb.g), gtf(m->rgb.b) );
    len += sprintf(EndBuf(cp),"amb %s %s %s ", gtf(m->ambient.r), gtf(m->ambient.g), gtf(m->ambient.b) );
    len += sprintf(EndBuf(cp),"emis %s %s %s ", gtf(m->emissive.r), gtf(m->emissive.g), gtf(m->emissive.b) );
    len += sprintf(EndBuf(cp),"spec %s %s %s ", gtf(m->specular.r), gtf(m->specular.g), gtf(m->specular.b) );
    len += sprintf(EndBuf(cp),"shi %s trans %s\n", gtf(m->shininess), gtf(m->transparency) );
    size_t res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"ERROR 12: Write of object to file FAILED! req=%d res=%d", (int)len, (int)res );
        SPRTF("%s\n",cp);
        if (alert) fl_alert("%s",cp);
        return 12;
    }
    if (ac_verb9()) SPRTF("%s\n", cp);
    return 0;
}

static int writeMaterials( ACObject *ob, FILE *fp )
{
    int iret = 0;
    if (!parser)
        return 2;
    // MATERIAL "DefaultWhite" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 64  trans 0
    /* stored as 
        typedef struct Material_t {
            ACCol rgb; / * diffuse * /
            ACCol ambient;
            ACCol specular;
            ACCol emissive;
            float shininess;
            float transparency;
            char *name;
        } ACMaterial, *PACMaterial;
        */
    int i;
    //char *cp = buffer;
    int max = parser->ac_palette_get_mat_size2();
    for (i = 0; i < max; i++) {
        ACMaterial * m = parser->ac_palette_get_material2(i);
        if (m) {
            iret = write_a_Material(m,fp);
            if (iret)
                break;
        }
    }
    return iret;
}

#ifdef _MSC_VER
#define PATH_SEP    '\\'
#define BAD_SEP     '/'
#else
#define PATH_SEP    '/'
#define BAD_SEP     '\\'
#endif

void ensure_os_seps(std::string & s)
{
    //std::string::size_type pos;
    //while ((pos = s.find(BAD_SEP)) != std::string::npos) {
    std::replace(s.begin(), s.end(), BAD_SEP, PATH_SEP);
}

static int next_multi = 0;
static int next_mat = 0;
static int add_int_2_vec( int m, vMOFFS &vmaps )
{
    size_t max = vmaps.size();
    size_t ii;
    MatOff mo;
    for (ii = 0; ii < max; ii++) {
        mo = vmaps[ii];
        if (m == mo.i)
            return 0;
    }
    mo.i = m; // rel index in this file = s->mat = mindx + startmatindex;
    mo.n = mat_ind++; // 
    vmaps.push_back(mo); // not there, add it
    return 1;
}

static void get_vmaps( ACObject *ob, vMOFFS &vmaps )
{
static int next_tmp_index = 0;
static int init_tmp_index = 0;
    int i;
    next_tmp_index = 0; // start indexing from ZERO
    init_tmp_index = 0;
    for (i = 0; i < ob->num_surf; i++) {
        ACSurface *s = &ob->surfaces[i];
        // add_int_2_vec( s->mindx, vmaps );
        s->tmpindx = next_tmp_index; // set a 'temp' index
        if (add_int_2_vec( s->mat, vmaps )) { // rel index in this file = s->mat = mindx + startmatindex;
            if (init_tmp_index) // only bump for additions AFTER adding the FIRST
                next_tmp_index = (int)(vmaps.size() - 1);
            init_tmp_index++;
        }
    }
}

// Output of a set of materials
int write_mats_per_vec(ACObject *ob, vMOFFS &vmaps, FILE *fp)
{
    if (!parser)
        return 2;
    size_t max = vmaps.size();
    size_t ii;
    MatOff mo;
    int max_mats = parser->ac_palette_get_mat_size2();
    for (ii = 0; ii < max; ii++) {
        mo = vmaps[ii];
        if (mo.i < max_mats) {
            ACMaterial * m = parser->ac_palette_get_material2(mo.i);
            if (m) {
                if (write_a_Material(m,fp))
                    return 1;
                mo.n = next_mat;    // set new offset in this file
                next_mat++;
                vmaps[ii] = mo;     // update record
            }
        }
    }
    return 0;
}

char *getTempFileName() 
{
    static char _s_tmpname[MAX_PATH];
    char *tn = _s_tmpname;
    if (fixed_temp_file) { 
        strcpy(tn,"tempac3d.ac");
        return tn;
    }
#ifdef _MSC_VER
    //  Gets the temp path env string (no guarantee it's a valid path).
    static char _s_tmppath[MAX_PATH];
    char *cp = _s_tmppath;
    int dwRetVal = GetTempPath(MAX_PATH,cp);          // length of the buffer
    if (dwRetVal > MAX_PATH || (dwRetVal == 0)) {
        SPRTF("Get temp path FAILED!\n");
        return 0;
    }
    //  Generates a temporary file name. 
    dwRetVal = GetTempFileName(cp, // directory for tmp files
                              "TEMP",     // temp file name prefix 
                              0,          // create unique file name 
                              tn);  // buffer for name 
    if (dwRetVal == 0) {
        SPRTF("Get temp name FAILED!\n");
        return 0;
    }
 #else
    strcpy(tn,"/tmp/tempac3d.ac");
 #endif
    return tn;
}

/* ----------------------------------------------------------
    int writeAC3DSingle( char *cpf, ACObject *bob, ACObject *ob)

    Write an AC3D file for a SINGLE object
   ---------------------------------------------------------- */
int writeAC3DSingle( char *cpf, ACObject *bob, ACObject *ob)
{
    char *cp = GetNxtBuf();
    FILE *fp = fopen(cpf,"w");
    if (!fp) {
        sprintf(cp,"Write object to file\n[%s]\nERROR: Unable to create/truncate file!",cpf);
        fl_alert("%s",cp);
        return 1;
    }

    strcpy(cp,"AC3Dd\n");
    size_t len = strlen(cp);
    size_t res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"Write object to file\n[%s]\nERROR: Write file FAILED!",cpf);
        fl_alert("%s",cp);
        fclose(fp);
        return 1;
    }

    vMOFFS vmaps;
    get_vmaps( ob, vmaps );
    if (write_mats_per_vec(bob, vmaps, fp)) {
        fclose(fp);
        return 1;
    }

    strcpy(cp,"OBJECT world\nkids 1\n");
    len = strlen(cp);
    res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"Write object to file\n[%s]\nERROR: Write file FAILED!",cpf);
        fl_alert("%s",cp);
        fclose(fp);
        return 1;
    }
    // write this object without any kids
    if (writeObject(ob, fp, 0)) {   // writeAC3DSingle - ie no kids
        fclose(fp);
        return 1;
    }
    fclose(fp);
    return 0;
}

// Write each object to a separate .ac file
static int writeMulti(ACObject *bob, std::string base, ACObject *ob) 
{
    //char *cp = GetNxtBuf();
    char *cpf = inp_file_buffer;
    if (ob->num_vert && ob->num_surf) {
        strcpy(cpf,base.c_str());
        next_multi++;
        sprintf(EndBuf(cpf),"%d.ac", next_multi);
        if (writeAC3DSingle(cpf, bob, ob))
            return 1;
    }

    int n;
    for (n = 0; n < ob->num_kids; n++) {
        if (writeMulti(bob, base, ob->kids[n]))
            return 1;
    }
    return 0;
}

// write the object back to an ac file
static void writeMultiAC(char *outfile)
{
    ACObject *rob = get_current_obj();
    if (!rob)
        return;
    if (!outfile || (*outfile == 0))
        return;
    next_multi = 0;
    next_mat = 0;
    mat_ind = 0;
    std::string s(outfile);
    std::string::size_type pos = s.rfind('.');
    if (pos != std::string::npos) {
        ensure_os_seps(s);
        std::string::size_type pos2 = s.rfind(PATH_SEP);
        if ((pos2 == std::string::npos)||(pos2 < pos))
            s = s.substr(0,pos);
    }

    writeMulti(rob,s,rob);

    //char *cp = GetNxtBuf();
    //FILE *fp = fopen(cpf,"w");
    //if (!fp) {
    //    sprintf(cp,"Write %s object to file\n[%s]\nERROR: Unable to create/truncate file!",
    //        (deletions ? "modified" : "unmodified"),
    //        cpf);
    //    fl_alert("%s",cp);
    //    return;
    //}

}

static int writeAC3D( char * cpf, ACObject *ob, int saveprefs = 1, int alert = 0 );
static int writeAC3D( char * cpf, ACObject *ob, int saveprefs, int alert )
{
    int verb9 = ac_verb9();
    if (!cpf || (*cpf == 0)) {
        SPRTF("ERROR: writeAC3D: passed a <null> file pointer!\n");
        return 1;
    }
    char *cp = GetNxtBuf();
    FILE *fp = fopen(cpf,"w");
    if (!fp) {
        if (alert) {
            sprintf(cp,"Write %s object to file\n[%s]\nERROR: Unable to create/truncate file!",
                (deletions ? "modified" : "unmodified"),
                cpf);
            SPRTF("%s\n",cp);
            fl_alert("%s",cp);
        } else {
            SPRTF("Write %s object to file\n[%s]\nERROR: Unable to create/truncate file!",
                (deletions ? "modified" : "unmodified"),
                cpf);
        }
        return 1;
    }
    if (verb9) SPRTF("Writing to [%s]\n", cpf);
    strcpy(cp,"AC3Dd\n");
    size_t len = strlen(cp);
    size_t res = fwrite(cp,1,len,fp);
    if (res != len) {
        if (alert) {
            sprintf(cp,"Write %s object to file\n[%s]\nERROR: Write file FAILED!",
                (deletions ? "modified" : "unmodified"),
                cpf);
            SPRTF("%s\n",cp);
            fl_alert("%s",cp);
        } else {
            SPRTF("Write %s object to file\n[%s]\nERROR: Write file FAILED!",
                (deletions ? "modified" : "unmodified"),
                cpf);
        }
        fclose(fp);
        return 1;
    }
    if (writeMaterials(ob,fp)) {
        if (alert) {
            sprintf(cp,"Write %s object to file\n[%s]\nERROR: Write file FAILED!",
                (deletions ? "modified" : "unmodified"),
                cpf);
            SPRTF("%s\n",cp);
            fl_alert("%s",cp);
        } else {
            SPRTF("Write %s object to file\n[%s]\nERROR: Write file FAILED!",
                (deletions ? "modified" : "unmodified"),
                cpf);
        }
        fclose(fp);
        return 1;
    }
    int ret = writeObject(ob,fp);  // recursively write objects with kids
    fclose(fp);
    sprintf(cp,"Written object to file\n[%s]\n%s\n",cpf,
        (ret ? "But appears a write failure!" : "appears successful") );
    set_info_display(cp);

    if (!ret && saveprefs) {
        // store last ac output
        set_output_file(cpf);
    }

    if (ac_verb1() || ret) SPRTF("%s",cp);
    return ret;   // 0=SUCCESS, 1=FAILED
}

static void writeTempAC3DFile(ACObject *rob)
{
    char *tf = (char *)"temptemp.ac";
    writeAC3D(tf,rob,0,0);
}


static void writeACCB( Fl_Widget*, void* )
{
    ACObject *rob = get_current_obj();
    if (!rob)
        return;

#ifdef ADD_NEW_NODE2
    char *cpf = get_input_widget();
#else
    char *cpf = get_new_ac_out();
#endif // #ifdef ADD_NEW_NODE2
    if (!cpf || (*cpf == 0))
        return;

    char *cp = GetNxtBuf();
    strcpy(cp,cpf);
    std::string file(cp);
    ensure_os_seps(file);
    strcpy(cp,file.c_str());
    
    if (multi_write) {
        writeMultiAC(cp);
        return;
    }
    if (writeAC3D( cp, rob ) == 0) {
        set_output_file(cp); // successful write - store last user OUTPUT
        set_output_path(cp);
    }
}

#ifdef ADD_PREF_DIALOG
static Fl_Double_Window *dlg = 0;
static GL_PARMS dlg_parms;
static void preferencesCB( Fl_Widget*, void* )
{
    int cxChild = DLG_WIDTH;    //rcChild.right - rcChild.left;
    int cyChild = DLG_HEIGHT;   // rcChild.bottom - rcChild.top;
    int cxParent = win_width;   // rcParent.right - rcParent.left;
    int cyParent = win_height;  // rcParent.bottom - rcParent.top;
    // Calculate new X position, then adjust for screen
    //xNew = rcParent.left + ((cxParent - cxChild) / 2);
    int xNew = pos_x + ((cxParent - cxChild) / 2);
    if (xNew < 0)
        xNew = 0;
    //else if ((xNew + cxChild) > cxScreen)
    //    xNew = cxScreen - cxChild;
    // Calculate new Y position, then adjust for screen
    //yNew = rcParent.top  + ((cyParent - cyChild) / 2);
    int yNew = pos_y  + ((cyParent - cyChild) / 2);
    if (yNew < 0)
        yNew = 0;
    //else if ((yNew + cyChild) > cyScreen)
    //    yNew = cyScreen - cyChild;

    if (!dlg) dlg = make_dialog_window();
    if (dlg) {
        get_gl_params_to_gl_parms(&dlg_parms);
        set_dialog_values(&dlg_parms);
        dlg->set_modal();
        //dlg->resize(pos_x,pos_y, DLG_WIDTH, DLG_HEIGHT);
        dlg->resize(xNew,yNew, DLG_WIDTH, DLG_HEIGHT);
        dlg->show();
    }
}
#endif

//    refreshBtn = new Fl_Button( 10, y_val, BUTTON_WIDTH, BUTTON_HEIGHT, "Refresh" );
//    refreshBtn->callback( refreshCB, NULL );
static void refreshCB( Fl_Widget*, void* )
{
    char *cp = info_buffer;
    tree->clear();  // clear all and start again the tree
    tree->label( proot_node );
    tree->redraw();
    sprintf(cp,"Moment loading\n[%s]\n", ac_in_file);
    set_info_display(cp);
 
    if (parser) {
        parser->ac_free_objects2();  // free the current
        delete parser;
        parser = 0;
    }
    clearNameVector();  // clear named vectors

    parser = new ac3d2gl;
    ACObject *rob = parser->ac_load_ac3d2((char *)ac_in_file);    // RELOAD file
    if (!rob) {
		SPRTF("%s: ERROR: failed to load %s! exiting 1.\n", mod_name, ac_in_file);
        pgm_exit(1);
    }
    set_current_obj(rob);   // RELOAD - set current object
    populate_Tree();    // populate tree
    sprintf(cp,"Reloaded file\n[%s]\n", ac_in_file);
    set_info_display(cp);
#ifndef ADD_ACTIONS_2_MENU
    removeBtn->color(0xff000000);
    removeBtn->redraw();
#endif
    deletions = 0;  // clear any deletions
    last_ob = 0;    // clear any mouse hover
}

static int export_to_threejs( ACObject *rob, std::string &file, int alert = 1 );
static int export_to_threejs( ACObject *rob, std::string &file, int alert )
{
    char *tb = GetNxtBuf();
    ensure_os_seps(file);
    FILE *fp = fopen(file.c_str(),"w");
    if (!fp) {
        sprintf(tb,"Unable to open/truncate/create file %s\n", file.c_str());
        strcat(tb,"So no possible EXPORT to Three.js!\n");
        SPRTF("%s",tb);
        if(alert) fl_alert("%s",tb); // complain!!!
        return 1;
    }
    if (parser->ac_export_threejs(rob,fp)) {
        fclose(fp);
        sprintf(tb,"Failed writing file %s\n", file.c_str());
        strcat(tb,"So EXPORT to Three.js FAILED!\n");
        SPRTF("%s",tb);
        if(alert) fl_alert("%s",tb); // FAILED to write file - complain
        return 1;
    }
    fclose(fp);
    out_filejs = strdup(file.c_str());
    sprintf(tb,"Written object to file %s\n", file.c_str());
    SPRTF("%s",tb);
    return 0;
}

const char *file_patternjs = "*.js";
void get_out_js_file( std::string & file )
{
    char *of = get_curr_output_file();
    // get file name to export to
    if (last_outjs) {
        // first choice - last js output
        file = last_outjs;
    } else if (script_ac_write) {
        file = script_ac_write; // is last ac out
        std::string::size_type pos = file.rfind('.');  // find the last '.'
        if (pos != std::string::npos)
            file = file.substr(0,pos);
        file += ".js";
    } else if (of) {
        file = of; // is last ac out
        std::string::size_type pos = file.rfind('.');  // find the last '.'
        if (pos != std::string::npos)
            file = file.substr(0,pos);
        file += ".js";
    } else {
        if (ac_in_file) 
            file = get_path_and_name( (char *)ac_in_file );
        else
            file = (char *)get_path_and_name((char *)out_filejs);
        if (file.size())
            file += ".js";
    }
}

// until ac3d_browser2 is FIXED, use assimp()
static void export3J( Fl_Widget*, void* )
{
    ACObject *ob = get_current_obj();
    if (!ob) {
        SPRTF("EXPORT FAILED! No current object loaded\n");
        return;
    }
    // get file name to export to
    std::string file;
    get_out_js_file( file );
    const char *nf = flu_save_chooser( "Three.js File Name", file_patternjs, 
        (file.size() ? file.c_str() : 0) );
    if (nf) {
        file = nf;
        if (is_file_or_directory((char *)nf) == MFT_FILE)
            rename_to_old_bak(nf);  // rename any existing file to .old or .bak
        int res = export_to_threejs( ob, file );
        if (res) {
            SPRTF("EXPORT to %s FAILED\n", file.c_str());
        } else {
            if (ac_verb1()) SPRTF("EXPORT to %s succeeded.\n", file.c_str());
            set_last_js_out((char *)file.c_str());
        }
    }
}

static void exportCB( Fl_Widget*, void* )
{
    static bool dn_assimp_check = false;
    char *cp = GetNxtBuf();
    bool assimp = true;
    // only check this once
    if (!dn_assimp_check) {
        sprintf(cp,"assimp --help");
        if (system(cp)) {
            SPRTF("Warning: program assimp may not be in PATH!\n");
            assimp = false;
            ask_assimp_run = true;
        }
        dn_assimp_check = true;
    }
    // get file name to export to
    std::string file;
    get_out_js_file( file );
    const char *nf = flu_save_chooser( "Three.js File Name", file_patternjs, 
        (file.size() ? file.c_str() : 0) );
    if (nf) {
        const char *ac_file;
        ac_file = output_ac_file;
        if (!ac_file)
            ac_file = script_ac_write;
        if (!ac_file) {
            ac_file = last_out_path;
            nf = strdup(nf);    // this is needed since static buffer will be reused
            ac_file = flu_file_chooser( "Choose ac file to export", "*.ac", ac_file );
        }

        if (!ac_file)
            return;

        // build the export command
        sprintf(cp,"assimp export %s %s -fjs -ptv -gn -l -v -lo -tri", ac_file, nf );
        SPRTF("CMD: %s\n",cp);
        if (ask_assimp_run) {
            char *ask = GetNxtBuf();
            sprintf(ask,"Run assimp with the following command?\n%s", cp);
            if (!assimp) {
                strcat(ask,"\nNote that assimp failed to run!\r\nIs it in your PATH?\n");
                strcat(ask,"Check: http://assimp.sourceforge.net/main_downloads.html\n");
                strcat(ask,"and it needs to include the export to Three.js modules.\n");
            }
            int ok = fl_choice("%s", "NO!", "Run", 0, ask);
            if (ok != 1)
                return;
        }
        if (is_file_or_directory((char *)nf) == MFT_FILE)
            rename_to_old_bak(nf);  // rename any existing file to .old or .bak

        int res = system(cp);

        cp = GetNxtBuf();
        if (is_file_or_directory((char *)nf) == MFT_FILE)
            sprintf(cp,"Appears %s was successfully generated (%d)\n", nf, res);
        else 
            sprintf(cp,"Appears %s was NOT generated (%d)\n", nf, res);
        SPRTF("%s",cp);
        set_info_display(cp);   // put results in info display
    }
}


typedef struct tagFINDOBJS {
    int type;
    int count;
    int found;
    ACObject *obj;
    int index;
}FINDOBJS, *PFINDOBJS;


int findObject(char *cp, const char *delname, ACObject *ob, PFINDOBJS pfo);
/* ----------------------------------------------------------------------------------
    int findObject(char *cp, const char *delname, ACObject *ob, int type, int count = 0);
    cp = information buffer only
    ob = object structure to search
    type = type of object
    count = if not unique name, count offset into find
   --------------------------------------------------------------------------------- */

int findObject(char *cp, const char *delname, ACObject *ob, PFINDOBJS pfo)
{
    if (ob->name) {
        if ( (strcmp(delname,ob->name) == 0) &&
             (ob->type == pfo->type))
        {
            // maybe this is it
            if (pfo->found >= pfo->count) {
                // this is our object
                found_obj.obj_found = ob;
                found_obj.index = -1;
                pfo->obj = ob;
                pfo->index = -1;
                return 1;   // name AND type AND count match
            }
            pfo->found++;
        }
    }
    int i;
    for (i = 0; i < ob->num_kids; i++) {
        if (findObject( cp, delname, ob->kids[i], pfo )) {
            found_obj.index = i;
            return 1;
        }
    }
    return 0;
}


// this is a single OBJECT, with vertices and SURF
int renderObject( ACObject *ob, ACObject *ob2 )
{
    if (ob2->num_vert && ob2->num_surf) {
        char *tn = getTempFileName();
        if (!tn) return 1;
        SPRTF("Writing to temp file [%s]\n",tn);
        if (writeAC3DSingle(tn, ob, ob2)) {
            DeleteFile(tn);
            return 1;
        }
        // have an AC3D file to load
        int list = ac_load_and_render_ac3d(tn, &found_parms, &found_bb);
        if (list <= 0) {
            DeleteFile(tn);
            return 1;
        }
        preview->select_list = preview->display_list;
        if (preview->display_list == preview->full_list) {
            get_gl_params_to_gl_parms( &file_parms );   // save user state
        }
        preview->display_list = list;
        SPRTF("Specifications of %s OBJECT kids %d vert %d\n",
            (ob2->name ? ob2->name : "<blank>"), 
            ob2->num_kids, ob2->num_vert );
        show_bb(&found_bb);
        show_gl_parms(&found_parms);
        reset_defaults(&found_parms);   // set drawing size position

        if (delete_temp_files)
            DeleteFile(tn);
        return 0;
    }
    return 1;
}

int findDelObject(char *cp, const char *delname, const char *objname, int rend)
{
    int iret = 1;
    int i, max;
    if (!parser)
        return iret;
    if (strcmp(objname,pmat_node) == 0) {
        strcat(cp,"Finding 'MATERIAL' ");
        max = parser->ac_palette_get_mat_size2();
        for (i = 0; i < max; i++) {
            ACMaterial * m = parser->ac_palette_get_material2(i);
            if (m && m->name ) {
                if (strcmp(delname,m->name) == 0) {
                    sprintf(EndBuf(cp)," at %d", i);
                    break;
                }
            }
        }
        if (i >= max) {
            // TODO: do do maybe a compound name
            strcat(cp,"NOT FOUND!");
        }
        strcat(cp,"\n");
    } else if (strcmp(objname,pobj_node) == 0) {
        // strip OFF the 'type' (world/poly/etc) to get name
        FINDOBJS fo;
        fo.count = 0;
        fo.found = 0;
        fo.type = OBJECT_NORMAL; // = 0
        std::string s(delname);
        std::string::size_type pos = s.find(' ');
        std::string stype;
        if (pos != std::string::npos) {
            stype = s.substr(0,pos);
            s = s.substr(pos+1);
            fo.type = parser->string_to_objecttype2((char *)stype.c_str());
        }
        // ========================================================
        pos = s.find('[');
        if (pos != std::string::npos) {
            std::string scnt = s.substr(pos+1);
            fo.count = atoi(scnt.c_str());
            s = s.substr(0,pos);    // trim back the name
        }
        sprintf(EndBuf(cp),"'OBJECT' %s %s\n", s.c_str(), stype.c_str());
        //if (findObject( cp, delname, rob)) {
        ACObject *rob = get_current_obj();
        if (rob && findObject( cp, s.c_str(), rob, &fo)) {
            if (rend) {
                if (renderObject( rob, get_found_obj() )) {
                    strcat(cp,"FOUND but render FAILED!\n");
                } else {
                    strcat(cp,"FOUND and rendered.\n");
                    iret = 0;
                }
            } else {
                strcat(cp,"FOUND no render.\n");
                iret = 0;
            }
        } else {
            // TODO: maybe a unique generated name
            sprintf(EndBuf(cp),"NOT FOUND %s!\n",delname);
        }
    } else {
        strcat(cp,"ERROR: Is not known!\n");
    }
    return iret;    // only ZERO is 100% success
}


void rest_to_full_display(char *cp)
{
    if ((preview->full_list > 0) &&
        (preview->full_list != preview->display_list)) {
        // TODO: Should DELETE this list
        if (preview->display_list > 0)
            glDeleteLists(preview->display_list, 1);    // delete the current list
        preview->display_list = preview->full_list;
        //reset_defaults(); // not exactly
        set_gl_paras_to_gl_params( &file_parms );   // restore where it was
        if (cp)
            strcat(cp,"Reset to full file\n");
    }
}

// learn some things about the node
#define MX_NODES 4
static int showSelected(char *cp, Flu_Tree_Browser::Node *in_n, int rend)
{
    int i;
    int upd = 0;
    //char *cp = buffer;
    Flu_Tree_Browser::Node *n = in_n;
    //ACObject *ob = (ACObject *) n->user_data();
    Flu_Tree_Browser::Node *np[MX_NODES];
    const char *names[MX_NODES];
    const char *delname;
    const char *objname;    // either 'MATERIAL' or 'OBJECT'
    int cnt = 0;
    // int kids = n->children();
    int dep = n->depth();   // depth in tree
    int ind = n->index();   // -1 if parent, else index into child list
    const char *lab = n->label();
    np[cnt] = n;
    names[cnt] = lab;
    cnt++;
    // walk back up the tree to the root
    for ( ; cnt < MX_NODES; cnt++ ) {
        n = n->parent();
        if (n) {
            np[cnt] = n;
            names[cnt] = n->label();
        } else {
            break;
        }
    }
    sprintf(EndBuf(cp),"dep %d, ind %d, cnt %d\n", dep, ind, cnt);

    for (i = 0; i < cnt; i++) {
        sprintf(EndBuf(cp),"%s: %s\n",
            ((cnt == 1) ? "R" : (cnt == 2) ? ((i == 0) ? "P" : "R") :
             (cnt == 3) ? ((i == 0) ? "N" : (i == 1) ? "P" : "R") :
             ((i == 0) ? "N" : (i == 1) ? "N" : (i == 2) ? "P" : "R")),
            names[i]);
    }
    if (cnt == 1) {
        sprintf(EndBuf(cp),"ROOT node - NO deletion!\n");
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0xff000000);
#endif
        //in_n->deactivate(); // hmmm, can't do this - need it selected to allow open node
        upd = 1;
    } else if (cnt == 2) {
        sprintf(EndBuf(cp),"PRIMARY node - NO deletion\n");
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0xff000000);
#endif
        //in_n->deactivate();
        upd = 1;
    } else if (cnt == 3) {
        delname = names[0];
        objname = names[1];
        n = np[0];
        sprintf(EndBuf(cp),"DN: %s\n", delname);
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0x00ff0000);
#endif
        upd = 2;
    } else if (cnt > 3) {
        delname = names[1];
        objname = names[2];
        n = np[1];
        sprintf(EndBuf(cp),"DN: %s\n", delname);
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0x00ff0000);
#endif
        upd = 2;
    }

    int reset_list = 1;
    int iret = 1;
    if (upd) {
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->redraw_label();
#endif
        if (upd == 2) {
            if (findDelObject(cp,delname,objname,rend)) {
                // failed to render it... put back full
                sprintf(EndBuf(cp),"P: %s FULL\n", objname);
            } else {
                sprintf(EndBuf(cp),"P: %s\n", objname);
                if (rend)
                    reset_list = 0; // display this object
                iret = 0;
            }
        }
    }
    if (reset_list) {
        rest_to_full_display(cp);
    }
    return iret;  // 0 = success - object ob_found is OBJECT (for display if rend), 1 = FAILED
}

static void tree_callback( Fl_Widget* w, void* )
{
    Flu_Tree_Browser *t = (Flu_Tree_Browser*)w;
    int reason = t->callback_reason();
    Flu_Tree_Browser::Node *n = t->callback_node();
    char *cp = info_buffer;
    char *info = GetNxtBuf();
    *info = 0;
    switch( reason ) {
    case FLU_HILIGHTED:
      sprintf(info, "%s hilighted\n", n->label() );
      break;

    case FLU_UNHILIGHTED:
      sprintf(info, "%s unhilighted\n", n->label() );
      break;

    case FLU_SELECTED:
      // TODO: Ensure selection is in view
      //t->show();
      //t->set_hilighted(n);
      sprintf(info, "%s selected\n", n->label() );
      strcpy(cp,"FLU_SELECTED\n");
      showSelected(cp,n,1);
      set_info_display(cp);
      SPRTF("%s",cp);
      //t->set_hilighted(n);
      break;

    case FLU_UNSELECTED:
      sprintf(info, "%s unselected\n", n->label() );
      break;

    case FLU_OPENED:
      sprintf(info, "%s opened\n", n->label() );
      break;

    case FLU_CLOSED:
      sprintf(info, "%s closed\n", n->label() );
      break;

    case FLU_DOUBLE_CLICK:
      sprintf(info, "%s double-clicked\n", n->label() );
      break;

    case FLU_WIDGET_CALLBACK:
      sprintf(info, "%s widget callback\n", n->label() );
      break;

    case FLU_MOVED_NODE:
      sprintf(info, "%s moved\n", n->label() );
      break;

    case FLU_NEW_NODE:
      sprintf(info, "node '%s' added to the tree\n", n->label() );
      break;
    }

    if (*info && ac_verb5())
        SPRTF("%s",info);
}

/* ---------------------------------------------------
    typedef struct ACObject_t {
        ACPoint loc;
        char *name;
        char *data;
        char *url;
        ACVertex *vertices;
        int num_vert;
        ACSurface *surfaces;
        int num_surf;
        float texture_repeat_x, texture_repeat_y;
        float texture_offset_x, texture_offset_y;
        float crease;   / * 2013/05/06 added * /
        int   subdiv;   / * 2013/05/06 added * /
        int num_kids;
        struct ACObject_t **kids;
        float matrix[9];
        int type;
        int texture;
    } ACObject, *PACObject;

   --------------------------------------------------- */

void clearNameVector()
{
    vObjNames.clear();
    vMatNames.clear();
    vMapNameObj.clear();
    vMapNameMat.clear();
}

int name_in_obj_vector( std::string name )
{
    size_t max = vObjNames.size();
    size_t ii;
    std::string s;
    for (ii = 0; ii < max; ii++) {
        s = vObjNames[ii];
        if (strcmp(s.c_str(), name.c_str()) == 0) {
            return 1;
        }
    }
    return 0;
}
int name_in_mat_vector( std::string name )
{
    size_t max = vMatNames.size();
    size_t ii;
    std::string s;
    for (ii = 0; ii < max; ii++) {
        s = vMatNames[ii];
        if (strcmp(s.c_str(), name.c_str()) == 0) {
            return 1;
        }
    }
    return 0;
}

static int forget_unique_name = 0;  // can NOT forget due to nature of LIST
static int forget_unique_mat = 0;
#define MX_OBJ_NAME_LEN 512
#define MX_OBJ_NAME_BUFS 128
static char _s_obj_name_buf[MX_OBJ_NAME_LEN * MX_OBJ_NAME_BUFS];
static int nxt_obj_name_buf = 0;
static char *get_obj_name_buf() {
    nxt_obj_name_buf++;
    if (nxt_obj_name_buf >= MX_OBJ_NAME_BUFS)
        nxt_obj_name_buf = 0;
    char *cp = &_s_obj_name_buf[MX_OBJ_NAME_LEN * nxt_obj_name_buf];
    return cp;
}

char *get_obj_name( ACObject *ob )
{
    char *cp = get_obj_name_buf();
    strcpy( cp, parser->ac_objecttype_to_string2(ob->type) );
    strcat(cp," ");
    if (ob->name && *ob->name) {
        strcat(cp,ob->name);
    } else {
        strcat(cp,"noName");
    }
    return cp;
}
char *get_unique_obj_name( ACObject *ob )
{
    char *cp = get_obj_name(ob);    // just 'type name'
    ob->ucount = 0; // clear objects offset count
    std::string name(cp);   // get as a string
    if (forget_unique_name || !name_in_obj_vector(name)) {
        vObjNames.push_back(name);  // now it is
        return cp;  // all done
    }

    has_duplicate_names++;  // mark this as a file with DUPLICATED object names
    std::string s(cp);
    int count = 0;
    while( name_in_obj_vector(name) ) {
        count++;
        //sprintf(cp,"%s.%d", s.c_str(), count);
        sprintf(cp,"%s[%d]", s.c_str(), count);
        name = cp;
    }
    ob->ucount = count; // set objects offset count
    strcpy(cp,name.c_str());
    if (strcmp(s.c_str(),name.c_str())) {
        MapName mn;
        mn.name = name;
        mn.s    = s;
        vMapNameObj.push_back(mn);
    }
    vObjNames.push_back(name);  // now it is
    return cp;
}

char *get_unique_mat_name( ACMaterial * m )
{
    static char _s_mname[512];
    int count = 0;
    char *cp = _s_mname;
    std::string s( (m->name ? m->name : "no-nm") );
    if (forget_unique_mat) {
        strcpy(cp,s.c_str());
        return cp;
    }

    std::string name = s;
    while( name_in_mat_vector(name) ) {
        count++;
        sprintf(cp,"%s.%d", s.c_str(), count);
        name = cp;
    }
    strcpy(cp,name.c_str());
    strcpy(cp,name.c_str());
    if (strcmp(s.c_str(),name.c_str())) {
        MapName mn;
        mn.name = name;
        mn.s    = s;
        vMapNameMat.push_back(mn);
    }
    vMatNames.push_back(name);  // now it is
    return cp;
}

static int no_tree_data = 0;
static int add_no_vertices = 0;
static int add_vertex_data = 1;     // this WAS causing a BREAK due to getdblbug BUG!!!
static int add_vertex_count = 1;
static int add_material_items = 1;  // this seems OK
static bool add_no_vertices_line = false;
static int max_len = 0;

void fill_Tree(ACObject *ob)
{
    int i, len;
    char *cp = info_buffer;
    int num_kids = ob->num_kids;
    sprintf(cp,"%s", get_unique_obj_name(ob)); // , ob->num_kids);
    Flu_Tree_Browser::Node* n = 0;
    Flu_Tree_Browser::Node* n2 = 0;
#ifdef NEW_DELETE_BEHAVIOUR // fill tree but only if NOT deleted
    if (ob->deleted) {
        if (ob->type == OBJECT_GROUP)
            num_kids = 0; // if a group is deleted then so are its kids
        goto Do_Kids;
    }
#endif
    n = obj_Node->add(cp);
    if (n) {
        if (add_vertex_count) {
            if (ob->num_vert) {
                sprintf(cp,"Item has %d vertices", ob->num_vert);
                n2 = n->add(cp);
                if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
            } else if ( add_no_vertices_line && (ob->type == OBJECT_NORMAL) ) {
                strcpy(cp,"Item has NO vertices");
                n2 = n->add(cp);
                if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
            }
        }
        if (add_vertex_data) {
            for (i = 0; i < ob->num_vert; i++) {
                ACVertex p = ob->vertices[i];
                len = (int)sprintf(cp,"%4d: %s %s %s", (i + 1), gtf(p.x), gtf(p.y), gtf(p.z));
                if (len > max_len) {
                    max_len = len;
                }
                n2 = n->add(cp);
                if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
            }
            if (add_no_vertices) {
                if (ob->num_vert == 0) {
                    strcpy(cp,"No vertices");
                    n2 = n->add(cp);
                    if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
                }
            }
        }
    }
#ifdef NEW_DELETE_BEHAVIOUR // fill tree jump label if deleted
Do_Kids:
#endif
	for (i = 0; i < num_kids; i++) {
		fill_Tree(ob->kids[i]);
    }
}

void populate_Tree()
{
    if (no_tree_data)
        return;
    Flu_Tree_Browser::Node* n;
    Flu_Tree_Browser::Node* n2;
    tree->clear();
    clearNameVector();      // clear all the duplicate name vectors and start again
    tree->label( proot_node );
    n = tree->get_root();

    char *cp = info_buffer;
    ACObject *rob = get_current_obj();
    if (n && parser && rob) {
        n->always_open(1);
        //n->label_color(FL_BLUE);
        mat_Node = n->add( pmat_node );
        obj_Node = n->add( pobj_node );
        if (mat_Node && obj_Node) {
            int i, max;
            //nm->label_color(FL_BLUE);   // TODO: does not seem to do anything?????
            //no->label_color(FL_BLUE);
            // MATERIAL "DefaultWhite" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 64  trans 0
            /* stored as 
                typedef struct Material_t {
                    ACCol rgb; / * diffuse * /
                    ACCol ambient;
                    ACCol specular;
                    ACCol emissive;
                    float shininess;
                    float transparency;
                    char *name;
                } ACMaterial, *PACMaterial;
             */
            max = parser->ac_palette_get_mat_size2();
            for (i = 0; i < max; i++) {
                ACMaterial * m = parser->ac_palette_get_material2(i);
                if (m) {
                    // check name is unique
                    char *matnm = get_unique_mat_name(m);
                    n = mat_Node->add(matnm);
                    if (n && add_material_items) {
                        // how to stop SORTING - found tree->insertion_mode( FLU_INSERT_BACK );
                        sprintf(cp,"rgb %s %s %s", gtf(m->rgb.r), gtf(m->rgb.g), gtf(m->rgb.b) );
                        n2 = n->add(cp);
                        if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
                        sprintf(cp,"amb %s %s %s", gtf(m->ambient.r), gtf(m->ambient.g), gtf(m->ambient.b) );
                        n2 = n->add(cp);
                        if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
                        sprintf(cp,"emis %s %s %s", gtf(m->emissive.r), gtf(m->emissive.g), gtf(m->emissive.b) );
                        n2 = n->add(cp);
                        if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
                        sprintf(cp,"spec %s %s %s", gtf(m->specular.r), gtf(m->specular.g), gtf(m->specular.b) );
                        n2 = n->add(cp);
                        if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
                        sprintf(cp,"shi %s trans %s", gtf(m->shininess), gtf(m->transparency) );
                        n2 = n->add(cp);
                        if (!n2) SPRTF("adding new node [%s] failed!\n",cp);
                    }
                }
            }
            fill_Tree(rob);
        }
    }
    tree->redraw();
}

char *get_previous_file() 
{
    char *cp = file_path_buffer;
    *cp = 0;
    if (prefs) { // = new Fl_Preferences( Fl_Preferences::USER, vendor, mod_name ); 
        const char *dummy = "-";
        prefs->get(szlast_infile, cp, dummy, FL_PATH_MAX);
        if (*cp && strcmp(cp,dummy) && (is_file_or_directory(cp) == MFT_FILE)) {
            return cp;
        }
    }
    return 0;
}

void give_help( char *full_name )
{
    char *name = get_base_name(full_name);
    printf("%s - Version %s, build %s, at %s\n", name, AC2GLVIEW_VERSION, __DATE__, __TIME__);
    printf("%s [options] ac3d_ac_file\n", name);
    printf("Options:\n");
    printf(" --help  (-h or -?) = THis help and exit(0)\n");
    printf(" --verb num    (-v) = Set verbosity 0-9. (def=%d)\n", ac_get_verbosity());

    printf(" --input file  (-i) = Alternative setting of input ac file.\n");
    printf(" If no imput file is given, the previous file will be loaded, ");
    char *prev = get_previous_file();
    if (prev)
        printf("namely [%s]\n", prev);
    else
        printf("but none presently exists.\n");

#ifdef ADD_SCRIPT_INTERFACE
    printf(" --script file (-s) = Load an run a script file.\n");
#endif
}

void set_ac_in_file( char *file )
{
    ac_in_file = strdup(file);
#ifdef _MSC_VER
    char *cp = GetNxtBuf();
    *cp = 0;
    cp = _fullpath(cp,file,MAX_PATH);
#else
    char *cp = inp_file_buffer;
    *cp = 0;
    fl_filename_absolute(cp, file);
#endif
    if (cp && *cp) {
        if (full_ac_in_file)
            free(full_ac_in_file);
        full_ac_in_file = strdup(cp);
        std::string f(get_base_name(cp));
        std::string::size_type pos = f.rfind('.');  // find the last '.'
        if (pos != std::string::npos)
            f = f.substr(0,pos); // got path sep but '.' is greater
        if (ac_in_name)
            free(ac_in_name);
        ac_in_name = strdup(f.c_str());
    }
}


typedef int (*ACTIONCMD)( char * );

typedef struct tagMYSCRIPT {
    const char *cmd;
    char *param;
    int res;
    int flag;
    ACTIONCMD ac;
}MYSCRIPT, *PMYSCRIPT;

int ac_load( char *file )
{
    if (ac_verb1() ) SPRTF("CMD: LOAD %s\n", (file ? file : ""));
    set_ac_in_file(file);
    return 0;
}
int ac_delete( char *cmd )
{
    if (!cmd || (*cmd == 0)) {
        SPRTF("CMD: DELETE FAILED! No object name given\n");
        return 1;
    }
    ACObject *ob = get_current_obj();
    if (!ob) {
        SPRTF("CMD: DELETE %s FAILED! No current object loaded\n",
            (cmd ? cmd : ""));
        return 1;
    }
    char *delname = strdup(cmd);
    if (!delname) {
        SPRTF("CMD: DELETE %s FAILED! Memory allocation failed!\n",
            (cmd ? cmd : ""));
        return 1;
    }
    if (ac_verb1() ) SPRTF("CMD: DELETE %s\n", (cmd ? cmd : ""));
    char *pname = delname;
    int len = (int)strlen(delname);
    int i, c, gq = 0;
    for (i = 0; i < len; i++) {
        c = delname[i];
        if ((i == 0)&&(c == '"')) {
            gq = c;
            pname++;
            continue;
        }
        if (gq) {
            // a quoted name - never happended so far
            if (c == gq) {
                // last char
                delname[i] = 0; // send quote to null
                i++;    // get to next char
                break;
            }
        } else if (c <= ' ') {
            delname[i] = 0; // send space to null
            i++;
            break;
        }
    }
    // Now 'pname' contains a null terminated name string
    // ----------------------------------------------------
    int type = 0;
    int count = 0;
    char *cp = &delname[i]; // get a moving pointer
    char *pt = 0;
    char *pc = 0;
    while (*cp && (*cp <= ' ')) cp++; // get past any spaces
    pt = cp;    // keep pointer to first
    while (*cp && (*cp > ' ')) cp++;  // scan over type
    if (*cp) {
        *cp = 0;    // null terminate
        cp++;
        while (*cp && (*cp <= ' ')) cp++; // get past any spaces
        if (*cp) {
            pc = cp;
            while (*cp && (*cp > ' ')) cp++;  // scan over count
            *cp = 0;
        }
    }
    if (pt && *pt)
        type = atoi(pt);
    if (pc && *pc)
        count = atoi(pc);
    cp = GetNxtBuf();
    *cp = 0;
    //ob_found = 0;
    FINDOBJS fo;
    fo.count = count;
    fo.found = 0;
    fo.type  = type;
    int res = findObject(cp, pname, ob, &fo);
    free(delname);
    if (!res || !get_found_obj()) {
        SPRTF("CMD: DELETE %s FAILED! Unable to find object!\n",
            (cmd ? cmd : ""));
        return 1;
    }
    // found the object
    deleteObject( ob, get_found_obj() );
    
    // TODO: now must select object in Tree view to delete from there
    //if (script_has_exit) {
        // could forget about the tree, since there is an EXIT coming up
        // but no, do it always...
    //}
    char *name = strdup(get_obj_name( get_found_obj() ));
    Flu_Tree_Browser::Node *obj = get_Tree_obj_Node_from_name(name);
    if (!obj) {
        SPRTF("CMD: DELETE %s from Tree FAILED! Unable to find Tree item!\n",name);
        free(name);
        return 0; // forget it
    }
    SPRTF("CMD: DELETE %s from Tree.\n",name);
    free(name);

    return 0;
}

int ac_write( char * cmd )
{
    if (!cmd || (*cmd == 0)) {
        SPRTF("CMD: WRITE FAILED! No out file name given\n");
        return 1;
    }
    ACObject *ob = get_current_obj();
    if (!ob) {
        SPRTF("CMD: WRITE %s FAILED! No current object loaded\n",
            (cmd ? cmd : ""));
        return 1;
    }
    int res = writeAC3D( cmd, ob, 0, 0 );
    if (res) {
        SPRTF("CMD: WRITE %s FAILED\n", (cmd ? cmd : ""));
    } else {
        if (ac_verb1() ) SPRTF("CMD: WRITE %s\n", (cmd ? cmd : ""));
#ifdef ADD_NEW_NODE2
        set_input_widget(cmd);
#else
        // set_output_file(cmd); - no scripting does NOT get written to prefs
#endif
        if (script_ac_write)
            free(script_ac_write);
        script_ac_write = strdup(cmd);
    }
    return res;
}

void set_last_js_out( char *cmd )
{
    char *cpf = file_path_buffer;
    fl_filename_absolute(cpf, cmd);
    std::string file(cpf);
    ensure_os_seps(file);
    if (last_outjs)
        free(last_outjs);
    last_outjs = strdup(file.c_str());
    prefs->set(szlast_outjs, last_outjs);
    prefs->flush();

}


int ac_export( char *cmd )
{
    if (!cmd || (*cmd == 0)) {
        SPRTF("CMD: EXPORT FAILED! No out file name given\n");
        return 1;
    }
    ACObject *ob = get_current_obj();
    if (!ob) {
        SPRTF("CMD: EXPORT %s FAILED! No current object loaded\n",
            (cmd ? cmd : ""));
        return 1;
    }
    std::string file(cmd);
    int res = export_to_threejs( ob, file, 0 );
    if (res) {
        SPRTF("CMD: EXPORT %s FAILED\n", (cmd ? cmd : ""));
    } else {
        if (ac_verb1()) SPRTF("CMD: EXPORT %s\n", (cmd ? cmd : ""));
        set_last_js_out(cmd);
    }
    return res;
}

int ac_exit( char *cmd )
{
    int iret = 0;
    if (cmd && *cmd)
        iret = atoi(cmd);
    if (ac_verb1() ) SPRTF("CMD: EXIT(%d)\n",iret);
    pgm_exit(iret);
    return 0;
}

int ac_verbosity( char *cmd)
{
    if (!cmd || !is_digits(cmd)) {
        SPRTF("ERROR: Action VERBOSITY must be followed by an integer number!\n");
        if (abort_on_script_err) pgm_exit(1);
        return 1;
    }
    int v = atoi(cmd);
    ac_set_verbosity(v);
    if (ac_verb1() ) SPRTF("CMD: VERBOSITY %d\n",v);
    return 0;
}

int ac_delay( char *cmd )
{
    if (!cmd || !is_double(cmd)) {
        SPRTF("ERROR: Action DELAY must be followed by a positive double value!\n");
        if (abort_on_script_err) pgm_exit(1);
        return 1;
    }
    double v = atof(cmd);
    script_delay_secs = v;
    if (ac_verb1() ) SPRTF("CMD: DELAY %f\n",v);
    return 0;
}

void avoid_overwrite( char *cmd )
{
    char *cp = GetNxtBuf();
    strcpy(cp,cmd);
    size_t ii, len = strlen(cp);
    int c, cnt = 0;
    char *bgn = cp;
    bool fnd = false;
    for (ii = 0; ii < len; ii++) {
        c = cp[ii];
        if (c <= ' ') {
            cp[ii] = 0;
            if (cnt == 0) {
                fnd = false;
                if (strcmpi(bgn,"assimp") == 0) {
                    ii++;
                    for ( ; ii < len; ii++) {
                        c = cp[ii];
                        if (c > ' ') {
                            bgn = &cp[ii];
                            ii--;
                            fnd = true;
                            cnt++;
                            break;
                        }
                    }
                }
                if (!fnd) 
                    break;
            } else if (cnt == 1) {
                fnd = false;
                if (strcmpi(bgn,"export") == 0) {
                    ii++;
                    for ( ; ii < len; ii++) {
                        c = cp[ii];
                        if (c > ' ') {
                            bgn = &cp[ii];
                            ii--;
                            fnd = true;
                            cnt++;
                            break;
                        }
                    }
                }
                if (!fnd) 
                    break;
            } else if (cnt == 2) {
                fnd = false;
                // this is input file
                ii++;
                for ( ; ii < len; ii++) {
                    c = cp[ii];
                    if (c > ' ') {
                        bgn = &cp[ii];
                        ii--;
                        fnd = true;
                        cnt++;
                        break;
                    }
                }
                if (!fnd) 
                    break;
            } else if (cnt == 3) {
                if (is_file_or_directory(bgn) == MFT_FILE) {
                    SPRTF("Avoiding overwrite of '%s'\n", bgn );
                    rename_to_old_bak(bgn);
                } else {
                    SPRTF("Assimp out file '%s' does NOT exist!\n", bgn );
                }
                break;
            }
        }
    }
    if (!fnd)
        SPRTF("CMD: %s NOT assimp export!\n",cmd);
}

void test_overwrite()
{
    char *cmd = (char *)"assimp export f:\\Projects\\aircraft\\707\\707-3.ac 707-3.js -fjs -ptv -gn -l -v -lo -tri";
    if (InStri( cmd, "assimp" ) && InStri( cmd, "export" )) {
        avoid_overwrite( cmd );
    }
    exit(1);
}



int ac_run( char *cmd)
{
    char *cp;
    if (InStri( cmd, "assimp" ) && InStri( cmd, "export" )) {
        avoid_overwrite(cmd);
    } else {
        SPRTF("cmd: %s NOT assimp export!\n",cmd);
    }
    // maybe ShellExecute, or simpler system(...), which uses CreateProcess()
#ifdef _MSC_VER
    _flushall(); // see http://msdn.microsoft.com/en-us/library/vstudio/277bwbdz.aspx
#else
    fflush(NULL);
#endif
    int res = system(cmd);    // TODO: test this simple implementation
    cp = GetNxtBuf();
    if (res) {
        sprintf(cp,"ERROR: Action 'RUN %s' returned %d\n", cmd, res );
        SPRTF("%s",cp);
    } else {
        sprintf(cp,"CMD: RUN %s appears OK\n",cmd);
        if (ac_verb1() ) SPRTF("%s",cp);
    }
    set_info_display(cp);
    return res;
}

int ac_script( char *cmd)
{
    int iret = 0;
    if (generate_a_script(cmd)) {
        if (ac_verb1() ) SPRTF("CMD: SCRIPT %s appears OK\n",cmd);
    } else {
        SPRTF("ERROR: Action 'SCRIPT %s' failed\n", cmd );
        iret = 1;
    }
    return iret;
}

static MYSCRIPT sscripts[] = {
    { "LOAD", 0, 1, 0, ac_load },
    { "DELETE", 0, 1, 0,ac_delete },
    { "WRITE", 0, 1, 0, ac_write },
    { "EXPORT", 0, 1, 0, ac_export },
    { "EXIT", 0, 0, 0, ac_exit },
    { "VERBOSITY", 0, 1, 0, ac_verbosity },
    { "DELAY", 0, 1, 0, ac_delay },
    { "RUN", 0, 1, 0, ac_run },
    { "SCRIPT", 0, 1, 0, ac_script },
    { 0, 0, 0, 0 }
};

typedef std::vector<MYSCRIPT> vSCRIPT;

static vSCRIPT vScripts;    // store script actions
void show_scripts() 
{
    size_t max = vScripts.size();
    SPRTF("Got %d script commands...\n", (int)max);
    size_t ii;
    MYSCRIPT ms;
    std::string s;
    int doerase = 0;
    int verb = ac_verb9();
    for (ii = 0; ii < max; ii++) {
        ms = vScripts[ii];
        s = "CMD: ";
        s += ms.cmd;
        if (ms.param) {
            s += " ";
            s += ms.param;
        }

        if (ms.flag) {
            s += " Done.";
            doerase = 1;
        }

        if (verb) SPRTF("%s\n", s.c_str());
    }
    while (doerase) {
        doerase = 0;
        max = vScripts.size();
        for (ii = 0; ii < max; ii++) {
            ms = vScripts[ii];
            if (ms.flag) {
                doerase = 1;
                vScripts.erase( vScripts.begin() + ii );
                break;
            }
        }
    }
}

void ac_init_script()
{
    script_has_exit = 0;
    script_delete_cnt = 0;
    vScripts.clear();   // clear any pevious stored script actions
}

// size_t max = vScripts.size();
//static void check_script()

static void check_script()
{
    size_t max = vScripts.size();
    MYSCRIPT ms;
    if (max) {
        double secs = get_seconds();
        if ((secs - script_next_secs) > script_delay_secs) {
            script_next_secs = secs;
            ms = vScripts[0];   // get first
            ms.ac( ms.param );  // and action the item
            vScripts.erase( vScripts.begin() ); // erase it
            max = vScripts.size();
            if (strcmp(ms.cmd,"DELETE") == 0) {
                if (script_delete_cnt) {
                    script_delete_cnt--;
                    if (script_delete_cnt == 0) {
                        SPRTF("Done last DELETE action... re-populate tree...\n");
                        populate_Tree();    // LAZY: Just reset the WHOLE Tree ;=()
                    }
                } else {
                    SPRTF("WAARNING: Done a DELETE but NO count remaining!\n");
                }
            }
            if (max == 0) {
                SPRTF("Done last script action...\n");
            }
        }
    }
}

int add_action(char *bgn, char *cmd, bool exit_on_err = true);

char *get_name_type_count(char *cmd, PFINDOBJS pfo)
{
    char *name = 0;
    char *cp = cmd;
    int n = 0;
    while (*cp && (*cp > ' ')) {
        n++;
        cp++;
    }
    while (*cp && (*cp <= ' ')) cp++;   // eat space
    char *tn = cp;  // pointer to type number
    while (*cp && (*cp > ' ')) cp++;   // crawl over number
    while (*cp && (*cp <= ' ')) cp++;   // eat space
    char *pc = cp;  // we have the count number
    if (*pc && n) {
        pfo->count = atoi(pc);  // the the count
        pfo->type  = atoi(tn);  // and the type
        name = (char *)malloc(n+2);
        strncpy(name,cmd,n);
        name[n] = 0;
    }
    return name;
}

int add_action(char *bgn, char *cmd, bool exit_on_err)
{
    PMYSCRIPT pms = &sscripts[0];
    while (pms->cmd) {
        if (strcmp(bgn,pms->cmd) == 0) {
            // found action
            MYSCRIPT ms;
            ms.cmd = pms->cmd;  // copy the const char *
            if (pms->res) {
                if (!cmd) {
                    SPRTF("ERROR: CMD: %s requires a following parameter!\n", bgn);
                    if (exit_on_err) pgm_exit(1);
                    return 1;
                }
            }
            if (cmd)
                ms.param = strdup(cmd);
            else
                ms.param = 0;
            ms.res = pms->res;
            ms.flag = 0;
            ms.ac   = pms->ac;  // copy the action
            if (cmd && (strcmp(bgn,"LOAD") == 0)) {
                ms.ac(cmd);
                ms.flag = 1;    // partially done - must actually do the load later
            } else if (cmd && (strcmp(bgn,"VERBOSITY") == 0)) {
                ms.ac(cmd);
                ms.flag = 2;    // ALL done
            } else if (cmd && (strcmp(bgn,"DELAY") == 0)) {
                ms.ac(cmd);
                ms.flag = 2;    // ALL done
            } else if (strcmp(bgn,"EXIT") == 0) {
                script_has_exit = 1;
            } else if (strcmp(bgn,"DELETE") == 0) {
                script_delete_cnt++;
                // UGH: Due to deletes can depend on an offset into the ac object
                // they must be inserted with the largest number first...
                // the cmd is 'name type count'
                size_t max = vScripts.size();
                size_t ii;
                MYSCRIPT ms2;
                for (ii = 0; ii < max; ii++) {
                    ms2 = vScripts[ii];
                    if (strcmp(ms2.cmd,"DELETE") == 0) {
                        break;
                    }
                }
                if (ii < max) {
                    // we have to do the work
                    FINDOBJS fo, fo2;
                    char *n1 = get_name_type_count(cmd,&fo);
                    if (n1) {
                        for( ; ii < max; ii++) {
                            ms2 = vScripts[ii];
                            if (strcmp(ms2.cmd,"DELETE") == 0) {
                                char *n2 = get_name_type_count(ms2.param,&fo2);
                                if (n2 && (strcmp(n1,n2) == 0) && (fo.count > fo2.count)) {
                                    // ok this delete MUST be inserted BEFORE this record
                                    vScripts.insert( vScripts.begin() + ii, ms );
                                    free(n1);
                                    free(n2);
                                    return 0;   // success
                                }
                                if (n2)
                                    free(n2);
                            }
                        }
                        free(n1);
                    }
                }
            }
            vScripts.push_back(ms);
            return 0;
        }
        pms++;
    }
    SPRTF("ERROR: Action [%s] NOT valid\nAborting...\n", bgn);
    free(script_buf);
    script_buf = 0;
    if (exit_on_err) pgm_exit(1);
    return 1;
}

void show_cwd_full_path( char *file )
{
#ifdef _MSC_VER
    char *cp = GetNxtBuf();
    if ( _getcwd(cp, MAX_PATH) )
        SPRTF("The current work directory is [%s]\n", cp);
    if (_fullpath(cp, file, MAX_PATH))
        SPRTF("The full file path is [%s]\n", cp);
#else
    // TODO: What is the eqivalent in unix?
#endif
}

int load_script_file(char *file, bool exit_on_err = true);

int load_script_file(char *file, bool exit_on_err)
{
    if (is_file_or_directory(file) != MFT_FILE) {
        show_cwd_full_path(file);
        SPRTF("ERROR: Unable to stat script file %s\n",file);
        if (exit_on_err) pgm_exit(1);
        return 1;
    }
    size_t size = get_last_stat_file_size();
    if (size == 0) {
        SPRTF("ERROR: script file %s in blank!\n",file);
        if (exit_on_err) pgm_exit(1);
        return 1;
    }
    FILE *fp = fopen(file,"rb");
    if (!fp) {
        SPRTF("ERROR: Unable to open script file %s!\n",file);
        if (exit_on_err) pgm_exit(1);
        return 1;
    }
    if (script_buf)
        free(script_buf);
    script_buf = (char *) malloc(size+2);
    if (!script_buf) {
        SPRTF("ERROR: Memory allocations FAILED on %ld bytes\n", (size+2));
        fclose(fp);
        if (exit_on_err) pgm_exit(1);
        return 1;
    }
    size_t res = fread(script_buf,1,size,fp);
    fclose(fp);
    if (res != size) {
        SPRTF("ERROR: Read of script file %s FAILED! Requested %ld bytes, got %ld!\n",file,size,res);
        free(script_buf);
        script_buf = 0;
        if (exit_on_err) pgm_exit(1);
        return 1;
    }
    script_buf[size] = 0;
    size_t ii, i2;
    int c;
    char *cp = script_buf;
    char *bgn;
    char *cmd;

    ac_init_script();   // clear any pevious stored script actions
    for (ii = 0; ii < size; ii++) {
        c = cp[ii];
        if (c <= ' ') continue;
        if ((c == '#')||(c == ';')) {
            // skip this line
            ii++;
            for (; ii < size; ii++) {
                c = cp[ii];
                if ((c == '\r') || (c == '\n')||(c == 0)) {
                    // end of line
                    break;
                }
            }
            continue;
        }
        bgn = &cp[ii];  // beginning of a command
        cmd = 0;
        ii++;
        for (i2 = ii; i2 < size; i2++) {
            c = cp[i2];
            if ((c == '\r')||(c == '\n')||(c == 0)) {
                cp[i2] = 0;
                while (i2 > ii) {
                    i2--;
                    if (cp[i2] > ' ')
                        break;
                    cp[i2] = 0;
                }
                break;
            }
        }
        for (; ii < size; ii++) {
            c = cp[ii];
            if (c <= ' ') {
                cp[ii] = 0;
                if ((c == '\r')||(c == '\n')||(c == 0)) {
                    cp[ii] = 0;
                    break;
                }
                ii++;
                for (; ii < size; ii++) {
                    c = cp[ii];
                    if ((c == '\r')||(c == '\n')||(c == 0)) {
                        cp[ii] = 0;
                        break;
                    } else if (c > ' ') {
                        cmd = &cp[ii];
                        // get to end of this line
                        ii++;
                        for (; ii < size; ii++) {
                            c = cp[ii];
                            if ((c == '\r') || (c == '\n')||(c == 0)) {
                                // end of line
                                cp[ii] = 0;
                                break;
                            }
                        }
                    }
                    if ((c == '\r') || (c == '\n')||(c == 0)) {
                        // end of line
                        break;
                    }
                }
                break;
            }
            if ((c == '\r') || (c == '\n')||(c == 0)) {
                // end of line
                break;
            }
        }
        if (add_action(bgn,cmd)) {
            free(script_buf);
            script_buf = 0;
            return 1;
        }
        if (script_has_exit)
            break;  // no need to parse more now that an EXIT is in the list!!!
    }
    free(script_buf);
    script_buf = 0;
    show_scripts();
    script_next_secs = get_seconds();   // init the script delay timer
    return 0;
}

void parse_commands( int argc, char **argv )
{
    int i, i2, c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case '?':
            case 'h':
                give_help(argv[0]);
                pgm_exit(0);
                break;
            case 'i':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_ac_in_file(sarg);
                } else {
                    SPRTF("ERROR: Input file name must follow [%s]\n", arg);
                    goto Bad_Arg;
                }
                break;
            case 'v':
                if ((i2 < argc)&&(is_digits(argv[i2]))) {
                    i++;
                    sarg = argv[i];
                    i2 = atoi(sarg);
                    ac_set_verbosity(i2);
                } else {
                    SPRTF("ERROR: Verbosity integer 0-9 must follow [%s]\n", arg);
                    goto Bad_Arg;
                }
                break;
#ifdef ADD_SCRIPT_INTERFACE
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    script_file = strdup(sarg);
                    if (is_file_or_directory(script_file) != MFT_FILE) {
                        show_cwd_full_path(script_file);
                        SPRTF("ERROR: Unable to 'stat' file [%s]\n", script_file);
                        goto Bad_Arg;
                    }
                } else {
                    SPRTF("ERROR: Input file name must follow [%s]\n", arg);
                    goto Bad_Arg;
                }
                break;
#endif
            default:
Bad_Arg:
                SPRTF("ERROR: Unknown command [%s]\nAborting...\n",arg);
                pgm_exit(1);
                break;
            }
        } else {
            set_ac_in_file(arg);
        }
    }

    if (script_file) {
        load_script_file(script_file);  // will ABORT if an ERROR
    }

    if (ac_in_file == 0) {
        // try loading last 
        char *prev_file = get_previous_file();
        if (prev_file) {
            set_ac_in_file(file_path_buffer);
        } else {
            SPRTF("ERROR: No input file found in command!\nAnd no previous available to load!\nAborting...\n");
            pgm_exit(1);
        }
    }
    if (ac_in_file) {
        if (is_file_or_directory((char *)ac_in_file) != MFT_FILE) {
            SPRTF("ERROR: Can not 'stat' file [%s]!\nAborting...\n", ac_in_file);
            pgm_exit(1);
        }
    }
}

char *get_in_file() 
{
    if (ac_in_file) {
        return get_base_name((char *)ac_in_file);
    }
    return 0;
}

const char *file_pattern = "*.ac";
char *get_new_ac_out()
{
    char *cf = get_curr_output_file();
    char *nf = (char *)flu_save_chooser( "AC Ouput File", file_pattern, cf );
    return nf;
}

const char *script_pattern = "*.script";
const char *last_script_out = 0;
static void writescriptCB( Fl_Widget*, void* )
{
    std::string last;
    if (last_script_out)
        last = last_script_out;
    if (!last.size() && last_out_path && ac_in_name) { 
        last = last_out_path;
        if (last.size()) {
            last += ac_in_name;
            last += ".script";
        }
    }
    const char *nf = flu_save_chooser("Script File Name", script_pattern,
        (last.size() ? last.c_str() : 0));
    if (nf) {
        if ( generate_a_script((char *)nf) ) {
            if (last_script_out)
                free((void *)last_script_out);
            last_script_out = strdup(nf);
        }
    }
}

void loadscriptCB(Fl_Widget *, void *)
{
    const char *csf = get_curr_script_file();
    const char *sf = flu_file_chooser( "Select Script File", "script_pattern", csf );
    if (sf) {
        if (is_file_or_directory((char *)sf) != MFT_FILE) {
            SPRTF("ERROR: Unable to stat script file %s\n",sf);
            return;
        }
        SPRTF("Loading script %s", sf);
        int verb = ac_get_verbosity();
        ac_set_verbosity(9);
        if (load_script_file((char *)sf, false)) {
            SPRTF("Script load FAILED!\n");
            return;
        }
        ac_set_verbosity(verb);
        // action the script commands
        // assume they are all related to the currently loaded file
        double sds = script_delay_secs;
        script_delay_secs = 0.0;
        bool aboe = abort_on_script_err;
        abort_on_script_err = false;
        while (vScripts.size()) {
            check_script();
            script_delay_secs = 0.0;
        }
        abort_on_script_err = aboe;
        script_delay_secs = sds;

    }
}

void open_cb(Fl_Widget*, void*) 
{
    int type = Flu_File_Chooser::STDFILE;
    Flu_File_Chooser *f = new Flu_File_Chooser( ac_in_file, file_pattern, type, "Select AC3D File" );
    //f->set_sort_function( length_sort );
    f->set_modal();
    f->show();
    f->add_type( "ac", "AC3D File" /*, icon */ );
    f->add_type( "*", "All Files" );
    while( f->shown() )
        Fl::wait();
    int count = f->count();
    if (count == 0) {
        SPRTF("Cancelled file selection\n");
    } else if (count > 1) {
        SPRTF("Multiple selection NOT allowed!\n");
    } else {
        int ok = 0;
        std::string s("Selected: ");
        const char *full_path = f->value(1);
	    s += full_path;
        ACObject *rob = get_current_obj();
        if (rob)
            rob->me->ac_free_objects2();
        if (parser)
            delete parser;
        if (preview->display_list > 0)
            glDeleteLists(preview->display_list, 1);    // delete the current list
        parser = new ac3d2gl;
        rob = parser->ac_load_ac3d2((char *)full_path);
        if (rob) {
            set_current_obj(rob); // NEW SELECTED FILE - set current object
            s += " loaded";
            int display_list = ac_display_list_render_object(rob);
            if (display_list > 0) {
                set_ac_in_file((char *)full_path);
                //done_ac_init = 0;
                char *cp = GetNxtBuf();
                if (preview->display_list > 0) {
                    sprintf(cp," del dl %d", preview->display_list);
                    glDeleteLists(preview->display_list, 1);    // delete the current list
                    s += cp;
                }
                sprintf(cp," new dl %d", display_list);
                preview->display_list = display_list; // set for this file
                s += cp;
                ok = 1;
            } else {
                s += " FAILED DL!";
            }
        } else {
            s += " FAILED!";
        }
        if (ok) {
            prefs->set(szlast_infile, full_path);
            prefs->flush();
            get_file_preferences(); // any preferences for this file?
            populate_Tree();
        }
        sprtf("%s\n", s.c_str());
    }
}

void close_cb(Fl_Widget*, void*) {}

void quit_cb(Fl_Widget*, void*) 
{
    //prefs->flush();
    if (parser) {
        parser->ac_free_objects2();
        delete parser;
        parser = 0;
    }
    clearNameVector();
    pgm_exit(0);
}

void test_cb(Fl_Widget* w, void*) 
{
    Fl_Menu_* mw = (Fl_Menu_*)w;
    const Fl_Menu_Item* m = mw->mvalue();
    if (!m)
        SPRTF("test_cb: NULL\n");
    else if (m->shortcut())
        SPRTF("test_cb: %s - %s\n", m->label(), fl_shortcut_label(m->shortcut()));
    else
        SPRTF("test_cb: %s\n", m->label());
}


// ========================================================
// MENU TABLE

Fl_Menu_Item menutable[] = {
  {"&File",0,0,0,FL_SUBMENU},
    {"&Open",    FL_ALT+'o', open_cb,  0 },
//    {"&Close",   FL_ALT+'c', close_cb, 0 },
    {"&Save AC...", FL_ALT+'s', writeACCB, 0 },     /* save loaded object to an ac file */
    {"&Export 3Js..." , FL_ALT+'e', exportCB, 0 },  /* save loaded object to a three.js file - using assimp export */
    {"&Quit",    FL_ALT+'q', quit_cb,  0 },
    {0},
//  {"&Edit",FL_F+2,0,0,FL_SUBMENU},
//    {"Undo",     FL_ALT+'z',    0},
//    {"Redo",     FL_ALT+'r',    0, 0, FL_MENU_DIVIDER},
//    {"Cut",      FL_ALT+'x',    0},
//    {"Copy",     FL_ALT+'c',    0},
//    {"Paste",    FL_ALT+'v',    0},
//    {0},
#ifdef ADD_ACTIONS_2_MENU
  {"&Actions",0,0,0,FL_SUBMENU},
      {"&Delete"  , FL_ALT+'D', removeCB,  0 },
      {"&Restore" , FL_ALT+'R', refreshCB, 0 },
      {"&Load Script...", FL_ALT+'L', loadscriptCB, 0 },
      {"&Write script...", FL_ALT+'W', writescriptCB, 0 },
//      {"&Export 3J..." , FL_ALT+'E', exportCB, 0 },
#ifdef ADD_PREF_DIALOG
      {"&Preferences..." , FL_ALT+'P', preferencesCB, 0 },
#endif
      {0},
#endif
  {0}
};


#define MX_SECS 10
static int frame_cnts[MX_SECS];
static time_t prev_tm;
static int idl_cnt = 0;
static int sec_cnt = 0;
static double next_secs;
static double min_secs = 0.05;  // run at 20 fps

void idle(void *)
{
    idl_cnt++;
    time_t curr = time(0);
    if (curr != prev_tm) {
        frame_cnts[sec_cnt++] = idl_cnt;
        idl_cnt = 0;
        if (sec_cnt >= MX_SECS) {
            if (ac_verb9()) {
                int total = 0;
                for (sec_cnt = 0; sec_cnt < MX_SECS; sec_cnt++)
                    total += frame_cnts[sec_cnt];
                if ((last_tottime > 0.0) && (last_framec > 0)) {
                    SPRTF("%.1f: fps: abs %d throttled: %.1f (%d)\n", total_time,
                        total / MX_SECS, 1.0/(last_tottime/last_framec), frames_per_sec);
                } else {
                    SPRTF("%.1f: fps: abs %d throttled %d\n", total_time, total / MX_SECS, frames_per_sec);
                }
            }
            sec_cnt = 0;
        }
        prev_tm = curr;
    }

    double dcurr = get_seconds();
    double elap = dcurr - next_secs;
    if (elap < min_secs)
        return;
    next_secs = dcurr;
    if (done_gl_init) 
        preview->draw();
}

void set_idle() {
    prev_tm = time(0);
    Fl::add_idle(idle);
}

static const char *szx_trans = "x_trans"; // = 0.0f;
static const char *szy_trans = "y_trans"; // = -4.0f;  // was 0.0;
static const char *szz_trans = "z_trans"; // = -10.0f;  // was -3.0
static const char *szangle_rate = "angle_rate"; // = 25.0f; // change at rate at ?? degrees per second
static const char *szfrozen = "frozen"; // = 0;
static const char *szx_axis = "x_axis"; // = 0.0f;
static const char *szy_axis = "y_axis"; // = 1.0f;
static const char *szz_axis = "z_axis"; // = 0.0f;
static const char *szrot = "rot"; // = 0.0;


char *get_file_preferences()
{
    char *inf = get_in_file();
    if (inf) {
        Fl_Preferences gl( prefs, inf );
        gl.get(szx_trans, x_trans, 0.0f);
        gl.get(szy_trans, y_trans, -4.0f);  // was 0.0;
        gl.get(szz_trans, z_trans, -10.0f);  // was -3.0
        gl.get(szangle_rate, angle_rate, 25.0f); // change at rate at ?? degrees per second
        gl.get(szfrozen, frozen, 0);
        gl.get(szx_axis, x_axis, 0.0f);
        gl.get(szy_axis, y_axis, 1.0f);
        gl.get(szz_axis, z_axis, 0.0f);
        gl.get(szrot, rot, 0.0);
    }
    get_gl_params_to_gl_parms( &file_parms );
    return inf;
}

void init_preferences()
{
    prefs = new Fl_Preferences( Fl_Preferences::USER, vendor, mod_name ); 
}

int load_winsizes()
{
    int iret = 0;
    int x,y,w,h;
    prefs->get(szwin_height, h, win_height);
    prefs->get(szwin_width, w, win_width);
    prefs->get(szwin_posx, x, pos_x);
    prefs->get(szwin_posy, y, pos_y);
    if (win_height != h) {
        win_height = h;
        iret++;
    }
    if (win_width != w) {
        win_width = w;
        iret++;
    }
    if (pos_x != x) {
        pos_x = x;
        iret++;
    }
    if (pos_y != y) {
        pos_y = y;
        iret++;
    }
    return iret;
}

void load_preferences()
{
    //prefs->get(szwin_height, win_height, win_height);
    //prefs->get(szwin_width, win_width, win_width);
    //prefs->get(szwin_posx, pos_x, 0);
    //prefs->get(szwin_posy, pos_y, 0);
    // last out file path
    char *of = get_curr_output_file();
    if (of && (*of != 0)) {
        char *cpf = file_path_buffer;
        fl_filename_absolute(cpf, of);
        std::string file(cpf);
        ensure_os_seps(file);
        strcpy(cpf,file.c_str());
        prefs->get(szlast_outfile, file_path_buffer, cpf, FL_PATH_MAX);
        file = file_path_buffer;
        ensure_os_seps(file);
#ifdef ADD_NEW_NODE2
        out_file = strdup(file.c_str());
#else
        output_ac_file = strdup(file.c_str());
#endif // #ifdef ADD_NEW_NODE2
        cpf = get_file_path_only(cpf);
        prefs->get(szlast_outpath, file_path_buffer, cpf, FL_PATH_MAX);
        file = file_path_buffer;
        ensure_os_seps(file);
        last_out_path = strdup(file.c_str());
    }
    get_file_preferences();
}

char *save_file_preferences()
{
    char *inf = get_in_file();
    if (inf) {
        Fl_Preferences gl( prefs, inf );
        gl.set(szx_trans, x_trans);
        gl.set(szy_trans, y_trans);  // was 0.0;
        gl.set(szz_trans, z_trans);  // was -3.0
        gl.set(szangle_rate, angle_rate); // change at rate at ?? degrees per second
        gl.set(szfrozen, frozen);
        gl.set(szx_axis, x_axis);
        gl.set(szy_axis, y_axis);
        gl.set(szz_axis, z_axis);
        gl.set(szrot, rot);
        prefs->flush();
    }
    return inf;
}

void new_size(int x, int y, int w, int h)
{
    int res, cnt;
    cnt = 0;
    res = 0;
    if ((x != pos_x) || (y != pos_y) || (w != win_width) || (h != win_height)) 
    {
        if (ac_verb9()) SPRTF("New size: x=%d, y=%d, w=%d, h=%d\n", x, y, w, h );
        if (pos_x != x) {
            pos_x = x;
            res += prefs->set(szwin_posx, pos_x);
            cnt++;
        }
        if (pos_y != y) {
            pos_y = y;
            res += prefs->set(szwin_posy, pos_y);
            cnt++;
        }
        if (win_width != w) {
            win_width = w;
            res += prefs->set(szwin_width, win_width);
            cnt++;
        }
        if (win_height != h) {
            win_height = h;
            res += prefs->set(szwin_height, win_height);
            cnt++;
        }
    }
    if (cnt || res) {
        prefs->flush(); // commit to disk
    }
}

void fix_log_file(const char *name)
{
    char *cp = GetNxtBuf();
    size_t len = GetBufSiz();
    *cp = 0;
#ifdef _MSC_VER
    DWORD dwd = GetModuleFileName( NULL, cp, (DWORD)len );
    DWORD i, last = 0;
    int c;
    for (i = 0; i < dwd; i++) {
        c = cp[i];
        if ((c == '\\')||(c == '/')) {
            last = i + 1;
        }
    }
    if (last)
        cp[last] = 0;
    if (*cp)
        output_path = strdup(cp);
    strcat(cp,def_log);
#else
    // TODO: Find a 'safe' place for this log file, probably HOME???
    char *home = getenv("HOME");
    if (home) {
        strcpy(cp,home);
        strcat(cp,"/");
    }
    output_path = strdup(cp);
    strcat(cp,def_log);
#endif
    //set_log_file(cp,false);
    log_fp = fopen(cp,"w");
    if (log_fp)
        set_log_file(log_fp);


}


// original full file : C:\FG\18\ac2glview\build\c172p.ac
// original full file : LOAD C:\FG\fgdata\Aircraft\f16\Models\f16.ac
// debug file : C:\FG\18\blendac3d\c172p\c172pm.ac
// trimmed version : C:\FG\18\ac2glview\build\c172pm2.ac
// or a very simple one : C:\FG\18\ac2glview\data\cube.ac
int main( int argc, char **argv )
{

    //test_overwrite();
    fix_log_file(argv[0]);

    init_preferences();

    ac_init_script();

    parse_commands( argc, argv );

    load_preferences();

    if (ac_in_file) {
        prefs->set(szlast_infile, ac_in_file);
        if (last_out_path) 
            prefs->set(szlast_outpath, last_out_path);
        prefs->flush();
    }

    char *cp = GetNxtBuf();
    if (ac_in_file) {
        sprintf(cp, "ac3d_browser2 %s", ac_in_file);
    } else {
        strcpy(cp,"ac3d_browser2");
    }
    mwin = new mainWin( pos_x, pos_y, win_width, win_height, cp );

    mwin->begin();
    {
        //win = new Fl_Double_Window( WINDOW_WIDTH, WINDOW_HEIGHT, "ac3d_browser" );
        if (ac_verb9()) SPRTF("mainWin: x=%d y=%d w=%d h=%d\n", pos_x, pos_y, win_width, win_height);
        // add MENU
        Fl_Menu_Bar menubar(0,0,win_width,30); 
        menubar.menu(menutable);    // set table
        menubar.callback(test_cb);  // not sure why this???
        tree_y = 30;
        tree_height = win_height - 30;
        group_y = 30;
        group_height -= 30;
        multi_height = win_height - rgl_height - (3 * y_val_steps);

        // build the tree
        // =============================================================
        tree = new Flu_Tree_Browser( tree_x, tree_y, tree_width, tree_height );
        tree->box( FL_DOWN_BOX );
        tree->selection_mode(FLU_SINGLE_SELECT);    // 20130801 - try single slection
        tree->auto_branches( true );
        if (no_sort_list)
            tree->insertion_mode( FLU_INSERT_BACK ); // does this STOP sorting??? YEAH! perfect - now ordered as per file
        // tree->label( proot_node );
        tree->callback( tree_callback );
        // =============================================================
        if (ac_verb9()) SPRTF("treeWin: x=%d y=%d w=%d h=%d\n", tree_x, tree_y, tree_width, tree_height);

        // build the group
        // ===============================================================
        Fl_Group *g = new Fl_Group( group_x, group_y, group_width, group_height );
        g->begin();
        {
            if (ac_verb9()) SPRTF("treeWin: x=%d y=%d w=%d h=%d\n", group_x, group_y, group_width, group_height);
            //g->resizable( NULL );
            int y_val = initial_y_val;

            // add the gl review window
            preview = new PreviewWin( 10, y_val, rgl_width, rgl_height );
            if (ac_verb9()) SPRTF("GLWin: x=%d y=%d w=%d h=%d\n", 10, y_val, rgl_width, rgl_height);
            y_val += rgl_height;

            //multi_height -= rgl_height;
            textOutput2 = new Fl_Multiline_Output(10, y_val, multi_width, multi_height);
            if (ac_verb9()) SPRTF("MultiWin: x=%d y=%d w=%d h=%d\n", 10, y_val, multi_width, multi_height);
            //g->resizable(preview); // this does NOT do what I want
        }
        g->end();
        // ===============================================================
    }
    mwin->end();

    //mwin->resizable( tree );    // this partially does what I want
    mwin->resizable( mwin );    // maybe this is what I want
    if (load_winsizes()) {
        mwin->resize(pos_x, pos_y, win_width, win_height);
    }

    mwin->show();
    mwin->set_resizecb( new_size );

    // load of ac3d file
    parser = new ac3d2gl;

    //parser->ac_set_opt_load_textures2(0);   // tell library NOT to load textures WHY NOT
    //parser->ac_set_opt_drop_groups(1);      // NOT GOOD IDEA - DROP groups

    ACObject *rob = parser->ac_load_ac3d2((char *)ac_in_file);
    if (!rob) {
		SPRTF("%s: ERROR: failed to load file \n[%s]!\nAborting... exit(1)\n", mod_name, ac_in_file);
        pgm_exit(1);
    }
    // ============================================================
    set_current_obj(rob); // INITIAL FILE LOAD - set current object
    // ============================================================

    sprintf(cp,"Loaded: %s", ac_in_file);
    set_info_display(cp);

    prev_secs = next_secs = get_seconds();
    last_secs = time(0);

    populate_Tree();

    set_idle(); // establish an IDLE callback

    int ret = Fl::run();

    clean_up();

    return ret;
}

// eof - ac3d_browser2.cpp
