/* ======================================================================================
   ac3d_browser.cpp

   20130526 - Initial cut
   Just some experiments is parsing an AC3D .ac model file, and reviewing the items is contains.
   Had the idea to allow modification, by deleting kids, and writing the modified object 
   back to a new .ac file, but never competed.
   It was also a learning tool for FLTK and FLU libraries...

   ====================================================================================== */
#ifndef FREEGLUT_STATIC
#define FREEGLUT_STATIC
#endif
#ifndef FREEGLUT_LIB_PRAGMAS
#define FREEGLUT_LIB_PRAGMAS 0
#endif
#include <stdio.h>
#include <time.h>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Counter.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>
#include <FL/fl_show_colormap.H>
#include <FL/filename.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Preferences.H>
#include <FL/gl.h>
#include <FL/Fl_Gl_Window.H>
#include <FL/names.h>         // defines fl_eventnames[]
#include <FL/Fl_Menu_Bar.H>

#include <FLU/Flu_File_Chooser.h>
#include <FLU/Flu_Tree_Browser.h>
#include <FLU/flu_pixmaps.h>

#include <GL/glu.h>

#include <vector>
#include <string>
#include <algorithm>
#include <map>

#include "misc_lib.hxx" // just some misc functions

#ifndef HAVE_AC2GL2
#include <ac3d2.hxx>
#else
#include <ac3d.hxx>
#endif


#define ADD_ACTIONS_2_MENU

#define WINDOW_WIDTH 700
#define WINDOW_HEIGHT 460
#define TREE_WIDTH 400
#define TREE_HEIGHT WINDOW_HEIGHT
#define GROUP_WIDTH (WINDOW_WIDTH - TREE_WIDTH)
#define GROUP_HEIGHT WINDOW_HEIGHT
#define BUTTON_WIDTH 80 // was 100
#define BUTTON_HEIGHT 20
#define INITIAL_Y_VAL 40;   // 20    // was 370
#define Y_VAL_STEPS 30
#define GL_WIDTH    (GROUP_WIDTH - 20)
#define GL_HEIGHT   (WINDOW_HEIGHT - (6 * Y_VAL_STEPS))
#define MULTI_WIDTH (GROUP_WIDTH - 20)
#ifdef ADD_ACTIONS_2_MENU
#define MULTI_HEIGHT (WINDOW_HEIGHT - GL_HEIGHT)
#else
#define MULTI_HEIGHT (WINDOW_HEIGHT - (5 * Y_VAL_STEPS))
#endif

static const char  *mod_name = "ac3d_browser";

static ACObject *rob = 0;
static int done_gl_init = 0;

static const char *vendor = "geoffair";
// some initial positions and sizes, but any changes in pos or size are written to preferences
// and will be restored on next restart
// In windows preferences written to C:\Users\user\AppData\Roaming\geoffair\ac3d_browser.prefs
// ===========================================================================================
static int pos_x = 20;
static int pos_y = 20;
static int win_width = WINDOW_WIDTH;
static int win_height = WINDOW_HEIGHT;
static int tree_width = TREE_WIDTH;
static int tree_height = TREE_HEIGHT;
static int group_width = GROUP_WIDTH;
static int group_height = GROUP_HEIGHT;
static int button_width = BUTTON_WIDTH; // 80 // was 100
static int button_height = BUTTON_HEIGHT;   // 20
static int initial_y_val = INITIAL_Y_VAL;   // 20    // was 370
static int y_val_steps = Y_VAL_STEPS;   // 30
static int multi_width = MULTI_WIDTH;   // (GROUP_WIDTH - 20)
static int multi_height = MULTI_HEIGHT; // (WINDOW_HEIGHT - (5 * Y_VAL_STEPS))
static int group_x = 0;
static int group_y = 0;
static int tree_x = GROUP_WIDTH;
static int tree_y = 0;
static int rgl_width = GL_WIDTH;   // (GROUP_WIDTH - 20)
static int rgl_height = GL_HEIGHT; // (WINDOW_HEIGHT - (5 * Y_VAL_STEPS))
// ==============================================================================================

// establish a re-size callback to save sizes to prefs
typedef void (*RESIZE) (int x, int y, int w, int h);

// main window preferences, and last file
static const char *szwin_height = "win_height";
static const char *szwin_width = "win_width";
static const char *szwin_posx = "win_posx";
static const char *szwin_posy = "win_posy";
static const char *szlast_outpath = "last_out_path";
static const char *szlast_infile = "last_in_file";
static Fl_Preferences *prefs; 

void pgm_exit(int v)
{
    exit(v);
}


// sub class
class mainWin : public Fl_Double_Window {
public:
    mainWin( int x, int y, int w, int h, const char *title );
    int handle(int e);
    void set_resizecb( RESIZE rscb )  { resizecb  = rscb; }

protected:
        RESIZE resizecb;
        virtual void resize(int x, int y, int w, int h) {
                Fl_Double_Window::resize(x, y, w, h);
                if (resizecb) resizecb(x,y,w,h);
        }

};

mainWin::mainWin( int x, int y, int w, int h, const char *title ) :
Fl_Double_Window( x, y, w, h, title ) 
{
    // any intialisation
    resizecb = 0;   // clear this...
}

static int keyfn ( unsigned char c, int i1, int i2 );

// ================================================================
// keyboard (and mouse) callbacks
int mainWin::handle(int e)
{
    static int last_event = 0;
    int res = 1;
    int key = 0;
    const char *text;
    switch (e) {
    case FL_KEYDOWN:
        if (Fl::event_state() & FL_CTRL) break;
        key = Fl::event_key();
        text = Fl::event_text();
        if (key == FL_Escape) { // ESC key
           res = Fl_Window::handle(e);
        } else {
            if (text && *text)
                key = *text;
            if (!keyfn( key, 0, 0 )) {
                // if key NOT used, pass to fl handler
               res = Fl_Window::handle(e);
            }
        }
        break;
    default:
       res = Fl_Window::handle(e);
    }
    if (e != last_event)
        printf("Event was %s (%d) res %d key %x\n", fl_eventnames[e], e, res, key);
    last_event = e;
    return res;
}


class PreviewWin : public Fl_Gl_Window 
{
public:
    PreviewWin(int x,int y,int w,int h,const char *l=0);
    void draw();
    int done_init, display_list;
};

PreviewWin::PreviewWin(int x,int y,int w,int h,const char *l) : Fl_Gl_Window(x,y,w,h,l)
{
    done_init = display_list = 0; // any init required
    // default mode is FL_RGB|FL_DOUBLE|FL_DEPTH
}

void set_projection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float)w/(float)h, 0.1, 10000.0);
}

void init_gfx(int w, int h)
{
    ac_prepare_render();
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    set_projection(w,h);
}

// ===========================================
// read and write this set to a config file
// ===========================================
static float x_trans =   0.0f;
static float y_trans =  -4.0f;  // was 0.0;
static float z_trans = -10.0f;  // was -3.0
static float angle_rate = 25.0f; // change at rate at ?? degrees per second
static int frozen = 0;
static float x_axis = 0.0f;
static float y_axis = 1.0f;
static float z_axis = 0.0f;
static float rot = 0.0;
static double prev_secs = 0.0;
static time_t last_secs = 0;
static double frametime;
static double tottime = 0.0;
static double total_time = 0.0;
static int framec = 0;
static int frames = 0;
static int frames_per_sec = 0;
static int elap_secs = 0;
static int last_framec = 0;
static double last_tottime = 0.0;

static void key_help()
{
    int tframes = frames_per_sec;
    printf("Keyboard Help - to size, position and rotate the model.\n");
    printf(" ESC or q = Quit\n");
    printf(" f/F      = Toggle rotation freeze. (def=%s)\n", (frozen ? "On" : "Off"));
    printf(" s/S      = Slow/Speed up rotation speed. angle_rate=%f\n", angle_rate);
    printf(" x/X      = Left/Right - x translation factor (%f)\n", x_trans);
    printf(" y/Y      = Up/Down    - y translation factor (%f)\n", y_trans);
    printf(" z/Z      = Near/Far   - z translation factor (%f)\n", z_trans);
    printf(" ?/h      = This help, frame rate and current angle...\n");
    printf(" 1-7      = Set glRotate(rot,x,y,z) - %.1f,%.1f,%.1f\n", x_axis, y_axis, z_axis);
    printf(" d/D      = Dump scene to numbered PPM file in work directory. (D in stereo)\n");
    printf(" v/V      = Reduce/Increase verbosity. (def=%d)\n", ac_get_verbosity());
    printf(" w/W      = Write/Read config file.\n");
    printf(" fps=%d, rotation angle=%f, secs %d\n", tframes, rot, elap_secs);
}

// in the range -10 to 10 subtract 0.5
// if lesser or greater   subtract 1/20 of the value
// if zero,               subtract 0.5
float sub_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next -= (next / 20.0f);
        } else {
            if (next < 0.5f)
                next = 0.0f;
            else
                next -= 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next -= (-next / 20.0f);
        else 
            next -= 0.5f;
    } else {
        next -= 0.5f;
    }
    return next;
}
// in the range -10 to 10 add 0.5
// if lesser or greater   add 1/20 of the value
// if zero,               add 0.5
float add_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next += (next / 20.0f);
        } else {
            next += 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next += (-next / 20.0f);
        else {
            if (next < -0.5f)
                next += 0.5f;
            else
                next = 0.0f;
        }
    } else {
        next += 0.5f;
    }
    return next;
}

char *save_file_preferences();
char *get_file_preferences();

static int keyfn ( unsigned char c, int i1, int i2 )
{
    int ret = 0;
    int v, sxyz = 0;
    char *fil  = 0;
    if ((c == 'q')||(c == 0x1b)) {
        printf("Got exit key...\n");
        ac_free_objects();
        pgm_exit ( 0 ) ;
    }

    if ((c == '?')||(c == 'h')) {
        key_help();
        ret = 1;
    } else if (c == 'w') {
        fil = save_file_preferences();
         if (fil && ac_verb2()) printf("w = Saves prefs for %s\n", fil);
//        v = write_config();
//       if (ac_verb2()) printf("w = Written config %s (%d)\n", get_config_file_name(), v);
         ret = 1;
    } else if (c == 'W') {
//        v = read_config();
//        if (ac_verb2()) printf("W = Read config %s (%d)\n", get_config_file_name(), v);
        fil = get_file_preferences();
        if (fil && ac_verb2()) printf("W = Read prefs for %s\n", fil);
        ret = 1;
    } else if ((c == 'f')||(c == 'F')) {
        if (frozen)
            frozen = 0;
        else
            frozen = 1;
        if (ac_verb2()) printf("f = Toggle frozen %d\n", frozen);
        ret = 1;
    } else if (c == 's') {
        angle_rate -= 1.0;
        if (ac_verb2()) printf("angle_rate = %f\n", angle_rate);
        ret = 1;
    } else if (c == 'S') {
        angle_rate += 1.0;
        if (ac_verb2()) printf("angle_rate = %f\n", angle_rate);
        ret = 1;
    } else if (c == 'x') {
        //x_trans -= 0.5f;
        x_trans = sub_5_percent(x_trans);
        if (ac_verb2()) printf("x_trans = %f\n", x_trans);
        ret = 1;
    } else if (c == 'X') {
        //x_trans += 0.5f;
        x_trans = add_5_percent(x_trans);
        if (ac_verb2()) printf("x_trans = %f\n", x_trans);
        ret = 1;
    } else if (c == 'y') {
        //y_trans -= 0.5f;
        y_trans = sub_5_percent(y_trans);
        if (ac_verb2()) printf("y_trans = %f\n", y_trans);
        ret = 1;
    } else if (c == 'Y') {
        //y_trans += 0.5f;
        y_trans = add_5_percent(y_trans);
        if (ac_verb2()) printf("y_trans = %f\n", y_trans);
        ret = 1;
    } else if (c == 'z') {
        //z_trans -= 0.5f;
        z_trans = sub_5_percent(z_trans);
        if (ac_verb2()) printf("z_trans = %f\n", z_trans);
        ret = 1;
    } else if (c == 'Z') {
        //z_trans += 0.5;
        z_trans = add_5_percent(z_trans);
        if (ac_verb2()) printf("z_trans = %f\n", z_trans);
        ret = 1;
    } else if (c == '1') {
        x_axis = 1.0;
        y_axis = 0.0;
        z_axis = 0.0;
        sxyz = 1;
        ret = 1;
    } else if (c == '2') {
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 0.0;
       sxyz = 1;
        ret = 1;
    } else if (c == '3') {
       x_axis = 0.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
    } else if (c == '4') {
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 0.0;
        ret = 1;
    } else if (c == '5') {
       x_axis = 1.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
    } else if (c == '6') {
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
   } else if (c == '7') {
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
        ret = 1;
    } else if (c == 'd') {
        WritePPM( win_width, win_height, 0 );
        ret = 1;
    } else if (c == 'D') {
        WritePPM( win_width, win_height, 0, 1 );
        ret = 1;
    } else if (c == 'v') {
        v = ac_get_verbosity();
        if (v) {
            v--;
            ac_set_verbosity(v);
        }
        ret = 1;
    } else if (c == 'V') {
        v = ac_get_verbosity();
        v++;
        ac_set_verbosity(v);
        ret = 1;
    }
    if (sxyz && ac_verb2()) printf("%c: set x,y,z axis %.1f,%.1f,%.1f\n", c, x_axis, y_axis, z_axis);
    return ret;
}


void drawscene(int w, int h, int dl, int val)
{
    timer_start();

    double curr = get_seconds();
    double elap = curr - prev_secs;
    time_t currs = time(0);
    GLfloat light_position[] = { -1000.0, 1000.0, 1000.0, 0.0 };


    if (val)
        set_projection(w,h);

    gluLookAt(0, 1.8, 4, 0, 0, 0, 0, 1, 0);

    glClearColor(0,0,0,0);
    glClearColor(0.5,0.5,0.5,0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glTranslatef ( x_trans, y_trans, z_trans ) ;

    // this can be TOO FAST!
    //rot += 0.01f; // was 1
    if (!frozen) {
        // bump the rotation angle 
        rot += (float) ( elap * angle_rate );
    }

    if (rot >= 360.0) rot -= 360.0f;

    // glRotatef(rot, 0, 1, 0);
    glRotatef(rot, x_axis, y_axis , z_axis );

    glCallList(dl);
    // think these are done by FL????
    // glFlush();
    // glFinish();
    // glutSwapBuffers(); // done automatically, I think

    frametime = timer_stop();   // hmmm, seems this is always 0.0????????
    tottime += frametime;
    framec++;
    if (tottime > 5.0) {
        total_time += tottime;
        if (ac_verb5()) {
    	    printf("%.1f: Approx frames per sec: %.1f (%d)\n", 
                total_time, 1.0/(tottime/framec), frames_per_sec);
        }
        last_tottime = tottime;
        last_framec = framec;
	    tottime = 0;
	    framec = 0;
    }

    frames++;
    if (currs != last_secs) {
        frames_per_sec = frames;
        frames = 0;
        last_secs = currs;
        elap_secs++;
    }
    prev_secs = curr;
}


void PreviewWin::draw()
{
    if (!done_init) {
        printf("Init of draw preview of ac\n");
        init_gfx( w(), h() );
        if (rob) {
             display_list = ac_display_list_render_object(rob);
            if (display_list > 0) {
                printf("Got display_list %d\n", display_list);
                done_gl_init = 1;
            } else {
                printf("ERROR: Failed to get display list!\n");
            }
        }
        done_init = 1;
    }
    if (display_list > 0) {
        int val = 0;
        if (!valid()) {
            val = 1;
        }
        drawscene(w(), h(), display_list, val);
        //draw_overlay();
        //invalidate();
        damage(1); // OK, this causes a re-paint
    }
}



static PreviewWin *preview;

// forward refs
void populate_Tree();
void clearNameVector();
static void showSelected(char *cp, Flu_Tree_Browser::Node *in_n);
static int writeObject(ACObject *ob, FILE *fp, int add_kids = 1, int kid_cnt = 0);

mainWin *mwin;
Fl_Double_Window *win;
Flu_Tree_Browser *tree;
Fl_Input *newNode;
#ifndef ADD_ACTIONS_2_MENU
Fl_Button *addChild, *removeBtn, *refreshBtn;
#endif
// from : http://seriss.com/people/erco/fltk/#Fl_Multiline_Output
Fl_Multiline_Output *textOutput; 
int next = 1;
static Flu_Tree_Browser::Node* nm = 0;
static Flu_Tree_Browser::Node* no = 0;
static char buffer[1024];
static char file_path_buffer[FL_PATH_MAX];
static int deletions = 0;
static const char *in_file = 0;
static const char *out_file = "temp.ac";
static int multi_write = 1;
static int mat_ind = 0;

#define EndBuf(a) ( a + strlen(a))

static const char *proot_node = "ac3d browser";
static const char *pmat_node = "MATERIAL";
static const char *pobj_node = "OBJECT";

typedef struct tagMapName {
    std::string s;
    std::string name;
}MapName;
typedef std::vector<MapName> vMAPNM;
typedef struct tagMatOff {
    int i, n;
}MatOff;

typedef std::vector<MatOff> vMOFFS;

typedef std::vector<std::string> vSTG;
typedef std::vector<int> vINT;

static vSTG vObjNames;
static vSTG vMatNames;

static vMAPNM vMapNameObj;
static vMAPNM vMapNameMat;

typedef std::vector<Flu_Tree_Browser::Node *> vTNODE;

static int is_deletable( Flu_Tree_Browser::Node *n )
{
    Flu_Tree_Browser::Node* pn = n->parent();
    if( !pn ) {
        return 0;
    }
    Flu_Tree_Browser::Node* mn = pn->parent();
    if (!mn) {
        return 0;
    }
    return 1;
}

// sort of get_parent, but really only up one if the parent is
// NOT one of the primary nodes
static Flu_Tree_Browser::Node *get_parent( Flu_Tree_Browser::Node *n )
{
    Flu_Tree_Browser::Node *tn = n->parent();
    if (strcmp(pmat_node,tn->label()) == 0)
        return n;
    else if (strcmp(pobj_node,tn->label()) == 0)
        return n;
    return tn;
}

int not_in_vTNODE(vTNODE &vDel,Flu_Tree_Browser::Node *tn)
{
    size_t max = vDel.size();
    size_t ii;
    for (ii = 0; ii < max; ii++) {
        if (vDel[ii] == tn)
            return 0;   // not not - 0 is it IS in vector
    }
    return 1; // 1 says it is NOT in vector
}

// examines the 
static void removeCB( Fl_Widget*, void* )
{
    int cnt = 0;
    char *cp = buffer;
    vTNODE vNodes, vDel;
    int sel_count = tree->num_selected();
    Flu_Tree_Browser::Node* n = tree->get_selected( 1 );
    if( !n ) {
        fl_alert("Appears no nodes are selected for deletion!");
        return;
    }
    Flu_Tree_Browser::Node* tn = n;
    while (tn) {
        cnt++;
        vNodes.push_back(tn);
        tn = tree->get_selected( cnt+1 ); // get NEXT
    }
    size_t max = vNodes.size();
    size_t ii;
    sprintf(cp,"Got %d selections (%d)\n", (int)max, sel_count);
    for (ii = 0; ii < max; ii++) {
        tn = vNodes[ii];
        if (is_deletable(tn)) {
            tn = get_parent(tn);
            if (tn && not_in_vTNODE(vDel,tn)) {
                vDel.push_back(tn);
                sprintf(EndBuf(cp),"%s (ok)\n", tn->label());
            }
        } else {
            sprintf(EndBuf(cp),"%s NO DELETE!\n", tn->label());
        }
    }
    max = vDel.size();
    sprintf(EndBuf(cp),"DELETIONS: %d for deletion\n", (int)max);
    for (ii = 0; ii < max; ii++) {
        tn = vNodes[ii];
        tn->select(1);
        showSelected(cp,tn);
        tree->remove(tn);
        deletions++;    // count a DELETION Node and children
        // TODO: Must now REMOVE it from the object, and fix any kids counts - quite tought
        // ================================================================================
    }
    textOutput->value(cp);
    //tree->redraw();
#ifndef ADD_ACTIONS_2_MENU
    //removeBtn->color(0xff000000);
    //removeBtn->redraw_label();
#endif
}

#if 0
// first try at this
static void removeCB2( Fl_Widget*, void* )
{
    //int cnt = 0;
    char *cp = buffer;
    int sel_count = tree->num_selected();
    Flu_Tree_Browser::Node* n = tree->get_selected( 1 );
    if( !n ) {
        fl_alert("Appears no nodes are selected for deletion!");
        return;
    }
    // is it a ROOT
    // could also test n->is_root()
    Flu_Tree_Browser::Node* pn = n->parent();
    if( !pn ) {
        sprintf(cp,"Node %s\nif a ROOT node\nand can NOT be deleted\n", n->label());
        textOutput->value(cp);
        fl_alert("%s",cp);
        return;
    }
    Flu_Tree_Browser::Node* mn = pn->parent();
    if (!mn) {
        sprintf(cp,"Node %s\nif a PRIMARY node\nand can NOT be deleted\n", pn->label());
        textOutput->value(cp);
        fl_alert("%s",cp);
        return;
    }
    // in case
    Flu_Tree_Browser::Node* cn = mn->parent();  // get parent
    if (!cn) {
        cn = mn;
    }

    // const char *path = n->find_path();
    // tree->remove( path );
    sprintf(cp,"DELETION: sel %d\n", sel_count);
    tree->remove(cn);
    showSelected(cp,cn);
    //while (n) {
    //    cnt++;
    //    showSelected(cp,n);
    //    tree->remove( n );
    //    n = tree->get_selected( 1 );
    //}
    sprintf(EndBuf(cp),"Deleted %s items\n", cn->label());
    textOutput->value(cp);
    tree->redraw();
#ifndef ADD_ACTIONS_2_MENU
    removeBtn->color(0xff000000);
    removeBtn->redraw_label();
#endif
}
#endif // 0 - code to be removed at some point


/* --------------------------------------------------
OBJECT world
kids 86
OBJECT group
name "LDoorGroup"
kids 4
OBJECT poly
name "doorhandle_intle"
data 8
Mesh.503
crease 55.000000
numvert 17
-0.007664 -0.244604 0.483142
...
0.165563 -0.262837 0.480377
numsurf 16
SURF 0x10
mat 1
refs 4
15 0.709214985371 0.487257003784
2 0.75809699297 0.487127989531
3 0.758024990559 0.495406001806
14 0.709181010723 0.494942009449
SURF 0x10
mat 1
refs 4
14 0.0 0.0
3 1.0 0.0
5 1.0 1.0
1 0.0 1.0
...
...
SURF 0x10
mat 1
refs 3
16 1.0 0.5
2 1.0 0.0
15 0.0 0.0
kids 0
   -------------------------------------------------- */

static int writeObject(ACObject *ob, FILE *fp, int add_kids, int kid_cnt)
{
    char *cp = buffer;
    char *obj = (char *)ac_objecttype_to_string( ob->type );
    size_t len = sprintf(cp,"OBJECT %s\n", obj);
    size_t res;
    int i;
    res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"Write of object to file FAILED!");
        fl_alert("%s",cp);
        return 1;
    }
    if (ob->name) {
        len = sprintf(cp,"name \"%s\"\n", ob->name);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"Write of object to file FAILED!");
            fl_alert("%s",cp);
            return 1;
        }
    }
    if (ob->num_vert) {
        len = sprintf(cp,"numvert %d\n", ob->num_vert);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"Write of object to file FAILED!");
            fl_alert("%s",cp);
            return 1;
        }
        for (i = 0; i < ob->num_vert; i++) {
            ACVertex p = ob->vertices[i];
            len = sprintf(cp,"%f %f %f\n", p.x, p.y, p.z);
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"Write of object to file FAILED!");
                fl_alert("%s",cp);
                return 1;
            }
        }
    }
    if (ob->num_surf) {
        len = sprintf(cp,"numsurf %d\n", ob->num_surf);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"Write of object to file FAILED!");
            fl_alert("%s",cp);
            return 1;
        }
        for (i = 0; i < ob->num_surf; i++) {
	        ACSurface *s = &ob->surfaces[i];
            len = sprintf(cp,"SURF 0x%02x\n", s->flags);
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"Write of object to file FAILED!");
                fl_alert("%s",cp);
                return 1;
            }
            //len = sprintf(cp,"mat %d\n", s->mindx);
            len = sprintf(cp,"mat %d\n", s->mat);   // rel index in this file = s->mat = mindx + startmatindex;
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"Write of object to file FAILED!");
                fl_alert("%s",cp);
                return 1;
            }
            len = sprintf(cp,"refs %d\n", s->num_vertref);
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                sprintf(cp,"Write of object to file FAILED!");
                fl_alert("%s",cp);
                return 1;
            }

            int vr;
	        for (vr = 0; vr < s->num_vertref; vr++) {
                len = sprintf(cp,"%d %f %f\n", s->vertref[vr], s->uvs[vr].u, s->uvs[vr].v);
                res = fwrite(cp,1,len,fp);
                if (res != len) {
                    sprintf(cp,"Write of object to file FAILED!");
                    fl_alert("%s",cp);
                    return 1;
                }
            }
        }
    }
    if (add_kids) {
        len = sprintf(cp,"kids %d\n", ob->num_kids);
    } else {
        len = sprintf(cp,"kids %d\n", kid_cnt);
    }
    res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"Write of object to file FAILED!");
        fl_alert("%s",cp);
        return 1;
    }
    if (add_kids) {
        for (i = 0; i < ob->num_kids; i++) {
            if (writeObject( ob->kids[i], fp))
                return 1;
        }
    }
    return 0;
}

int write_a_Material(ACMaterial *m, FILE *fp)
{
    char *cp = buffer;
    size_t len = sprintf(cp,"MATERIAL \"%s\" rgb %f %f %f ", (m->name ? m->name : "NoName"),
                m->rgb.r, m->rgb.g, m->rgb.b );
    len += sprintf(EndBuf(cp),"amb %f %f %f ", m->ambient.r, m->ambient.g, m->ambient.b );
    len += sprintf(EndBuf(cp),"emis %f %f %f ", m->emissive.r, m->emissive.g, m->emissive.b );
    len += sprintf(EndBuf(cp),"spec %f %f %f ", m->specular.r, m->specular.g, m->specular.b );
    len += sprintf(EndBuf(cp),"shi %f trans %f\n", m->shininess, m->transparency );
    size_t res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"Write of object to file FAILED!");
        fl_alert("%s",cp);
        return 1;
    }
    return 0;
}

static int writeMaterials( ACObject *ob, FILE *fp )
{
    // MATERIAL "DefaultWhite" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 64  trans 0
    /* stored as 
        typedef struct Material_t {
            ACCol rgb; / * diffuse * /
            ACCol ambient;
            ACCol specular;
            ACCol emissive;
            float shininess;
            float transparency;
            char *name;
        } ACMaterial, *PACMaterial;
        */
    int i;
    char *cp = buffer;
    int max = ac_palette_get_mat_size();
    for (i = 0; i < max; i++) {
        ACMaterial * m = ac_palette_get_material(i);
        if (m) {
            if (write_a_Material(m,fp))
                return 1;
        }
    }
    return 0;
}

#ifdef _MSC_VER
#define PATH_SEP    '\\'
#define BAD_SEP     '/'
#else
#define PATH_SEP    '/'
#define BAD_SEP     '\\'
#endif

void ensure_os_seps(std::string & s)
{
    //std::string::size_type pos;
    //while ((pos = s.find(BAD_SEP)) != std::string::npos) {
    std::replace(s.begin(), s.end(), BAD_SEP, PATH_SEP);
}

static int next_multi = 0;
static int next_mat = 0;
static int add_int_2_vec( int m, vMOFFS &vmaps )
{
    size_t max = vmaps.size();
    size_t ii;
    MatOff mo;
    for (ii = 0; ii < max; ii++) {
        mo = vmaps[ii];
        if (m == mo.i)
            return 0;
    }
    mo.i = m; // rel index in this file = s->mat = mindx + startmatindex;
    mo.n = mat_ind++; // 
    vmaps.push_back(mo); // not there, add it
    return 1;
}

static void get_vmaps( ACObject *ob, vMOFFS &vmaps )
{
    int i;
    for (i = 0; i < ob->num_surf; i++) {
        ACSurface *s = &ob->surfaces[i];
        // add_int_2_vec( s->mindx, vmaps );
        add_int_2_vec( s->mat, vmaps ); // rel index in this file = s->mat = mindx + startmatindex;
    }
}

// Output of a set of materials
int write_mats_per_vec(ACObject *ob, vMOFFS &vmaps, FILE *fp)
{
    size_t max = vmaps.size();
    size_t ii;
    MatOff mo;
    int max_mats = ac_palette_get_mat_size();
    for (ii = 0; ii < max; ii++) {
        mo = vmaps[ii];
        if (mo.i < max_mats) {
            ACMaterial * m = ac_palette_get_material(mo.i);
            if (m) {
                if (write_a_Material(m,fp))
                    return 1;
                mo.n = next_mat;    // set new offset in this file
                next_mat++;
                vmaps[ii] = mo;     // update record
            }
        }
    }
    return 0;
}

// Write each object to a separate .ac file
static int writeMulti(ACObject *bob, std::string base, ACObject *ob) 
{
    char *cp = GetNxtBuf();
    char *cpf = buffer;
    if (ob->num_vert && ob->num_surf) {
        strcpy(cpf,base.c_str());
        next_multi++;
        sprintf(EndBuf(cpf),"%d.ac", next_multi);
        FILE *fp = fopen(cpf,"w");
        if (!fp) {
            sprintf(cp,"Write object to file\n[%s]\nERROR: Unable to create/truncate file!",cpf);
            fl_alert("%s",cp);
            return 1;
        }
        strcpy(cp,"AC3Dd\n");
        size_t len = strlen(cp);
        size_t res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"Write object to file\n[%s]\nERROR: Write file FAILED!",cpf);
            fl_alert("%s",cp);
            fclose(fp);
            return 1;
        }
        vMOFFS vmaps;
        get_vmaps( ob, vmaps );
        if (write_mats_per_vec(bob, vmaps, fp)) {
            fclose(fp);
            return 1;
        }
        strcpy(cp,"OBJECT world\nkids 1\n");
        len = strlen(cp);
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            sprintf(cp,"Write object to file\n[%s]\nERROR: Write file FAILED!",cpf);
            fl_alert("%s",cp);
            fclose(fp);
            return 1;
        }
        // write this object without any kids
        if (writeObject(ob, fp, 0)) {
            fclose(fp);
            return 1;
        }
        fclose(fp);
    }

    int n;
    for (n = 0; n < ob->num_kids; n++) {
        if (writeMulti(bob, base, ob->kids[n]))
            return 1;
    }
    return 0;
}

// write the object back to an ac file
static void writeMultiAC()
{
    char *cpf = buffer;
    next_multi = 0;
    next_mat = 0;
    mat_ind = 0;
    fl_filename_absolute(cpf, newNode->value());
    std::string s(cpf);
    std::string::size_type pos = s.rfind('.');
    if (pos != std::string::npos) {
        ensure_os_seps(s);
        std::string::size_type pos2 = s.rfind(PATH_SEP);
        if ((pos2 == std::string::npos)||(pos2 < pos))
            s = s.substr(0,pos);
    }

    writeMulti(rob,s,rob);

    //char *cp = GetNxtBuf();
    //FILE *fp = fopen(cpf,"w");
    //if (!fp) {
    //    sprintf(cp,"Write %s object to file\n[%s]\nERROR: Unable to create/truncate file!",
    //        (deletions ? "modified" : "unmodified"),
    //        cpf);
    //    fl_alert("%s",cp);
    //    return;
    //}

}

static void writeACCB( Fl_Widget*, void* )
{
    Flu_Tree_Browser::Node* n = tree->get_selected( 1 );
    if( !n )
        n = tree->last();
    if (multi_write) {
        writeMultiAC();
        return;
    }
    char *cp = GetNxtBuf();
    char *cpf = buffer;
    fl_filename_absolute(cpf, newNode->value());

    sprintf(cp,"Write %s object to file\n[%s]\n",
        (deletions ? "modified" : "unmodified"),
        cpf);

    FILE *fp = fopen(cpf,"w");
    if (!fp) {
        sprintf(cp,"Write %s object to file\n[%s]\nERROR: Unable to create/truncate file!",
            (deletions ? "modified" : "unmodified"),
            cpf);
        fl_alert("%s",cp);
        return;
    }

    strcpy(cp,"AC3Dd\n");
    size_t len = strlen(cp);
    size_t res = fwrite(cp,1,len,fp);
    if (res != len) {
        sprintf(cp,"Write %s object to file\n[%s]\nERROR: Write file FAILED!",
            (deletions ? "modified" : "unmodified"),
            cpf);
        fl_alert("%s",cp);
        fclose(fp);
        return;
    }
    if (writeMaterials(rob,fp)) {
        sprintf(cp,"Write %s object to file\n[%s]\nERROR: Write file FAILED!",
            (deletions ? "modified" : "unmodified"),
            cpf);
        fl_alert("%s",cp);
        fclose(fp);
        return;
    }
    int ret = writeObject(rob,fp);  // recursively write objects
    fclose(fp);
    fl_filename_absolute(cpf, newNode->value());
    sprintf(cp,"Written object to file\n[%s]\n%s\n",cpf,
        (ret ? "But appears a write failure!" : "appears successful") );
    textOutput->value(cp);

}


//    refreshBtn = new Fl_Button( 10, y_val, BUTTON_WIDTH, BUTTON_HEIGHT, "Refresh" );
//    refreshBtn->callback( refreshCB, NULL );
static void refreshCB( Fl_Widget*, void* )
{
    char *cp = buffer;
    tree->clear();  // clear all and start again the tree
    tree->label( proot_node );
    tree->redraw();
    sprintf(cp,"Moment loading\n[%s]\n", in_file);
    textOutput->value(cp);
 
    ac_free_objects();  // free the current
    clearNameVector();  // clear named vectors
    rob = ac_load_ac3d((char *)in_file);    // RELOAD file
    if (!rob) {
		printf("%s: ERROR: failed to load %s! exiting 1.\n", mod_name, in_file);
        pgm_exit(1);
    }
    populate_Tree();    // populate tree
    sprintf(cp,"Reloaded file\n[%s]\n", in_file);
    textOutput->value(cp);
#ifndef ADD_ACTIONS_2_MENU
    removeBtn->color(0xff000000);
    removeBtn->redraw();
#endif
    deletions = 0;  // clear any deletions
}

static ACObject *ob_found;
int findObject(char *cp, const char *delname, ACObject *ob)
{
    if (ob->name) {
        if (strcmp(delname,ob->name)==0) {
            ob_found = ob;
            return 1;
        }
    }
    int i;
    for (i = 0; i < ob->num_kids; i++) {
        if (findObject( cp, delname, ob->kids[i] ))
            return 1;
    }
    return 0;
}

void findDelObject(char *cp, const char *delname, const char *objname)
{
    int i, max;
    if (strcmp(objname,pmat_node) == 0) {
        strcat(cp,"Finding 'MATERIAL' ");
        max = ac_palette_get_mat_size();
        for (i = 0; i < max; i++) {
            ACMaterial * m = ac_palette_get_material(i);
            if (m && m->name ) {
                if (strcmp(delname,m->name) == 0) {
                    sprintf(EndBuf(cp)," at %d", i);
                    break;
                }
            }
        }
        if (i >= max) {
            // TODO: do do maybe a compound name
            strcat(cp,"NOT FOUND!");
        }
        strcat(cp,"\n");

    } else if (strcmp(objname,pobj_node) == 0) {
        // strip OFF the 'type' (world/poly/etc) to get name
        std::string s(delname);
        std::string::size_type pos = s.find(' ');
        if (pos != std::string::npos) {
            s = s.substr(pos+1);
        }
        sprintf(EndBuf(cp),"'OBJECT' %s\n", s.c_str());
        //if (findObject( cp, delname, rob)) {
        if (findObject( cp, s.c_str(), rob)) {
            strcat(cp,"FOUND object\n");
        } else {
            // TODO: maybe a unique generated name
            sprintf(EndBuf(cp),"NOT FOUND %s!\n",delname);
        }
    } else {
        strcat(cp,"ERROR: Is not known!\n");
    }
}


// learn some things about the node
#define MX_NODES 4
static void showSelected(char *cp, Flu_Tree_Browser::Node *in_n)
{
    int i;
    int upd = 0;
    //char *cp = buffer;
    Flu_Tree_Browser::Node *n = in_n;
    //ACObject *ob = (ACObject *) n->user_data();
    Flu_Tree_Browser::Node *np[MX_NODES];
    const char *names[MX_NODES];
    const char *delname;
    const char *objname;    // either 'MATERIAL' or 'OBJECT'
    int cnt = 0;
    int kids = n->children();
    int dep = n->depth();   // depth in tree
    int ind = n->index();   // -1 if parent, else index into child list
    const char *lab = n->label();
    np[cnt] = n;
    names[cnt] = lab;
    cnt++;
    // walk back up the tree to the root
    for ( ; cnt < MX_NODES; cnt++ ) {
        n = n->parent();
        if (n) {
            np[cnt] = n;
            names[cnt] = n->label();
        } else {
            break;
        }
    }
    sprintf(EndBuf(cp),"kids %d, dep %d, ind %d, cnt %d\n", kids, dep, ind, cnt);

    for (i = 0; i < cnt; i++) {
        sprintf(EndBuf(cp),"%s: %s\n",
            ((cnt == 1) ? "R" : (cnt == 2) ? ((i == 0) ? "P" : "R") :
             (cnt == 3) ? ((i == 0) ? "N" : (i == 1) ? "P" : "R") :
             ((i == 0) ? "N" : (i == 1) ? "N" : (i == 2) ? "P" : "R")),
            names[i]);
    }
    if (cnt == 1) {
        sprintf(EndBuf(cp),"ROOT node - NO deletion!\n");
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0xff000000);
#endif
        //in_n->deactivate(); // hmmm, can't do this - need it selected to allow open node
        upd = 1;
    } else if (cnt == 2) {
        sprintf(EndBuf(cp),"PRIMARY node - NO deletion\n");
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0xff000000);
#endif
        //in_n->deactivate();
        upd = 1;
    } else if (cnt == 3) {
        delname = names[0];
        objname = names[1];
        n = np[0];
        sprintf(EndBuf(cp),"DN: %s\n", delname);
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0x00ff0000);
#endif
        upd = 2;
    } else if (cnt > 3) {
        delname = names[1];
        objname = names[2];
        n = np[1];
        sprintf(EndBuf(cp),"DN: %s\n", delname);
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->color(0x00ff0000);
#endif
        upd = 2;
    }

    if (upd) {
#ifndef ADD_ACTIONS_2_MENU
        removeBtn->redraw_label();
#endif
        if (upd == 2) {
            findDelObject(cp,delname,objname);         
            sprintf(EndBuf(cp),"P: %s\n", objname);
        }
    }
}

static void tree_callback( Fl_Widget* w, void* )
{
    Flu_Tree_Browser *t = (Flu_Tree_Browser*)w;
    int reason = t->callback_reason();
    Flu_Tree_Browser::Node *n = t->callback_node();
    char *cp = buffer;
    switch( reason ) {
    case FLU_HILIGHTED:
      printf( "%s hilighted\n", n->label() );
      break;

    case FLU_UNHILIGHTED:
      printf( "%s unhilighted\n", n->label() );
      break;

    case FLU_SELECTED:
      printf( "%s selected\n", n->label() );
      strcpy(cp,"FLU_SELECTED\n");
      showSelected(cp,n);
      textOutput->value(cp);
      break;

    case FLU_UNSELECTED:
      printf( "%s unselected\n", n->label() );
      break;

    case FLU_OPENED:
      printf( "%s opened\n", n->label() );
      break;

    case FLU_CLOSED:
      printf( "%s closed\n", n->label() );
      break;

    case FLU_DOUBLE_CLICK:
      printf( "%s double-clicked\n", n->label() );
      break;

    case FLU_WIDGET_CALLBACK:
      printf( "%s widget callback\n", n->label() );
      break;

    case FLU_MOVED_NODE:
      printf( "%s moved\n", n->label() );
      break;

    case FLU_NEW_NODE:
      printf( "node '%s' added to the tree\n", n->label() );
      break;
    }
}

/* ---------------------------------------------------
    typedef struct ACObject_t {
        ACPoint loc;
        char *name;
        char *data;
        char *url;
        ACVertex *vertices;
        int num_vert;
        ACSurface *surfaces;
        int num_surf;
        float texture_repeat_x, texture_repeat_y;
        float texture_offset_x, texture_offset_y;
        float crease;   / * 2013/05/06 added * /
        int   subdiv;   / * 2013/05/06 added * /
        int num_kids;
        struct ACObject_t **kids;
        float matrix[9];
        int type;
        int texture;
    } ACObject, *PACObject;

   --------------------------------------------------- */

void clearNameVector()
{
    vObjNames.clear();
    vMatNames.clear();
    vMapNameObj.clear();
    vMapNameMat.clear();
}

int name_in_obj_vector( std::string name )
{
    size_t max = vObjNames.size();
    size_t ii;
    std::string s;
    for (ii = 0; ii < max; ii++) {
        s = vObjNames[ii];
        if (strcmp(s.c_str(), name.c_str()) == 0) {
            return 1;
        }
    }
    return 0;
}
int name_in_mat_vector( std::string name )
{
    size_t max = vMatNames.size();
    size_t ii;
    std::string s;
    for (ii = 0; ii < max; ii++) {
        s = vMatNames[ii];
        if (strcmp(s.c_str(), name.c_str()) == 0) {
            return 1;
        }
    }
    return 0;
}


char *get_unique_obj_name( ACObject *ob )
{
    static char _s_name[256];
    int count = 0;
    char *cp = _s_name;
    std::string s( ac_objecttype_to_string(ob->type) );
    if (ob->name) {
        s += " ";
        s += (ob->name ? ob->name : "noname");
    }
    std::string name = s;
    while( name_in_obj_vector(name) ) {
        count++;
        sprintf(cp,"%s.%d", s.c_str(), count);
        name = cp;
    }
    strcpy(cp,name.c_str());
    if (strcmp(s.c_str(),name.c_str())) {
        MapName mn;
        mn.name = name;
        mn.s    = s;
        vMapNameObj.push_back(mn);
    }
    vObjNames.push_back(name);  // now it is
    return cp;
}

char *get_unique_mat_name( ACMaterial * m )
{
    static char _s_mname[256];
    int count = 0;
    char *cp = _s_mname;
    std::string s( (m->name ? m->name : "no-nm") );
    std::string name = s;
    while( name_in_mat_vector(name) ) {
        count++;
        sprintf(cp,"%s.%d", s.c_str(), count);
        name = cp;
    }
    strcpy(cp,name.c_str());
    strcpy(cp,name.c_str());
    if (strcmp(s.c_str(),name.c_str())) {
        MapName mn;
        mn.name = name;
        mn.s    = s;
        vMapNameMat.push_back(mn);
    }
    vMatNames.push_back(name);  // now it is
    return cp;
}

static int add_no_vertices = 0;
void fill_Tree(ACObject *ob)
{
    int i;
    char *cp = buffer;
    sprintf(cp,"%s", get_unique_obj_name(ob)); // , ob->num_kids);
    Flu_Tree_Browser::Node* n = no->add(cp);
    Flu_Tree_Browser::Node* n2;
    if (n) {
        //n->user_data( (void *)ob->order );
        //n->user_data(ob);
        for (i = 0; i < ob->num_vert; i++) {
            ACVertex p = ob->vertices[i];
            sprintf(cp,"%4d: %f %f %f", (i + 1), p.x, p.y, p.z);
            n2 = n->add(cp);
            //if (n2)
            //    n2->user_data(ob);
        }
        if (add_no_vertices) {
            if (ob->num_vert == 0) {
                strcpy(cp,"No vertices");
                n2 = n->add(cp);
                //if (n2) {
                //    n2->user_data(ob);
                //}
            }
        }
    }

	for (i = 0; i < ob->num_kids; i++)
		fill_Tree(ob->kids[i]);
}

/* ----------------------------------------------------------
   Simple colors
const Fl_Color FL_BLACK   = 56;
const Fl_Color FL_RED     = 88;
const Fl_Color FL_GREEN   = 63;
const Fl_Color FL_YELLOW  = 95;
const Fl_Color FL_BLUE    = 216;
const Fl_Color FL_MAGENTA = 248;
const Fl_Color FL_CYAN    = 223;
const Fl_Color FL_DARK_RED = 72;

const Fl_Color FL_DARK_GREEN    = 60;
const Fl_Color FL_DARK_YELLOW   = 76;
const Fl_Color FL_DARK_BLUE     = 136;
const Fl_Color FL_DARK_MAGENTA  = 152;
const Fl_Color FL_DARK_CYAN     = 140;

const Fl_Color FL_WHITE         = 255;
    ------------------------------------------------------------ */
int add_materials_params = 0;
void populate_Tree()
{
    Flu_Tree_Browser::Node* n;
    Flu_Tree_Browser::Node* n2;
    tree->clear();
    tree->label( proot_node );
    n = tree->get_root();
    char *cp = buffer;
    if (n) {
        //n->label_color(FL_BLUE);
        nm = n->add( pmat_node );
        no = n->add( pobj_node );
        if (nm && no) {
            int i, max;
            //nm->label_color(FL_BLUE);   // TODO: does not seem to do anything?????
            //no->label_color(FL_BLUE);
            //nm->user_data(rob);
            //no->user_data(rob);
            // MATERIAL "DefaultWhite" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 64  trans 0
            /* stored as 
                typedef struct Material_t {
                    ACCol rgb; / * diffuse * /
                    ACCol ambient;
                    ACCol specular;
                    ACCol emissive;
                    float shininess;
                    float transparency;
                    char *name;
                } ACMaterial, *PACMaterial;
             */
            max = ac_palette_get_mat_size();
            for (i = 0; i < max; i++) {
                ACMaterial * m = ac_palette_get_material(i);
                if (m) {
                    // check name is unique
                    char *matnm = get_unique_mat_name(m);
                    n = nm->add(matnm);
                    if (n && add_materials_params) {
                        ///n->user_data(rob);
                        // how to stop SORTING - found tree->insertion_mode( FLU_INSERT_BACK );
                        sprintf(cp,"rgb %f %f %f", m->rgb.r, m->rgb.g, m->rgb.b );
                        n2 = n->add(cp);
                        //if (n2) n2->user_data(rob);
                        sprintf(cp,"amb %f %f %f", m->ambient.r, m->ambient.g, m->ambient.b );
                        n2 = n->add(cp);
                        //if (n2) n2->user_data(rob);
                        sprintf(cp,"emis %f %f %f", m->emissive.r, m->emissive.g, m->emissive.b );
                        n2 = n->add(cp);
                        //if (n2) n2->user_data(rob);
                        sprintf(cp,"spec %f %f %f", m->specular.r, m->specular.g, m->specular.b );
                        n2 = n->add(cp);
                        //if (n2) n2->user_data(rob);
                        sprintf(cp,"shi %f trans %f", m->shininess, m->transparency );
                        n2 = n->add(cp);
                        //if (n2) n2->user_data(rob);
                    }
                }
            }
            fill_Tree(rob);
        }
    }
    tree->redraw();
}

void parse_commands( int argc, char **argv )
{
    int i, i2, c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case '?':
            case 'h':
                printf("Give an input AC3D .ac file.\n");
                pgm_exit(0);
                break;
            case 'i':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    in_file = strdup(sarg);
                } else {
                    printf("ERROR: Input file name must follow [%s]\n", arg);
                    pgm_exit(1);
                }
                break;
            default:
                printf("ERROR: Unknown command [%s]\nAborting...\n",arg);
                pgm_exit(1);
                break;
            }
        } else {
            in_file = strdup(arg);
        }
    }

    if (in_file == 0) {
        // try loading last 
        if (prefs) { // = new Fl_Preferences( Fl_Preferences::USER, vendor, mod_name ); 
            const char *dummy = "-";
            file_path_buffer[0] = 0;
            int res = prefs->get(szlast_infile, file_path_buffer, dummy, FL_PATH_MAX);
            if (file_path_buffer[0] && strcmp(file_path_buffer,dummy) && 
                (is_file_or_directory(file_path_buffer) == DT_FILE)) {
                in_file = strdup(file_path_buffer);
                printf("Set input file to previous [%s]\n",in_file);
            } else {
                printf("ERROR: No input file found in command!\nAnd no previous file to load!\nAborting...");
                pgm_exit(1);
            }
        } else {
            printf("ERROR: No input file found in command!\nAborting...\n");
            pgm_exit(1);
        }
    }
    if (in_file) {
        if (is_file_or_directory((char *)in_file) != DT_FILE) {
            printf("ERROR: Can not 'stat' file [%s]!\nAborting...\n", in_file);
            pgm_exit(1);
        }
    }
}

char *get_in_file() 
{
    if (in_file) {
        return get_base_name((char *)in_file);
    }
    return 0;
}

const char *file_pattern = "*.ac";
void open_cb(Fl_Widget*, void*) 
{
    int type = Flu_File_Chooser::STDFILE;
    Flu_File_Chooser *f = new Flu_File_Chooser( in_file, file_pattern, type, "Select AC3D File" );
    //f->set_sort_function( length_sort );
    f->set_modal();
    f->show();
    f->add_type( "ac", "AC3D File" /*, icon */ );
    f->add_type( "*", "All Files" );
    while( f->shown() )
        Fl::wait();
    int count = f->count();
    if (count == 0) {
        printf("Cancelled file selection\n");
    } else if (count > 1) {
        printf("Multiple selection NOT allowed!\n");
    } else {
        int ok = 0;
        std::string s("Selected: ");
        const char *full_path = f->value(1);
	    s += full_path;
        if (rob)
            ac_free_objects();
        rob = ac_load_ac3d((char *)full_path);
        if (rob) {
            s += " loaded";
            int display_list = ac_display_list_render_object(rob);
            if (display_list > 0) {
                in_file = strdup(full_path);
                //done_ac_init = 0;
                char *cp = GetNxtBuf();
                if (preview->display_list > 0) {
                    sprintf(cp," del dl %d", preview->display_list);
                    glDeleteLists(preview->display_list, 1);    // delete the current list
                    s += cp;
                }
                sprintf(cp," new dl %d", display_list);
                preview->display_list = display_list; // set for this file
                s += cp;
                ok = 1;
            } else {
                s += " FAILED DL!";
            }
        } else {
            s += " FAILED!";
        }
        if (ok) {
            prefs->set(szlast_infile, full_path);
            prefs->flush();
            get_file_preferences(); // any preferences for this file?
            populate_Tree();
        }
        sprtf("%s\n", s.c_str());
    }
}

void close_cb(Fl_Widget*, void*) {}

void quit_cb(Fl_Widget*, void*) 
{
    //prefs->flush();
    ac_free_objects();
    clearNameVector();
    pgm_exit(0);
}

void test_cb(Fl_Widget* w, void*) 
{
    Fl_Menu_* mw = (Fl_Menu_*)w;
    const Fl_Menu_Item* m = mw->mvalue();
    if (!m)
        printf("test_cb: NULL\n");
    else if (m->shortcut())
        printf("test_cb: %s - %s\n", m->label(), fl_shortcut_label(m->shortcut()));
    else
        printf("test_cb: %s\n", m->label());
}


Fl_Menu_Item menutable[] = {
  {"&File",0,0,0,FL_SUBMENU},
    {"&Open",    FL_ALT+'o', open_cb,  0 },
    {"&Close",   FL_ALT+'c', close_cb, 0 },
    {"&Quit",    FL_ALT+'q', quit_cb,  0 },
    {0},
//  {"&Edit",FL_F+2,0,0,FL_SUBMENU},
//    {"Undo",     FL_ALT+'z',    0},
//    {"Redo",     FL_ALT+'r',    0, 0, FL_MENU_DIVIDER},
//    {"Cut",      FL_ALT+'x',    0},
//    {"Copy",     FL_ALT+'c',    0},
//    {"Paste",    FL_ALT+'v',    0},
//    {0},
#ifdef ADD_ACTIONS_2_MENU
  {"&Actions",0,0,0,FL_SUBMENU},
      {"&Write AC", FL_ALT+'W', writeACCB, 0 },
      {"&Delete"  , FL_ALT+'D', removeCB,  0 },
      {"&Restore" , FL_ALT+'R', refreshCB, 0 },
      {0},
#endif
  {0}
};


#define MX_SECS 10
static int frame_cnts[MX_SECS];
static time_t prev_tm;
static int idl_cnt = 0;
static int sec_cnt = 0;
static double next_secs;
static double min_secs = 0.05;  // run at 20 fps

void idle(void *)
{
    idl_cnt++;
    time_t curr = time(0);
    if (curr != prev_tm) {
        frame_cnts[sec_cnt++] = idl_cnt;
        idl_cnt = 0;
        if (sec_cnt >= MX_SECS) {
            int total = 0;
            for (sec_cnt = 0; sec_cnt < MX_SECS; sec_cnt++)
                total += frame_cnts[sec_cnt];
            if ((last_tottime > 0.0) && (last_framec > 0)) {
                printf("%.1f: fps: abs %d throttled: %.1f (%d)\n", total_time,
                    total / MX_SECS, 1.0/(last_tottime/last_framec), frames_per_sec);
            } else {
                printf("%.1f: fps: abs %d throttled %d\n", total_time, total / MX_SECS, frames_per_sec);
            }
            sec_cnt = 0;
        }
        prev_tm = curr;
    }
    double dcurr = get_seconds();
    double elap = dcurr - next_secs;
    if (elap < min_secs)
        return;
    next_secs = dcurr;
    if (done_gl_init) 
        preview->draw();
}

void set_idle() {
    prev_tm = time(0);
    Fl::add_idle(idle);
}

static const char *szx_trans = "x_trans"; // = 0.0f;
static const char *szy_trans = "y_trans"; // = -4.0f;  // was 0.0;
static const char *szz_trans = "z_trans"; // = -10.0f;  // was -3.0
static const char *szangle_rate = "angle_rate"; // = 25.0f; // change at rate at ?? degrees per second
static const char *szfrozen = "frozen"; // = 0;
static const char *szx_axis = "x_axis"; // = 0.0f;
static const char *szy_axis = "y_axis"; // = 1.0f;
static const char *szz_axis = "z_axis"; // = 0.0f;
static const char *szrot = "rot"; // = 0.0;

char *get_file_preferences()
{
    char *inf = get_in_file();
    if (inf) {
        Fl_Preferences gl( prefs, inf );
        gl.get(szx_trans, x_trans, 0.0f);
        gl.get(szy_trans, y_trans, -4.0f);  // was 0.0;
        gl.get(szz_trans, z_trans, -10.0f);  // was -3.0
        gl.get(szangle_rate, angle_rate, 25.0f); // change at rate at ?? degrees per second
        gl.get(szfrozen, frozen, 0);
        gl.get(szx_axis, x_axis, 0.0f);
        gl.get(szy_axis, y_axis, 1.0f);
        gl.get(szz_axis, z_axis, 0.0f);
        gl.get(szrot, rot, 0.0);
    }
    return inf;
}

void init_preferences()
{
    prefs = new Fl_Preferences( Fl_Preferences::USER, vendor, mod_name ); 
}

void load_preferences()
{
    int res;
    res = prefs->get(szwin_height, win_height, win_height);
    res = prefs->get(szwin_width, win_width, win_width);
    res = prefs->get(szwin_posx, pos_x, 0);
    res = prefs->get(szwin_posy, pos_y, 0);
    // last out file path
    res = prefs->get(szlast_outpath, file_path_buffer, "~", FL_PATH_MAX);
    out_file = strdup(file_path_buffer);
    get_file_preferences();
}

char *save_file_preferences()
{
    char *inf = get_in_file();
    if (inf) {
        Fl_Preferences gl( prefs, inf );
        gl.set(szx_trans, x_trans);
        gl.set(szy_trans, y_trans);  // was 0.0;
        gl.set(szz_trans, z_trans);  // was -3.0
        gl.set(szangle_rate, angle_rate); // change at rate at ?? degrees per second
        gl.set(szfrozen, frozen);
        gl.set(szx_axis, x_axis);
        gl.set(szy_axis, y_axis);
        gl.set(szz_axis, z_axis);
        gl.set(szrot, rot);
        prefs->flush();
    }
    return inf;
}

void new_size(int x, int y, int w, int h)
{
    int res, cnt;
    cnt = 0;
    res = 0;
    if ((x != pos_x) || (y != pos_y) || (w != win_width) || (h != win_height)) {
        printf("New size: x=%d, y=%d, w=%d, h=%d\n", x, y, w, h );
        if (pos_x != x) {
            pos_x = x;
            res += prefs->set(szwin_posx, pos_x);
            cnt++;
        }
        if (pos_y != y) {
            pos_y = y;
            res += prefs->set(szwin_posy, pos_y);
            cnt++;
        }
        if (win_width != w) {
            win_width = w;
            res += prefs->set(szwin_width, win_width);
            cnt++;
        }
        if (win_height != h) {
            win_height = h;
            res += prefs->set(szwin_height, win_height);
            cnt++;
        }
    }
    if (cnt || res)
        prefs->flush(); // commit to disk

}


// debug file : C:\FG\18\blendac3d\c172p\c172pm.ac
// or a very simple one : C:\FG\18\ac2glview\data\cube.ac
int main( int argc, char **argv )
{
    init_preferences();

    parse_commands( argc, argv );

    load_preferences();

    if (in_file) {
        prefs->set(szlast_infile, in_file);
        prefs->flush();
    }

    mwin = new mainWin( pos_x, pos_y, win_width, win_height, "ac3d_browser" );
    //win = new Fl_Double_Window( WINDOW_WIDTH, WINDOW_HEIGHT, "ac3d_browser" );
    printf("mainWin: x=%d y=%d w=%d h=%d\n", pos_x, pos_y, win_width, win_height);
    // add MENU
    Fl_Menu_Bar menubar(0,0,win_width,30); 
    menubar.menu(menutable);    // set table
    menubar.callback(test_cb);  // not sure why this???
    tree_y = 30;
    tree_height = win_height - 30;
    group_y = 30;
    group_height -= 30;
    multi_height = win_height - rgl_height - (3 * y_val_steps);

    // build the tree
    // =============================================================
    tree = new Flu_Tree_Browser( tree_x, tree_y, tree_width, tree_height );
    tree->box( FL_DOWN_BOX );
    tree->auto_branches( true );
    tree->insertion_mode( FLU_INSERT_BACK ); // does this STOP sorting??? YEAH! perfect - no ordered as per file
    // tree->label( proot_node );
    tree->callback( tree_callback );
    // =============================================================
    printf("treeWin: x=%d y=%d w=%d h=%d\n", tree_x, tree_y, tree_width, tree_height);

    // build the group
    // ===============================================================
    Fl_Group *g = new Fl_Group( group_x, group_y, group_width, group_height );
    printf("treeWin: x=%d y=%d w=%d h=%d\n", group_x, group_y, group_width, group_height);
    g->resizable( NULL );
    int y_val = initial_y_val;
    newNode = new Fl_Input( 50, y_val, group_width - 60, 20, "File:" );
    newNode->value( out_file );
    y_val += y_val_steps;
#ifndef ADD_ACTIONS_2_MENU
    addChild = new Fl_Button( 10, y_val, button_width, button_height, "Write ac" );
    addChild->callback( writeACCB, NULL );
    y_val += Y_VAL_STEPS;
    removeBtn = new Fl_Button( 10, y_val, button_width, button_height, "Delete" );
    removeBtn->callback( removeCB, NULL );
    removeBtn->color(0xff000000);
    y_val += Y_VAL_STEPS;
    refreshBtn = new Fl_Button( 10, y_val, button_width, button_height, "Restore" );
    refreshBtn->callback( refreshCB, NULL );
    y_val += Y_VAL_STEPS;
    textOutput = new Fl_Multiline_Output(10, y_val, multi_width, multi_height);
#else
    // add the gl review window
    preview = new PreviewWin( 10, y_val, rgl_width, rgl_height );
    printf("GLWin: x=%d y=%d w=%d h=%d\n", 10, y_val, rgl_width, rgl_height);
    y_val += rgl_height;
    //multi_height -= rgl_height;
    textOutput = new Fl_Multiline_Output(10, y_val, multi_width, multi_height);
    printf("MultiWin: x=%d y=%d w=%d h=%d\n", 10, y_val, multi_width, multi_height);
#endif
    g->end();
    // ===============================================================

    mwin->end();
    mwin->resizable( tree );
    mwin->show( 1, argv );
    mwin->set_resizecb( new_size );

    ac_set_load_textures(0);    // tell library NOT to load textures
    rob = ac_load_ac3d((char *)in_file);
    if (!rob) {
		printf("%s: ERROR: failed to load file \n[%s]!\nAborting... exit(1)\n", mod_name, in_file);
        pgm_exit(1);
    }
    prev_secs = next_secs = get_seconds();
    last_secs = time(0);
    populate_Tree();

    set_idle(); // establish an IDLE callback

    int ret = Fl::run();

    ac_free_objects();
    clearNameVector();

    return ret;
}

// eof - ac3d_browser.cpp
