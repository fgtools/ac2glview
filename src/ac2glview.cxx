//
// "$Id: gl_overlay.cxx 8864 2011-07-19 04:49:30Z greg.ercolano $"
//
// OpenGL overlay test program for the Fast Light Tool Kit (FLTK).
//
// Copyright 1998-2010 by Bill Spitzak and others.
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     http://www.fltk.org/COPYING.php
//
// Please report all bugs and problems on the following page:
//
//     http://www.fltk.org/str.php
//
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H
#include <sys/types.h>
#include <sys/stat.h> // mkdir()
#include <stdio.h>
#include <time.h>
#include <string>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Hor_Slider.H>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/fl_draw.H>
#include <FL/math.h>
#include <FL/filename.H>
#include <FL/Fl_Preferences.H>
#include <FLU/Flu_File_Chooser.h>
#include <FL/gl.h>
#include <FL/Fl_Gl_Window.H>
#include <FL/names.h>         // defines fl_eventnames[]

#include <GL/glu.h>
#ifdef WIN32
#include <Windows.h>
#include <direct.h>
#endif
#include "misc_lib.hxx"

#ifndef HAVE_AC2GL2
#include <ac3d2.hxx>
#else
#include <ac3d.hxx>
#endif


// forward ref 
void drawscene(int);


#define WIN_WIDTH 300*2
#define WIN_HEIGHT 370*2
static int win_width = WIN_WIDTH;
static int win_height = WIN_HEIGHT;
static const char *szwin_height = "win_height";
static const char *szwin_width = "win_width";
static const char *szwin_posx = "win_posx";
static const char *szwin_posy = "win_posy";

static const char *vendor = "geoffair";
static const char *mod_name = "ac2glview";
static Fl_Preferences *prefs; 

static char file_path_buffer[FL_PATH_MAX+2];
const char *last_path = "~";
const char *file_pattern = "*.ac";
const char *szlast_path = "last_path";
const char *szfile_pattern = "file_pattern";
#define MX_TITLE_BUF 264
static char title_buff[MX_TITLE_BUF+2];

static int pos_x = 0;
static int pos_y = 0;

static PACObject pob = 0;
static int display_list = 0;
static int window_width, window_height;
static const char *acFileName = 0;
static int done_ac_init = 0;

// ===========================================
// read and write this set to a config file
// ===========================================
static float x_trans =   0.0f;
static float y_trans =  -4.0f;  // was 0.0;
static float z_trans = -10.0f;  // was -3.0
static float angle_rate = 25.0f; // change at rate of ?? degrees per second
static int frozen = 1;  // untill too fast spin fixed, defaault to OFF
static float x_axis = 0.0f;
static float y_axis = 1.0f;
static float z_axis = 0.0f;
static float rot = 0.0;
//static int window_width = WINDOW_WIDTH;
//static int window_height = WINDOW_HEIGHT;
// ===========================================

static float frametime;
static float tottime = 0.0;
static int framec = 0;
static float total_time = 0.0;

static time_t last_secs;
static int frames = 0;
static int frames_per_sec = 0;
static int elap_secs = 0;
static double begin_secs, prev_secs;


class MyWin : public Fl_Window {
public:
    int handle(int event);
    MyWin(int x, int y, int w, int h, const char *label);
};

MyWin::MyWin(int x, int y, int w, int h, const char *label) : Fl_Window(x, y, w, h, label)
{
    // do any initialization stuff...
}

static void keyfn ( unsigned char c, int i1, int i2 );

int MyWin::handle(int e)
{
    int res = 1;
    int key = 0;
    switch (e) {
    case FL_KEYDOWN:
        if (Fl::event_state() & FL_CTRL) break;
        key = Fl::event_key();
        keyfn( key, 0, 0 );
        break;
    default:
       res = Fl_Window::handle(e);
    }
    if (ac_verb9()) {
        printf("Event was %s (%d) res %d\n", fl_eventnames[e], e, res);
    }
    return res;
}

//static Fl_Window *window;
static MyWin *window;

//void resize_cb(Fl_Widget* w, void*) 
// in windows, written to C:\Users\user\AppData\Roaming\geoffair\ac2glview.prefs
void resize_cb() 
{
    prefs->set(szwin_height, window->h());
    prefs->set(szwin_width, window->w());
    // how to get window position
    prefs->set(szwin_posx, window->x());
    prefs->set(szwin_posy, window->y());
    prefs->flush(); // hope fully write to disk
}

class shape_window : public Fl_Gl_Window 
{
public:
    void setup(int resize = 0);
    void draw();
    void draw_overlay();
    int sides;
    int overlay_sides;
    shape_window(int x,int y,int w,int h,const char *l=0);
};

shape_window::shape_window(int x,int y,int w,int h,const char *l) : Fl_Gl_Window(x,y,w,h,l)
{
    sides = overlay_sides = 3;
}

void ac_deprep_render()
{
    glMatrixMode(GL_PROJECTION);
    glEnable( GL_COLOR_MATERIAL ); 
    glDisable( GL_DEPTH_TEST );
}

void ac_prep_render()
{

/*
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);
*/

    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    glDisable( GL_COLOR_MATERIAL ); 

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

}

typedef void (*DRAWER)(int);

void d_ovr( int sides )
{
    // draw an amazing graphic:
    gl_color(FL_RED);
    glBegin(GL_LINE_LOOP);
    for (int j = 0; j < sides; j++) {
        double ang = j*2*M_PI/sides;
        glVertex3f(cos(ang),sin(ang),0);
    }
    glEnd();
}

void d_scn( int sides )
{
    // draw an amazing but slow graphic:
    glClear(GL_COLOR_BUFFER_BIT);
    //  for (int j=1; j<=1000; j++) {
    glBegin(GL_POLYGON);
    for (int j = 0; j < sides; j++) {
        double ang = j*2*M_PI/sides;
        glColor3f(float(j)/sides,float(j)/sides,float(j)/sides);
        glVertex3f(cos(ang),sin(ang),0);
    }
    glEnd();
}


DRAWER dd_ovr = d_ovr;
DRAWER dd_scn = d_scn;

void shape_window::setup(int resize) 
{
    if (resize) {
        //resize_cb( (Fl_Widget *)window, 0 );
        resize_cb();
    }
    if (acFileName)
        return;
    ac_deprep_render();
    glLoadIdentity();
    window_width = w();
    window_height = h();
    glViewport(0,0,window_width,window_height);
}

void shape_window::draw() 
{
    // the valid() property may be used to avoid reinitializing your
    // GL transformation for each redraw:
    if (!valid()) {
        valid(1);
        setup(1);
    }
    dd_scn(sides);
}

void shape_window::draw_overlay() 
{
    // the valid() property may be used to avoid reinitializing your
    // GL transformation for each redraw:
    if (!valid()) {
        valid(1);
        setup(0);
    }
    dd_ovr(overlay_sides);
}

static shape_window *psw; // (10, 40, window->w()-20, window->h()-50);

/* ---------------------------------------------------
    save and restore configuration file
    An INI type file, with section [name_of.ac]
    conf_file = ".ac3d2glrc";
    parameters kept/restored
    float x_trans =   0.0f;
    float y_trans =  -4.0f;  // was 0.0;
    float z_trans = -10.0f;  // was -3.0
    double angle_rate = 25.0; // change at rate of ?? degrees per second
    int frozen = 0;
    float x_axis = 0.0f;
    float y_axis = 1.0f;
    float z_axis = 0.0f;
    float rot = 0.0;
  ----------------------------------------------------- */
static const char*conf_file = ".ac3d2glrc";
static const char *app_name = "ac3dview";

static const char *szx_trans = "x_trans";
static const char *szy_trans = "y_trans";
static const char *szz_trans = "z_trans";
static const char *szangle_rate = "angle_rate";
static const char *szfrozen = "frozen";
static const char *szx_axis = "x_axis";
static const char *szy_axis = "y_axis";
static const char *szz_axis = "z_axis";
static const char *szrot = "rot";
static const char *szwwid = "window_width";
static const char *szwhei = "window_height";

#ifdef WIN32
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

#define t_int 1
#define t_float 2

typedef struct tagMYINI {
    const char *sz;
    int typ;
    char *val;
}MYINI, *PMYINI;

static MYINI myini[] = {
    { szx_trans, t_float, (char *)&x_trans },
    { szy_trans, t_float, (char *)&y_trans },
    { szz_trans, t_float, (char *)&z_trans },
    { szx_axis,  t_float, (char *)&x_axis  },
    { szy_axis,  t_float, (char *)&y_axis  },
    { szz_axis,  t_float, (char *)&z_axis  },
    { szrot,     t_float, (char *)&rot     },
    { szangle_rate,t_float,(char *)&angle_rate },
    { szfrozen,  t_int,   (char *)&frozen  },
    { szwwid,    t_int,   (char *)&window_width },
    { szwhei,    t_int,   (char *)&window_height },
    // always last
    { 0,        0,        0               }
};


char *get_config_dir()
{
    char *data = 0;
#ifdef _MSC_VER
    data = getenv("APPDATA");
    if (!data) {
        data = getenv("LOCALAPPDATA");
    }
#else
    data = getenv("HOME");
#endif
    return data;
}

int get_config_file( std::string &s )
{
    s += PATH_SEP;
    s += app_name;
    if ( is_file_or_directory ( (char *) s.c_str() ) != DT_DIR ) return 0;
    s += PATH_SEP;
    s += conf_file;
    if ( is_file_or_directory ( (char *) s.c_str() ) != DT_FILE ) return 0;
    return 1;
}

int my_strncmp(const char *string1, const char *string2, size_t count )
{
    while (*string1 && *string2 && count) {
        if (*string1 != *string2)
            return *string1 - *string2;
        string1++;
        string2++;
        count--;
    }
    return 0;
}

const char *get_config_file_name()
{
    static std::string cfg;
    char *data = get_config_dir();
    if (!data) return "";
    cfg = data;
    if (!get_config_file(cfg)) return "";
    return cfg.c_str();
}


int read_config()
{
    int ichg = 0;
    int iclean = 0;
    char *data = get_config_dir();
    if (!data) return -1;
    std::string s(data);
    if (!get_config_file(s)) return -2;
    FILE *fp = fopen(s.c_str(),"r");
    if (!fp) return -3;
    if (!acFileName) {
        fclose(fp);
        return -4;
    }
    std::string sect(get_base_name((char *)acFileName));
    int len = (int)sect.size();
    char *rb = title_buff;
    int in_section = 0;
    int had_section = 0;
    int vi;
    float fi;
    while (!feof(fp)) {
        char *gp = fgets(rb,MX_TITLE_BUF,fp);
        if (!gp) break;
        size_t red = strlen(rb);
        clean_buff_tail(rb);
        red = strlen(rb);
        if (rb[0] == '[') {
            int cmp = my_strncmp(&rb[1],sect.c_str(),len);
            if ((cmp == 0) && (rb[len+1] == ']')) {
                in_section = 1; // found 'our' section
                had_section = 1;
            } else {
                in_section = 0;
                if (had_section) break;
            }
        } else if (in_section) {
            PMYINI pini = &myini[0];
            size_t len2;
            char *tp;
            int fnd = 0;
            while (pini->sz) {
                len2 = strlen(pini->sz);
                if (red > len2) {
                    if ((strncmp(rb,pini->sz,len2) == 0) && (rb[len2] == '=')) {
                        fnd = 1;
                        tp = &rb[len2+1];
                        switch (pini->typ) {
                        case t_int:
                            vi = atoi(tp);
                            if (*(int *)pini->val != vi) {
                                *(int *)pini->val = vi;
                                ichg++;
                            }
                            break;
                        case t_float:
                            fi = (float)atof(tp);
                            if (*(float *)pini->val != fi) {
                                *(float *)pini->val = fi;
                                ichg++;
                            }
                            break;
                        }
                    }
                }
                if (fnd) break; // done this parameter
                pini++;
            }
            if (!fnd) {
                if (iclean == 0)
                    printf("In the [%s] section\n", sect.c_str());
                printf("Found [%s] in config file, not used!\n", rb);
                iclean++;
            }
        }

    }
    fclose(fp);
    if (iclean) {
        printf("Note the above %d item(s) should be removed from the\n%s config file\n",
            iclean, s.c_str());
    }
    return ichg;
}

// just an estimate of the maximum size of a section
size_t maximum_section(int slen)
{
    size_t total = slen + 4;
    PMYINI pini = &myini[0];
    size_t len2;
    while (pini->sz) {
        len2 = strlen(pini->sz);
        len2 += 32; // + '=' and a float
        total += len2;
        pini++;
    }
    return total;
}

#ifdef _WIN32
#  define MkDir(d,m)       _mkdir(d)
#else
#  define MkDir(d,m)       mkdir(d,m)
#endif

#define EndBuf(a)   ( a + strlen(a) )

int write_config()
{
    char *data = get_config_dir();
    if (!data) return -1;
    std::string s(data);
    s += PATH_SEP;
    s += app_name;
    if ( is_file_or_directory ( (char *) s.c_str() ) != DT_DIR ) {
        // need to create directory
        if ( MkDir( s.c_str(), 0777 )) {
            printf("error: Unable to create directory [%s]!\nThus unable to write config!\n", s.c_str());   
            return -2;
        }
    }
    s += PATH_SEP;
    s += conf_file;
    FILE *fp = 0;
    size_t size = 0;
    if ( is_file_or_directory ( (char *) s.c_str() ) == DT_FILE ) {
        fp = fopen(s.c_str(),"r");
        if (!fp) {
            printf("error: Unable to read file [%s]!\nThus unable to write config!\n", s.c_str());   
            return -3;
        }
        fseek(fp,0,SEEK_END);   // = 2
        size = ftell(fp);
        fseek(fp,0,SEEK_SET);   // = 0
    }
    if (!acFileName) {
        fclose(fp);
        return -4;
    }
    std::string sect(get_base_name((char *)acFileName));
    int len = (int)sect.size();
    size_t sbuf = size + maximum_section(len);
    char *rb = (char *)malloc(sbuf);
    if (!rb) {
        // complain about memory???
        if (fp) fclose(fp);
        return -4;
    }

    // write this section into allocated memory
    PMYINI pini = &myini[0];
    *rb = 0;
    size_t cfglen = sprintf(EndBuf(rb),"[%s]\n", sect.c_str());
    while (pini->sz) {
        cfglen += sprintf(EndBuf(rb),"%s=", pini->sz);
        switch (pini->typ) {
        case t_int:
            cfglen += sprintf(EndBuf(rb),"%d\n", *(int *)pini->val);
            break;
        case t_float:
            cfglen += sprintf(EndBuf(rb),"%f\n", *(float *)pini->val);
            break;
        }
        pini++;
    }

    // reallocate if we need more buffer
    if ((cfglen + size + 2) >= sbuf) {
        sbuf = cfglen + size + 2;
        rb = (char *)realloc(rb,sbuf);
        if (!rb) {
            // complain about memory???
            fclose(fp);
            return -5;
        }
    }

    if (fp) {
        // ok, must read and filter the existing file
        int in_section = 0;
        char *gp = EndBuf(rb);
        while (!feof(fp)) {
            gp = fgets(gp,MX_TITLE_BUF,fp);
            if (!gp) break;
            size_t red = strlen(gp);
            if (gp[0] == '[') {
                if ((strncmp(&rb[1],sect.c_str(),len) == 0) && (rb[len+1] == ']')) {
                    in_section = 1; // found 'our' section
                } else {
                    in_section = 0;
                    cfglen += red;
                    gp = EndBuf(gp);
                }
            } else if (!in_section) {
                cfglen += red;
                gp = EndBuf(gp);
            }
        }
        fclose(fp);
    }

    // =================================================================
    // overwrite any existing file with buffered config data
    fp = fopen(s.c_str(),"w");
    if (!fp) {
        printf("error: Unable to write file [%s]!\nThus unable to write config!\n", s.c_str());
        free(rb);
        return -6;
    }
    size_t wtn = fwrite(rb,1,cfglen,fp);
    fclose(fp);
    free(rb);
    return ((wtn == cfglen) ? 0 : -7);   // success in writting config file
}


static void key_help()
{
    int tframes = frames_per_sec;
    printf("Keyboard Help - to size, position and rotate the model.\n");
    printf(" ESC or q = Quit\n");
    printf(" f/F      = Toggle rotation freeze. (def=%s)\n", (frozen ? "On" : "Off"));
    printf(" s/S      = Slow/Speed up rotation speed. angle_rate=%f\n", angle_rate);
    printf(" x/X      = Left/Right - x translation factor (%f)\n", x_trans);
    printf(" y/Y      = Up/Down    - y translation factor (%f)\n", y_trans);
    printf(" z/Z      = Near/Far   - z translation factor (%f)\n", z_trans);
    printf(" ?/h      = This help, frame rate and current angle...\n");
    printf(" 1-7      = Set glRotate(rot,x,y,z) - %.1f,%.1f,%.1f\n", x_axis, y_axis, z_axis);
    printf(" d/D      = Dump scene to numbered PPM file in work directory. (D in stereo)\n");
    printf(" v/V      = Reduce/Increase verbosity. (def=%d)\n", ac_get_verbosity());
    printf(" w/W      = Write/Read config file.\n");
    printf(" fps=%d, rotation angle=%f, secs %d\n", tframes, rot, elap_secs);
}

// in the range -10 to 10 subtract 0.5
// if lesser or greater   subtract 1/20 of the value
// if zero,               subtract 0.5
float sub_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next -= (next / 20.0f);
        } else {
            if (next < 0.5f)
                next = 0.0f;
            else
                next -= 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next -= (-next / 20.0f);
        else 
            next -= 0.5f;
    } else {
        next -= 0.5f;
    }
    return next;
}
// in the range -10 to 10 add 0.5
// if lesser or greater   add 1/20 of the value
// if zero,               add 0.5
float add_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next += (next / 20.0f);
        } else {
            next += 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next += (-next / 20.0f);
        else {
            if (next < -0.5f)
                next += 0.5f;
            else
                next = 0.0f;
        }
    } else {
        next += 0.5f;
    }
    return next;
}

static void keyfn ( unsigned char c, int i1, int i2 )
{
    int v, sxyz = 0;
    if ((c == 'q')||(c == 0x1b)) {
        printf("Got exit key...\n");
        ac_free_objects();
        exit ( 0 ) ;
    }

    if ((c == '?')||(c == 'h')) {
        key_help();
    } else if (c == 'w') {
        v = write_config();
        if (ac_verb2()) printf("w = Written config %s (%d)\n", get_config_file_name(), v);
    } else if (c == 'W') {
        v = read_config();
        if (ac_verb2()) printf("W = Read config %s (%d)\n", get_config_file_name(), v);
    } else if ((c == 'f')||(c == 'F')) {
        if (frozen)
            frozen = 0;
        else
            frozen = 1;
        if (ac_verb2()) printf("f = Toggle frozen %d\n", frozen);
    } else if (c == 's') {
        angle_rate -= 1.0;
        if (ac_verb2()) printf("angle_rate = %f\n", angle_rate);
    } else if (c == 'S') {
        angle_rate += 1.0;
        if (ac_verb2()) printf("angle_rate = %f\n", angle_rate);
    } else if (c == 'x') {
        //x_trans -= 0.5f;
        x_trans = sub_5_percent(x_trans);
        if (ac_verb2()) printf("x_trans = %f\n", x_trans);
    } else if (c == 'X') {
        //x_trans += 0.5f;
        x_trans = add_5_percent(x_trans);
        if (ac_verb2()) printf("x_trans = %f\n", x_trans);
    } else if (c == 'y') {
        //y_trans -= 0.5f;
        y_trans = sub_5_percent(y_trans);
        if (ac_verb2()) printf("y_trans = %f\n", y_trans);
    } else if (c == 'Y') {
        //y_trans += 0.5f;
        y_trans = add_5_percent(y_trans);
        if (ac_verb2()) printf("y_trans = %f\n", y_trans);
    } else if (c == 'z') {
        //z_trans -= 0.5f;
        z_trans = sub_5_percent(z_trans);
        if (ac_verb2()) printf("z_trans = %f\n", z_trans);
    } else if (c == 'Z') {
        //z_trans += 0.5;
        z_trans = add_5_percent(z_trans);
        if (ac_verb2()) printf("z_trans = %f\n", z_trans);
    } else if (c == '1') {
       x_axis = 1.0;
       y_axis = 0.0;
       z_axis = 0.0;
       sxyz = 1;
    } else if (c == '2') {
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 0.0;
       sxyz = 1;
    } else if (c == '3') {
       x_axis = 0.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
    } else if (c == '4') {
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 0.0;
    } else if (c == '5') {
       x_axis = 1.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
    } else if (c == '6') {
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
   } else if (c == '7') {
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
    } else if (c == 'd') {
        WritePPM( window_width, window_height, 0 );
    } else if (c == 'D') {
        WritePPM( window_width, window_height, 0, 1 );
    } else if (c == 'v') {
        v = ac_get_verbosity();
        if (v) {
            v--;
            ac_set_verbosity(v);
        }
    } else if (c == 'V') {
        v = ac_get_verbosity();
        v++;
        ac_set_verbosity(v);
    }
    if (sxyz && ac_verb2()) printf("%c: set x,y,z axis %.1f,%.1f,%.1f\n", c, x_axis, y_axis, z_axis);
}


void set_projection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float)w/(float)h, 0.1, 10000.0);
}

void init_gfx(void)
{
    ac_prepare_render();
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    set_projection(window_width,window_height);
}


void drawscene(int)
{
    double curr = get_seconds();
    double elap = curr - prev_secs;
    time_t currs = time(0);
    GLfloat light_position[] = { -1000.0, 1000.0, 1000.0, 0.0 };

    timer_start();

    set_projection(window_width, window_height);

    gluLookAt(0, 1.8, 4, 0, 0, 0, 0, 1, 0);

    glClearColor(0,0,0,0);
    glClearColor(0.5,0.5,0.5,0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glTranslatef ( x_trans, y_trans, z_trans ) ;

    // this can be TOO FAST!
    //rot += 0.01f; // was 1
    if (!frozen) {
        // bump the rotation angle 
        rot += (float) ( elap * angle_rate );
    }

    if (rot >= 360.0) rot -= 360.0f;

    // glRotatef(rot, 0, 1, 0);
    glRotatef(rot, x_axis, y_axis , z_axis );

    glCallList(display_list);
    glFlush();
    glFinish();
    //glutSwapBuffers();

    frametime = timer_stop();
    tottime += frametime;
    framec++;
    if (tottime > 5.0) {
        total_time += tottime;
        if (ac_verb5()) {
    	    printf("%.1f: Approx frames per sec: %.1f (%d)\n", 
                total_time, 1.0/(tottime/framec), frames_per_sec);
        }
	    tottime = 0;
	    framec = 0;
    }
    frames++;
    if (currs != last_secs) {
        frames_per_sec = frames;
        frames = 0;
        last_secs = currs;
        elap_secs++;
    }
    prev_secs = curr;
}



void quit_cb(Fl_Widget*, void*) 
{
    prefs->flush();
    exit(0);
}


void open_cb(Fl_Widget*, void*) 
{
    int type = Flu_File_Chooser::STDFILE;
    Flu_File_Chooser *f = new Flu_File_Chooser( last_path, file_pattern, type, "Select AC3d File" );
    //f->set_sort_function( length_sort );
    f->set_modal();
    f->show();
    f->add_type( "ac", "AC3D File" /*, icon */ );
    f->add_type( "*", "All Files" );
    while( f->shown() )
        Fl::wait();
    int count = f->count();
    if (count == 0) {
        printf("Cancelled file selection\n");
    } else if (count > 1) {
        printf("Multiple selection NOT allowed!\n");
    } else {
        int ok = 0;
        printf( "selected:" );
        const char *full_path = f->value(1);
	    printf( "  %s", full_path );
        if (pob)
            ac_free_objects();
        pob = ac_load_ac3d((char *)full_path);
        if (pob) {
            printf(" loaded");
            display_list = ac_display_list_render_object(pob);
            if (display_list > 0) {
                acFileName = strdup(full_path);
                //done_ac_init = 0;
                ok = 1;
            } else {
                printf(" FAILED!");
            }
        } else {
            printf(" FAILED!");
        }
        printf("\n");
        if (ok) {
            init_gfx();
            dd_ovr = drawscene;
            dd_scn = drawscene;
            prefs->set(szlast_path, full_path);
            prefs->flush();
        }
    }
}

void close_cb(Fl_Widget*, void*) {}

void test_cb(Fl_Widget* w, void*) 
{
    Fl_Menu_* mw = (Fl_Menu_*)w;
    const Fl_Menu_Item* m = mw->mvalue();
    if (!m)
        printf("NULL\n");
    else if (m->shortcut())
        printf("%s - %s\n", m->label(), fl_shortcut_label(m->shortcut()));
    else
        printf("%s\n", m->label());
}

Fl_Menu_Item menutable[] = {
  {"&File",0,0,0,FL_SUBMENU},
    {"&Open",    FL_ALT+'o', open_cb,  0 },
    {"&Close",   FL_ALT+'c', close_cb, 0 },
    {"&Quit",    FL_ALT+'q', quit_cb,  0 },
    {0},
  {"&Edit",FL_F+2,0,0,FL_SUBMENU},
    {"Undo",     FL_ALT+'z',    0},
    {"Redo",     FL_ALT+'r',    0, 0, FL_MENU_DIVIDER},
    {"Cut",      FL_ALT+'x',    0},
    {"Copy",     FL_ALT+'c',    0},
    {"Paste",    FL_ALT+'v',    0},
    {0},
  {0}
};

static int idle_called = 0;
void idle_cb(Fl_Widget* w, void*) 
{
    idle_called++;
}

void setup_ac_file()
{
    if (!acFileName)
        return;
    // ac_set_load_textures(0); // turn of texture processing
    init_gfx();
    dd_ovr = drawscene;
    dd_scn = drawscene;
    if (pob) 
        ac_free_objects();
    pob = ac_load_ac3d((char *)acFileName);
    if (!pob) {
        printf("ERROR: Unable to load file [%s]\n", acFileName);
        exit(1);
    }
    display_list = ac_display_list_render_object(pob);
    if (display_list <= 0) {
        printf("FAILED to get a display list! aborting...\n");
        ac_free_objects();
        exit(1);
    }
    done_ac_init = 1;
}


static int ran_idle = 0;
#define MX_SECS 20
static int icounter = 0;
static int frame_cnts[MX_SECS];
static double next_secs;
static int change = 1;
static double min_secs = 0.05;  // run at 20 fps
static int change_ovr = 1;
void idle(void *)
{
    double curr = get_seconds();
    double elap = curr - next_secs;
    if (elap < min_secs)
        return;
    next_secs = curr;
    time_t curr_secs = time(0);
    ran_idle++;
    if (curr_secs != prev_secs) {
        frame_cnts[icounter] = ran_idle;
        icounter++;
        if (icounter >= MX_SECS) {
            icounter = 0;
            if (ac_verb5()) {
                int total = 0;
                int i;
                for (i = 0; i < MX_SECS; i++) 
                    total += frame_cnts[i];
                printf("fps %d\n", total / MX_SECS);
            }
        }
        prev_secs = curr_secs;
        ran_idle = 0;
        if (change_ovr) {
            psw->overlay_sides += change;
            if (psw->overlay_sides > 30)
                change = -1;
            else if (psw->overlay_sides < 4) {
                change = 1;
                change_ovr = 0;
                psw->redraw_overlay();
            }
        } else {
            psw->sides += change;
            if (psw->sides > 30)
                change = -1;
            else if (psw->sides < 4) {
                change = 1;
                change_ovr = 1;
                psw->redraw();
            }
        }
    }
    if (change_ovr)
        psw->redraw_overlay();
    else
        psw->redraw();
    if (acFileName && !done_ac_init)
        setup_ac_file();

}

void set_idle() {
    Fl::add_idle(idle);
}

void load_preferences()
{
    int res;
    prefs = new Fl_Preferences( Fl_Preferences::USER, vendor, mod_name ); 
    res = prefs->get(szwin_height, win_height, WIN_HEIGHT);
    res = prefs->get(szwin_width, win_width, WIN_WIDTH);
    res = prefs->get(szwin_posx, pos_x, 0);
    res = prefs->get(szwin_posy, pos_y, 0);
    res = prefs->get(szlast_path, file_path_buffer, "~", FL_PATH_MAX);
    last_path = strdup(file_path_buffer);
}

void give_cmd_help(char *name)
{
    printf("\n");
    printf("%s: version 1.14, compiled %s, at %s\n", name, __DATE__, __TIME__ );
    printf("\n");

    printf("USAGE: %s [options] [ac_file]\n", name );

    printf("\nOPTIONS:\n");
    printf(" --help    (-h or -?) = This help and exit(2)\n");
    printf(" --tex path      (-t) = Add a texture path.\n");
    printf(" --verb num      (-v) = Set verbosity 0-9. (def=%d)\n", ac_get_verbosity());
    printf(" --notex         (-n) = Make no attempt to load any textures.\n");
    //printf(" --dump file     (-d) = Just a DUMP of the ac object to a file.\n");
    //printf(" --export file   (-e) = Export the ac object as a Three.js (json) file, and exit.\n");
    //printf(" These options only apply to the export of the json Three.js\n");
    //printf(" --addnorm       (-a) = Add 'normal' generation to export. (def=0)\n");
    //printf(" --scale float   (-s) = Set the 'scale' used in the export. (def=%f)\n", ac_get_scale());
    if (acFileName)
        printf("\nIf no 'ac_file' given, then the default [%s] will be loaded.\n", acFileName);

    printf("\n");

    printf("AIM: To load an AC3D file, with textures, and display the model in a GL\n");
    printf("     context, and allow some rotation, sizing and positioning of the model\n");
    printf("     through keyboard input. When running '?' to show keys, with\n");
    printf("     ESC or q to exit program.\n");
    printf("\n");

    printf("Enjoy... aborting with error level 2\n");
    exit(2);
}

char *base_name( char *name )
{
    int i, len, c;
    char *bn = name;
    len = (int)strlen(name);
    for (i = 0; i < len; i++) {
        c = name[i];
        if ((c == '/')||(c == '\\'))
            bn = &name[i+1];
    }
    return bn;
}

int process_command(int argc, char *argv[])
{
    int i, c, i2;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'a':   /* --addnorm */
                ac_set_add_normals(1);
                break;
#if 0
            case 'd':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    dump_file = STRING(sarg);
                } else {
                    printf("%s: ERROR: a file name must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                do_dump =1;
                break;
            case 'e':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    dump_file = STRING(sarg);
                } else {
                    printf("%s: ERROR: a file name must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                do_dump = 2;
                break;
#endif // 0
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    ac_set_scale((float)atof(sarg));
                } else {
                    printf("%s: ERROR: a float value must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                break;
            case '?':
            case 'h':
                give_cmd_help(base_name(argv[0]));
                break;
            case 'n':   /* --notex */
                ac_set_load_textures(0);
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    if (is_file_or_directory ( sarg ) == DT_DIR) {
                        ac_add_texture_paths(sarg); /* add to texture paths */
                    } else {
                        printf("%s: ERROR: Unable to stat directory [%s]! aborting....\n", mod_name, sarg);
                        goto Bad_Arg;
                    }
                } else {
                    printf("%s: ERROR: a path must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                break;
            case 'v':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    ac_set_verbosity( atoi(sarg) );
                } else {
                    printf("%s: ERROR: a verbosity value must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                break;
            default:
Bad_Arg:
                printf("%s: ERROR: Unknown argument [%s]... aborting...\n", mod_name, arg );
                return 1;
                break;
            }
        } else {
            acFileName = STRING(arg);
        }
    }

    if (acFileName) {
        if (is_file_or_directory ( (char *)acFileName ) == DT_FILE)
            ac_set_file_name((char *)acFileName);   /* to be used as alternative path to a texture */
        else {
            printf("%s: ERROR: Unable to stat file [%s]! aborting....\n", mod_name, acFileName);
            return 1;
        }
    }
    return 0;
}



// example ac file : c:\FG\18\blendac3d\c172p\c172pm.ac
int main(int argc, char **argv)
{
    load_preferences();
    //read_config(); // has to be after reading command to get acFileName...
    if (process_command( argc, argv ))
        return 1;

    read_config();

    //window = new Fl_Window(win_width, win_height);
    //window = new Fl_Window(pos_x, pos_y, win_width, win_height, mod_name);
    if (acFileName) {
        char *cp = GetNxtBuf();
        sprintf(cp,"%s - %s", mod_name, get_base_name((char *)acFileName));
        window = new MyWin(pos_x, pos_y, win_width, win_height, cp);
    } else {
        window = new MyWin(pos_x, pos_y, win_width, win_height, mod_name);
    }

    // add MENU
    Fl_Menu_Bar menubar(0,0,win_width,30); 
    menubar.menu(menutable);    // set table
    menubar.callback(test_cb);  // not sure why this???

    // GL window
    //shape_window sw(10, 75, window.w()-20, window.h()-90);
    //shape_window sw(10, 40, window->w()-20, window->h()-50);
    window_width  = window->w() - 20;
    window_height = window->h() - 50;
    psw = new shape_window(10, 40, window_width, window_height);
    // sw.mode(FL_RGB); // not sure what this does, but was commented out anyway
    //window->resizable(&sw);
    window->resizable(psw);
    //window->add_idle(idle_cb);
    //window->callback(idle_cb);
    //window->when(-1);
    // window->callback( resize_cb );
    set_idle();
#if 0
    // none of this needed yet
    Fl_Hor_Slider slider(60, 5, window.w()-70, 30, "Sides:");
    slider.align(FL_ALIGN_LEFT);
    slider.callback(sides_cb,&sw);
    slider.value(sw.sides);
    slider.step(1);
    slider.bounds(3,40);
    // slider.hide();

    Fl_Hor_Slider oslider(60, 40, window.w()-70, 30, "Overlay:");
    oslider.align(FL_ALIGN_LEFT);
#if HAVE_GL
    oslider.callback(overlay_sides_cb,&sw);
    oslider.value(sw.overlay_sides);
#endif
    oslider.step(1);
    oslider.bounds(3,40);
    oslider.hide();

#endif // 0

    window->end();
    //window.show(argc,argv);
    window->show(1,argv);
#if HAVE_GL
    //printf("Can do overlay = %d\n", sw.can_do_overlay());
    //sw.show();
    //sw.redraw_overlay();
    psw->show();
    psw->redraw_overlay();
#else
    sw.show();
    printf("Rather useless without GL!!!!\n");
    return 1;
#endif


    last_secs = time(0);
    begin_secs = get_seconds();
    prev_secs  = begin_secs;
    //glutIdleFunc(drawscene);
    next_secs = get_seconds();
    return Fl::run();
}

// eof

