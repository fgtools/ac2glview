/* ======================================================================================
 * misc_lib.hxx - just some stuff
 *
 * LICENSE:
 *
 * Copyright (C) 2013  Geoff R. McLane - http://geoffair.org/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ====================================================================================== */
#ifndef _MISC_LIB_HXX_
#define _MISC_LIB_HXX_


extern const char *get_misc_lib_vers();

#ifdef WIN32
extern void gettimeofday( struct timeval *tv, void *dummy );
#endif
extern double get_seconds();

// static start stop (= get elapsed) seconds
extern void timer_start();
extern double timer_stop();

// determine if a FILE or DIRECTORY
#define MFT_NONE     0
#define MFT_FILE     1
#define MFT_DIR      2
extern int is_file_or_directory ( char * path );
extern size_t get_last_stat_file_size();

extern int WritePPM(int width, int height, const char *name = 0, int stereo = 0);

extern char *get_base_name( char *name ); // return pointer to char following last '/' or '\'
extern char *get_path_and_name( char *path ); // return buffer up to, but excluding the last '.'
extern char *get_file_path_only( char *full_file ); // return buffer up to, and including last '/' or '\'

extern void clean_buff_tail(char *rb); // zero tail of buffer of chars <= ' '
extern int is_digits(char *buf);
extern int is_double(char *buf);

// logging, potentially to a file if set_log_file used...
#ifdef _MSC_VER
#define _C_T _cdecl
#else
#define _C_T
#endif
#define MMX_BUFFER 2048
// default is to stderr, unless set_log_file(fp) used
extern int _C_T sprtf( const char * pf, ... );
extern void set_log_file( FILE *fp );   // { log_out_file = fp; }
extern void log_write_stg( char *cp );

#define NXT_BUF_SIZE 2048
#define NXT_BUF_COUNT 256
extern char *GetNxtBuf();
extern size_t GetBufSiz(); // { return NXT_BUF_SIZE; }

#define DBL_BUF_SIZ 64
#define DBL_BUF_CNT 256
extern char *gtf( double d );

// Creates the UTC time string from timestamp (unix secs since epoch)
extern char *Get_UTC_Time_Stg(time_t Timestamp);
extern char *Get_Current_UTC_Time_Stg();

extern int rename_to_old_bak( const char *file ); // do not overwrite

extern int InStr( char *lpb, char *lps );
extern int InStri( char *lpb, char *lps );


#endif // #ifndef _MISC_LIB_HXX_
// eof - misc_lib.hxx

