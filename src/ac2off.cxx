// ac2off.cxx

#include <stdio.h>
#include <stdlib.h>
#include <sstream> // ostringstream msg;
#include <ac3d2.hxx>
#include "misc_lib.hxx" // just some misc functions
/* --------------------------------------------------------------------------
    Object File Format (.off)
    Object File Format (.off) files are used to represent the geometry of a model by specifying the 
    polygons of the model's surface. The polygons can have any number of vertices.
    The .off files in the Princeton Shape Benchmark conform to the following standard. OFF files 
    are all ASCII files beginning with the keyword OFF. The next line states the number of vertices, 
    the number of faces, and the number of edges. The number of edges can be safely ignored.
    The vertices are listed with x, y, z coordinates, written one per line. After the list of 
    vertices, the faces are listed, with one face per line. For each face, the number of 
    vertices is specified, followed by indices into the list of vertices. See the examples below.
    Note that earlier versions of the model files had faces with -1 indices into the vertex list. 
    That was due to an error in the conversion program and should be corrected now.
    OFF numVertices numFaces numEdges
    x y z
    x y z
    ... numVertices like above
    NVertices v1 v2 v3 ... vN
    MVertices v1 v2 v3 ... vM
    ... numFaces like above
    Note that vertices are numbered starting at 0 (not starting at 1), and that numEdges will always be zero.
    Comment lines may be inserted anywhere, and are denoted by the character # appearing in column 1. Blank lines are also acceptable.
    ---------------------------------------------------------------------------- */

const char *in_ac = 0;
const char *out_off = "tempout.off";
static char *poly_name = 0;

static ac3d2gl *parser = 0;
static ACObject *rob = 0;
static int find_largest = 0;

static void clean_up() 
{
    if (parser) {
        int fixw = parser->fix_warnings;
        parser->ac_free_objects2();
        if (fixw) 
            fprintf(stderr, "NOTE: %d 'fix' me warnings...\n", fixw);
        delete parser;
        parser = 0;
    }
}

void pgm_exit(int v)
{
    clean_up();
    exit(v);
}

void give_help(char *name)
{
    printf("%s [options] in_ac_file\n");
    printf("Options:\n");
    printf(" --help (-h or -?) = This help and exit(2)\n");
    printf(" --poly name  (-p) = Name of specific poly to write to OFF\n");
    printf(" --largest    (-l) = Select object with largest set of vertices\n");
    printf(" --out file   (-o) = Set output OFF file name. (def=%s)\n", out_off);
    printf(" If no poly name given, and not largest, write the whole ac file to OFF\n");

    pgm_exit(2);
}

int parse_command(int argc, char **argv)
{
    int i, i2, c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case '?':
            case 'h':
                give_help(argv[0]);
                break;
            case 'l':
                find_largest = 1;
                break;
            case 'o':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    out_off = strdup(sarg);
                } else {
                    fprintf(stderr,"An output file name must follow %s\n", arg);
                    goto Bad_Arg;
                }
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    poly_name = strdup(sarg);
                } else {
                    fprintf(stderr,"A poly name must follow %s\n", arg);
                    goto Bad_Arg;
                }
                break;
            default:
Bad_Arg:
                fprintf(stderr,"Unknown argument %s\n", arg);
                pgm_exit(1);
                break;
            }
        } else {
            in_ac = strdup(arg);
        }
    }

    if (!in_ac) {
        fprintf(stderr,"ERROR: No input .ac file found in command!\nAborting...\n");
        return 1;
    }
    if (is_file_or_directory((char *)in_ac) != DT_FILE) {
        fprintf(stderr,"ERROR: Unable to 'stat' file %s!\nAborting...\n", in_ac);
        return 1;
    }
    return 0;
}

static ACFNDOBJ fndObj;

int ac_write_OFF_File( const char *out, ACObject *fob );

void ac_get_Vert_Face_count(ACObject *ob, int *vcnt, int *fcnt)
{
    if (ob->type == OBJECT_NORMAL) {
        *vcnt += ob->num_vert;
        *fcnt += ob->num_surf;
    }
    int i;
    for (i = 0; i < ob->num_kids; i++) {
        ac_get_Vert_Face_count( ob->kids[i], vcnt, fcnt);
    }
}
static int add_offset = 0;
void ac_Add_Verts_Faces( std::ostringstream &verts, std::ostringstream &faces, ACObject *fob )
{
    int n, sr, off;
    if (fob->type == OBJECT_NORMAL) {
        for (n = 0; n < fob->num_vert; n++) {
            ACVertex p = fob->vertices[n];
            verts << p.x << " " << p.y << " " << p.z << std::endl;
        }
        for (n = 0; n < fob->num_surf; n++) {
	        ACSurface *surf = &fob->surfaces[n];
            if (surf->num_vertref > 0) {
                faces << surf->num_vertref;
                for (sr = 0; sr < surf->num_vertref; sr++) {
                    off = (surf->vertref[sr] + add_offset);
                    faces << " " <<  off;
                }
                faces << std::endl;
            }
        }
        add_offset += fob->num_vert;
    }
    for (n = 0; n < fob->num_kids; n++) {
        ac_Add_Verts_Faces( verts, faces, fob->kids[n] );
    }
}

int ac_write_OFF_File( const char *out, ACObject *fob )
{
    char *on = parser->get_Object_Name(fob);
    int vcnt = 0;
    int fcnt = 0;
    ac_get_Vert_Face_count(fob, &vcnt, &fcnt);

    if (!vcnt || !fcnt) {
        fprintf(stderr,"ERROR: NO vert count %d, or no face count %d!\n", vcnt, fcnt);
        return 1;
    }

    printf("Write object %s, with %d vert, %d surf, to %s\n", on, vcnt, fcnt, out);
    FILE *fd = fopen(out, "w");
    if (!fd) {
        fprintf(stderr,"ERROR: Unable to create/truncate file %s!\n", out);
        return 1;
    }

    std::ostringstream verts;
    std::ostringstream faces;
    add_offset = 0;
    ac_Add_Verts_Faces( verts, faces, fob );

    std::ostringstream os;
    os << "OFF" << std::endl;
    os << "# extract from " << in_ac << std::endl;
    os << "# object name " << parser->get_Object_Name(fob) << std::endl;
    os << vcnt << " " << fcnt << " 0" << std::endl;
    os << verts.str();
    os << faces.str();
    os << "# eof" << std::endl;

    size_t len = os.str().size();
    size_t res = fwrite( os.str().c_str(), 1, len, fd );
    fclose(fd);
    if (len != res) {
        fprintf(stderr,"ERROR: Write file %s FAILED! req=%d written %d\n", out, (int)len, (int)res);
        return 1;
    }
    return 0;
}



int main( int argc, char **argv )
{
    if (parse_command(argc,argv)) {
        pgm_exit(1);
    }

    parser = new ac3d2gl;
    parser->ac_set_opt_load_textures2(0);   // tell library NOT to load textures
    //parser->ac_set_opt_drop_groups(1);      // and TODO: DROP groups, but maybe NOT such a good idea?????
    rob = parser->ac_load_ac3d2((char *)in_ac);
    if (!rob) {
        fprintf(stderr,"ERROR: Unable to load file %s!\nAborting...\n", in_ac);
        pgm_exit(1);
    }
    ACObject *fob = 0;

    if (poly_name) {
        printf("Searching for poly name [%s]...\n", poly_name);
        fob = parser->find_Object( poly_name, rob, OBJECT_NORMAL );
    } else if (find_largest) {
        printf("No poly name, so will searching for poly with largest surface count...\n");
        fndObj.ob = 0;
        fndObj.vcount = 0;
        fob = parser->find_Largest( &fndObj, rob );
    } else {
        printf("No poly name, no largest so writing whole ac to OFF...\n");
        fob = rob;
    }

    if (!fob) {
        fprintf(stderr,"ERROR: No object found!\nAborting...\n");
        pgm_exit(1);
    }

    int iret = ac_write_OFF_File( out_off, fob );

    clean_up();

    return iret;
}

// eof - ac2off.cxx
