/* ======================================================================================
 * ac3d_browser2.h
 *
 * LICENSE:
 *
 * Copyright (C) 2013  Geoff R. McLane - http://geoffair.org/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ====================================================================================== */
#ifndef _AC3D_BROWSER2_H_
#define _AC3D_BROWSER2_H_
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Gl_Window.H>
#include <ac3d2.hxx>

#define ADD_PREF_DIALOG

// establish a re-size callback to save sizes to prefs
typedef void (*RESIZE) (int x, int y, int w, int h);

// sub class
class mainWin : public Fl_Double_Window {
public:
    mainWin( int x, int y, int w, int h, const char *title );
    int handle(int e);
    void set_resizecb( RESIZE rscb )  { resizecb  = rscb; }

//protected:
        RESIZE resizecb;
        virtual void resize(int x, int y, int w, int h) {
                Fl_Double_Window::resize(x, y, w, h);
                if (resizecb) resizecb(x,y,w,h);
        }

};

class PreviewWin : public Fl_Gl_Window 
{
public:
    PreviewWin(int x,int y,int w,int h,const char *l=0);
    void draw();
    int handle(int e);
    int done_init, display_list, select_list, full_list;
};

// options
#define ADD_ACTIONS_2_MENU

// just some DEFAULT values
#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 660
#define TREE_WIDTH 500
#define TREE_HEIGHT WINDOW_HEIGHT
#define GROUP_WIDTH (WINDOW_WIDTH - TREE_WIDTH)
#define GROUP_HEIGHT WINDOW_HEIGHT
#define BUTTON_WIDTH 80 // was 100
#define BUTTON_HEIGHT 20
#define INITIAL_Y_VAL 40;   // 20    // was 370
#define Y_VAL_STEPS 30
#define GL_WIDTH    (GROUP_WIDTH - 20)
#define GL_HEIGHT   (WINDOW_HEIGHT - (6 * Y_VAL_STEPS))
#define MULTI_WIDTH (GROUP_WIDTH - 20)
#ifdef ADD_ACTIONS_2_MENU
#define MULTI_HEIGHT (WINDOW_HEIGHT - GL_HEIGHT)
#else
#define MULTI_HEIGHT (WINDOW_HEIGHT - (5 * Y_VAL_STEPS))
#endif

extern void get_gl_params_to_gl_parms( PGL_PARMS pgl );
extern void set_gl_paras_to_gl_params( PGL_PARMS pgl );

#define ADD_SCRIPT_INTERFACE

#endif // #ifndef _AC3D_BROWSER2_H_
// eof - ac3d_browser2.h

