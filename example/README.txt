file: README.txt - 20130730

This is an attempt to semi-automate the 'fixing' of an aircraft model 
file. As with all semi-automated batch systems, things can go wrong...

The 'fixing' would involve deleting light cones, sometimes navigation 
light disks, and undercarriage if the aircraft is normally flown with 
it wheels up.

It uses two programs -
1: ac3d_browser2 - This is what is built by this source
2: assimp        - This is a modified version of this http://assimp.sourceforge.net/ source.
                   The modification is to add a Three.js exporter function.

BOTH these programs must be available either through your PATH environment variable, 
or locally.

And it also uses a little program ask, but this source has now been added to contrib/ask,
- see http://geoffair.org/ms/ask.htm - but this could be replaced with a simple 'pause'...
It is a cmake option in CMakeLists.txt. To compile this ask utility use 
-DBUILD_ASK_UTILITY:BOOL=TRUE. It has been tested in window and Ubuntu linux.

There are two main batch file to use -

A: dofixes.bat - Will search for the next aircraft to fix in a acfix.txt list
B: fixone.bat  - If you know the 'name' of an aircraft, then it can be loaded with this.

So as to not disturb git, the output should be to the data directory...

Of course this has been setup using the path to fgdata in my system, so to 
use it the path in the aclist-ac.txt. In my case this is C:\FG\fgdata\Aircraft\.
You should be able to fix this with a quick edit and replace with your path.

When you have done an aircraft or two, you should update acfix.txt, putting ok 
in the second column. Send me the modified file and the js json file to update.

Have FUN!

Regards,
Geoff.

