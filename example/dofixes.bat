@setlocal
@set TMPFIL1=acfix.txt
@set TMPFIL2=aclist-ac.txt
@set TMPFIL3=fixone.bat
@set TMPFIL4=fndone.bat
@set TMPFIL5=bac.bat
@set TMPDIR1=data

@if NOT EXIST %TMPFIL1% goto NOFIL1
@if NOT EXIST %TMPFIL2% goto NOFIL2
@if NOT EXIST %TMPFIL3% goto NOFIL3
@if NOT EXIST %TMPFIL4% goto NOFIL4
@if NOT EXIST %TMPFIL5% goto NOFIL5
@if NOT EXIST %TMPDIR1%\nul goto NODIR1

@call bac --help >nul
@if ERRORLEVEL 1 goto NORUN

@ask -? >nul
@if ERRORLEVEL 1 goto NOASK


@set TMPCNT1=0
@set TMPCNT2=0
@set TMPCNT3=0
@set TMPCNT4=0

@echo Moment... getting the counts...
@set TMPFIRST=

@for /F "tokens=1-10,*" %%i in (%TMPFIL1%) do @(call :CNTIT %%i %%j %%k %%l %%m %%n %%o %%p %%q %%r %%s)

@echo.
@echo Got %TMPCNT2% of %TMPCNT1% lines in file %TMPFIL1% not 'ok'
@echo First to process is %TMPFIRST%
@echo.
@ask *** CONITNUE with fixing? ***? Only 'y' will continue...
@if ERRORLEVEL 2 goto NOASK
@if ERRORLEVEL 1 goto CONT
@echo.
@echo Aborting fixing...
@echo.
@goto END

@for /F "tokens=1-10,*" %%i in (%TMPFIL1%) do @(call :CHKIT %%i %%j %%k %%l %%m %%n %%o %%p %%q %%r %%s)

@goto END

:CNTIT
@if "%~1x" == "x" goto :EOF
@set TMPAIR=%1
@set TMPCMD=
@shift
@set TMPACT=%1
@shift
:RPT1
@if "%~1x" == "x" goto GOTCMD1
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT1
:GOTCMD1
@set /A TMPCNT1+=1
@REM echo %TMPAIR% %TMPACT% [%TMPCMD%]
@if "%TMPACT%x" == "okx" goto :EOF
@if "%TMPCNT2%" == "0" (
@set TMPFIRST=%TMPAIR%
)
@set /A TMPCNT2+=1
@goto :EOF

:CHKIT
@if "%~1x" == "x" goto :EOF
@set TMPAIR=%1
@set TMPCMD=
@shift
@set TMPACT=%1
@shift
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD
@set /A TMPCNT1+=1
@echo %TMPAIR% %TMPACT% [%TMPCMD%]
@if "%TMPACT%x" == "okx" goto :EOF
@set TMPACF=
@REM call fndone
@REM if "%TMPACF%x" == "x" goto NOTFND
@REM echo LOAD %TMPACF% > temp.script
@REM echo WRITE %TMPAIR%.ac >> temp.script
@REM call bac -s temp.script
@call %TMPONE% %TMPAIR%
@pause

@goto :EOF

:NOTFND
@echo WARNING: AC file NOT found...
@pause
@goto :EOF

:NOASK
@echo Can NOT find the ask program... Is it in your PATH?
@echo ask site: http://geoffair.org/ms/ask.htm
@echo It source is included in contrib/ask, and can be compiled by 
@echo adding the cmake option -DBUILD_ASK_UTILITY:BOOL=TRUE
@echo It has been tested in windows and linux.
@echo.
@goto END

:NORUN
@echo.
@echo Error: Unable to cleanly run bac.bat, which runs ac3d_browser2.exe!...
@echo Is ac3d_browser2 in your PATH?  FIX ME!!!
@echo.
@goto END


:NOFIL1
@echo.
@echo Error: Can NOT locate file %TMPFIL1%, which contains the list to be fixed... FIX ME!!!
@echo.
@goto END

:NOFIL2
@echo.
@echo Error: Can NOT locate file %TMPFIL2%, which maps aircraft to model file... FIX ME!!!
@echo.
@goto END

:NOFIL3
@echo.
@echo Error: Can NOT locate file %TMPFIL3%, which does one aircraft... FIX ME!!!
@echo.
@goto END

:NOFIL4
@echo.
@echo Error: Can NOT locate file %TMPFIL4%, used by %TMPFIL3% to find the model file... FIX ME!!!
@echo.
@goto END

:NOFIL5
@echo.
@echo Error: Can NOT locate file %TMPFIL5%, which runs ac3d_browser2... FIX ME!!!
@echo.
@goto END

:NODIR1
@echo.
@echo Error: Can NOT locate folder %TMPDIR1%, for output! Create it first...
@echo.
@goto END

:END

