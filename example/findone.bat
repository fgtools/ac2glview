@setlocal
@set TMPAIR=%1
@if "%TMPAIR%x" == "x" goto NOGO

@set TMPFIL2=aclist-ac.txt

@set TMPCNT3=0
@set TMPCNT4=0
@for /F "tokens=*" %%i in (%TMPFIL2%) do @(call :CHKAIR %%i)

@echo Found %TMPCNT4% of %TMPCNT2% lines in file %TMPFIL2%

@goto END

:CHKAIR
@if "%~1x" == "x" goto :EOF
@set /a TMPCNT3+=1
@set TMPNAM=%~n1
@if NOT "%TMPNAM%x" == "%TMPAIR%x" goto :EOF
@set TMPACF=%1
@echo %TMPACF%
@set /A TMPCNT4+=1
@goto :EOF

:NOGO
@echo.
@echo Error: TMPAIR is NOT set in the environment...
@echo This batch file is not meant to be used directly
@echo Start with dofixes.bat to do ALL aircraft needing a fix,
@echo or fixone.bat, if you know the name of the aircraft...
@echo Only Ctrl+c can abort this...
@echo.
@pause
@goto NOGO

:END
