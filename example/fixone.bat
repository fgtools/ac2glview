@setlocal
@if "%~1x" == "x" goto FXHELP
@set TMPAIR=%1
@set TMPFIL2=aclist-ac.txt
@set TMPFIL5=bac.bat

@if NOT EXIST %TMPFIL2% goto NOFIL2
@if NOT EXIST fndone.bat goto NOFIL4
@if NOT EXIST %TMPFIL5% goto NOFIL5

@call bac --help >nul
@if ERRORLEVEL 1 goto NORUN

@set TMPCMD=
@shift
@set TMPACF=
@echo Moment... searching for model file for %TMPAIR%...
@call fndone
@if "%TMPACF%x" == "x" goto NOTFND

@echo LOAD %TMPACF% > temp.script
@echo WRITE data\%TMPAIR%.ac >> temp.script
call bac -s temp.script
@dir data\%TMPAIR%.*
@if NOT EXIST copy2lh.bat goto NOUPD
@echo.
@echo IF ALL OK, use copy2lh1 %TMPAIR%.js to test file
@echo.
@ask *** CONTINUE with copy? ***
@if ERRORLEVEL 2 goto NOASK
@if ERRORLEVEL 1 goto DOUPD
@echo Skipping the update...
@goto END

:NOUPD
@echo Unable to locate copy2lh1.bat to copy js to test site...
@goto END


:DOUPD
@echo.
@call copy2lh1 %TMPAIR%.js
@echo.
@goto END

:NOTFND
@echo.
@echo WARNING: AC model file NOT found for aircraft %TMPAIR%...
@echo.
@goto END

:FXHELP
@echo.
@echo Give the name of the name of the aircraft to fix...
@echo Or commence with the dofixes.bat batch file...
@echo.
@goto END

:NOASK
@echo.
@echo System does NOT appear to have ask.exe in the PATH...
@echo url: [http://geoffair.org/ms/ask.htm]
@echo But its source is included in contrib/ask, and can be compiled by 
@echo adding the cmake option -DBUILD_ASK_UTILITY:BOOL=TRUE
@echo It has been tested in windows and linux.
@echo.
@goto END

:NOFIL2
@echo.
@echo Error: Can NOT locate file %TMPFIL2%, which maps aircraft to model file... FIX ME!!!
@echo.
@goto END

:NOFIL4
@echo.
@echo Error: Can NOT locate file fndone.bat, used to find the model file... FIX ME!!!
@echo.
@goto END

:NORUN
@echo.
@echo Error: Unable to cleanly run bac.bat, which runs ac3d_browser2.exe!...
@echo Is ac3d_browser2 in your PATH?  FIX ME!!!
@echo.
@goto END

:END

