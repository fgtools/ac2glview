@setlocal
@echo Browse AC file...
@if "%~1x" == "x" goto BACHELP

@set BACCMD=%1
@shift
:RPT
@if "%~1x" == "x" goto GOTBCMD
@set BACCMD=%BACCMD% %1
@shift
@goto RPT
:GOTBCMD

ac3d_browser2 %BACCMD%

@goto END

:BACHELP
@echo Enter a valid ac file to load into the ac3d_browser2...
@goto END

:END
